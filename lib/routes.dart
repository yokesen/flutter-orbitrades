import 'package:flutter/material.dart';
import 'package:orbitrade/views/home/Home.dart';
import 'package:orbitrade/views/home/deposit_request/components/email_edit_screen.dart';
import 'package:orbitrade/views/home/deposit_request/components/name_edit_screen.dart';
import 'package:orbitrade/views/home/deposit_request/components/whatsapp_edit_screen.dart';
import 'package:orbitrade/views/home/home_page/request_withdrawal_page.dart';
import 'package:orbitrade/views/home/profile_page/account_settings/account_setting_page.dart';
import 'package:orbitrade/views/home/profile_page/account_settings/change_password_page.dart';
import 'package:orbitrade/views/home/profile_page/account_settings/pin_setting_page.dart';
import 'package:orbitrade/views/home/profile_page/bankSetting/bank_setting_page.dart';
import 'package:orbitrade/views/home/profile_page/mt4_account/daily_summary_page.dart';
import 'package:orbitrade/views/home/profile_page/mt4_account/mt4_account_create_page.dart';
import 'package:orbitrade/views/home/profile_page/mt4_account/summaryList_detail_page.dart';
import 'package:orbitrade/views/home/profile_page/referral/referral_first_page.dart';
import 'package:orbitrade/views/home/profile_page/referral/referral_second_page.dart';
import 'package:orbitrade/views/home/strateygy_page/acedmy_details/detail.dart';
import 'package:orbitrade/views/home/strateygy_page/quizScreen/quiz_questions_page.dart';
import 'package:orbitrade/views/home/strateygy_page/signal_details_page.dart';
import 'package:orbitrade/views/test/test_post_api.dart';

import 'views/auth/auth_home.dart';
import 'views/auth/login.dart';
import 'views/auth/signup.dart';
import 'views/home/calender_page/news_screen.dart';
import 'views/home/calender_page/trade_balance.dart';
import 'views/home/deposit_request/components/username_edit_screen.dart';
import 'views/home/deposit_request/deposit_request_screen.dart';
import 'views/home/home_page/internal_transfer_page.dart';
import 'views/home/home_page/request_berhasil_page.dart';
import 'views/home/home_page/request_deposit_page.dart';
import 'views/home/profile_page/deposit/deposit_deatils_page.dart';
import 'views/home/profile_page/deposit/deposit_page.dart';
import 'views/home/profile_page/internal_transfer/internal_transfer_page.dart';
import 'views/home/profile_page/withdrawl/withdrawl_page.dart';
import 'views/home/strateygy_page/quiz_complete_page.dart';
import 'views/splash/splash_screen.dart';

final Map<String, WidgetBuilder> routes = {
  SplashScreen.routeName: (context) => SplashScreen(),
  LogIn.routeName: (context) => LogIn(),
  SignUp.routeName: (context) => SignUp(),
  AuthHome.routeName: (context) => AuthHome(),
  Home.routeName: (context) => Home(),
  TradeBalance.routeName: (context) => TradeBalance(),
  NewsScreen.routeName: (context) => NewsScreen(),
  DepositRequestScreen.routeName: (context) => DepositRequestScreen(),
  UserNameEditScreen.routeName: (context) => UserNameEditScreen(),
  NameEditScreen.routeName: (context) => NameEditScreen(),
  EmailEditScreen.routeName: (context) => EmailEditScreen(),
  WhatsappEditScreen.routeName: (context) => WhatsappEditScreen(),
  DepositPage.routeName: (context) => DepositPage(),
  InternalTransferPage.routeName: (context) => InternalTransferPage(),
  WithdrawlPage.routeName: (context) => WithdrawlPage(),
  DepositDetailsPage.routeName: (context) => DepositDetailsPage(),
  QuizCompletePage.routeName: (context) => QuizCompletePage(),
  RequestDepositPage.routeName: (context) => RequestDepositPage(),
  InternalHomeTransferPage.routeName: (context) => InternalHomeTransferPage(),
  RequestBerhasilPage.routeName: (context) => RequestBerhasilPage(),
  SignalDetailsPage.routeName: (context) => SignalDetailsPage(),
  QuizQuestionPage.routeName: (context) => QuizQuestionPage(),
  Detail.routeName: (context) => Detail(),
  Mt4AccountCreatePage.routeName: (context) => Mt4AccountCreatePage(),
  AccountSettingPage.routeName: (context) => AccountSettingPage(),
  ChangePasswordPage.routeName: (context) => ChangePasswordPage(),
  PinSettingPage.routeName: (context) => PinSettingPage(),
  BankSettingPage.routeName: (context) => BankSettingPage(),
  ReferralFirstPage.routeName: (context) => ReferralFirstPage(),
  ReferralSecondPage.routeName: (context) => ReferralSecondPage(),
  RequestWithdrawalPage.routeName: (context) => RequestWithdrawalPage(),
  DailySummaryPage.routeName: (context) => DailySummaryPage(),
  SummaryListDetailsPage.routeName: (context) => SummaryListDetailsPage(),
  TestPostApi.routeName: (context) => TestPostApi(),
};
