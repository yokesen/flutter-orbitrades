import 'package:flutter/material.dart';

import '../constants.dart';

class DefaultBtn extends StatelessWidget {
  final String title;
  final Function onPress;
  final bool isChange;
  final double radius;
  final double border;
  final Color textColor;
  final Color btnColor;
  final double width;
  DefaultBtn({
    @required this.title,
    @required this.width,
    this.textColor = kPrimaryColor,
    this.btnColor = kSecondaryColor,
    this.border = 0,
    this.onPress,
    this.isChange = false,
    this.radius = 30,
  });
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 2),
      decoration: BoxDecoration(
        color: btnColor,
        border: Border.all(width: border, color: kSecondaryColor),
        borderRadius: BorderRadius.circular(radius),
      ),
      width: width,
      child: FlatButton(
        child: Text(
          title,
          style: kRegularText.copyWith(
            color: textColor,
            fontWeight: FontWeight.w600,
          ),
        ),
        onPressed: onPress,
      ),
    );
  }
}
