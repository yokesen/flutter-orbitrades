import 'package:flutter/material.dart';

import '../constants.dart';

class InputFormWidgetNullIcon extends StatelessWidget {
  final String hintText;
  final String labelText;
  final TextEditingController fieldController;
  final Function onSaved;
  final Function validation;
  final IconData icon;
  final int maxLines;
  final String preText;
  final Color fillColor;
  final bool isProtected;
  final bool isEditable;
  final bool centerText;
  final FocusNode focusNode;
  final keyType;
  InputFormWidgetNullIcon({
    this.preText,
    this.isEditable,
    this.maxLines,
    this.centerText = false,
    this.validation,
    this.focusNode,
    this.labelText,
    this.fillColor,
    this.isProtected = false,
    this.hintText,
    this.icon,
    this.fieldController,
    this.keyType,
    this.onSaved,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10),
      child: TextFormField(
        maxLines: maxLines == null ? 1 : maxLines,
        enabled: isEditable,
        validator: validation,
        controller: fieldController,
        keyboardType: keyType,
        obscureText: isProtected,
        obscuringCharacter: '*',
        onChanged: onSaved,
        focusNode: focusNode,
        textAlign: centerText == true ? TextAlign.center : TextAlign.start,
        autofocus: false,
        decoration: InputDecoration(
          errorStyle: TextStyle(fontSize: 11, height: 0.3),
          focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(color: kPrimaryColor),
          ),
          prefixText: preText,
          prefixStyle: TextStyle(
            fontSize: 16,
          ),
          labelText: labelText,
          // labelStyle: kSmallText.copyWith(color: kOrdinaryColor),
          // floatingLabelBehavior: FloatingLabelBehavior.always,
          hintText: hintText,
          hintStyle: TextStyle(fontSize: 14, color: kOrdinaryColor),
          // filled: fillColor == null ? false : true,
          // fillColor: fillColor,
          contentPadding:
              const EdgeInsets.symmetric(vertical: 15, horizontal: 20),
          // hintStyle: kDescriptionText.copyWith(color: kWhiteColor),
          alignLabelWithHint: true,
          border: new OutlineInputBorder(
            borderRadius: BorderRadius.circular(5),
            borderSide:
                BorderSide(color: Colors.grey.withOpacity(.5), width: 0),
            gapPadding: 3,
          ),
        ),
      ),
    );
  }
}
