//home page appbar(Request deposit)
import 'package:flutter/material.dart';

import '../constants.dart';

AppBar buildDepositAppBar({
  BuildContext context,
  String title = kAppName,
  String rightTitle = 'Submit',
  final Function onPress,
}) {
  return AppBar(
    brightness: Brightness.light,
    centerTitle: true,
    backgroundColor: Colors.white,
    iconTheme: IconThemeData(color: Colors.black),
    leading: Builder(
      builder: (context) => IconButton(
        icon: Icon(Icons.arrow_back, color: Colors.black),
        onPressed: () => Navigator.of(context).pop(),
      ),
    ),
    title: Text(
      title,
      overflow: TextOverflow.ellipsis,
      maxLines: 1,
      style: kAppBarText.copyWith(
        color: kBlackColor,
        fontSize: 16,
        fontWeight: FontWeight.w600,
      ),
    ),
    elevation: 0.8,
    titleSpacing: 0,
    actions: [
      GestureDetector(
        onTap: onPress,
        child: Container(
          alignment: Alignment.centerLeft,
          margin: EdgeInsets.only(right: 10),
          child: Text(
            rightTitle,
            style: kRegularText.copyWith(
              color: kPrimaryColor,
            ),
          ),
        ),
      ),
    ],
  );
}
