import 'package:get/state_manager.dart';
import 'package:orbitrade/model/custody-bank-list.dart';
import 'package:orbitrade/services/network_services.dart';

class CusodyBankListController extends GetxController {
  CustodyBankList dataResponse;
  var dataList = List<Datum>().obs;
  var isLoading = false.obs;
  /* @override
  void onInit() {
    super.onInit();
    fetchCusodyBankList();
  }*/

  Future<void> fetchCusodyBankList(String token) async {
    try {
      isLoading.value = true;
      var data = await NetworkServices().fetchCusodyBankList(token);
      if (data != null) {
        dataResponse = data;
        // dataList.value = data.data;
      }
    } finally {
      isLoading.value = false;
    }
  }
}
