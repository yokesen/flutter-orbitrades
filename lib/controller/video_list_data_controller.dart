import 'dart:convert';
import 'dart:developer';

import 'package:get/get.dart';
import 'package:http/http.dart';
import 'package:orbitrade/constants.dart';
import 'package:orbitrade/main.dart';
import 'package:orbitrade/model/score_data.dart';
import 'package:orbitrade/model/video_list_data_model.dart';

import '../app_config.dart';

class VideoListDataController extends GetxController{

  var videoListData=VideoDataList().obs;
  var videoData=Datam().obs;
  var score = ScoreData().obs;

  Future getVideoDataList()async{

    Map<String, String> header = {
      'Accept': 'application/json',
      'Authorization': 'Bearer ${prefs.getString('token')}',
    };

    var response=await get(Uri.parse('$BASE_URL/video-list'), headers: header);

    log(response.body);
    try{
      if(response.statusCode==200){
        var dataMap=jsonDecode(response.body);

        VideoDataList videoDataList=VideoDataList.fromJson(dataMap);
        videoListData.value=videoDataList;

      }else {
        throw ("Error code " + response.statusCode.toString());
      }
    }catch(e){
      Get.snackbar('${e.toString()}!', 'Server is currently unavailable',
          snackPosition: SnackPosition.TOP,
          backgroundColor: kWhiteColor);
    }
  }

  Future getSingleVideoData(videoID)async{
    Map<String, String> header = {
      'Accept': 'application/json',
      'Authorization': 'Bearer ${prefs.getString('token')}',
    };

    var response=await get(Uri.parse('$BASE_URL/video-detail/$videoID'), headers: header);
    log(response.body);
    try{
      if(response.statusCode==200){
        var dataMap=jsonDecode(response.body);

        Datam data=Datam.fromJson(dataMap);
        videoData.value=data;

      }else {
        throw ("Error code " + response.statusCode.toString());
      }
    }catch(e){
      Get.snackbar('${e.toString()}!', 'Server is currently unavailable',
          snackPosition: SnackPosition.TOP,
          backgroundColor: kWhiteColor);
    }
  }

  Future getScore()async{

    Map<String, String> header = {
      'Accept': 'application/json',
      'Authorization': 'Bearer ${prefs.getString('token')}',
    };

    var response=await get(Uri.parse('$BASE_URL/academy-score'), headers: header);

    log(response.body);
    try{
      if(response.statusCode==200){
        var dataMap=jsonDecode(response.body);

        ScoreData scoreData=ScoreData.fromJson(dataMap);
        score.value=scoreData;

      }else {
        throw ("Error code " + response.statusCode.toString());
      }
    }catch(e){
      Get.snackbar('${e.toString()}!', 'Server is currently unavailable',
          snackPosition: SnackPosition.TOP,
          backgroundColor: kWhiteColor);
    }
  }

  @override
  void onInit() {
    // TODO: implement onInit
    getScore();
    getVideoDataList();

    super.onInit();
  }

}