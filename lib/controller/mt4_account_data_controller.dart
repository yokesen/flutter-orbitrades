import 'package:get/get.dart';
import 'package:orbitrade/model/check_changed_user_name.dart';
import 'package:orbitrade/model/myAccountModel.dart';
import 'package:orbitrade/services/network_services.dart';

class Mt4AccountDataController extends GetxController {
  MyAccount myAccount;
  int count;
  var isLoading = false.obs;

  @override
  void onInit() {
    // TODO: implement onInit
    fetchMyAccount();
    super.onInit();
  }
  void fetchMyAccount() async {
    print("calling fetch");
    try {
      //isLoading(true);
      var data = await NetworkServices().fetchMyAccount();
      if (data != null) {
        myAccount = data;
      }
    } finally {
      isLoading(false);
    }
  }

}
