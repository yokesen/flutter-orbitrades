import 'dart:convert';
import 'dart:developer';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:orbitrade/sheared/custom_loader.dart';
import 'package:http/http.dart' as http;

import '../app_config.dart';
import '../main.dart';

class AccountSettingController extends GetxController{
  var oldShowPass=false.obs;
  var newShowPass=false.obs;
  var confirmShowPass=false.obs;


  Future changePassword({BuildContext context,String password})async{

    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) => CustomLoader());
    final response = await http.post(
        '$BASE_URL/update-password',
        headers: {
          'Accept': 'application/json',
          'Authorization': 'Bearer ${prefs.getString('token')}',
        },
        body: {
          "password":password
        }
    );

    if(response.statusCode==200 || response.statusCode==201){
      Navigator.of(context).pop();
      print(response.statusCode);
      Map<String,dynamic> dataMap = jsonDecode(response.body);
      Get.snackbar('Success', dataMap["response"]);
      print(dataMap["response"]);
    }else{
      print(response.body);
      Navigator.of(context).pop();
      Exception("failed to update password");
    }

  }

}