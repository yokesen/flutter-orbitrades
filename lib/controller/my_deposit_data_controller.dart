import 'package:get/get.dart';
import 'package:orbitrade/model/my_deposit_data.dart';
import 'package:orbitrade/services/network_services.dart';

class MyDepositDataController extends GetxController {
  MyDepositData myAccount;
  int count;
  var isLoading = false.obs;

  @override
  void onInit() {
    // TODO: implement onInit
    fetchMyDepositData();
    super.onInit();
  }

  void fetchMyDepositData() async {
    print("calling fetch");
    try {
      //isLoading(true);
      var data = await NetworkServices().fetchMyDepositData();
      if (data != null) {
        myAccount = data;
      }
    } finally {
      isLoading(false);
    }
  }
}
