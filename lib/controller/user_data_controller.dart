import 'dart:io';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:orbitrade/model/myAccountModel.dart';
import 'package:orbitrade/model/myBank.dart';
import 'package:orbitrade/model/user_data.dart';
import 'package:orbitrade/services/network_services.dart';

class UserDataController extends GetxController {
  UserData dataResponse;

  var isLoading = false.obs;

  var userName = "".obs;
  var name = "".obs;
  var userEmail = "".obs;
  var userWhatsApp = "".obs;
  var userDOB = "".obs;

  var accountName="".obs;
  var accountNumber="".obs;
  var bankName="".obs;

  var imagePath = "".obs;
  File profileImgFile;
  void getProfileImage(ImageSource imageSource, BuildContext context) async {
    final pickedFile = await ImagePicker().getImage(source: imageSource);

    if (pickedFile != null) {
      imagePath.value = pickedFile.path;
      profileImgFile = File(pickedFile.path);
      fetchUserDAta();
      //call method
      var p = await NetworkServices().uploadImage(context, profileImgFile);
      Map<String, dynamic> js = p;
      if (p['success'] == true) {
        Navigator.of(context).pop();
        Get.snackbar('Success', 'Profile image upload successfully!');
      } else {
        print(p['message']);
        Get.snackbar('Error', p['message']);
      }
      print('ok');
      print("Path ${pickedFile.path}");
    } else {
      print("No Image Selected");
    }
  }

  //      'Content-Type': 'multipart/form-data; charset=UTF-8',

  @override
  void onInit() {
    // TODO: implement onInit
    fetchUserDAta();
    super.onInit();
  }

  void fetchUserDAta() async {
    print("calling fetch");
    try {
      isLoading.value=true;
      var data = await NetworkServices().fetchUserData();
      if (data != null) {
        dataResponse = data;
        userName.value = dataResponse.user.username;
        name.value = dataResponse.user.name;
        userEmail.value = dataResponse.user.email;
        userWhatsApp.value = dataResponse.user.whatsapp;
        userDOB.value = dataResponse.user.dob;
      }
    } finally {
      isLoading.value=false;
    }
  }



}
