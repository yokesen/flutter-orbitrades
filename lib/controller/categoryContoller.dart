import 'dart:convert';
import 'dart:developer';

import 'package:get/get.dart';
import 'package:http/http.dart';
import 'package:orbitrade/main.dart';
import 'package:orbitrade/model/category_signal_model.dart';
import 'package:orbitrade/model/signal_release_model.dart';
import 'package:orbitrade/model/singnal_list_model.dart';

import '../app_config.dart';
import '../constants.dart';

class CategoryController extends GetxController{

  var categoryData=CategorySingnal().obs;

  var singnalListData=SignalList().obs;
  var signalReleaseData=SignalRelease().obs;

  var categoryListType="".obs;

  var signalReleaseID="".obs;

  var showDropDown=false.obs;

  Future getCategorySignal()async{

    Map<String, String> header = {
      'Accept': 'application/json',
      'Authorization': 'Bearer ${prefs.getString('token')}',
    };

    var response=await get(Uri.parse('$BASE_URL/signal-category'), headers: header);

    log(response.body);
    try{
      if(response.statusCode==200){
        var dataMap=jsonDecode(response.body);

        CategorySingnal videoDataList=CategorySingnal.fromJson(dataMap);
        categoryData.value=videoDataList;

      }else {
        throw ("Error code " + response.statusCode.toString());
      }
    }catch(e){
      Get.snackbar('${e.toString()}!', 'Server is currently unavailable',
          snackPosition: SnackPosition.TOP,
          backgroundColor: kWhiteColor);
    }
  }

  Future getSignalList()async{

    Map<String, String> header = {
      'Accept': 'application/json',
      'Authorization': 'Bearer ${prefs.getString('token')}',
    };

    var response=await get(Uri.parse('$BASE_URL/signal-list/2'), headers: header);

    log(response.body);
    try{
      if(response.statusCode==200){

        var dataMap=jsonDecode(response.body);

        SignalList dataList=SignalList.fromJson(dataMap);
        singnalListData.value=dataList;

      }else {
        throw ("Error code " + response.statusCode.toString());
      }
    }catch(e){
      Get.snackbar('${e.toString()}!', 'Server is currently unavailable',
          snackPosition: SnackPosition.TOP,
          backgroundColor: kWhiteColor);
    }
  }

  Future getSignalRelease(id)async{
    Map<String, String> header = {
      'Accept': 'application/json',
      'Authorization': 'Bearer ${prefs.getString('token')}',
    };

    var response=await get(Uri.parse('$BASE_URL/signal-release/$id'), headers: header);

    log(response.body);

    try{
      if(response.statusCode==200){

        var dataMap=jsonDecode(response.body);
        SignalRelease dataList=SignalRelease.fromJson(dataMap);
        signalReleaseData.value=dataList;

      }else {
        throw ("Error code " + response.statusCode.toString());
      }
    }catch(e){
      Get.snackbar('${e.toString()}!', 'Server is currently unavailable',
          snackPosition: SnackPosition.TOP,
          backgroundColor: kWhiteColor);
    }

  }

}