import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:orbitrade/app_config.dart';
import 'package:orbitrade/model/mt4_account_type.dart';
import 'package:orbitrade/model/user_data.dart';
import 'package:orbitrade/services/network_services.dart';
import 'package:orbitrade/sheared/custom_loader.dart';
import 'package:orbitrade/views/home/home_page/request_berhasil_page.dart';

import '../main.dart';

class Mt4AccountTypeController extends GetxController {
  Mt4AccountType dataResponse;
  var isLoading = false.obs;

  var accountID="4".obs;

  @override
  void onInit() {
    // TODO: implement onInit
    fetchMt4AccountType();
    super.onInit();
  }


  void createAccountType(String accountID,context)async{

    print("id $accountID");

    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) => CustomLoader());

    final response = await http.post(
      '$BASE_URL/create-account',
      headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer ${prefs.getString('token')}',
      },
      body: {
        'accountType':accountID
      }
    );
    if(response.statusCode==200 || response.statusCode==201){
      Navigator.of(context).pop();
      print(response.statusCode);
      Map<String,dynamic> dataMap = jsonDecode(response.body);
      Get.snackbar('Success', "Account Created Successfully");
      Navigator.pushNamed(context, RequestBerhasilPage.routeName);
      print(dataMap["response"]);
    }else{
      Navigator.of(context).pop();
      print(response.body);
      print(response.statusCode);
      Exception("failed to create password");
    }
  }



  void fetchMt4AccountType() async {
    print("calling fetch");
    try {
      isLoading(true);
      var data = await NetworkServices().fetchMt4AccountType();
      if (data != null) {
        dataResponse = data;
        //dataResponse.response[3].i=accountID.value;
      }
    } finally {
      isLoading(false);
    }
  }
}
