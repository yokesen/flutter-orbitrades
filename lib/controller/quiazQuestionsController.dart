import 'dart:convert';
import 'dart:developer';
import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:http/http.dart';
import 'package:orbitrade/app_config.dart';
import 'package:orbitrade/main.dart';
import 'package:orbitrade/model/quiz_questions_model.dart';

import '../constants.dart';

class QuizQuestionController extends GetxController{

  var quizData=QuizQuestions().obs;

  var ansA="".obs;
  var ansB="".obs;
  var ansC="".obs;
  var ansD="".obs;

  var indexValue=0.obs;

  //RxList<Data> list=[].obs;

  Future getQuizQuestion(id)async{
    Map<String, String> header = {
      'Accept': 'application/json',
      'Authorization': 'Bearer ${prefs.getString('token')}',
    };

    var response=await get(Uri.parse('$BASE_URL/question/$id'), headers: header);

    log(response.body);
    try{
      if(response.statusCode==200){
        var dataMap=jsonDecode(response.body);
        QuizQuestions quizDataList=QuizQuestions.fromJson(dataMap);
        quizData.value=quizDataList;

        quizData.value.data.forEach((element) {
/*
          ansA.value=element.qAnswerA;
          ansB.value=element.qAnswerB;
          ansC.value=element.qAnswerC;
          ansD.value=element.qAnswerD;*/

          print("--------------"+element.qAnswerA+"-------------------");
        });
        //list.addAll(quizData.value.data);

      }else {
        throw ("Error code " + response.statusCode.toString());
      }
    }catch(e){
      Get.snackbar('${e.toString()}!', 'Server is currently unavailable',
          snackPosition: SnackPosition.TOP,
          backgroundColor: kWhiteColor);
    }
  }


}