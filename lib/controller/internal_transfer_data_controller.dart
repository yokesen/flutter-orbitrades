import 'package:get/get.dart';
import 'package:orbitrade/model/internal_transfer_data.dart';
import 'package:orbitrade/services/network_services.dart';

class InternalTransDataController extends GetxController {
  InternalTransferData myAccount;
  int count;
  var isLoading = false.obs;

  @override
  void onInit() {
    // TODO: implement onInit
    fetchInternalTransferData();
    super.onInit();
  }

  void fetchInternalTransferData() async {
    print("calling fetch");
    try {
      //isLoading(true);
      var data = await NetworkServices().fetchInternalTransferData();
      if (data != null) {
        myAccount = data;
      }
    } finally {
      isLoading(false);
    }
  }
}
