import 'dart:convert';
import 'dart:developer';
import 'package:http/http.dart';
import 'package:get/get.dart';
import 'package:orbitrade/app_config.dart';
import 'package:orbitrade/model/calendarEventModel.dart';

import '../constants.dart';

class CalendarEventController extends GetxController{

  var calendarObj=CalendarEeventModel().obs;

  var valueList=List<Values>().obs;

  var cityList=[];

  Future getCalendarEvent()async{

    Map<String, String> header = {
      'Accept': 'application/json',
    };
    var response=await get(Uri.parse('$BASE_URL/calendar'), headers: header);

    log(response.body);

    try{
      if(response.statusCode==200){
        var dataMap=jsonDecode(response.body);
        CalendarEeventModel data=CalendarEeventModel.fromJson(dataMap);
        calendarObj.value=data;
        calendarObj.value.data.forEach((element) {
          valueList.addAll(element.value);
        });
      }else {
        throw ("Error code " + response.statusCode.toString());
      }
    }catch(e){
      Get.snackbar('${e.toString()}!', 'Server is currently unavailable',
          snackPosition: SnackPosition.TOP,
          backgroundColor: kWhiteColor);
    }
  }


}