import 'package:get/get.dart';
import 'package:orbitrade/model/myBank.dart';
import 'package:orbitrade/services/network_services.dart';

class BankSettingController extends GetxController {
  var isLoading = false.obs;
  var isUbah = true.obs;
  MyBank myBankData;

  changeToSimpan() {
    isUbah.value = false;
  }

  changeUbahToD() {
    isUbah.value = true;
  }

  @override
  void onInit() {
    // TODO: implement onInit

    fetchMyBankData();
  }

  void fetchMyBankData() async {
    print("calling My Bank");
    try {
      isLoading.value=true;
      var data = await NetworkServices().myBank();
      if (data != null) {
        myBankData = data;
      }
    } finally {
      isLoading.value=false;
    }
  }
}
