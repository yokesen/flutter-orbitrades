import 'dart:convert';
import 'dart:developer';

import 'package:get/get.dart';
import 'package:http/http.dart';
import 'package:orbitrade/constants.dart';
import 'package:orbitrade/main.dart';
import 'package:orbitrade/model/road_map_data.dart';

import '../app_config.dart';

class RoadMapDataController extends GetxController{

  var dataResponse = RoadMapData().obs;

  Future getRoadMapDataList()async{

    Map<String, String> header = {
      'Accept': 'application/json',
      'Authorization': 'Bearer ${prefs.getString('token')}',
    };

    var response=await get(Uri.parse('$BASE_URL/signal-roadmap'), headers: header);

    log(response.body);
    try{
      if(response.statusCode==200){
        var dataMap=jsonDecode(response.body);

        RoadMapData data = RoadMapData.fromJson(dataMap);
        dataResponse.value = data;

      }else {
        throw ("Error code " + response.statusCode.toString());
      }
    }catch(e){
      Get.snackbar('${e.toString()}!', 'Server is currently unavailable',
          snackPosition: SnackPosition.TOP,
          backgroundColor: kWhiteColor);
    }
  }

 @override
  void onInit() {
    // TODO: implement onInit
   getRoadMapDataList();

    super.onInit();
  }
}