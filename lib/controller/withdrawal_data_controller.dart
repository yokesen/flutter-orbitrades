import 'package:get/get.dart';
import 'package:orbitrade/model/my_withdrawal_data.dart';
import 'package:orbitrade/services/network_services.dart';

class WithdrawalDataController extends GetxController {
  MyWithdrawalData myAccount;
  var isLoading = false.obs;

  @override
  void onInit() {
    // TODO: implement onInit
    fetchWithdrawalData();
    super.onInit();
  }

  void fetchWithdrawalData() async {
    print("calling fetch");
    try {
      //isLoading(true);
      var data = await NetworkServices().fetchWithdrawalData();
      if (data != null) {
        myAccount = data;
      }
    } finally {
      isLoading(false);
    }
  }
}
