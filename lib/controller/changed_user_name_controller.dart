import 'package:get/get.dart';
import 'package:orbitrade/model/check_changed_user_name.dart';
import 'package:orbitrade/services/network_services.dart';

class ChangedUserNameController extends GetxController {
  ChangedUserName dataResponse;
  int count;
  var isLoading = false.obs;

  @override
  void onInit() {
    // TODO: implement onInit
    checkChangeUserName();
    super.onInit();
  }

  void checkChangeUserName() async {
    print("calling fetch");
    try {
      isLoading(true);
      var data = await NetworkServices().checkChangeUserName();
      if (data != null) {
        dataResponse = data;
        count = dataResponse.response.check;
      }
    } finally {
      isLoading(false);
    }
  }
}
