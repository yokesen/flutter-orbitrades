class Mt4RequestData {
  Mt4RequestData({
    this.id,
    this.uuid,
    this.typeAccount,
    this.mt4Id,
    this.tipeDeposit,
    this.status,
    this.createdAt,
    this.updatedAt,
  });

  int id;
  String uuid;
  String typeAccount;
  String mt4Id;
  int tipeDeposit;
  String status;
  DateTime createdAt;
  DateTime updatedAt;

  factory Mt4RequestData.fromJson(Map<String, dynamic> json) => Mt4RequestData(
        id: json["id"] == null ? null : json["id"],
        uuid: json["uuid"] == null ? null : json["uuid"],
        typeAccount: json["typeAccount"] == null ? null : json["typeAccount"],
        mt4Id: json["mt4_id"] == null ? null : json["mt4_id"],
        tipeDeposit: json["tipe_deposit"] == null ? null : json["tipe_deposit"],
        status: json["status"] == null ? null : json["status"],
        createdAt: json["created_at"] == null
            ? null
            : DateTime.parse(json["created_at"]),
        updatedAt: json["updated_at"] == null
            ? null
            : DateTime.parse(json["updated_at"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "uuid": uuid == null ? null : uuid,
        "typeAccount": typeAccount == null ? null : typeAccount,
        "mt4_id": mt4Id == null ? null : mt4Id,
        "tipe_deposit": tipeDeposit == null ? null : tipeDeposit,
        "status": status == null ? null : status,
        "created_at": createdAt == null ? null : createdAt.toIso8601String(),
        "updated_at": updatedAt == null ? null : updatedAt.toIso8601String(),
      };
}
