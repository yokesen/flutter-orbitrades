class CalendarEeventModel {
  int success;
  List<Data> data;

  CalendarEeventModel({this.success, this.data});

  CalendarEeventModel.fromJson(Map<String, dynamic> json) {
    success = json['success'];
    if (json['data'] != null) {
      data = new List<Data>();
      json['data'].forEach((v) {
        data.add(new Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['success'] = this.success;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Data {
  String date;
  List<Values> value;

  Data({this.date, this.value});

  Data.fromJson(Map<String, dynamic> json) {
    date = json['date'];
    if (json['value'] != null) {
      value = new List<Values>();
      json['value'].forEach((v) {
        value.add(new Values.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['date'] = this.date;
    if (this.value != null) {
      data['value'] = this.value.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Values {
  String title;
  String country;
  String z;
  String date;
  String time;
  String impact;
  String forecast;
  String previous;

  Values(
      {this.title,
        this.country,
        this.z,
        this.date,
        this.time,
        this.impact,
        this.forecast,
        this.previous});

  Values.fromJson(Map<String, dynamic> json) {
    title = json['title'];
    country = json['country'];
    z = json['z'];
    date = json['date'];
    time = json['time'];
    impact = json['impact'];
    forecast = json['forecast'];
    previous = json['previous'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['title'] = this.title;
    data['country'] = this.country;
    data['z'] = this.z;
    data['date'] = this.date;
    data['time'] = this.time;
    data['impact'] = this.impact;
    data['forecast'] = this.forecast;
    data['previous'] = this.previous;
    return data;
  }
}
