// To parse this JSON data, do
//
//     final myAccount = myAccountFromJson(jsonString);

import 'dart:convert';

MyAccount myAccountFromJson(String str) => MyAccount.fromJson(json.decode(str));

String myAccountToJson(MyAccount data) => json.encode(data.toJson());

class MyAccount {
  MyAccount({
    this.success,
    this.response,
  });

  bool success;
  List<ResponseData> response;

  factory MyAccount.fromJson(Map<String, dynamic> json) => MyAccount(
    success: json["success"] == null ? null : json["success"],
    response: json["response"] == null ? null : List<ResponseData>.from(json["response"].map((x) => ResponseData.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "success": success == null ? null : success,
    "response": response == null ? null : List<dynamic>.from(response.map((x) => x.toJson())),
  };
}

class ResponseData {
  ResponseData({
    this.id,
    this.uuid,
    this.typeAccount,
    this.mt4Id,
    this.tipeDeposit,
    this.password,
    this.kodeGroup,
    this.status,
    this.processAt,
    this.createdAt,
    this.updatedAt,
    this.processBy,
  });

  int id;
  String uuid;
  String typeAccount;
  String mt4Id;
  int tipeDeposit;
  dynamic password;
  dynamic kodeGroup;
  String status;
  dynamic processAt;
  DateTime createdAt;
  DateTime updatedAt;
  dynamic processBy;

  factory ResponseData.fromJson(Map<String, dynamic> json) => ResponseData(
    id: json["id"] == null ? null : json["id"],
    uuid: json["uuid"] == null ? null : json["uuid"],
    typeAccount: json["typeAccount"] == null ? null : json["typeAccount"],
    mt4Id: json["mt4_id"] == null ? null : json["mt4_id"],
    tipeDeposit: json["tipe_deposit"] == null ? null : json["tipe_deposit"],
    password: json["password"],
    kodeGroup: json["kodeGroup"],
    status: json["status"] == null ? null : json["status"],
    processAt: json["process_at"],
    createdAt: json["created_at"] == null ? null : DateTime.parse(json["created_at"]),
    updatedAt: json["updated_at"] == null ? null : DateTime.parse(json["updated_at"]),
    processBy: json["process_by"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "uuid": uuid == null ? null : uuid,
    "typeAccount": typeAccount == null ? null : typeAccount,
    "mt4_id": mt4Id == null ? null : mt4Id,
    "tipe_deposit": tipeDeposit == null ? null : tipeDeposit,
    "password": password,
    "kodeGroup": kodeGroup,
    "status": status == null ? null : status,
    "process_at": processAt,
    "created_at": createdAt == null ? null : createdAt.toIso8601String(),
    "updated_at": updatedAt == null ? null : updatedAt.toIso8601String(),
    "process_by": processBy,
  };
}
