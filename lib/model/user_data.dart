// To parse this JSON data, do
//
//     final userData = userDataFromJson(jsonString);

import 'dart:convert';

UserData userDataFromJson(String str) => UserData.fromJson(json.decode(str));

String userDataToJson(UserData data) => json.encode(data.toJson());

class UserData {
  UserData({
    this.user,
  });

  User user;

  factory UserData.fromJson(Map<String, dynamic> json) => UserData(
        user: json["user"] == null ? null : User.fromJson(json["user"]),
      );

  Map<String, dynamic> toJson() => {
        "user": user == null ? null : user.toJson(),
      };
}

class User {
  User({
    this.id,
    this.uuid,
    this.username,
    this.name,
    this.photo,
    this.email,
    this.phone,
    this.whatsapp,
    this.mobile,
    this.dob,
    this.address,
    this.city,
    this.photoKtp,
    this.photoTabungan,
    this.disclaimer,
    this.consentWa,
    this.consentEmail,
    this.consentMarketing,
    this.rememberToken,
    this.idCmsPrivileges,
    this.emailValidation,
    this.emailVerification,
    this.providerId,
    this.providerOrigin,
    this.parent,
    this.firstLanding,
    this.utmCampaign,
    this.utmSource,
    this.utmMedium,
    this.utmContent,
    this.utmTerm,
    this.ipaddress,
    this.device,
    this.screen,
    this.platform,
    this.platformVersion,
    this.browser,
    this.browserVersion,
    this.language,
    this.robot,
    this.cekAnalytic,
    this.onboardingStep,
    this.status,
    this.createdAt,
    this.updatedAt,
    this.deletedAt,
  });

  int id;
  String uuid;
  String username;
  String name;
  String photo;
  String email;
  String phone;
  String whatsapp;
  dynamic mobile;
  dynamic dob;
  dynamic address;
  dynamic city;
  String photoKtp;
  String photoTabungan;
  dynamic disclaimer;
  dynamic consentWa;
  dynamic consentEmail;
  dynamic consentMarketing;
  dynamic rememberToken;
  dynamic idCmsPrivileges;
  String emailValidation;
  String emailVerification;
  dynamic providerId;
  dynamic providerOrigin;
  dynamic parent;
  dynamic firstLanding;
  dynamic utmCampaign;
  dynamic utmSource;
  dynamic utmMedium;
  dynamic utmContent;
  dynamic utmTerm;
  dynamic ipaddress;
  dynamic device;
  dynamic screen;
  dynamic platform;
  dynamic platformVersion;
  dynamic browser;
  dynamic browserVersion;
  dynamic language;
  dynamic robot;
  int cekAnalytic;
  int onboardingStep;
  dynamic status;
  DateTime createdAt;
  DateTime updatedAt;
  dynamic deletedAt;

  factory User.fromJson(Map<String, dynamic> json) => User(
        id: json["id"] == null ? null : json["id"],
        uuid: json["uuid"] == null ? null : json["uuid"],
        username: json["username"] == null ? null : json["username"],
        name: json["name"] == null ? null : json["name"],
        photo: json["photo"] == null ? null : json["photo"],
        email: json["email"] == null ? null : json["email"],
        phone: json["phone"] == null ? null : json["phone"],
        whatsapp: json["whatsapp"] == null ? null : json["whatsapp"],
        mobile: json["mobile"],
        dob: json["dob"],
        address: json["address"],
        city: json["city"],
        photoKtp: json["photoKTP"] == null ? null : json["photoKTP"],
        photoTabungan:
            json["photoTabungan"] == null ? null : json["photoTabungan"],
        disclaimer: json["disclaimer"],
        consentWa: json["consent_wa"],
        consentEmail: json["consent_email"],
        consentMarketing: json["consent_marketing"],
        rememberToken: json["remember_token"],
        idCmsPrivileges: json["id_cms_privileges"],
        emailValidation:
            json["email_validation"] == null ? null : json["email_validation"],
        emailVerification: json["email_verification"] == null
            ? null
            : json["email_verification"],
        providerId: json["providerId"],
        providerOrigin: json["providerOrigin"],
        parent: json["parent"],
        firstLanding: json["first_landing"],
        utmCampaign: json["utm_campaign"],
        utmSource: json["utm_source"],
        utmMedium: json["utm_medium"],
        utmContent: json["utm_content"],
        utmTerm: json["utm_term"],
        ipaddress: json["ipaddress"],
        device: json["device"],
        screen: json["screen"],
        platform: json["platform"],
        platformVersion: json["platformVersion"],
        browser: json["browser"],
        browserVersion: json["browserVersion"],
        language: json["language"],
        robot: json["robot"],
        cekAnalytic: json["cekAnalytic"] == null ? null : json["cekAnalytic"],
        onboardingStep:
            json["onboardingStep"] == null ? null : json["onboardingStep"],
        status: json["status"],
        createdAt: json["created_at"] == null
            ? null
            : DateTime.parse(json["created_at"]),
        updatedAt: json["updated_at"] == null
            ? null
            : DateTime.parse(json["updated_at"]),
        deletedAt: json["deleted_at"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "uuid": uuid == null ? null : uuid,
        "username": username == null ? null : username,
        "name": name == null ? null : name,
        "photo": photo == null ? null : photo,
        "email": email == null ? null : email,
        "phone": phone == null ? null : phone,
        "whatsapp": whatsapp == null ? null : whatsapp,
        "mobile": mobile,
        "dob": dob,
        "address": address,
        "city": city,
        "photoKTP": photoKtp == null ? null : photoKtp,
        "photoTabungan": photoTabungan == null ? null : photoTabungan,
        "disclaimer": disclaimer,
        "consent_wa": consentWa,
        "consent_email": consentEmail,
        "consent_marketing": consentMarketing,
        "remember_token": rememberToken,
        "id_cms_privileges": idCmsPrivileges,
        "email_validation": emailValidation == null ? null : emailValidation,
        "email_verification":
            emailVerification == null ? null : emailVerification,
        "providerId": providerId,
        "providerOrigin": providerOrigin,
        "parent": parent,
        "first_landing": firstLanding,
        "utm_campaign": utmCampaign,
        "utm_source": utmSource,
        "utm_medium": utmMedium,
        "utm_content": utmContent,
        "utm_term": utmTerm,
        "ipaddress": ipaddress,
        "device": device,
        "screen": screen,
        "platform": platform,
        "platformVersion": platformVersion,
        "browser": browser,
        "browserVersion": browserVersion,
        "language": language,
        "robot": robot,
        "cekAnalytic": cekAnalytic == null ? null : cekAnalytic,
        "onboardingStep": onboardingStep == null ? null : onboardingStep,
        "status": status,
        "created_at": createdAt == null ? null : createdAt.toIso8601String(),
        "updated_at": updatedAt == null ? null : updatedAt.toIso8601String(),
        "deleted_at": deletedAt,
      };
}
