class SignalRelease {
  bool success;
  List<Data> data;

  SignalRelease({this.success, this.data});

  SignalRelease.fromJson(Map<String, dynamic> json) {
    success = json['success'];
    if (json['Data'] != null) {
      data = new List<Data>();
      json['Data'].forEach((v) {
        data.add(new Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['success'] = this.success;
    if (this.data != null) {
      data['Data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Data {
  int id;
  int signalCategory;
  int signalName;
  int userId;
  Null judul;
  Null slug;
  Null redirect;
  Null callToAction;
  Null artikel;
  Null metaDescription;
  Null metaTags;
  String image;
  int views;
  int likes;
  int shares;
  Null ticket;
  Null openTime;
  String type;
  Null size;
  String item;
  String price;
  String stoploss;
  Null magicNumber;
  Null kodeAkun;
  String takeprofit;
  Null closeTime;
  Null closePrice;
  Null profit;
  String status;
  String createdAt;
  String updatedAt;
  int showResult;
  Null deletedAt;
  String expiredAt;
  String signalDate;
  String signalNote;
  String signalNames;
  int signalCategorys;
  String signalLogo;
  String pair;

  Data(
      {this.id,
        this.signalCategory,
        this.signalName,
        this.userId,
        this.judul,
        this.slug,
        this.redirect,
        this.callToAction,
        this.artikel,
        this.metaDescription,
        this.metaTags,
        this.image,
        this.views,
        this.likes,
        this.shares,
        this.ticket,
        this.openTime,
        this.type,
        this.size,
        this.item,
        this.price,
        this.stoploss,
        this.magicNumber,
        this.kodeAkun,
        this.takeprofit,
        this.closeTime,
        this.closePrice,
        this.profit,
        this.status,
        this.createdAt,
        this.updatedAt,
        this.showResult,
        this.deletedAt,
        this.expiredAt,
        this.signalDate,
        this.signalNote,
        this.signalNames,
        this.signalCategorys,
        this.signalLogo,
        this.pair});

  Data.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    signalCategory = json['signal_category'];
    signalName = json['signal_name'];
    userId = json['user_id'];
    judul = json['judul'];
    slug = json['slug'];
    redirect = json['redirect'];
    callToAction = json['callToAction'];
    artikel = json['artikel'];
    metaDescription = json['metaDescription'];
    metaTags = json['metaTags'];
    image = json['image'];
    views = json['views'];
    likes = json['likes'];
    shares = json['shares'];
    ticket = json['ticket'];
    openTime = json['openTime'];
    type = json['type'];
    size = json['size'];
    item = json['item'];
    price = json['price'];
    stoploss = json['stoploss'];
    magicNumber = json['magicNumber'];
    kodeAkun = json['kodeAkun'];
    takeprofit = json['takeprofit'];
    closeTime = json['closeTime'];
    closePrice = json['closePrice'];
    profit = json['profit'];
    status = json['status'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    showResult = json['showResult'];
    deletedAt = json['deleted_at'];
    expiredAt = json['expired_at'];
    signalDate = json['signal_date'];
    signalNote = json['signal_note'];
    signalNames = json['signalName'];
    signalCategorys = json['signalCategory'];
    signalLogo = json['signalLogo'];
    pair = json['pair'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['signal_category'] = this.signalCategory;
    data['signal_name'] = this.signalName;
    data['user_id'] = this.userId;
    data['judul'] = this.judul;
    data['slug'] = this.slug;
    data['redirect'] = this.redirect;
    data['callToAction'] = this.callToAction;
    data['artikel'] = this.artikel;
    data['metaDescription'] = this.metaDescription;
    data['metaTags'] = this.metaTags;
    data['image'] = this.image;
    data['views'] = this.views;
    data['likes'] = this.likes;
    data['shares'] = this.shares;
    data['ticket'] = this.ticket;
    data['openTime'] = this.openTime;
    data['type'] = this.type;
    data['size'] = this.size;
    data['item'] = this.item;
    data['price'] = this.price;
    data['stoploss'] = this.stoploss;
    data['magicNumber'] = this.magicNumber;
    data['kodeAkun'] = this.kodeAkun;
    data['takeprofit'] = this.takeprofit;
    data['closeTime'] = this.closeTime;
    data['closePrice'] = this.closePrice;
    data['profit'] = this.profit;
    data['status'] = this.status;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['showResult'] = this.showResult;
    data['deleted_at'] = this.deletedAt;
    data['expired_at'] = this.expiredAt;
    data['signal_date'] = this.signalDate;
    data['signal_note'] = this.signalNote;
    data['signalName'] = this.signalNames;
    data['signalCategory'] = this.signalCategorys;
    data['signalLogo'] = this.signalLogo;
    data['pair'] = this.pair;
    return data;
  }
}