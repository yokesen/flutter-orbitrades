// To parse this JSON data, do
//
//     final internalTransferData = internalTransferDataFromJson(jsonString);

import 'dart:convert';

InternalTransferData internalTransferDataFromJson(String str) =>
    InternalTransferData.fromJson(json.decode(str));

String internalTransferDataToJson(InternalTransferData data) =>
    json.encode(data.toJson());

class InternalTransferData {
  InternalTransferData({
    this.success,
    this.data,
  });

  bool success;
  List<Datum> data;

  factory InternalTransferData.fromJson(Map<String, dynamic> json) =>
      InternalTransferData(
        success: json["success"] == null ? null : json["success"],
        data: json["Data"] == null
            ? null
            : List<Datum>.from(json["Data"].map((x) => Datum.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "success": success == null ? null : success,
        "Data": data == null
            ? null
            : List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class Datum {
  Datum({
    this.id,
    this.uuid,
    this.metatrader,
    this.amount,
    this.tipeDeposit,
    this.currency,
    this.currencyRate,
    this.approvedAmount,
    this.status,
    this.createdAt,
    this.updatedAt,
    this.processBy,
    this.signatures,
    this.reason,
    this.bankName,
    this.swiftCode,
    this.accountName,
    this.accountNumber,
    this.internalToAccountMeta,
    this.origin,
    this.internal,
  });

  int id;
  String uuid;
  String metatrader;
  int amount;
  int tipeDeposit;
  String currency;
  String currencyRate;
  int approvedAmount;
  String status;
  DateTime createdAt;
  DateTime updatedAt;
  dynamic processBy;
  dynamic signatures;
  dynamic reason;
  String bankName;
  dynamic swiftCode;
  String accountName;
  String accountNumber;
  int internalToAccountMeta;
  dynamic origin;
  int internal;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        id: json["id"] == null ? null : json["id"],
        uuid: json["uuid"] == null ? null : json["uuid"],
        metatrader: json["metatrader"] == null ? null : json["metatrader"],
        amount: json["amount"] == null ? null : json["amount"],
        tipeDeposit: json["tipe_deposit"] == null ? null : json["tipe_deposit"],
        currency: json["currency"] == null ? null : json["currency"],
        currencyRate:
            json["currency_rate"] == null ? null : json["currency_rate"],
        approvedAmount:
            json["approved_amount"] == null ? null : json["approved_amount"],
        status: json["status"] == null ? null : json["status"],
        createdAt: json["created_at"] == null
            ? null
            : DateTime.parse(json["created_at"]),
        updatedAt: json["updated_at"] == null
            ? null
            : DateTime.parse(json["updated_at"]),
        processBy: json["process_by"],
        signatures: json["signatures"],
        reason: json["reason"],
        bankName: json["bank_name"] == null ? null : json["bank_name"],
        swiftCode: json["swift_code"],
        accountName: json["account_name"] == null ? null : json["account_name"],
        accountNumber:
            json["account_number"] == null ? null : json["account_number"],
        internalToAccountMeta: json["internalToAccountMeta"] == null
            ? null
            : json["internalToAccountMeta"],
        origin: json["origin"],
        internal: json["internal"] == null ? null : json["internal"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "uuid": uuid == null ? null : uuid,
        "metatrader": metatrader == null ? null : metatrader,
        "amount": amount == null ? null : amount,
        "tipe_deposit": tipeDeposit == null ? null : tipeDeposit,
        "currency": currency == null ? null : currency,
        "currency_rate": currencyRate == null ? null : currencyRate,
        "approved_amount": approvedAmount == null ? null : approvedAmount,
        "status": status == null ? null : status,
        "created_at": createdAt == null ? null : createdAt.toIso8601String(),
        "updated_at": updatedAt == null ? null : updatedAt.toIso8601String(),
        "process_by": processBy,
        "signatures": signatures,
        "reason": reason,
        "bank_name": bankName == null ? null : bankName,
        "swift_code": swiftCode,
        "account_name": accountName == null ? null : accountName,
        "account_number": accountNumber == null ? null : accountNumber,
        "internalToAccountMeta":
            internalToAccountMeta == null ? null : internalToAccountMeta,
        "origin": origin,
        "internal": internal == null ? null : internal,
      };
}
