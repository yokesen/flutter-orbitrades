

import 'dart:convert';

CustodyBank custodyTypeFromJson(String str) => CustodyBank.fromJson(json.decode(str));

String custodyTypeToJson(CustodyBank data) => json.encode(data.toJson());

class CustodyBank {
  CustodyBank({
    this.success,
    this.data,
  });

  bool success;
  List<Data> data;

  factory CustodyBank.fromJson(Map<String, dynamic> json) => CustodyBank(
    success: json["success"] == null ? null : json["success"],
    data: json["Data"] == null ? null : List<Data>.from(json["Data"].map((x) => Data.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "success": success == null ? null : success,
    "Data": data == null ? null : List<dynamic>.from(data.map((x) => x.toJson())),
  };
}


class Data {
  int id;
  String namaBank;
  String cabangBank;
  String nomorRekening;
  String namaRekening;
  String statusRekening;

  Data(
      {this.id,
        this.namaBank,
        this.cabangBank,
        this.nomorRekening,
        this.namaRekening,
        this.statusRekening});

  Data.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    namaBank = json['namaBank'];
    cabangBank = json['cabangBank'];
    nomorRekening = json['nomorRekening'];
    namaRekening = json['namaRekening'];
    statusRekening = json['statusRekening'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['namaBank'] = this.namaBank;
    data['cabangBank'] = this.cabangBank;
    data['nomorRekening'] = this.nomorRekening;
    data['namaRekening'] = this.namaRekening;
    data['statusRekening'] = this.statusRekening;
    return data;
  }
}
