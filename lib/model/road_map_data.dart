// To parse this JSON data, do
//
//     final roadMapData = roadMapDataFromJson(jsonString);

import 'dart:convert';

RoadMapData roadMapDataFromJson(String str) => RoadMapData.fromJson(json.decode(str));

String roadMapDataToJson(RoadMapData data) => json.encode(data.toJson());

class RoadMapData {
  RoadMapData({
    this.success,
    this.images,
  });

  bool success;
  List<Image> images;

  factory RoadMapData.fromJson(Map<String, dynamic> json) => RoadMapData(
    success: json["success"] == null ? null : json["success"],
    images: json["images"] == null ? null : List<Image>.from(json["images"].map((x) => Image.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "success": success == null ? null : success,
    "images": images == null ? null : List<dynamic>.from(images.map((x) => x.toJson())),
  };
}

class Image {
  Image({
    this.id,
    this.roadmad,
    this.status,
    this.createdAt,
  });

  int id;
  String roadmad;
  String status;
  DateTime createdAt;

  factory Image.fromJson(Map<String, dynamic> json) => Image(
    id: json["id"] == null ? null : json["id"],
    roadmad: json["roadmad"] == null ? null : json["roadmad"],
    status: json["status"] == null ? null : json["status"],
    createdAt: json["created_at"] == null ? null : DateTime.parse(json["created_at"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "roadmad": roadmad == null ? null : roadmad,
    "status": status == null ? null : status,
    "created_at": createdAt == null ? null : createdAt.toIso8601String(),
  };
}
