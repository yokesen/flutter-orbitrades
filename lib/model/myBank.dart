class MyBank {
  bool success;
  Data data;

  MyBank({this.success, this.data});

  MyBank.fromJson(Map<dynamic, dynamic> json) {
    success = json['success'];
    data = json['Data'] != null ? new Data.fromJson(json['Data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['success'] = this.success;
    if (this.data != null) {
      data['Data'] = this.data.toJson();
    }
    return data;
  }
}

class Data {
  int id;
  String userId;
  String uuid;
  String bankName;
  String swiftCode;
  String accountName;
  String accountNumber;
  String province;
  String city;
  String branch;
  String istalven;
  String createdAt;
  String updatedAt;
  String longName;
  String deletedAt;

  Data(
      {this.id,
        this.userId,
        this.uuid,
        this.bankName,
        this.swiftCode,
        this.accountName,
        this.accountNumber,
        this.province,
        this.city,
        this.branch,
        this.istalven,
        this.createdAt,
        this.updatedAt,
        this.longName,
        this.deletedAt});

  Data.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    userId = json['user_id'];
    uuid = json['uuid'];
    bankName = json['bank_name'];
    swiftCode = json['swift_code'];
    accountName = json['account_name'];
    accountNumber = json['account_number'];
    province = json['province'];
    city = json['city'];
    branch = json['branch'];
    istalven = json['istalven'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    longName = json['long_name'];
    deletedAt = json['deleted_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['user_id'] = this.userId;
    data['uuid'] = this.uuid;
    data['bank_name'] = this.bankName;
    data['swift_code'] = this.swiftCode;
    data['account_name'] = this.accountName;
    data['account_number'] = this.accountNumber;
    data['province'] = this.province;
    data['city'] = this.city;
    data['branch'] = this.branch;
    data['istalven'] = this.istalven;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['long_name'] = this.longName;
    data['deleted_at'] = this.deletedAt;
    return data;
  }
}
