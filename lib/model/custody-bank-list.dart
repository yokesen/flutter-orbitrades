// To parse this JSON data, do
//
//     final custodyBankBist = custodyBankBistFromJson(jsonString);

import 'dart:convert';


CustodyBankList custodyBankBistFromJson(String str) =>
    CustodyBankList.fromJson(json.decode(str));

String custodyBankBistToJson(CustodyBankList data) =>
    json.encode(data.toJson());

class CustodyBankList {
  CustodyBankList({
    this.success,
    this.data,
  });

  bool success;
  List<Datum> data;

  factory CustodyBankList.fromJson(Map<String, dynamic> json) =>
      CustodyBankList(
        success: json["success"] == null ? null : json["success"],
        data: json["Data"] == null
            ? null
            : List<Datum>.from(json["Data"].map((x) => Datum.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "success": success == null ? null : success,
        "Data": data == null
            ? null
            : List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class Datum {
  Datum({
    this.id,
    this.namaBank,
    this.cabangBank,
    this.nomorRekening,
    this.namaRekening,
    this.statusRekening,
  });

  int id;
  String namaBank;
  String cabangBank;
  String nomorRekening;
  String namaRekening;
  String statusRekening;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        id: json["id"] == null ? null : json["id"],
        namaBank: json["namaBank"] == null ? null : json["namaBank"],
        cabangBank: json["cabangBank"] == null ? null : json["cabangBank"],
        nomorRekening:
            json["nomorRekening"] == null ? null : json["nomorRekening"],
        namaRekening:
            json["namaRekening"] == null ? null : json["namaRekening"],
        statusRekening:
            json["statusRekening"] == null ? null : json["statusRekening"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "namaBank": namaBank == null ? null : namaBank,
        "cabangBank": cabangBank == null ? null : cabangBank,
        "nomorRekening": nomorRekening == null ? null : nomorRekening,
        "namaRekening": namaRekening == null ? null : namaRekening,
        "statusRekening": statusRekening == null ? null : statusRekening,
      };
}
