// To parse this JSON data, do
//
//     final scoreData = scoreDataFromJson(jsonString);

import 'dart:convert';

ScoreData scoreDataFromJson(String str) => ScoreData.fromJson(json.decode(str));

String scoreDataToJson(ScoreData data) => json.encode(data.toJson());

class ScoreData {
  ScoreData({
    this.success,
    this.score,
  });

  bool success;
  int score;

  factory ScoreData.fromJson(Map<String, dynamic> json) => ScoreData(
    success: json["success"] == null ? null : json["success"],
    score: json["score"] == null ? null : json["score"],
  );

  Map<String, dynamic> toJson() => {
    "success": success == null ? null : success,
    "score": score == null ? null : score,
  };
}
