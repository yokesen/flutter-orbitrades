class QuizQuestions {
  bool success;
  List<Data> data;

  QuizQuestions({this.success, this.data});

  QuizQuestions.fromJson(Map<String, dynamic> json) {
    success = json['success'];
    if (json['Data'] != null) {
      data = new List<Data>();
      json['Data'].forEach((v) {
        data.add(new Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['success'] = this.success;
    if (this.data != null) {
      data['Data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Data {
  int id;
  String uaqid;
  String materiType;
  String materiId;
  String qQuestion;
  String qAnswerA;
  String qAnswerB;
  String qAnswerC;
  String qAnswerD;
  String qJawaban;
  String createdAt;
  int createdBy;
  String updatedAt;
  int updatedBy;

  Data(
      {this.id,
        this.uaqid,
        this.materiType,
        this.materiId,
        this.qQuestion,
        this.qAnswerA,
        this.qAnswerB,
        this.qAnswerC,
        this.qAnswerD,
        this.qJawaban,
        this.createdAt,
        this.createdBy,
        this.updatedAt,
        this.updatedBy});

  Data.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    uaqid = json['uaqid'];
    materiType = json['materiType'];
    materiId = json['materiId'];
    qQuestion = json['qQuestion'];
    qAnswerA = json['qAnswer_a'];
    qAnswerB = json['qAnswer_b'];
    qAnswerC = json['qAnswer_c'];
    qAnswerD = json['qAnswer_d'];
    qJawaban = json['qJawaban'];
    createdAt = json['created_at'];
    createdBy = json['created_by'];
    updatedAt = json['updated_at'];
    updatedBy = json['updated_by'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['uaqid'] = this.uaqid;
    data['materiType'] = this.materiType;
    data['materiId'] = this.materiId;
    data['qQuestion'] = this.qQuestion;
    data['qAnswer_a'] = this.qAnswerA;
    data['qAnswer_b'] = this.qAnswerB;
    data['qAnswer_c'] = this.qAnswerC;
    data['qAnswer_d'] = this.qAnswerD;
    data['qJawaban'] = this.qJawaban;
    data['created_at'] = this.createdAt;
    data['created_by'] = this.createdBy;
    data['updated_at'] = this.updatedAt;
    data['updated_by'] = this.updatedBy;
    return data;
  }
}