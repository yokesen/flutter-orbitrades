class SignalItem {
  int id;
  String name;
  String image;

  SignalItem(this.id, this.name, this.image);
}
