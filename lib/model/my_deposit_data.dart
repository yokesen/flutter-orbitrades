// To parse this JSON data, do
//
//     final myDepositData = myDepositDataFromJson(jsonString);

import 'dart:convert';

MyDepositData myDepositDataFromJson(String str) => MyDepositData.fromJson(json.decode(str));

String myDepositDataToJson(MyDepositData data) => json.encode(data.toJson());

class MyDepositData {
  MyDepositData({
    this.success,
    this.data,
  });

  bool success;
  List<Datum> data;

  factory MyDepositData.fromJson(Map<String, dynamic> json) => MyDepositData(
    success: json["success"] == null ? null : json["success"],
    data: json["Data"] == null ? null : List<Datum>.from(json["Data"].map((x) => Datum.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "success": success == null ? null : success,
    "Data": data == null ? null : List<dynamic>.from(data.map((x) => x.toJson())),
  };
}

class Datum {
  Datum({
    this.id,
    this.udid,
    this.uuid,
    this.metatrader,
    this.amount,
    this.tipeDeposit,
    this.currency,
    this.approvedAmount,
    this.currencyRate,
    this.photo,
    this.status,
    this.createdAt,
    this.updatedAt,
    this.processBy,
    this.reason,
    this.fromBank,
    this.fromName,
    this.fromRekening,
    this.toBank,
    this.toName,
    this.toRekening,
    this.internal,
  });

  int id;
  dynamic udid;
  String uuid;
  String metatrader;
  int amount;
  int tipeDeposit;
  String currency;
  int approvedAmount;
  String currencyRate;
  String photo;
  String status;
  DateTime createdAt;
  DateTime updatedAt;
  dynamic processBy;
  dynamic reason;
  String fromBank;
  String fromName;
  String fromRekening;
  String toBank;
  String toName;
  String toRekening;
  dynamic internal;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
    id: json["id"] == null ? null : json["id"],
    udid: json["udid"],
    uuid: json["uuid"] == null ? null : json["uuid"],
    metatrader: json["metatrader"] == null ? null : json["metatrader"],
    amount: json["amount"] == null ? null : json["amount"],
    tipeDeposit: json["tipe_deposit"] == null ? null : json["tipe_deposit"],
    currency: json["currency"] == null ? null : json["currency"],
    approvedAmount: json["approved_amount"] == null ? null : json["approved_amount"],
    currencyRate: json["currency_rate"] == null ? null : json["currency_rate"],
    photo: json["photo"] == null ? null : json["photo"],
    status: json["status"] == null ? null : json["status"],
    createdAt: json["created_at"] == null ? null : DateTime.parse(json["created_at"]),
    updatedAt: json["updated_at"] == null ? null : DateTime.parse(json["updated_at"]),
    processBy: json["process_by"],
    reason: json["reason"],
    fromBank: json["from_bank"] == null ? null : json["from_bank"],
    fromName: json["from_name"] == null ? null : json["from_name"],
    fromRekening: json["from_rekening"] == null ? null : json["from_rekening"],
    toBank: json["to_bank"] == null ? null : json["to_bank"],
    toName: json["to_name"] == null ? null : json["to_name"],
    toRekening: json["to_rekening"] == null ? null : json["to_rekening"],
    internal: json["internal"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "udid": udid,
    "uuid": uuid == null ? null : uuid,
    "metatrader": metatrader == null ? null : metatrader,
    "amount": amount == null ? null : amount,
    "tipe_deposit": tipeDeposit == null ? null : tipeDeposit,
    "currency": currency == null ? null : currency,
    "approved_amount": approvedAmount == null ? null : approvedAmount,
    "currency_rate": currencyRate == null ? null : currencyRate,
    "photo": photo == null ? null : photo,
    "status": status == null ? null : status,
    "created_at": createdAt == null ? null : createdAt.toIso8601String(),
    "updated_at": updatedAt == null ? null : updatedAt.toIso8601String(),
    "process_by": processBy,
    "reason": reason,
    "from_bank": fromBank == null ? null : fromBank,
    "from_name": fromName == null ? null : fromName,
    "from_rekening": fromRekening == null ? null : fromRekening,
    "to_bank": toBank == null ? null : toBank,
    "to_name": toName == null ? null : toName,
    "to_rekening": toRekening == null ? null : toRekening,
    "internal": internal,
  };
}
