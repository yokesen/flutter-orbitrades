class CategorySingnal {
  bool success;
  List<Data> data;

  CategorySingnal({this.success, this.data});

  CategorySingnal.fromJson(Map<String, dynamic> json) {
    success = json['success'];
    if (json['Data'] != null) {
      data = new List<Data>();
      json['Data'].forEach((v) {
        data.add(new Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['success'] = this.success;
    if (this.data != null) {
      data['Data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Data {
  int id;
  String categoryName;
  String categoryDesc;
  String icon;
  String categoryStatus;

  Data(
      {this.id,
        this.categoryName,
        this.categoryDesc,
        this.icon,
        this.categoryStatus});

  Data.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    categoryName = json['categoryName'];
    categoryDesc = json['categoryDesc'];
    icon = json['icon'];
    categoryStatus = json['categoryStatus'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['categoryName'] = this.categoryName;
    data['categoryDesc'] = this.categoryDesc;
    data['icon'] = this.icon;
    data['categoryStatus'] = this.categoryStatus;
    return data;
  }
}