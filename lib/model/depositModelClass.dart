class DepositModel{
  String nominal;
  int accountID;
  String approved;

  DepositModel({this.nominal,this.accountID,this.approved});



}

List<DepositModel> depositModelData=[
  DepositModel(nominal: "IDR 50,000.000",accountID: 99987653,approved: "Approved"),
  DepositModel(nominal: "IDR 100,000.000",accountID: 99987653,approved: "Approved"),
  DepositModel(nominal: "IDR 41,000.000",accountID: 99987151,approved: "Waiting"),
  DepositModel(nominal: "IDR 51,000.000",accountID: 99987653,approved: "Approved"),
  DepositModel(nominal: "IDR 40,000.000",accountID: 99987135,approved: "Approved"),
  DepositModel(nominal: "IDR 13,000.000",accountID: 99987458,approved: "Approved"),
  DepositModel(nominal: "IDR 58,000.000",accountID: 99987235,approved: "Rejected"),
];