// To parse this JSON data, do
//
//     final changedUserName = changedUserNameFromJson(jsonString);

import 'dart:convert';

ChangedUserName changedUserNameFromJson(String str) =>
    ChangedUserName.fromJson(json.decode(str));

String changedUserNameToJson(ChangedUserName data) =>
    json.encode(data.toJson());

class ChangedUserName {
  ChangedUserName({
    this.success,
    this.response,
  });

  bool success;
  Response response;

  factory ChangedUserName.fromJson(Map<String, dynamic> json) =>
      ChangedUserName(
        success: json["success"] == null ? null : json["success"],
        response: json["response"] == null
            ? null
            : Response.fromJson(json["response"]),
      );

  Map<String, dynamic> toJson() => {
        "success": success == null ? null : success,
        "response": response == null ? null : response.toJson(),
      };
}

class Response {
  Response({
    this.check,
  });

  int check;

  factory Response.fromJson(Map<String, dynamic> json) => Response(
        check: json["check"] == null ? null : json["check"],
      );

  Map<String, dynamic> toJson() => {
        "check": check == null ? null : check,
      };
}
