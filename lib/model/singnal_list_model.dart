class SignalList {
  bool success;
  List<Datas> data;

  SignalList({this.success, this.data});

  SignalList.fromJson(Map<String, dynamic> json) {
    success = json['success'];
    if (json['Data'] != null) {
      data = new List<Datas>();
      json['Data'].forEach((v) {
        data.add(new Datas.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['success'] = this.success;
    if (this.data != null) {
      data['Data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Datas {
  int id;
  String signalName;
  int signalCategory;
  String signalLogo;
  String status;

  Datas(
      {this.id,
        this.signalName,
        this.signalCategory,
        this.signalLogo,
        this.status});

  Datas.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    signalName = json['signalName'];
    signalCategory = json['signalCategory'];
    signalLogo = json['signalLogo'];
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['signalName'] = this.signalName;
    data['signalCategory'] = this.signalCategory;
    data['signalLogo'] = this.signalLogo;
    data['status'] = this.status;
    return data;
  }
}