class VideoDataList {
  bool success;
  List<Datam> data;

  VideoDataList({this.success, this.data});

  VideoDataList.fromJson(Map<String, dynamic> json) {
    success = json['success'];
    if (json['Data'] != null) {
      data = new List<Datam>();
      json['Data'].forEach((v) {
        data.add(new Datam.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['success'] = this.success;
    if (this.data != null) {
      data['Data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Datam {
  int Id;
  String uavid;
  String videoTitle;
  String videoSlug;
  String videoDesc;
  String videoUrl;
  String videoCTA;
  int videoLength;
  String videoTag;
  String videoStatus;
  int videoPoin;
  String createdAt;
  int createdBy;
  int id;
  String name;
  String photo;
  String email;
  String password;
  int idCmsPrivileges;
  String updatedAt;
  Null status;

  Datam(
      {this.Id,
        this.uavid,
        this.videoTitle,
        this.videoSlug,
        this.videoDesc,
        this.videoUrl,
        this.videoCTA,
        this.videoLength,
        this.videoTag,
        this.videoStatus,
        this.videoPoin,
        this.createdAt,
        this.createdBy,
        this.id,
        this.name,
        this.photo,
        this.email,
        this.password,
        this.idCmsPrivileges,
        this.updatedAt,
        this.status});

  Datam.fromJson(Map<String, dynamic> json) {
    Id = json['Id'];
    uavid = json['uavid'];
    videoTitle = json['videoTitle'];
    videoSlug = json['videoSlug'];
    videoDesc = json['videoDesc'];
    videoUrl = json['videoUrl'];
    videoCTA = json['videoCTA'];
    videoLength = json['videoLength'];
    videoTag = json['videoTag'];
    videoStatus = json['videoStatus'];
    videoPoin = json['videoPoin'];
    createdAt = json['created_at'];
    createdBy = json['created_by'];
    id = json['id'];
    name = json['name'];
    photo = json['photo'];
    email = json['email'];
    password = json['password'];
    idCmsPrivileges = json['id_cms_privileges'];
    updatedAt = json['updated_at'];
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Id'] = this.Id;
    data['uavid'] = this.uavid;
    data['videoTitle'] = this.videoTitle;
    data['videoSlug'] = this.videoSlug;
    data['videoDesc'] = this.videoDesc;
    data['videoUrl'] = this.videoUrl;
    data['videoCTA'] = this.videoCTA;
    data['videoLength'] = this.videoLength;
    data['videoTag'] = this.videoTag;
    data['videoStatus'] = this.videoStatus;
    data['videoPoin'] = this.videoPoin;
    data['created_at'] = this.createdAt;
    data['created_by'] = this.createdBy;
    data['id'] = this.id;
    data['name'] = this.name;
    data['photo'] = this.photo;
    data['email'] = this.email;
    data['password'] = this.password;
    data['id_cms_privileges'] = this.idCmsPrivileges;
    data['updated_at'] = this.updatedAt;
    data['status'] = this.status;
    return data;
  }
}