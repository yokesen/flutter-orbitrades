// To parse this JSON data, do
//
//     final mt4AccountType = mt4AccountTypeFromJson(jsonString);

import 'dart:convert';

Mt4AccountType mt4AccountTypeFromJson(String str) => Mt4AccountType.fromJson(json.decode(str));

String mt4AccountTypeToJson(Mt4AccountType data) => json.encode(data.toJson());

class Mt4AccountType {
  Mt4AccountType({
    this.success,
    this.response,
  });

  bool success;
  List<Response> response;

  factory Mt4AccountType.fromJson(Map<String, dynamic> json) => Mt4AccountType(
    success: json["success"] == null ? null : json["success"],
    response: json["response"] == null ? null : List<Response>.from(json["response"].map((x) => Response.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "success": success == null ? null : success,
    "response": response == null ? null : List<dynamic>.from(response.map((x) => x.toJson())),
  };
}

class Response {
  Response({
    this.namaCategory,
    this.id,
    this.namaAccount,
    this.deskripsiAccount,
    this.minimumDepo,
    this.bestChoice,
  });

  String namaCategory;
  int id;
  String namaAccount;
  String deskripsiAccount;
  int minimumDepo;
  String bestChoice;

  factory Response.fromJson(Map<String, dynamic> json) => Response(
    namaCategory: json["namaCategory"] == null ? null : json["namaCategory"],
    id: json["id"] == null ? null : json["id"],
    namaAccount: json["namaAccount"] == null ? null : json["namaAccount"],
    deskripsiAccount: json["deskripsiAccount"] == null ? null : json["deskripsiAccount"],
    minimumDepo: json["minimumDepo"] == null ? null : json["minimumDepo"],
    bestChoice: json["bestChoice"] == null ? null : json["bestChoice"],
  );

  Map<String, dynamic> toJson() => {
    "namaCategory": namaCategory == null ? null : namaCategory,
    "id": id == null ? null : id,
    "namaAccount": namaAccount == null ? null : namaAccount,
    "deskripsiAccount": deskripsiAccount == null ? null : deskripsiAccount,
    "minimumDepo": minimumDepo == null ? null : minimumDepo,
    "bestChoice": bestChoice == null ? null : bestChoice,
  };
}
