import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import 'size_config.dart';

const String kAppName = '';
const String kImageDir = 'assets/images/';
const String kImageUrl = 'https://trustcollect.qrbdl.com/';
const String kBaseUrl = 'https://trustcollect.qrbdl.com/api/';
const String kBase64Extend = 'data:image/jpeg;base64,';
const String kBase64ExtendVideo = 'data:video/mp4;base64,';
const String kBase64ExtendAudio = 'data:audio/mp3;base64,';
// String kIsBangla = LocalizationService().getCurrentLang();

const kPrimaryColor = Color(0xFF97272C);
const kSecondaryColor = Color(0xFFF4C55C);
const kWhiteColor = Colors.white;
const kBlackColor = Colors.black;
const kDarkGrayColor = Color(0xFF4E4E4E);
const kOrdinaryColor = Color(0xFFA1A1A1);
const kOrdinaryColor2 = Color(0xFFE5E5E5);
const kBackGroundColor = Color(0xFF2A2C36);
const kDarkColor = Color(0xFF1E1F28);
const kErrorColor = Color(0xFFFF2424);
const kSuccessColor = Color(0xFF2EB843);
const kSaleColor = Color(0xFFF14705);
const kIconColor = Color(0xFFF9784F);
const khomeFirstCardColor = Color(0xFFFEF9EF);
const khomeSecCardColor = Color(0xFF2EB843);
const kSubscriptionColor = Color(0xFFF2C822);

//FontWeight

const weight400 = FontWeight.w400;
const weight500 = FontWeight.w500;
const weightBold = FontWeight.bold;

//SizedBox Height
const size5 = SizedBox(height: 5);
const size8 = SizedBox(height: 8);
const size10 = SizedBox(height: 10);
const size15 = SizedBox(height: 15);
const size20 = SizedBox(height: 20);

//SizedBox Width
const width5 = SizedBox(width: 5);
const width10 = SizedBox(width: 10);
const width15 = SizedBox(width: 15);
const width20 = SizedBox(width: 20);

//Axis Alignment
const crossAxisCenter = CrossAxisAlignment.center;
const mainAxisCenter = MainAxisAlignment.center;
const mainAxisStart = MainAxisAlignment.start;
const crossAxisStart = CrossAxisAlignment.start;
const mainAxisSpaceBetween = MainAxisAlignment.spaceBetween;

const String kUSD = 'USD';
const String kIDR = 'IDR';
//Container BoxDecoration

BoxDecoration containerBoxDecoration(
    {double borderRadius, Color color, List<BoxShadow> boxShadow}) {
  return BoxDecoration(
    borderRadius: BorderRadius.circular(borderRadius),
    color: color,
    boxShadow: boxShadow,
  );
}

final kHomeTitle = TextStyle(
  color: kBlackColor,
  fontWeight: FontWeight.w600,
  fontSize: getProportionateScreenWidth(18),
);

final kHeadLine = TextStyle(
  fontFamily: GoogleFonts.roboto().fontFamily,
  fontSize: getProportionateScreenWidth(34.0),
  fontWeight: FontWeight.w700,
  height: 1.25,
);
final kHeadLineTest = TextStyle(
  fontSize: getProportionateScreenWidth(34.0),
  fontWeight: FontWeight.w700,
  height: 1.25,
);
final kHeadLine2 = TextStyle(
  fontSize: getProportionateScreenWidth(24.0),
  fontWeight: FontWeight.w600,
  height: 1.25,
);
final kHeadLine3 = TextStyle(
  fontSize: getProportionateScreenWidth(22.0),
  fontWeight: FontWeight.w500,
  height: 1.25,
);
final kAppBarText = TextStyle(
  fontSize: getProportionateScreenWidth(18.0),
  fontWeight: FontWeight.w500,
  height: 1.25,
);
final kRegularText = TextStyle(
  fontSize: getProportionateScreenWidth(16.0),
  fontWeight: FontWeight.w400,
  height: 1.25,
);
final kRegularText2 = TextStyle(
  fontSize: getProportionateScreenWidth(14.0),
  fontWeight: FontWeight.w400,
  height: 1.25,
);
final kDescriptionText = TextStyle(
  fontSize: getProportionateScreenWidth(12.0),
  fontWeight: FontWeight.w400,
  height: 1.25,
);
final kSmallText = TextStyle(
  fontSize: getProportionateScreenWidth(11.0),
  fontWeight: FontWeight.w400,
  height: 1.25,
);
final kBoldStyle = TextStyle(
  fontSize: getProportionateScreenWidth(16.0),
  fontWeight: FontWeight.bold,
  height: 1.25,
);

Text textRoboto(String title, Color color,
    {double fontSize,
    FontWeight fontWeight,
    int maxLine,
    TextDecoration textDecoration}) {
  return Text(
    title,
    style: GoogleFonts.roboto(
        color: color,
        fontWeight: fontWeight,
        fontSize: fontSize,
        decoration: textDecoration),
    maxLines: maxLine ?? 1,
    overflow: TextOverflow.ellipsis,
    softWrap: false,
  );
}

// Form Error
final RegExp emailValidatorRegExp =
    RegExp(r"^[a-zA-Z0-9.]+@[a-zA-Z0-9]+\.[a-zA-Z]+");
const String kEmailNullError = "Please enter your email";
const String kInvalidEmailError = "Please enter valid Email";
const String kPassNullError = "Please enter your password";
const String kShortPassError = "Password is too short";
const String kMatchPassError = "Passwords don't match";
const String kNameNullError = "Please enter your name";
const String kPhoneNumberNullError = "Please enter your phone number";
const String kAddressNullError = "Please enter your address";
const String kInvalidNumberError = "Invalid phone number";
const String kmnWithdlimit = "Minimum withdraw limit 1 USD";

final otpInputDecoration = InputDecoration(
  fillColor: kWhiteColor,
  contentPadding: EdgeInsets.symmetric(vertical: 15),
  border: outlineInputBorder(),
  focusedBorder: outlineInputBorder(),
  enabledBorder: outlineInputBorder(),
);

OutlineInputBorder outlineInputBorder() {
  return OutlineInputBorder(
    borderRadius: BorderRadius.circular(5),
    borderSide: BorderSide(color: kOrdinaryColor),
  );
}

//AppBar method
AppBar buildAppBar({
  BuildContext context,
  String title = kAppName,
  bool leading = true,
}) {
  return AppBar(
    automaticallyImplyLeading: leading,
    backgroundColor: Colors.white,
    brightness: Brightness.light,
    centerTitle: true,
    leading: Builder(
      builder: (context) => IconButton(
        icon: Icon(Icons.arrow_back, color: Colors.black),
        onPressed: () => Navigator.of(context).pop(),
      ),
    ),
    title: Text(
      title,
      overflow: TextOverflow.ellipsis,
      maxLines: 1,
      style: TextStyle(color: Colors.black),
    ),
    elevation: 0.0,
    titleSpacing: 0,
  );
}
