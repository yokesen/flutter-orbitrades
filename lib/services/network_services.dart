import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:orbitrade/model/check_changed_user_name.dart';
import 'package:orbitrade/model/custody-bank-list.dart';
import 'package:orbitrade/model/custodyBank.dart';
import 'package:orbitrade/model/internal_transfer_data.dart';
import 'package:orbitrade/model/mt4_account_type.dart';
import 'package:orbitrade/model/myAccountModel.dart';
import 'package:orbitrade/model/myBank.dart';
import 'package:orbitrade/model/my_deposit_data.dart';
import 'package:orbitrade/model/my_withdrawal_data.dart';
import 'package:orbitrade/model/user_data.dart';
import 'package:orbitrade/sheared/custom_loader.dart';
import 'package:orbitrade/views/home/Home.dart';

import '../app_config.dart';
import '../main.dart';

class NetworkServices {
  //add headers
  Map<String, String> headers = {
    'Accept': 'application/json',
  };
  //common user api start here//

  Future userLogin(
      {BuildContext context, String username, String password}) async {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) => CustomLoader());
    final response = await http.post('$BASE_URL/login', body: {
      'email': username,
      'password': password,
    });
    print(response.statusCode);
    print(response.body.toString());
    return json.decode(response.body);
  }

  Future userLoginText(
      {BuildContext context, String username, String password}) async {
    final response = await http.post('$BASE_URL/login', body: {
      'email': username,
      'password': password,
    });
    print(response.statusCode);
    print(response.body.toString());
    return json.decode(response.body);
  }

  //User Registration
  Future userRegistration(
      {BuildContext context,
      String username,
      String email,
      String phone,
      String password}) async {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) => CustomLoader());
    final response = await http.post('$BASE_URL/register', body: {
      'name': username,
      'email': email,
      'phone': phone,
      'password': password,
    });
    print(response.statusCode);
    print(response.body.toString());
    return json.decode(response.body);
  }

  //User Registration
  Future userGoogleRegistration(
      {BuildContext context,
        String displayName,
        String email,
        String id,
        String photoUrl,
        String idToken,
      }) async {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) => CustomLoader());
    final response = await http.post('$BASE_URL/login-with-google', body: {
      'displayName': displayName,
      'email': email,
      'id': id,
      'photoUrl': photoUrl,
      '_idToken': idToken,
    });
    print(response.statusCode);
    print(response.body.toString());
    return json.decode(response.body);
  }

  Future uploadKTPFile(BuildContext context, File uploadFile) async {
    showDialog(
        builder: (context) => CustomLoader(),
        context: context,
        barrierDismissible: false);

    Map<String, String> headers = {
      'Accept': 'application/json',
      'Authorization': 'Bearer ${prefs.getString('token')}',
    };

    var result;
    var uri = Uri.parse("$BASE_URL/uploadKTP");
    var request = new http.MultipartRequest("POST", uri);
    request.headers.addAll(headers);

    if (uploadFile != null) {
      var stream = new http.ByteStream(uploadFile.openRead());
      var length = await uploadFile.length();
      var multipartFile = new http.MultipartFile('gambar', stream, length,
          filename: uploadFile.path);
      request.files.add(multipartFile);
    }
    var response = await request.send();
    await response.stream.transform(utf8.decoder).listen((value) {
      result = value;
    });

    print(result);
    return json.decode(result);
  }

  Future<CustodyBankList> fetchCusodyBankList(String token) async {
    Map<String, String> headers = {
      'Accept': 'application/json',
      'Authorization': 'Bearer $token',
    };
    final response =
        await http.get('$BASE_URL/custody-bank-list', headers: headers);
    if (response.statusCode == 200) {
      var data = json.decode(response.body);

      print(data);
      return CustodyBankList.fromJson(data);
    } else {
      throw Exception('Failed to load post');
    }
  }

  Future updateBank(String bankName, String accountName, String accountNumber,
      BuildContext context, File file) async {
    showDialog(
        builder: (context) => CustomLoader(),
        context: context,
        barrierDismissible: false);

    var data = {
      'bank_name': bankName,
      'account_name': accountName,
      'account_number': accountNumber
    };

    print("My Input Data: $data");

    final response = await http.post('$BASE_URL/update-bank', headers: {
      'Accept': 'application/json',
      'Authorization': 'Bearer ${prefs.getString('token')}',
    }, body: {
      'bank_name': bankName,
      'account_name': accountName,
      'account_number': accountNumber
    });

    if (response.statusCode == 200 || response.statusCode == 201) {
      print(response.statusCode);
      print(".............Bank Name info added............");
      print(response.body.toString());

      await uploadBukuTabunganFile(context, file);
    } else {
      Navigator.of(context).pop();
      Map<String, dynamic> dataMap = jsonDecode(response.body);
      Get.snackbar('Success', dataMap["response"]);
      print(dataMap["response"]);
    }
  }

  Future uploadBukuTabunganFile(BuildContext context, File uploadFile) async {
    /*showDialog(
        builder: (context) => CustomLoader(),
        context: context,
        barrierDismissible: false);*/

    print(".........calling file uplod ----------------");

    Map<String, String> headers = {
      'Accept': 'application/json',
      'Authorization': 'Bearer ${prefs.getString('token')}',
    };

    var result;
    var uri = Uri.parse("$BASE_URL/uploadBukuTabungan");
    var request = new http.MultipartRequest("POST", uri);
    request.headers.addAll(headers);

    if (uploadFile != null) {
      var stream = new http.ByteStream(uploadFile.openRead());
      var length = await uploadFile.length();
      var multipartFile = new http.MultipartFile('gambar', stream, length,
          filename: uploadFile.path);
      request.files.add(multipartFile);
    }
    var response = await request.send();
    await response.stream.transform(utf8.decoder).listen((value) {
      result = value;
    });
    if (response.statusCode == 200 || response.statusCode == 201) {
      print(response.stream.toString());
      print(".........success ----------------");
      Get.snackbar('Success', 'upload BukuTabungan successfully!');
      Navigator.of(context).pop();
      Navigator.of(context).pushNamedAndRemoveUntil(
          Home.routeName, (Route<dynamic> route) => false);
    } else {
      Navigator.of(context).pop();
      print(response.stream.toString());

      Map<String, dynamic> dataMap = json.decode(result);
      Get.snackbar('Error!', dataMap["message"]);
      print(dataMap["message"]);
    }

    print(result);
    //return json.decode(result);
  }

  Future userLogout({
    BuildContext context,
  }) async {
    showDialog(
        builder: (context) => CustomLoader(),
        context: context,
        barrierDismissible: false);
    final response = await http.get(
      '$BASE_URL/logout',
      headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer ${prefs.getString('token')}',
      },
    );
    print(response.statusCode);
    print(response.body.toString());
    return json.decode(response.body);
  }

  Future changePassword({BuildContext context, String password}) async {
    final response = await http.post('$BASE_URL/change-password', headers: {
      'Accept': 'application/json',
      'Authorization': 'Bearer ${prefs.getString('token')}',
    }, body: {
      "password": password
    });

    if (response.statusCode == 200 || response.statusCode == 201) {
      print(response.statusCode);
      print(response.body.toString());
      return json.decode(response.body);
    } else {
      print(response.statusCode);
      print(response.body);
      throw Exception('Fail to load');
    }
  }

  Future<UserData> fetchUserData() async {
    print(prefs.getString('token'));
    final response = await http.get(
      '$BASE_URL/get_user',
      headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer ${prefs.getString('token')}',
      },
    );
    if (response.statusCode == 200) {
      var posts = json.decode(response.body);
      print(response.statusCode);
      log(response.body);

      return UserData.fromJson(posts);
    } else {
      print(response.statusCode);
      throw Exception('Fail to load');
    }
  }

  Future<CustodyBank> custodyBankList() async {
    print(prefs.getString('token'));
    final response = await http.get(
      '$BASE_URL/custody-bank-list',
      headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer ${prefs.getString('token')}',
      },
    );
    if (response.statusCode == 200) {
      var posts = json.decode(response.body);
      print(response.statusCode);
      log(response.body);

      return CustodyBank.fromJson(posts);
    } else {
      print(response.statusCode);
      throw Exception('Fail to load');
    }
  }

  Future<MyBank> myBank() async {
    print(prefs.getString('token'));
    final response = await http.get(
      '$BASE_URL/my-bank',
      headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer ${prefs.getString('token')}',
      },
    );
    if (response.statusCode == 200) {
      print("MY Bank ${response.statusCode}");
      log("My Bank BODY ${response.body}");
      var posts = json.decode(response.body);

      return MyBank.fromJson(posts);
    } else {
      print(response.statusCode);
      throw Exception('Fail to load');
    }
  }

  Future<Mt4AccountType> fetchMt4AccountType() async {
    final response = await http.get(
      '$BASE_URL/account-type-list',
      headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer ${prefs.getString('token')}',
      },
    );
    if (response.statusCode == 200) {
      var posts = json.decode(response.body);
      print(response.statusCode);
      log(response.body);

      return Mt4AccountType.fromJson(posts);
    } else {
      print(response.statusCode);
      throw Exception('Fail to load');
    }
  }

  Future<MyAccount> fetchMyAccount() async {
    print("my account token ${prefs.getString('token')}");

    final response = await http.get(
      '$BASE_URL/my-account',
      headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer ${prefs.getString('token')}',
      },
    );
    if (response.statusCode == 200) {
      var posts = json.decode(response.body);
      print(response.statusCode);
      log(response.body);

      return MyAccount.fromJson(posts);
    } else {
      print(response.statusCode);
      throw Exception('Fail to load');
    }
  }

  Future uploadImage(BuildContext context, File profileImgFile) async {
    showDialog(
        builder: (context) => CustomLoader(),
        context: context,
        barrierDismissible: false);

    Map<String, String> headers = {
      'Accept': 'application/json',
      'Authorization': 'Bearer ${prefs.getString('token')}',
    };

    var result;
    var uri = Uri.parse("$BASE_URL/uploadprofileimage");
    var request = new http.MultipartRequest("POST", uri);
    request.headers.addAll(headers);

    if (profileImgFile != null) {
      var stream = new http.ByteStream(profileImgFile.openRead());
      var length = await profileImgFile.length();
      var multipartFile = new http.MultipartFile('gambar', stream, length,
          filename: profileImgFile.path);
      request.files.add(multipartFile);
    }
    var response = await request.send();
    await response.stream.transform(utf8.decoder).listen((value) {
      result = value;
    });

    print(result);
    return json.decode(result);
  }

  Future<ChangedUserName> checkChangeUserName() async {
    final response = await http.get(
      '$BASE_URL/check-username-changed',
      headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer ${prefs.getString('token')}',
      },
    );
    if (response.statusCode == 200) {
      var posts = json.decode(response.body);
      print(response.statusCode);
      log(response.body);

      return ChangedUserName.fromJson(posts);
    } else {
      print(response.statusCode);
      throw Exception('Fail to load');
    }
  }

  Future changeUserName({BuildContext context, String userName}) async {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) => CustomLoader());
    final response = await http.post(
      '$BASE_URL/change-username',
      body: {"username": userName},
      headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer ${prefs.getString('token')}',
      },
    );
    print(response.statusCode);
    print(response.body.toString());
    return json.decode(response.body);
  }

  Future createMt4Account({BuildContext context, String id}) async {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) => CustomLoader());
    final response = await http.post(
      '$BASE_URL/create-account',
      body: {"accountType": id},
      headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer ${prefs.getString('token')}',
      },
    );
    print(response.statusCode);
    print(response.body.toString());
    return json.decode(response.body);
  }

  //start Withdrawal
  //withdrawal request
  Future requestWithdrawal({
    BuildContext context,
    String metatrader,
    String tipe_deposit,
    String amount,
    String bank_name,
    String account_name,
    String account_number,
  }) async {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) => CustomLoader());
    final response = await http.post(
      '$BASE_URL/request-withdrawal',
      body: {
        "metatrader": metatrader,
        "tipe_deposit": tipe_deposit,
        "amount": amount,
        "bank_name": bank_name,
        "account_name": account_name,
        "account_number": account_number,
      },
      headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer ${prefs.getString('token')}',
      },
    );
    print(response.statusCode);
    print(response.body.toString());
    return json.decode(response.body);
  }

  //get my withdrawal data
  Future<MyWithdrawalData> fetchWithdrawalData() async {
    final response = await http.get(
      '$BASE_URL/my-withdrawal',
      headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer ${prefs.getString('token')}',
      },
    );
    if (response.statusCode == 200) {
      var posts = json.decode(response.body);
      print(response.statusCode);
      log(response.body);

      return MyWithdrawalData.fromJson(posts);
    } else {
      print(response.statusCode);
      throw Exception('Fail to load');
    }
  }

  ///end withdrawal

  //Start Internal Transfer
//request internal transfer

  Future requestInternalTransfer({
    BuildContext context,
    String fromAccount,
    String amount,
    String toAccount,
  }) async {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) => CustomLoader());
    final response = await http.post(
      '$BASE_URL/request-internal-transfer',
      body: {
        "fromAccount": fromAccount,
        "amount": amount,
        "toAccount": toAccount,
      },
      headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer ${prefs.getString('token')}',
      },
    );
    print(response.statusCode);
    print(response.body.toString());
    return json.decode(response.body);
  }

  //get my withdrawal data
  Future<InternalTransferData> fetchInternalTransferData() async {
    final response = await http.get(
      '$BASE_URL/my-internal-transfer',
      headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer ${prefs.getString('token')}',
      },
    );
    if (response.statusCode == 200) {
      var posts = json.decode(response.body);
      print(response.statusCode);
      log(response.body);

      return InternalTransferData.fromJson(posts);
    } else {
      print(response.statusCode);
      throw Exception('Fail to load');
    }
  }

  ///end internal transfer

  /// start Deposit
  //Request deposit to create deposit data
  Future requestDeposit({
    BuildContext context,
    String amount,
    String tipeDeposit,
    String photo,
    String bank_name,
    String account_name,
    String account_number,
    String namaBank,
    String namaRekening,
    String nomorRekening,
    String accountmt4,
  }) async {
    final response = await http.post(
      '$BASE_URL/request-deposit',
      body: {
        "amount": amount,
        "tipeDeposit": tipeDeposit,
        "photo": photo,
        "bank_name": bank_name,
        "account_name": account_name,
        "account_number": account_number,
        "namaBank": namaBank,
        "namaRekening": namaRekening,
        "nomorRekening": nomorRekening,
        "accountmt4": accountmt4,
      },
      headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer ${prefs.getString('token')}',
      },
    );
    print(response.statusCode);
    print(response.body.toString());
    return json.decode(response.body);
  }

  // get my deposit data
  Future<MyDepositData> fetchMyDepositData() async {
    final response = await http.get(
      '$BASE_URL/my-deposit',
      headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer ${prefs.getString('token')}',
      },
    );
    if (response.statusCode == 200) {
      var posts = json.decode(response.body);
      print(response.statusCode);
      log(response.body);

      return MyDepositData.fromJson(posts);
    } else {
      print(response.statusCode);
      throw Exception('Fail to load');
    }
  }

  ///end deposit

  Future submitQuiz({BuildContext context,String vid,  List<String> qJawaban}) async {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) => CustomLoader());
    print(qJawaban.toList());
    final datas =  jsonEncode( {
      "qJawaban": qJawaban.toList().toString(),
    });
    final response = await http.post('$BASE_URL/submit-quiz/$vid', headers: {
      'Accept': 'application/json',
      'Authorization': 'Bearer ${prefs.getString('token')}',
    }, body: {
      "qJawaban": qJawaban.toList().toString()
    });

    print(response.statusCode);
    print(response.body.toString());
    return json.decode(response.body);
  }
/*  Future reminderProductLinkUpload({
    BuildContext context,
    String link,
    String image,
    String description,
    DateTime endTime,
  }) async {
    showDialog(
        builder: (context) => CustomLoader(), context: context, barrierDismissible: false);
    final http.Response response = await client.post(
      baseUrl + 'reminder-add',
      body: {
        'link': link,
        'thumbnail': kBase64Extend + image,
        'description': description,
        'left_time': endTime.toString(),
      },
      headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer ${prefs.getString('token')}',
      },
    );
    if (response.statusCode == 200) {
      var posts = jsonDecode(response.body);
      print(response.statusCode);
      print(response.body);
      return posts;
    } else {
      print(response.statusCode);
      print(response.body);
      throw Exception('Fail to load');
    }
  }*/

  // Future<RpReminderListModel> reminderList() async {
  //   final http.Response response = await client.get(
  //     baseUrl + 'reminder-list',
  //     headers: {
  //       'Accept': 'application/json',
  //       'Authorization': 'Bearer ${prefs.getString('token')}',
  //     },
  //   );
  //   if (response.statusCode == 200) {
  //     var posts = jsonDecode(response.body);
  //     print(response.statusCode);
  //     return RpReminderListModel.fromJson(posts);
  //   } else {
  //     print(response.statusCode);
  //     throw Exception('Fail to load');
  //   }
  // }

}
