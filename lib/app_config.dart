var this_year = DateTime.now().year.toString();

const String app_name = "ORBI Trade"; //this shows in the splash screen
String copyright_text =
    "@ $app_name " + this_year; //this shows in the splash screen

//full url = https://dev-grif.1129-1891.cyou/api/v1/logout
//configure this
const bool HTTPS = true;

//configure this
const DOMAIN_PATH =
    "repo-api-orbit.1129-1891.cyou"; // directly inside the public folder

//do not configure these below
const String API_ENDPATH = "api/v1";
const String PUBLIC_FOLDER = "public";
const String PROTOCOL = HTTPS ? "https://" : "http://";
const String RAW_BASE_URL = "${PROTOCOL}${DOMAIN_PATH}";
const String BASE_URL = "${RAW_BASE_URL}/${API_ENDPATH}";

//configure this if you are using amazon s3 like services
//give direct link to file like https://[[bucketname]].s3.ap-southeast-1.amazonaws.com/
//otherwise do not change anythink
const String BASE_PATH =
    "https://we-are-the-blues.sg-sin1.upcloudobjects.com/the-color-is-blue/images/orbitrade/static/peta-harga/";