import 'package:flutter/material.dart';

import '../../constants.dart';
import '../../size_config.dart';
import 'calender_page/calender_page_home.dart';
import 'home_page/homepage.dart';
import 'profile_page/profile_home_page.dart';
import 'strateygy_page/strategy_page_home.dart';

class Home extends StatefulWidget {
  static const routeName = 'home_menu_bottom';

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<Home> {
  PageController _pageController = PageController();
  int _selectedIndex = 0;
  List<Widget> _screen = [
    HomePage(),
    CalenderHomePage(),
    StrategyHomePage(),
    ProfileHomePage()
  ];

  void _onPageChanged(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  void _onItemTaped(int selectedIndex) {
    setState(() {
      _pageController.jumpToPage(selectedIndex);
    });
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return WillPopScope(
      onWillPop: _onBackPressed,
      child: Scaffold(
        backgroundColor: Colors.white70,
        body: _screen[_selectedIndex],
        bottomNavigationBar: BottomNavigationBar(
          type: BottomNavigationBarType.fixed,
          showUnselectedLabels: true,
          unselectedItemColor: kOrdinaryColor,
          selectedItemColor: kPrimaryColor,
          onTap: _onPageChanged,
          currentIndex: _selectedIndex,
          items: [
            BottomNavigationBarItem(
              icon: Icon(
                Icons.home_outlined,
              ),
              label: "Home",
            ),
            BottomNavigationBarItem(
              icon: Icon(
                Icons.calendar_today_outlined,
              ),
              label: "Calender",
            ),
            BottomNavigationBarItem(
              icon: Icon(
                Icons.app_registration,
              ),
              label: "Strategy",
            ),
            BottomNavigationBarItem(
              icon: Icon(
                Icons.person,
              ),
              label: "Account",
            ),
          ],
        ),
      ),
    );
  }

  Future<bool> _onBackPressed() {
    return showDialog(
          context: context,
          builder: (context) {
            return AlertDialog(
              title: Text('Are you sure?'),
              content: Text('Do you want to exit an App'),
              actions: <Widget>[
                FlatButton(
                  child: Text('No'),
                  onPressed: () {
                    Navigator.of(context).pop(false);
                  },
                ),
                FlatButton(
                  child: Text('Yes'),
                  onPressed: () {
                    Navigator.of(context).pop(true);
                  },
                )
              ],
            );
          },
        ) ??
        false;
  }
}
