import 'package:flutter/material.dart';
import 'package:orbitrade/constants.dart';
import 'package:orbitrade/views/home/calender_page/widget/related_news_widgets.dart';

class TradeBalance extends StatelessWidget {
  static const routeName = 'trade_balance';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kWhiteColor,
      appBar: AppBar(
        iconTheme: IconThemeData(color: kBlackColor),
        elevation: 1,
        backgroundColor: kWhiteColor,
        centerTitle: true,
        title: Text(
          'Trader Balance',
          style: kRegularText2.copyWith(
            color: kBlackColor,
            fontWeight: FontWeight.w600,
          ),
        ),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(
            vertical: 10,
            horizontal: 20,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              GestureDetector(
                onTap: () {
                  // Navigator.pushNamed(context, DepositRequestScreen.routeName);
                },
                child: Container(
                  padding: EdgeInsets.all(20),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    border: Border.all(
                      width: 1,
                      color: kOrdinaryColor2,
                    ),
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          Expanded(
                            child: Text(
                              'Country',
                              style: kRegularText2.copyWith(
                                color: kPrimaryColor,
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                          ),
                          Expanded(
                            child: Text(
                              'Impact',
                              style: kRegularText2.copyWith(
                                color: kPrimaryColor,
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Row(
                        children: [
                          Expanded(
                            child: Row(
                              children: [
                                Image.asset(kImageDir + 'us_image.png'),
                                SizedBox(
                                  width: 5,
                                ),
                                Text(
                                  'USD',
                                  style: kRegularText2.copyWith(
                                    fontWeight: FontWeight.w600,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Expanded(
                            child: Text(
                              'Low',
                              style: kRegularText2.copyWith(
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Text(
                        'Source',
                        style: kRegularText2.copyWith(
                          color: kPrimaryColor,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Text(
                        'Commodity Futures Trading Commission',
                        style: kRegularText2.copyWith(
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Row(
                        children: [
                          Expanded(
                            child: Text(
                              'Frequency',
                              style: kRegularText2.copyWith(
                                color: kPrimaryColor,
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                          ),
                          Expanded(
                            child: Text(
                              'Next release',
                              style: kRegularText2.copyWith(
                                color: kPrimaryColor,
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Row(
                        children: [
                          Expanded(
                            child: Text(
                              'Irregular',
                              style: kRegularText2.copyWith(
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                          ),
                          Expanded(
                            child: Text(
                              'Sat, Jul 31st, 2021',
                              style: kRegularText2.copyWith(
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Text(
                'The weekly Commitments of Traders (COT) report provides '
                'information on the size and the direction of the positions taken,'
                ' across all maturities, participants primarily based in Chicago and '
                'New York futures markets. Forex trades focus in "non-commercial" or speculative positions, to determinate whether a trend remains healthy or not, and also markets sentiment towards a certain asset.',
                style: kDescriptionText.copyWith(
                  fontWeight: FontWeight.w600,
                  fontSize: 12,
                  height: 2.2,
                ),
                textAlign: TextAlign.justify,
              ),
              RelatedNewsWidgets(),
            ],
          ),
        ),
      ),
    );
  }
}
