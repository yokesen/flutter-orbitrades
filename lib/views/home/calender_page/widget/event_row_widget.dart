import 'package:flutter/material.dart';
import 'package:orbitrade/size_config.dart';

import '../../../../constants.dart';

class EventRowWidget extends StatelessWidget {
  final String time;
  final String eventTitle;
  final String actualAmount;
  final String consAmount;
  final String prevAmount;
  final String currency;
  final String status;
  final String country;
  final Color statusColor;

  EventRowWidget({
    @required this.time,
    @required this.eventTitle,
    @required this.actualAmount,
    @required this.consAmount,
    @required this.prevAmount,
    @required this.country,
    @required this.currency,
    @required this.status,this.statusColor
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Flexible(
                flex: 1,
                child: Text(
                  time,
                  style: kDescriptionText.copyWith(
                    color: kBlackColor,
                    fontWeight: FontWeight.w600,
                  ),
                  maxLines: 1,
                ),
              ),
              Flexible(
                flex: 2,
                fit: FlexFit.tight,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Text(
                      eventTitle,
                      style: kSmallText.copyWith(
                        color: kBlackColor,
                        fontWeight: FontWeight.w600,
                      ),
                      maxLines: 2,
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Image.asset(
                          kImageDir + country,
                          width: getProportionateScreenWidth(24),
                          fit: BoxFit.cover,
                        ),
                        SizedBox(
                          width: 5,
                        ),
                        Text(
                          currency,
                          style: kSmallText.copyWith(
                            color: kBlackColor.withOpacity(.5),
                          ),
                        ),
                        SizedBox(
                          width: 5,
                        ),
                        Container(
                          decoration: BoxDecoration(
                            color: statusColor??kSecondaryColor,
                            borderRadius: BorderRadius.circular(5),
                          ),
                          padding:
                              EdgeInsets.symmetric(vertical: 5, horizontal: 8),
                          child: Text(
                            status,
                            style: kSmallText.copyWith(),
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              ),
              Flexible(
                flex: 1,
                child: Text(
                  actualAmount,
                  style: kSmallText.copyWith(
                    color: kBlackColor,
                    fontWeight: FontWeight.w600,
                  ),
                  maxLines: 1,
                ),
              ),
              Flexible(
                flex: 1,
                child: Text(
                  consAmount,
                  style: kSmallText.copyWith(
                    color: kBlackColor,
                    fontWeight: FontWeight.w600,
                  ),
                  maxLines: 1,
                  textAlign: TextAlign.center,
                ),
              ),
              Flexible(
                flex: 1,
                child: Text(
                  prevAmount,
                  maxLines: 1,
                  style: kSmallText.copyWith(
                    color: kBlackColor,
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
