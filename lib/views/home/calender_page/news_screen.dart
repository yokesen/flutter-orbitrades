import 'package:flutter/material.dart';
import 'package:orbitrade/size_config.dart';
import 'package:orbitrade/views/home/calender_page/widget/related_news_widgets.dart';

import '../../../constants.dart';

class NewsScreen extends StatelessWidget {
  static const routeName = 'news_screen';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kWhiteColor,
      appBar: AppBar(
        iconTheme: IconThemeData(color: kBlackColor),
        elevation: 1,
        backgroundColor: kWhiteColor,
        centerTitle: true,
        title: Text(
          'News',
          style: kRegularText2.copyWith(
            color: kBlackColor,
            fontWeight: FontWeight.w600,
          ),
        ),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: 20,
            vertical: 15,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'USD/JPY retakes 110.00 after Japan’s government maintains overall economic view',
                style: kAppBarText.copyWith(
                    fontWeight: FontWeight.w600, height: 1.8),
                maxLines: 3,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(
                  vertical: 5,
                ),
                child: Text(
                  'Jul 19, 07:06 GMT',
                  style: kDescriptionText.copyWith(
                    color: kOrdinaryColor,
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ),
              SizedBox(
                height: getProportionateScreenHeight(25),
              ),
              Text(
                'The weekly Commitments of Traders (COT) report provides information on the size and the direction of the positions taken, across all maturities, participants primarily based in Chicago and New York futures markets. Forex trades focus in "non-commercial" or speculative positions, to determinate whether a trend remains healthy or not, and also markets sentiment towards a certain asset.\n\n\nThe weekly Commitments of Traders (COT) report provides information on the size and the direction of the positions taken, across all maturities, participants primarily based in Chicago and New York futures markets.',
                textAlign: TextAlign.justify,
                style: kDescriptionText.copyWith(
                    fontWeight: FontWeight.w600, fontSize: 12, height: 2.2),
              ),
              RelatedNewsWidgets(),
            ],
          ),
        ),
      ),
    );
  }
}
