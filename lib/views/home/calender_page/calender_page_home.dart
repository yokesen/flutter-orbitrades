import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:orbitrade/constants.dart';
import 'package:orbitrade/controller/calendarController.dart';
import 'package:orbitrade/sheared/custom_loader.dart';
import 'package:orbitrade/size_config.dart';
import 'package:orbitrade/views/home/calender_page/trade_balance.dart';
import 'package:orbitrade/views/home/calender_page/widget/event_row_widget.dart';
import 'package:table_calendar/table_calendar.dart';

import '../../../constants.dart';

class CalenderHomePage extends StatefulWidget {
  static const routeName = 'calender_home_page';
  const CalenderHomePage({Key key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<CalenderHomePage> {
  CalendarController calenderController = CalendarController();

  var calendarC=Get.put(CalendarEventController());

  @override
  void initState() {
    // TODO: implement initState
    calendarC.getCalendarEvent();
    super.initState();
  }
  @override
  void dispose() {
    // TODO: implement dispose
    calenderController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kWhiteColor,
      appBar: AppBar(
        elevation: 0,
        brightness: Brightness.light,
        backgroundColor: Colors.white,
        title: Text(
          'Economic Calender',
          style: kAppBarText.copyWith(
            color: kBlackColor,
            fontWeight: FontWeight.w600,
          ),
        ),
      ),
      body: SingleChildScrollView(
        child: Container(
          child: Column(
            children: [
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 10),
                child: TableCalendar(
                  rowHeight: 50,
                  calendarController: calenderController,
                  initialSelectedDay: DateTime.now(),
                  weekendDays: [5, 6],
                  startingDayOfWeek: StartingDayOfWeek.monday,
                  daysOfWeekStyle: DaysOfWeekStyle(
                    weekdayStyle: kDescriptionText.copyWith(
                      color: Color(0xFF3C3C43).withOpacity(.4),
                      fontWeight: FontWeight.w600,
                    ),
                    weekendStyle: kDescriptionText.copyWith(
                      color: Color(0xFF3C3C43).withOpacity(.4),
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  headerStyle: HeaderStyle(
                    leftChevronMargin: EdgeInsets.only(
                      left: getProportionateScreenWidth(50),
                    ),
                    leftChevronIcon: Icon(
                      Icons.arrow_back_ios,
                      color: kPrimaryColor,
                    ),
                    rightChevronMargin: EdgeInsets.only(
                      right: getProportionateScreenWidth(50),
                    ),
                    rightChevronIcon: Icon(
                      Icons.arrow_forward_ios,
                      color: kPrimaryColor,
                    ),
                    formatButtonVisible: false,
                    centerHeaderTitle: true,
                    titleTextStyle: kAppBarText.copyWith(
                      color: kPrimaryColor,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  calendarStyle: CalendarStyle(
                    contentPadding: EdgeInsets.zero,
                    cellMargin: EdgeInsets.all(5),
                    todayColor: kPrimaryColor,
                    selectedColor: kPrimaryColor,
                    highlightToday: false,
                    weekdayStyle: kRegularText.copyWith(
                      color: kPrimaryColor,
                      fontWeight: FontWeight.w600,
                    ),
                    weekendStyle: kRegularText.copyWith(
                      color: kPrimaryColor,
                      fontWeight: FontWeight.w600,
                    ),
                    outsideWeekendStyle: kRegularText.copyWith(
                      color: kPrimaryColor.withOpacity(.3),
                      fontWeight: FontWeight.w600,
                    ),
                    outsideStyle: kRegularText.copyWith(
                      color: kPrimaryColor.withOpacity(.3),
                      fontWeight: FontWeight.w600,
                    ),
                    markersColor: kPrimaryColor,
                  ),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Container(
                width: double.infinity,
                padding: EdgeInsets.symmetric(vertical: 20, horizontal: 10),
                decoration: BoxDecoration(
                  color: kWhiteColor,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(25),
                    topRight: Radius.circular(25),
                  ),
                  boxShadow: [
                    BoxShadow(
                      color: kOrdinaryColor2.withOpacity(0.5),
                      spreadRadius: 3,
                      blurRadius: 10,
                      offset: Offset(0, -10), // changes position of shadow
                    ),
                  ],
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 10),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Flexible(
                            flex: 1,
                            child: Text(
                              'Waktu',
                              style: kSmallText.copyWith(
                                color: kBlackColor.withOpacity(.4),
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                          ),
                          Flexible(
                            flex: 2,
                            fit: FlexFit.tight,
                            child: Text(
                              'Event',
                              style: kSmallText.copyWith(
                                color: kBlackColor.withOpacity(.4),
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                          ),
                          Flexible(
                            flex: 1,
                            child: Text(
                              'Actual',
                              style: kSmallText.copyWith(
                                color: kBlackColor.withOpacity(.4),
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                          ),
                          Flexible(
                            flex: 1,
                            child: Text(
                              'Cons',
                              style: kSmallText.copyWith(
                                color: kBlackColor.withOpacity(.4),
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                          ),
                          Flexible(
                            flex: 1,
                            child: Text(
                              'Prev',
                              style: kSmallText.copyWith(
                                color: kBlackColor.withOpacity(.4),
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),


                    Obx(()=>calendarC.calendarObj.value.data==null?CustomLoader():
                    calendarC.calendarObj.value.data.length==0
                        ?textRoboto("No Data Found", kPrimaryColor):Container(
                      height: 700,
                      child: ListView.builder(
                        //physics: NeverScrollableScrollPhysics(),
                        itemCount: calendarC.calendarObj.value.data.length,
                        itemBuilder: (_,index){
                          return Column(
                              children: [
                                Container(
                                  padding: EdgeInsets.all(8),
                                  decoration: BoxDecoration(
                                    color: kPrimaryColor,
                                    borderRadius: BorderRadius.circular(5),
                                  ),
                                  width: double.infinity,
                                  child: Text(
                                    '${DateFormat.yMMMEd('en_US').format(DateTime.parse(calendarC.calendarObj.value.data[index].date))   }',
                                    style: kRegularText2.copyWith(
                                      fontWeight: FontWeight.w600,
                                      color: Color(0xFFF9DD6A),
                                    ),
                                    textAlign: TextAlign.center,
                                  ),
                                ),

                                Container(
                                  height:400,
                                  child: ListView.builder(
                                    itemCount:5,
                                    physics:NeverScrollableScrollPhysics(),
                                    itemBuilder: (_,index){
                                      return GestureDetector(
                                        onTap: () =>
                                            Navigator.pushNamed(context, TradeBalance.routeName),
                                        child: EventRowWidget(
                                          time: '${calendarC.calendarObj.value.data[index].value[index].time}',
                                          eventTitle: '${calendarC.calendarObj.value.data[index].value[index].title}',
                                          actualAmount: '297M',
                                          consAmount: '297M',
                                          prevAmount: '297M',
                                          country: 'us_image.png',
                                          currency: '${calendarC.calendarObj.value.data[index].value[index].country}',
                                          status: '${calendarC.calendarObj.value.data[index].value[index].impact}',
                                        ),
                                      );
                                    },
                                  ),
                                ),

                                /*GestureDetector(
                                  onTap: () =>
                                      Navigator.pushNamed(context, TradeBalance.routeName),
                                  child: EventRowWidget(
                                    time: '${calendarC.calendarObj.value.data[index].value}',
                                    eventTitle: 'Trade Balance',
                                    actualAmount: '297M',
                                    consAmount: '297M',
                                    prevAmount: '297M',
                                    country: 'us_image.png',
                                    currency: 'USD',
                                    status: 'Low',
                                  ),
                                )*/
                              ],

                            );
                        },
                      ),
                    ),),


                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}


/*
GestureDetector(
onTap: () =>
Navigator.pushNamed(context, TradeBalance.routeName),
child: EventRowWidget(
time: '13:45',
eventTitle: 'Flash Manufactering PMI',
actualAmount: '297M',
consAmount: '297M',
prevAmount: '297M',
country: 'japan.png',
currency: 'JPY',
status: 'MEDIUM',
statusColor: kIconColor,
),
),
GestureDetector(
onTap: () =>
Navigator.pushNamed(context, TradeBalance.routeName),
child: EventRowWidget(
time: '11:59',
eventTitle: 'BOJ Core CPI y/y',
actualAmount: '1.0%',
consAmount: '0.0%',
prevAmount: '0.0%',
country: 'japan.png',
currency: 'JPY',
status: 'Medium',
statusColor: kIconColor,
),
),
SizedBox(
height: 20,
),
Container(
padding: EdgeInsets.all(8),
decoration: BoxDecoration(
color: kPrimaryColor,
borderRadius: BorderRadius.circular(5),
),
width: double.infinity,
child: Text(
'KAMIS, 11 JUNI 2021',
style: kRegularText2.copyWith(
fontWeight: FontWeight.w600,
color: Color(0xFFF9DD6A),
),
textAlign: TextAlign.center,
),
),
GestureDetector(
onTap: () =>
Navigator.pushNamed(context, TradeBalance.routeName),
child: EventRowWidget(
time: '13:45',
eventTitle: 'Trade Balance',
actualAmount: '297M',
consAmount: '297M',
prevAmount: '297M',
country: 'us_image.png',
currency: 'USD',
status: 'Low',
),
),
GestureDetector(
onTap: () =>
Navigator.pushNamed(context, TradeBalance.routeName),
child: EventRowWidget(
time: '13:45',
eventTitle: 'Flash Manufactering PMI',
actualAmount: '297M',
consAmount: '297M',
prevAmount: '297M',
country: 'japan.png',
currency: 'JPY',
status: 'Medium',
statusColor: kIconColor,
),
),
GestureDetector(
onTap: () =>
Navigator.pushNamed(context, TradeBalance.routeName),
child: EventRowWidget(
time: '11:59',
eventTitle: 'BOJ Core CPI y/y',
actualAmount: '1.0%',
consAmount: '0.0%',
prevAmount: '0.0%',
country: 'japan.png',
currency: 'JPY',
status: 'Medium',
statusColor: kIconColor,
),
),
GestureDetector(
onTap: () =>
Navigator.pushNamed(context, TradeBalance.routeName),
child: EventRowWidget(
time: '11:59',
eventTitle: 'Trad Balance',
actualAmount: '1.0%',
consAmount: '0.0%',
prevAmount: '0.0%',
country: 'us_image.png',
currency: 'USD',
status: 'LOW',
),
),
GestureDetector(
onTap: () =>
Navigator.pushNamed(context, TradeBalance.routeName),
child: EventRowWidget(
time: '11:59',
eventTitle: 'Flash Manufactering PMI',
actualAmount: '1.0%',
consAmount: '0.0%',
prevAmount: '0.0%',
country: 'japan.png',
currency: 'JPY',
status: 'Medium',
statusColor: kIconColor,
),
),*/
