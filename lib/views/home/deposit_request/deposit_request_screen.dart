import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:orbitrade/controller/changed_user_name_controller.dart';
import 'package:orbitrade/controller/user_data_controller.dart';
import 'package:orbitrade/sheared/custom_loader.dart';
import 'package:orbitrade/views/home/deposit_request/components/email_edit_screen.dart';
import 'package:orbitrade/views/home/deposit_request/components/name_edit_screen.dart';
import 'package:orbitrade/views/home/deposit_request/components/username_edit_screen.dart';
import 'package:orbitrade/views/home/deposit_request/components/whatsapp_edit_screen.dart';
import 'package:orbitrade/views/home/deposit_request/widgets/info_row_widget.dart';

import '../../../constants.dart';

class DepositRequestScreen extends StatefulWidget {
  static const routeName = 'deposit_request_screen';

  @override
  _DepositRequestScreenState createState() => _DepositRequestScreenState();
}

class _DepositRequestScreenState extends State<DepositRequestScreen> {
  bool isEditing = false;

  var userController = Get.put(UserDataController());
  var changedUserNameController = Get.put(ChangedUserNameController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kWhiteColor,
      appBar: AppBar(
        iconTheme: IconThemeData(color: kBlackColor),
        elevation: 1,
        backgroundColor: kWhiteColor,
        centerTitle: true,
        title: Text(
          'Data Pribadi',
          style: kRegularText2.copyWith(
            color: kBlackColor,
            fontWeight: FontWeight.w600,
          ),
        ),
        actions: [
          isEditing == false
              ? FlatButton(
                  onPressed: () {
                    setState(() {
                      isEditing = true;
                    });
                  },
                  child: Text(
                    'Simpan',
                    style: kDescriptionText.copyWith(
                      color: kPrimaryColor,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                )
              : FlatButton(
                  onPressed: () {},
                  child: Text(
                    'Ubah',
                    style: kDescriptionText.copyWith(
                      color: kPrimaryColor,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                ),
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(
          vertical: 10,
          horizontal: 20,
        ),
        child: isEditing == false
            ? Obx(() => Column(
                  children: [
                    InfoRowWidget(
                      title: 'Username',
                      titleValue: userController.userName.value ?? "",
                    ),
                    InfoRowWidget(
                      title: 'Nama',
                      titleValue: userController.name.value ?? '',
                    ),
                    InfoRowWidget(
                      title: 'Email',
                      titleValue: userController.userEmail.value ?? '',
                      isVerified: true,
                    ),
                    InfoRowWidget(
                      title: 'Whatsapp',
                      titleValue: userController.userWhatsApp.value ?? '',
                      isVerified: true,
                    ),
                    InfoRowWidget(
                      title: 'Tanggal Lahir',
                      titleValue: userController.userDOB.value ?? '',
                      isLast: true,
                    ),
                    SizedBox(
                      height: 30,
                    ),
                    Text(
                      'Pastikan nama kamu sama dengan nama yang tertera di rekening bank kamu.',
                      style: kRegularText2.copyWith(
                        fontWeight: FontWeight.w600,
                        color: kPrimaryColor,
                        height: 1.6,
                      ),
                    ),
                  ],
                ))
            : Obx(() {
                if (changedUserNameController.isLoading.value) {
                  return CustomLoader();
                } else {
                  return Column(
                    children: [
                      InfoRowWidget(
                        onPress: () {
                          if (changedUserNameController.count == 0) {
                            Navigator.push(context,
                                MaterialPageRoute(builder: (context) {
                              return UserNameEditScreen(
                                checkChange: true,
                                userName: userController.userName.value ?? '',
                              );
                            }));
                          } else if (changedUserNameController.count == 1) {
                            Navigator.push(context,
                                MaterialPageRoute(builder: (context) {
                              return UserNameEditScreen(
                                checkChange: false,
                                userName: userController.userName.value ?? '',
                              );
                            }));
                          } else {
                            Navigator.push(context,
                                MaterialPageRoute(builder: (context) {
                              return UserNameEditScreen(
                                checkChange: false,
                                userName: userController.userName.value ?? '',
                              );
                            }));
                          }

                          // Navigator.pushNamed(context, UserNameEditScreen.routeName);
                        },
                        title: 'Username',
                        titleValue: userController.userName.value ?? '',
                        isEditing: true,
                      ),
                      InfoRowWidget(
                        onPress: () {
                          Navigator.pushNamed(
                              context, NameEditScreen.routeName);
                        },
                        title: 'Nama',
                        titleValue: userController.name.value ?? '',
                        isEditing: true,
                      ),
                      InfoRowWidget(
                        onPress: () {
                          Navigator.pushNamed(
                              context, EmailEditScreen.routeName);
                        },
                        title: 'Email',
                        titleValue: userController.userEmail.value ?? '',
                        isEditing: true,
                      ),
                      InfoRowWidget(
                        onPress: () {
                          Navigator.pushNamed(
                              context, WhatsappEditScreen.routeName);
                        },
                        title: 'Whatsapp',
                        titleValue: userController.userWhatsApp.value ?? '',
                        isEditing: true,
                      ),
                      InfoRowWidget(
                        title: 'Tanggal Lahir',
                        titleValue: userController.userDOB.value ?? '',
                        isEditing: true,
                        isLast: true,
                      ),
                      SizedBox(
                        height: 30,
                      ),
                      Text(
                        'Pastikan nama kamu sama dengan nama yang tertera di rekening bank kamu.',
                        style: kRegularText2.copyWith(
                          fontWeight: FontWeight.w600,
                          color: kPrimaryColor,
                          height: 1.6,
                        ),
                      ),
                    ],
                  );
                }
              }),
      ),
    );
  }
}
