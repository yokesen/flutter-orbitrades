import 'package:flutter/material.dart';
import 'package:orbitrade/sheared/default_btn.dart';
import 'package:orbitrade/size_config.dart';
import 'package:orbitrade/views/home/deposit_request/widgets/info_row_widget.dart';

import '../../../../constants.dart';

class EmailEditScreen extends StatefulWidget {
  static const routeName = 'edit_email';

  @override
  _EmailEditScreenState createState() => _EmailEditScreenState();
}

class _EmailEditScreenState extends State<EmailEditScreen> {
  bool isVerified = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kWhiteColor,
      appBar: AppBar(
        iconTheme: IconThemeData(color: kBlackColor),
        elevation: 1,
        backgroundColor: kWhiteColor,
        centerTitle: true,
        title: Text(
          'Email',
          style: kRegularText2.copyWith(
            color: kBlackColor,
            fontWeight: FontWeight.w600,
          ),
        ),
        actions: [
          FlatButton(
            onPressed: () {},
            child: Text(
              'Selesai',
              style: kDescriptionText.copyWith(
                color: kPrimaryColor,
                fontWeight: FontWeight.w600,
              ),
            ),
          ),
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(
          vertical: 10,
          horizontal: 20,
        ),
        child: Column(
          children: [
            InfoRowWidget(
              title: 'Email',
              titleValue: 'jkmiller@gmail.com',
              isEditing: true,
            ),
            SizedBox(
              height: 30,
            ),
            isVerified == false
                ? DefaultBtn(
                    onPress: () {
                      setState(() {
                        isVerified = true;
                      });
                    },
                    title: 'Verifikasi Email',
                    width: SizeConfig.screenWidth,
                  )
                : DefaultBtn(
                    onPress: () {},
                    isChange: true,
                    btnColor: kSecondaryColor.withOpacity(.5),
                    title: 'Terverifikasi',
                    width: SizeConfig.screenWidth,
                  ),
          ],
        ),
      ),
    );
  }
}
