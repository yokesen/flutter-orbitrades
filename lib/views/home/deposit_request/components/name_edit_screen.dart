import 'package:flutter/material.dart';
import 'package:orbitrade/views/home/deposit_request/widgets/info_row_widget.dart';

import '../../../../constants.dart';

class NameEditScreen extends StatelessWidget {
  static const routeName = 'edit_name';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kWhiteColor,
      appBar: AppBar(
        iconTheme: IconThemeData(color: kBlackColor),
        elevation: 1,
        backgroundColor: kWhiteColor,
        centerTitle: true,
        title: Text(
          'Nama',
          style: kRegularText2.copyWith(
            color: kBlackColor,
            fontWeight: FontWeight.w600,
          ),
        ),
        actions: [
          FlatButton(
            onPressed: () {},
            child: Text(
              'Selesai',
              style: kDescriptionText.copyWith(
                color: kPrimaryColor,
                fontWeight: FontWeight.w600,
              ),
            ),
          ),
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(
          vertical: 10,
          horizontal: 20,
        ),
        child: Column(
          children: [
            InfoRowWidget(
              title: 'Nama',
              titleValue: 'Jacob Miller',
              isEditing: true,
            ),
            SizedBox(
              height: 30,
            ),
            Text(
              'Pastikan nama kamu sama dengan nama yang tertera di rekening bank kamu. Kamu hanya dapat mengganti username dua kali dalam waktu 14 hari.',
              style: kRegularText2.copyWith(
                fontWeight: FontWeight.w600,
                color: kPrimaryColor,
                height: 1.6,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
