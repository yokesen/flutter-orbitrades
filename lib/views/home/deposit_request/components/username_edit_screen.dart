import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:orbitrade/controller/user_data_controller.dart';
import 'package:orbitrade/services/network_services.dart';
import 'package:orbitrade/views/home/deposit_request/widgets/info_row_eidt_widget.dart';

import '../../../../constants.dart';

class UserNameEditScreen extends StatefulWidget {
  static const routeName = 'edit_username';
  UserNameEditScreen({Key key, this.userName, this.checkChange})
      : super(key: key);
  final String userName;
  final checkChange;
  @override
  _yourPageState createState() => _yourPageState();
}

class _yourPageState extends State<UserNameEditScreen> {
  TextEditingController _userNameController = TextEditingController();
  var userController = Get.put(UserDataController());
  @override
  void initState() {
    // TODO: implement initState

    setPreviewsUserName(_userNameController);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kWhiteColor,
      appBar: AppBar(
        iconTheme: IconThemeData(color: kBlackColor),
        elevation: 1,
        backgroundColor: kWhiteColor,
        centerTitle: true,
        title: Text(
          'Username',
          style: kRegularText2.copyWith(
            color: kBlackColor,
            fontWeight: FontWeight.w600,
          ),
        ),
        actions: [
          FlatButton(
            onPressed: () async {
              if (widget.checkChange) {
                //call method
                var p = await NetworkServices().changeUserName(
                  context: context,
                  userName: _userNameController.text,
                );
                Map<String, dynamic> js = p;
                if (p['success'] == false) {
                  Navigator.of(context).pop();
                  print(p['response']);
                  Get.snackbar('Error', p['response']);
                } else {
                  Navigator.of(context).pop();
                  Navigator.of(context).pop();
                  Get.snackbar('Success', 'Username change successfully!');
                  userController.fetchUserDAta();
                }
                print('ok');
              } else {
                Get.snackbar('Error',
                    'Kamu hanya dapat mengganti username dua kali dalam waktu 14 hari.');
              }
            },
            child: Text(
              'Selesai',
              style: kDescriptionText.copyWith(
                color: kPrimaryColor,
                fontWeight: FontWeight.w600,
              ),
            ),
          ),
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(
          vertical: 10,
          horizontal: 20,
        ),
        child: Column(
          children: [
            InfoEditRowWidget(
              fieldController: _userNameController,
              title: 'Username',
              hint: widget.userName.length > 0 ? ' ' : 'JacobMiller123',
            ),
            SizedBox(
              height: 30,
            ),
            Text(
              'Kamu hanya dapat mengganti username dua kali dalam waktu 14 hari.',
              style: kRegularText2.copyWith(
                fontWeight: FontWeight.w600,
                color: kPrimaryColor,
                height: 1.6,
              ),
            ),
          ],
        ),
      ),
    );
  }

  void setPreviewsUserName(TextEditingController userNameController) {
    if (widget.userName.length > 0) {
      userNameController.text = widget.userName;
    }
  }
}
