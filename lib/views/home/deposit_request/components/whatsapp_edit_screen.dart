import 'package:flutter/material.dart';
import 'package:orbitrade/views/home/deposit_request/widgets/info_row_widget.dart';

import '../../../../constants.dart';
import '../../../../size_config.dart';

class WhatsappEditScreen extends StatefulWidget {
  static const routeName = 'edit_whatsapp';

  @override
  _WhatsappEditScreenState createState() => _WhatsappEditScreenState();
}

class _WhatsappEditScreenState extends State<WhatsappEditScreen> {
  bool isVerified = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kWhiteColor,
      appBar: AppBar(
        iconTheme: IconThemeData(color: kBlackColor),
        elevation: 1,
        backgroundColor: kWhiteColor,
        centerTitle: true,
        title: Text(
          'Whatsapp',
          style: kRegularText2.copyWith(
            color: kBlackColor,
            fontWeight: FontWeight.w600,
          ),
        ),
        actions: [
          FlatButton(
            onPressed: () {},
            child: Text(
              'Selesai',
              style: kDescriptionText.copyWith(
                color: kPrimaryColor,
                fontWeight: FontWeight.w600,
              ),
            ),
          ),
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(
          vertical: 10,
          horizontal: 20,
        ),
        child: Column(
          children: [
            InfoRowWidget(
              title: 'Whatsapp',
              titleValue: '+6281324957134',
              isEditing: true,
            ),
            SizedBox(
              height: 30,
            ),
            Container(
              width: SizeConfig.screenWidth,
              padding: EdgeInsets.symmetric(
                vertical: 1,
              ),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(30),
                color: isVerified == false
                    ? kSuccessColor
                    : kSuccessColor.withOpacity(.5),
              ),
              child: FlatButton(
                onPressed: () {
                  setState(() {
                    isVerified = true;
                  });
                },
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image.asset(kImageDir + 'w_image.png'),
                    SizedBox(
                      width: 10,
                    ),
                    Text(
                      isVerified == false
                          ? 'Verifikasi Disini'
                          : 'Terverifikasi',
                      style: kRegularText2.copyWith(
                        fontWeight: FontWeight.w600,
                        color: kWhiteColor,
                      ),
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
