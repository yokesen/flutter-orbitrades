import 'package:flutter/material.dart';

import '../../../../constants.dart';

class InfoRowWidget extends StatelessWidget {
  final String title;
  final String titleValue;
  final bool isVerified;
  final bool isLast;
  final bool isEditing;
  final Function onPress;
  InfoRowWidget({
    this.onPress,
    this.title,
    this.titleValue,
    this.isLast = false,
    this.isVerified = false,
    this.isEditing = false,
  });
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onPress,
      child: Container(
        decoration: BoxDecoration(
          border: Border(
            bottom: BorderSide(
              width: 1,
              color: isLast == true ? kWhiteColor : kOrdinaryColor2,
            ),
          ),
        ),
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 15),
          child: Row(
            children: [
              Text(
                title,
                style: kRegularText2.copyWith(
                  fontWeight: FontWeight.w600,
                ),
              ),
              Spacer(),
              Text(
                titleValue,
                style: kRegularText2.copyWith(
                  fontWeight: FontWeight.w600,
                  color: isEditing == true
                      ? kBlackColor.withOpacity(.4)
                      : kBlackColor,
                ),
              ),
              isVerified == false
                  ? SizedBox()
                  : SizedBox(
                      width: 5,
                    ),
              isVerified == false
                  ? SizedBox()
                  : Icon(
                      Icons.verified,
                      color: kSuccessColor,
                    ),
            ],
          ),
        ),
      ),
    );
  }
}
