import 'package:flutter/material.dart';

import '../../../../constants.dart';

class InfoEditRowWidget extends StatelessWidget {
  final String title;
  final String hint;
  final TextEditingController fieldController;
  final editEnabled;
  InfoEditRowWidget({
    this.title,
    this.hint,
    this.fieldController,
    this.editEnabled = true,
  });
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(
            width: 1,
            color: kOrdinaryColor2,
          ),
        ),
      ),
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 5),
        child: Row(
          children: [
            Expanded(
              flex: 1,
              child: Text(
                title,
                style: kBoldStyle.copyWith(
                    color: kBlackColor, fontWeight: FontWeight.w600),
              ),
            ),
            Expanded(
                child: TextFormField(
              controller: fieldController,
              cursorColor: Colors.black,
              obscureText: false,
              enabled: editEnabled,
              textAlign: TextAlign.right,
              keyboardType: TextInputType.text,
              decoration: InputDecoration(
                  border: InputBorder.none,
                  focusedBorder: InputBorder.none,
                  enabledBorder: InputBorder.none,
                  errorBorder: InputBorder.none,
                  disabledBorder: InputBorder.none,
                  hintText: hint),
            )),
          ],
        ),
      ),
    );
  }
}
