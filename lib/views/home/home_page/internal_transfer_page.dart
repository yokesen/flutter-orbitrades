import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:orbitrade/controller/internal_transfer_data_controller.dart';
import 'package:orbitrade/services/network_services.dart';
import 'package:orbitrade/sheared/buildDepositAppBar.dart';
import 'package:orbitrade/sheared/input_form_widget.dart';
import 'package:orbitrade/views/home/home_page/request_berhasil_page.dart';

import '../../../constants.dart';
import '../../../size_config.dart';

class InternalHomeTransferPage extends StatefulWidget {
  static const routeName = 'internal_home_transfer_page';
  @override
  _yourPageState createState() => _yourPageState();
}

class _yourPageState extends State<InternalHomeTransferPage> {
  var _internalDataCon = Get.put(InternalTransDataController());
  TextEditingController _amountController = TextEditingController();
  FocusNode amountFocusNode;
  @override
  void initState() {
    // TODO: implement initState
    amountFocusNode = FocusNode();
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    amountFocusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildDepositAppBar(
          context: context,
          title: 'Internal Transfer',
          onPress: () async {
            if (_amountController.text.isEmpty) {
              Get.snackbar('Null', 'Please enter amount');
            } else {
              var p = await NetworkServices().requestInternalTransfer(
                context: context,
                fromAccount: '0454',
                amount: _amountController.text ?? " ",
                toAccount: '04574',
              );
              Map<String, dynamic> js = p;
              if (p['success'] == true) {
                Navigator.of(context).pop();
                //fetch new my internal transfer data
                _internalDataCon.fetchInternalTransferData();
                Navigator.pushNamed(context, RequestBerhasilPage.routeName);
                Get.snackbar('Success',
                    "Internal Transfer request create successfully!");
              } else {
                Navigator.of(context).pop();
                print(p['message']);
                Get.snackbar('Error', p['message']);
              }
            }
          }),
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(
            horizontal: getProportionateScreenWidth(20),
          ),
          child: Column(
            children: [
              size20,
              Container(
                alignment: Alignment.centerLeft,
                child: Text(
                  'Dari Account Trading',
                  style: kRegularText2.copyWith(
                    color: kBlackColor,
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ),
              SizedBox(
                height: 5,
              ),
              Container(
                width: SizeConfig.screenWidth,
                alignment: Alignment.center,
                padding: EdgeInsets.all(16),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    border: Border.all(
                      width: 1,
                      color: Colors.grey,
                    )),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      '5001236788',
                      style: kRegularText2.copyWith(
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                    Icon(Icons.keyboard_arrow_down_outlined),
                  ],
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                alignment: Alignment.centerLeft,
                child: Text(
                  'Nilai Transfer',
                  style: kRegularText2.copyWith(
                    color: kBlackColor,
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    width: 65,
                    alignment: Alignment.center,
                    padding: EdgeInsets.all(16),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(5),
                        border: Border.all(
                          width: 1,
                          color: amountFocusNode.hasFocus
                              ? kPrimaryColor
                              : Colors.grey,
                        )),
                    child: Text(
                      kUSD,
                      style: kRegularText2.copyWith(
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Container(
                    width: SizeConfig.screenWidth - 120,
                    child: FocusScope(
                      child: Focus(
                        focusNode: amountFocusNode,
                        onFocusChange: (node) {
                          setState(() {});
                        },
                        child: InputFormWidget(
                          keyType: TextInputType.phone,
                          fieldController: _amountController,
                          validation: (value) {
                            if (value.isEmpty) {
                              return kInvalidNumberError;
                            }else if(value.length < 1){
                              return 'Minimum internal transfer limit 1 USD';
                            }
                            return null;
                          },
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              Container(
                alignment: Alignment.centerLeft,
                child: Text(
                  'Untuk Account Trading',
                  style: kRegularText2.copyWith(
                    color: kBlackColor,
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ),
              SizedBox(
                height: 5,
              ),
              Container(
                width: SizeConfig.screenWidth,
                alignment: Alignment.center,
                padding: EdgeInsets.all(16),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    border: Border.all(
                      width: 1,
                      color: Colors.grey,
                    )),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      '9912401515',
                      style: kRegularText2.copyWith(
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                    Icon(Icons.keyboard_arrow_down_outlined),
                  ],
                ),
              ),
              SizedBox(
                height: 10,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
