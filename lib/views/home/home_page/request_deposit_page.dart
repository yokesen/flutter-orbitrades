import 'dart:io';

import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:orbitrade/controller/bank_setting_controller.dart';
import 'package:orbitrade/controller/custody_bank_list_controller.dart';
import 'package:orbitrade/controller/mt4_account_data_controller.dart';
import 'package:orbitrade/controller/my_deposit_data_controller.dart';
import 'package:orbitrade/controller/user_data_controller.dart';
import 'package:orbitrade/model/custody-bank-list.dart';
import 'package:orbitrade/model/mt4_request_data.dart';
import 'package:orbitrade/model/mt4_tip_deposit.dart';
import 'package:orbitrade/model/myAccountModel.dart';
import 'package:orbitrade/services/network_services.dart';
import 'package:orbitrade/sheared/buildDepositAppBar.dart';
import 'package:orbitrade/sheared/custom_loader.dart';
import 'package:orbitrade/sheared/input_form_widget.dart';
import 'package:orbitrade/views/home/home_page/request_berhasil_page.dart';

import '../../../constants.dart';
import '../../../size_config.dart';

class RequestDepositPage extends StatefulWidget {
  static const routeName = 'request_deposit_page';
  @override
  _yourPageState createState() => _yourPageState();
}

class _yourPageState extends State<RequestDepositPage> {
  final userDataController = Get.put(UserDataController());
  CusodyBankListController custodyBankController = Get.find();
  final mt4AccountController = Get.put(Mt4AccountDataController());
  var bankC = Get.put(BankSettingController());
  var _depositDataCon = Get.put(MyDepositDataController());
  List<String> bankList = [
    'Deposit',
    'Internal Transfer',
    'Withdrawal',
  ];
  String _selected_mt4_account;
  String selectedAccount;
  String selectedTipe;
  var checkTipe;
  int selectedBank = 0;
  Datum selectedBankInfo;
  TextEditingController _amountController = TextEditingController();
  FocusNode amountFocusNode;
  List<Mt4RequestData> m4RequestList = List<Mt4RequestData>();
  Mt4RequestData m4Requestdata;
  File profileImgFile;
  String imagePath;
  bool valFixedRate = false;
  bool valFloatingRate = false;
  void _setMt4List(List<ResponseData> response) async {
    m4RequestList.clear();
    print('' + response.length.toString());
    for (int i = 0; i < response.length; i++) {
      m4Requestdata = Mt4RequestData();
      if (response[i].mt4Id != null) {
        m4Requestdata.id = response[i].id;
        m4Requestdata.uuid = response[i].uuid;
        m4Requestdata.typeAccount = response[i].typeAccount;
        m4Requestdata.mt4Id = response[i].mt4Id;
        m4Requestdata.tipeDeposit = response[i].tipeDeposit;
        m4Requestdata.status = response[i].status;
        m4Requestdata.createdAt = response[i].createdAt;
        m4Requestdata.updatedAt = response[i].updatedAt;
        m4RequestList.add(m4Requestdata);
        //setState(() {});
      }
    }
  }

  var servicesChecked = List<int>();
  var servicesNameChecked = List<String>();
  String servicesNameString = "";
  var servicesStatus = List<bool>();
  bool lastNameFocuce = false;

  Future<List<TipeDeposit>> _getTipeDeposit(
      List<Mt4RequestData> typeDepositList) async {
    List<TipeDeposit> typeList = [];
    for (var u in typeDepositList) {
      TipeDeposit heath = TipeDeposit(u.id, u.mt4Id, u.tipeDeposit);

      typeList.add(heath);
      servicesStatus.add(false);
    }
    return typeList;
  }

  Future<void> getProfileImage(
      ImageSource imageSource, BuildContext context) async {
    final pickedFile = await ImagePicker().getImage(source: imageSource);

    if (pickedFile != null) {
      setState(() {
        imagePath = pickedFile.path;
        profileImgFile = File(pickedFile.path);
      });
      print("Path ${pickedFile.path}");
    } else {
      print("No Image Selected");
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    amountFocusNode = FocusNode();
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    amountFocusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFFFFFFF),
      appBar: buildDepositAppBar(
          context: context,
          title: 'Request Deposit',
          onPress: () async {
            if (_selected_mt4_account == null) {
              Get.snackbar('Pilih Account Trading', '');
            } else if (selectedTipe == null) {
              Get.snackbar('Silakan periksa jenis setoran', '');
            } else if (_amountController.text.isEmpty) {
              Get.snackbar('IDR amount empty', '');
            } else if (profileImgFile == null) {
              Get.snackbar('Bukti transfer image empty', '');
            } else {
              //call method
              var p =
                  await NetworkServices().uploadImage(context, profileImgFile);
              Map<String, dynamic> js = p;
              if (p['success'] == true) {
                String photo = p['response'];
                var pr = await NetworkServices().requestDeposit(
                  context: context,
                  amount: _amountController.text ?? " ",
                  tipeDeposit: selectedTipe,
                  photo: photo,
                  bank_name: bankC.myBankData.data.bankName,
                  account_number: bankC.myBankData.data.accountNumber,
                  account_name: bankC.myBankData.data.accountNumber,
                  namaBank: selectedBankInfo.namaBank,
                  namaRekening: selectedBankInfo.namaRekening,
                  nomorRekening: selectedBankInfo.nomorRekening,
                  accountmt4: _selected_mt4_account,
                );
                Map<String, dynamic> js = pr;
                if (pr['success'] == true) {
                  Navigator.of(context).pop();
                  //fetch new my deposit data
                  _depositDataCon.fetchMyDepositData();
                  Navigator.pushNamed(context, RequestBerhasilPage.routeName);
                  Get.snackbar(
                      'Success', "Request deposit create successfully!");
                } else {
                  Navigator.of(context).pop();
                  print(pr['message']);
                  Get.snackbar('Error', pr['message']);
                }
              } else {
                Navigator.of(context).pop();
                print(p['message']);
                Get.snackbar('Error', p['message']);
              }
              print('ok');
            }
          }),
      body: Obx(() {
        if (custodyBankController.isLoading.value &&
            mt4AccountController.isLoading.value) {
          return CustomLoader();
        } else {
          _setMt4List(mt4AccountController.myAccount.response);
          if (bankC.isLoading.value) {
            return CustomLoader();
          } else {
            return SingleChildScrollView(
              child: Padding(
                padding: EdgeInsets.symmetric(
                  horizontal: getProportionateScreenWidth(20),
                ),
                child: Column(
                  children: [
                    size15,
                    Container(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        'Pilih Rekening',
                        style: kRegularText2.copyWith(
                          color: kBlackColor,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Container(
                      width: SizeConfig.screenWidth,
                      alignment: Alignment.center,
                      padding: EdgeInsets.all(16),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5),
                          border: Border.all(
                            width: 1,
                            color: Colors.grey,
                          )),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            bankC.myBankData != null
                                ? '${bankC.myBankData.data.accountNumber ?? " "} - ${bankC.myBankData.data.accountName ?? ""}'
                                : " ",
                            style: kRegularText2.copyWith(
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                          // Icon(Icons.keyboard_arrow_down_outlined),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Container(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        'Pilih Account Trading',
                        style: kRegularText2.copyWith(
                          color: kBlackColor,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Container(
                      alignment: Alignment.centerLeft,
                      padding: EdgeInsets.all(16),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5),
                          border: Border.all(
                            width: 1,
                            color: Colors.grey,
                          )),
                      margin: EdgeInsets.only(
                        top: 2,
                      ),
                      width: MediaQuery.of(context).size.width,
                      child: DropdownButtonHideUnderline(
                        child: DropdownButton(
                          icon: Icon(
                            Icons.keyboard_arrow_down_outlined,
                            size: 20.09,
                          ),
                          hint: Text("Pilih Bank"),
                          value: selectedAccount,
                          isDense: true,
                          onChanged: (String newValue) {
                            setState(() {
                              selectedAccount = newValue;
                              for (int i = 0; i < m4RequestList.length; i++) {
                                if (m4RequestList[i].id.toString() !=
                                    selectedAccount) {
                                  checkTipe = m4RequestList[i].tipeDeposit;
                                  _selected_mt4_account =
                                      m4RequestList[i].mt4Id.toString();
                                  print(checkTipe);
                                  print(_selected_mt4_account);
                                }
                              }
                            });
                          },
                          items: m4RequestList.map((Mt4RequestData map) {
                            return DropdownMenuItem<String>(
                                value: map.id.toString(),
                                child: Container(
                                  margin: EdgeInsets.only(left: 10),
                                  child: SizedBox(
                                    width: SizeConfig.screenWidth -
                                        110, // for example
                                    child: Text(map.mt4Id.toString(),
                                        textAlign: TextAlign.left),
                                  ),
                                ));
                          }).toList(),
                        ),
                      ),
                    ),
                    Container(
                        alignment: Alignment.centerLeft,
                        child: checkTipe == 0
                            ? Column(
                                children: [
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Container(
                                    alignment: Alignment.centerLeft,
                                    child: Text(
                                      'Tipe Deposit',
                                      style: kRegularText2.copyWith(
                                        color: kBlackColor,
                                        fontWeight: FontWeight.w600,
                                      ),
                                    ),
                                  ),
                                  Row(
                                    children: [
                                      Row(
                                        children: <Widget>[
                                          Checkbox(
                                            value: valFixedRate,
                                            onChanged: (bool value) {
                                              setState(() {
                                                valFixedRate = value;
                                                selectedTipe = '11';
                                                valFloatingRate = false;
                                              });
                                            },
                                          ),
                                          Text("Fixed Rate"),
                                        ],
                                      ),
                                      Row(
                                        children: <Widget>[
                                          Checkbox(
                                            value: valFloatingRate,
                                            onChanged: (bool value) {
                                              setState(() {
                                                valFloatingRate = value;
                                                selectedTipe = '11';
                                                valFixedRate = false;
                                              });
                                            },
                                          ),
                                          Text("Floating Rate"),
                                        ],
                                      ),
                                    ],
                                  ),
                                ],
                              )
                            : checkTipe == 10
                                ? Column(
                                    children: [
                                      SizedBox(
                                        height: 10,
                                      ),
                                      Container(
                                        alignment: Alignment.centerLeft,
                                        child: Text(
                                          'Tipe Deposit',
                                          style: kRegularText2.copyWith(
                                            color: kBlackColor,
                                            fontWeight: FontWeight.w600,
                                          ),
                                        ),
                                      ),
                                      Row(
                                        children: <Widget>[
                                          Checkbox(
                                            value: valFixedRate,
                                            onChanged: (bool value) {
                                              setState(() {
                                                valFixedRate = value;
                                                selectedTipe = '11';
                                                valFloatingRate = false;
                                              });
                                            },
                                          ),
                                          Text("Fixed Rate"),
                                        ],
                                      ),
                                    ],
                                  )
                                : checkTipe == 11
                                    ? Column(
                                        children: [
                                          SizedBox(
                                            height: 10,
                                          ),
                                          Container(
                                            alignment: Alignment.centerLeft,
                                            child: Text(
                                              'Tipe Deposit',
                                              style: kRegularText2.copyWith(
                                                color: kBlackColor,
                                                fontWeight: FontWeight.w600,
                                              ),
                                            ),
                                          ),
                                          Row(
                                            children: <Widget>[
                                              Checkbox(
                                                value: valFloatingRate,
                                                onChanged: (bool value) {
                                                  setState(() {
                                                    valFloatingRate = value;
                                                    selectedTipe = '11';
                                                    valFixedRate = false;
                                                  });
                                                },
                                              ),
                                              Text("Floating Rate"),
                                            ],
                                          ),
                                        ],
                                      )
                                    : SizedBox()),
                    /*        Container(
                    alignment: Alignment.centerLeft,
                    height: 50.0,
                    child: FutureBuilder(
                      future: _getTipeDeposit(m4RequestList),
                      builder: (BuildContext context, AsyncSnapshot snapshot) {
                        print(snapshot.data);
                        if (snapshot.data == null) {
                          return Container(
                              child: Center(child: Text("Loading...")));
                        } else {
                          return Container(
                            height: 50.0,
                            alignment: Alignment.centerLeft,
                            child: ListView.builder(
                              scrollDirection: Axis.horizontal,
                              shrinkWrap: true,
                              physics: ScrollPhysics(),
                              itemCount: snapshot.data.length,
                              itemBuilder: (BuildContext context, int index) {
                                return Row(children: [
                                  Checkbox(
                                      value: servicesStatus[index],
                                      onChanged: (bool val) {
                                        setState(() {
                                          servicesStatus[index] =
                                              !servicesStatus[index];
                                          if (servicesStatus[0] ==
                                              servicesStatus[index]) {
                                            servicesStatus.remove(1);
                                            print('0');
                                            servicesStatus.clear();
                                          } else if (servicesStatus[1] ==
                                              servicesStatus[index]) {
                                            servicesStatus.remove(0);
                                            print('2');
                                          }
                                        });
                                      }),
                                  Text(snapshot.data[index].mt4Id)
                                ]);
                              },
                            ),
                          );
                        }
                      },
                    ),
                  ),*/
                    size5,
                    SizedBox(
                      height: 10,
                    ),
                    Container(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        'Pilih Account Trading',
                        style: kRegularText2.copyWith(
                          color: kBlackColor,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          width: 65,
                          alignment: Alignment.center,
                          padding: EdgeInsets.all(16),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(5),
                              border: Border.all(
                                width: 1,
                                color: amountFocusNode.hasFocus
                                    ? kPrimaryColor
                                    : Colors.grey,
                              )),
                          child: Text(
                            'IDR',
                            style: kRegularText2.copyWith(
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Container(
                          width: SizeConfig.screenWidth - 120,
                          child: FocusScope(
                            child: Focus(
                              focusNode: amountFocusNode,
                              onFocusChange: (node) {
                                setState(() {});
                              },
                              child: InputFormWidget(
                                keyType: TextInputType.number,
                                fieldController: _amountController,
                                validation: (value) {
                                  if (value.isEmpty) {
                                    return kInvalidNumberError;
                                  }
                                  return null;
                                },
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    size5,
                    Container(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        'Pilihan Bank Tujuan Transfer',
                        style: kRegularText2.copyWith(
                          color: kBlackColor,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),
                    Container(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        'Pilih salah satu untuk menampilkan detil keterangan transfer',
                        style: TextStyle(color: Colors.grey),
                      ),
                    ),
                    size10,
                    Container(
                        child: custodyBankController.dataResponse != null
                            ? custodyBankController.dataResponse.data.length > 0
                                ? Container(
                                    height:
                                        MediaQuery.of(context).size.height / 5 -
                                            10,
                                    child: GridView.builder(
                                      gridDelegate:
                                          SliverGridDelegateWithMaxCrossAxisExtent(
                                              maxCrossAxisExtent: 200,
                                              childAspectRatio: 3 / 1,
                                              crossAxisSpacing: 12,
                                              mainAxisSpacing: 12),
                                      shrinkWrap: true,
                                      physics: NeverScrollableScrollPhysics(),
                                      itemCount: custodyBankController
                                          .dataResponse.data.length,
                                      itemBuilder: (_, index) {
                                        var bankObject =
                                            custodyBankController.dataResponse;
                                        if (selectedBank == 0) {
                                          selectedBankInfo = bankObject.data[0];
                                          print('select1');
                                        }
                                        return GestureDetector(
                                          onTap: () {
                                            print("$index");
                                            setState(() {
                                              selectedBank = index;
                                              selectedBankInfo =
                                                  bankObject.data[index];
                                            });
                                          },
                                          child: Container(
                                            height: 50,
                                            decoration: BoxDecoration(
                                                color: kWhiteColor,
                                                borderRadius:
                                                    BorderRadius.circular(5.0),
                                                border: Border.all(
                                                    color: selectedBank == index
                                                        ? kPrimaryColor
                                                        : kOrdinaryColor),
                                                boxShadow: [
                                                  BoxShadow(
                                                    color: kOrdinaryColor
                                                        .withOpacity(0.1),
                                                    spreadRadius: 5,
                                                    blurRadius: 7,
                                                    offset: Offset(0,
                                                        3), // changes position of shadow
                                                  ),
                                                ]),
                                            child: Center(
                                              child: textRoboto(
                                                  "${bankObject.data[index].namaBank}",
                                                  kBlackColor),
                                            ),
                                          ),
                                        );
                                      },
                                    ),
                                  )
                                : SizedBox()
                            : SizedBox()),
                    Container(
                      child: bankC.myBankData != null
                          ? Container(
                              width: SizeConfig.screenWidth,
                              padding: EdgeInsets.all(5.0),
                              decoration: BoxDecoration(
                                color: kPrimaryColor,
                                borderRadius: new BorderRadius.all(
                                  const Radius.circular(10.0),
                                ),
                              ),
                              child: Column(
                                children: [
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Container(
                                        alignment: Alignment.centerLeft,
                                        margin:
                                            EdgeInsets.only(left: 10, top: 7),
                                        child: Text(
                                          'Detail Keterangan Transfer',
                                          style: kRegularText.copyWith(
                                              color: Colors.white),
                                        ),
                                      ),
                                    ],
                                  ),
                                  SizedBox(
                                    height: 15,
                                  ),
                                  Container(
                                    alignment: Alignment.bottomCenter,
                                    child: Column(
                                      children: [
                                        Container(
                                          alignment: Alignment.centerLeft,
                                          margin: EdgeInsets.only(left: 10),
                                          child: Text(
                                            "Metode:",
                                            style: TextStyle(
                                                color: Colors.grey[400],
                                                fontSize: 11),
                                          ),
                                        ),
                                        SizedBox(
                                          height: 2,
                                        ),
                                        Container(
                                          alignment: Alignment.centerLeft,
                                          margin: EdgeInsets.only(left: 10),
                                          child: Text(
                                            'Transfer Antar Bank',
                                            style: kRegularText.copyWith(
                                                fontSize: 16,
                                                color: Colors.white),
                                          ),
                                        ),
                                        SizedBox(
                                          height: 10,
                                        ),
                                        Container(
                                          alignment: Alignment.centerLeft,
                                          margin: EdgeInsets.only(left: 10),
                                          child: Text(
                                            "Bank:",
                                            style: TextStyle(
                                                color: Colors.grey[400],
                                                fontSize: 11),
                                          ),
                                        ),
                                        SizedBox(
                                          height: 2,
                                        ),
                                        Container(
                                          alignment: Alignment.centerLeft,
                                          margin: EdgeInsets.only(left: 10),
                                          child: Text(
                                            bankC.myBankData.data.bankName ??
                                                " ",
                                            style: kRegularText.copyWith(
                                                fontSize: 16,
                                                color: Colors.white),
                                          ),
                                        ),
                                        SizedBox(
                                          height: 10,
                                        ),
                                        Container(
                                          alignment: Alignment.centerLeft,
                                          margin: EdgeInsets.only(left: 10),
                                          child: Text(
                                            "Nomor Rekening:",
                                            style: TextStyle(
                                                color: Colors.grey[400],
                                                fontSize: 11),
                                          ),
                                        ),
                                        SizedBox(
                                          height: 2,
                                        ),
                                        Container(
                                          alignment: Alignment.centerLeft,
                                          margin: EdgeInsets.only(left: 10),
                                          child: Text(
                                            bankC.myBankData.data
                                                    .accountNumber ??
                                                " ",
                                            style: kRegularText.copyWith(
                                                fontSize: 16,
                                                color: Colors.white),
                                          ),
                                        ),
                                        SizedBox(
                                          height: 10,
                                        ),
                                        Container(
                                          alignment: Alignment.centerLeft,
                                          margin: EdgeInsets.only(left: 10),
                                          child: Text(
                                            "Nama Rekening:",
                                            style: TextStyle(
                                                color: Colors.grey[400],
                                                fontSize: 11),
                                          ),
                                        ),
                                        SizedBox(
                                          height: 2,
                                        ),
                                        Container(
                                          alignment: Alignment.centerLeft,
                                          margin: EdgeInsets.only(left: 10),
                                          child: Text(
                                            bankC.myBankData.data.accountName ??
                                                " ",
                                            style: kRegularText.copyWith(
                                                fontSize: 16,
                                                color: Colors.white),
                                          ),
                                        ),
                                        SizedBox(
                                          height: 10,
                                        ),
                                        Container(
                                          alignment: Alignment.centerLeft,
                                          margin: EdgeInsets.only(left: 10),
                                          child: Text(
                                            "Batas Kadaluarsa::",
                                            style: TextStyle(
                                                color: Colors.grey[400],
                                                fontSize: 11),
                                          ),
                                        ),
                                        SizedBox(
                                          height: 2,
                                        ),
                                        Container(
                                          alignment: Alignment.centerLeft,
                                          margin: EdgeInsets.only(left: 10),
                                          child: Text(
                                            bankC.myBankData.data.updatedAt ??
                                                '01-08-2021 12:00 WIB',
                                            style: kRegularText.copyWith(
                                                fontSize: 16,
                                                color: Colors.white),
                                          ),
                                        ),
                                        SizedBox(
                                          height: 5,
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            )
                          : SizedBox(),
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    Container(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        'Upload Bukti Transfer',
                        style: kRegularText2.copyWith(
                          color: kBlackColor,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    GestureDetector(
                      onTap: () {
                        showModalSheet(context, cameraCallBack: () {
                          getProfileImage(ImageSource.camera, context);
                          Navigator.of(context).pop();
                        }, galleryCallBack: () {
                          getProfileImage(ImageSource.gallery, context);
                          Navigator.of(context).pop();
                        });
                      },
                      child: DottedBorder(
                        color: kOrdinaryColor2,
                        radius: Radius.circular(5),
                        borderType: BorderType.RRect,
                        child: Container(
                          alignment: Alignment.center,
                          child: Padding(
                            padding: const EdgeInsets.symmetric(vertical: 20),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                SvgPicture.asset(
                                  kImageDir + 'upload.svg',
                                  height: getProportionateScreenWidth(40),
                                  width: getProportionateScreenWidth(40),
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                Text(
                                  profileImgFile != null
                                      ? profileImgFile.path
                                      : 'Klik disini untuk upload',
                                  textAlign: TextAlign.center,
                                  style: kDescriptionText,
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 15,
                    ),
                  ],
                ),
              ),
            );
          }
        }
      }),
    );
  }

  Future showModalSheet(BuildContext context,
      {Function cameraCallBack, Function galleryCallBack}) {
    return Get.bottomSheet(
      Container(
        height: 180,
        padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
        decoration: BoxDecoration(
            color: kWhiteColor,
            borderRadius: BorderRadius.only(
                topRight: Radius.circular(15.0),
                topLeft: Radius.circular(15.0))),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            textRoboto("Upload Bukti Transfer", kBlackColor,
                fontSize: 20.0, fontWeight: FontWeight.w500),
            SizedBox(
              height: 20.0,
            ),
            GestureDetector(
              onTap: cameraCallBack,
              child: Row(
                children: [
                  Container(
                    height: 35,
                    width: 35,
                    decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: kOrdinaryColor.withOpacity(0.2)),
                    child: Center(
                      child: Icon(
                        Icons.camera,
                        color: kBlackColor,
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 10.0,
                  ),
                  textRoboto("Dari kamera", kBlackColor,
                      fontSize: 18.0, fontWeight: FontWeight.w500),
                ],
              ),
            ),
            SizedBox(
              height: 10.0,
            ),
            GestureDetector(
              onTap: galleryCallBack,
              child: Row(
                children: [
                  Container(
                    height: 35,
                    width: 35,
                    decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: kOrdinaryColor.withOpacity(0.2)),
                    child: Center(
                      child: Icon(
                        Icons.perm_media,
                        color: kBlackColor,
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 10.0,
                  ),
                  textRoboto("Dari galeri", kBlackColor,
                      fontSize: 18.0, fontWeight: FontWeight.w500),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}

/*              Container(
                alignment: Alignment.centerLeft,
                child: Text(
                  'Pilihan Bank Tujuan Transfer',
                  style: kRegularText2.copyWith(
                    color: kBlackColor,
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ),
              Container(
                alignment: Alignment.centerLeft,
                child: Text(
                  'Pilih salah satu untuk menampilkan detil keterangan transfer',
                  style: TextStyle(color: Colors.grey),
                ),
              ),
              SizedBox(
                height: 5,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  GestureDetector(
                    onTap: () {
                      setState(() {
                        bcaBankSelect = true;
                        briBankSelect = false;
                        bniBankSelect = false;
                        mandiriBankSelect = false;
                      });
                    },
                    child: Container(
                      width: SizeConfig.screenWidth / 2 - 25,
                      decoration: BoxDecoration(
                        border: Border.all(
                            color: bcaBankSelect
                                ? kPrimaryColor.withOpacity(.5)
                                : Colors.grey.withOpacity(.5),
                            width: 2),
                        borderRadius: new BorderRadius.all(
                          const Radius.circular(5.0),
                        ),
                      ),
                      child: Image.asset(
                        kImageDir + 'bca_bank.png',
                        height: 50,
                        width: 100,
                      ),
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      setState(() {
                        bcaBankSelect = false;
                        briBankSelect = true;
                        bniBankSelect = false;
                        mandiriBankSelect = false;
                      });
                    },
                    child: Container(
                      width: SizeConfig.screenWidth / 2 - 25,
                      decoration: BoxDecoration(
                        border: Border.all(
                            color: briBankSelect
                                ? kPrimaryColor.withOpacity(.5)
                                : Colors.grey.withOpacity(.5),
                            width: 2),
                        borderRadius: new BorderRadius.all(
                          const Radius.circular(5.0),
                        ),
                      ),
                      child: Image.asset(
                        kImageDir + 'bri_bank.png',
                        height: 50,
                        width: 100,
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 8,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  GestureDetector(
                    onTap: () {
                      setState(() {
                        bcaBankSelect = false;
                        briBankSelect = false;
                        bniBankSelect = true;
                        mandiriBankSelect = false;
                      });
                    },
                    child: Container(
                      width: SizeConfig.screenWidth / 2 - 25,
                      decoration: BoxDecoration(
                        border: Border.all(
                            color: bniBankSelect
                                ? kPrimaryColor.withOpacity(.5)
                                : Colors.grey.withOpacity(.5),
                            width: 2),
                        borderRadius: new BorderRadius.all(
                          const Radius.circular(5.0),
                        ),
                      ),
                      child: Image.asset(
                        kImageDir + 'bni_bank.png',
                        height: 50,
                        width: 100,
                      ),
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      setState(() {
                        bcaBankSelect = false;
                        briBankSelect = false;
                        bniBankSelect = false;
                        mandiriBankSelect = true;
                      });
                    },
                    child: Container(
                      width: SizeConfig.screenWidth / 2 - 25,
                      decoration: BoxDecoration(
                        border: Border.all(
                            color: mandiriBankSelect
                                ? kPrimaryColor.withOpacity(.5)
                                : Colors.grey.withOpacity(.5),
                            width: 2),
                        borderRadius: new BorderRadius.all(
                          const Radius.circular(5.0),
                        ),
                      ),
                      child: Image.asset(
                        kImageDir + 'mandiri_bank.png',
                        height: 50,
                        width: 100,
                      ),
                    ),
                  ),
                ],
              ),*/
