import 'dart:ui';

import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:orbitrade/controller/bank_setting_controller.dart';
import 'package:orbitrade/controller/custody_bank_list_controller.dart';
import 'package:orbitrade/controller/internal_transfer_data_controller.dart';
import 'package:orbitrade/controller/mt4_account_data_controller.dart';
import 'package:orbitrade/controller/my_deposit_data_controller.dart';
import 'package:orbitrade/controller/withdrawal_data_controller.dart';
import 'package:orbitrade/model/depositModelClass.dart';
import 'package:orbitrade/sheared/custom_loader.dart';
import 'package:orbitrade/sheared/default_btn.dart';
import 'package:orbitrade/views/home/home_page/internal_transfer_page.dart';
import 'package:orbitrade/views/home/home_page/request_deposit_page.dart';
import 'package:orbitrade/views/home/home_page/request_withdrawal_page.dart';
import 'package:orbitrade/views/home/profile_page/bankSetting/bank_setting_page.dart';
import 'package:orbitrade/views/home/profile_page/deposit/deposit_deatils_page.dart';
import 'package:orbitrade/views/home/profile_page/deposit/deposit_page.dart';
import 'package:orbitrade/views/home/profile_page/internal_transfer/internal_trans_deatils_page.dart';
import 'package:orbitrade/views/home/profile_page/internal_transfer/internal_transfer_page.dart';
import 'package:orbitrade/views/home/profile_page/mt4_account/classOne.dart';
import 'package:orbitrade/views/home/profile_page/mt4_account/mt4_account_create_page.dart';
import 'package:orbitrade/views/home/profile_page/withdrawl/withdawal_deatils_page.dart';
import 'package:orbitrade/views/home/profile_page/withdrawl/withdrawl_page.dart';
import 'package:shimmer/shimmer.dart';

import '../../../constants.dart';
import '../../../main.dart';
import '../../../size_config.dart';

class HomePage extends StatefulWidget {
  static const routeName = 'home_page';
  const HomePage({Key key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  var bankC = Get.put(BankSettingController());
  final mt4AccountController = Get.put(Mt4AccountDataController());
  var _depositDataCon = Get.put(MyDepositDataController());
  var _internalDataCon = Get.put(InternalTransDataController());
  var _withdrawalDataCon = Get.put(WithdrawalDataController());
  var custodyBankController = Get.put(CusodyBankListController());
  int _current_slider = 0;
  int _selectedIndex = 0;
  List<String> list = [
    'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTohhL2UjMfESB9IPj1EM7J_UtBamk2DuiapsR9vS2Yk1mRcU5dHtH3qEhvLNsHn3dhE3T1laNB_jwUaw&usqp=CAU',
    'https://43a7183y5zx2q5qg935lc2o59l-wpengine.netdna-ssl.com/wp-content/uploads/2018/04/Australian-Interiors-Design-Awards-TDF-8-2200x1235.jpg',
    'https://media3.architecturemedia.net/site_media/media/cache/74/af/74afe5fa6f90cffad88eb9fc71ceae70.jpg',
    'https://cdn.shopify.com/s/files/1/0004/4630/0222/files/Top_10_Australian_Interior_Designers_You_Need_To_Know_-_Interior_Design_Australia_-_David_Hicks_-_LuxDeco.jpg?52083',
    'https://brabbu.com/blog/wp-content/uploads/2017/03/Top-10-Best-Interior-Designers-In-Australia-mim-design.jpg'
  ];

  List<String> cashlierList = [
    'Deposit',
    'Internal Transfer',
    'Withdrawal',
  ];

  @override
  void initState() {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
        statusBarColor: Colors.white, statusBarBrightness: Brightness.light));

    // TODO: implement initState

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kWhiteColor,
      appBar: AppBar(
        elevation: 0.0,
        automaticallyImplyLeading: false,
        backgroundColor: Colors.white,
        brightness: Brightness.light,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                  width: 80.0,
                  height: 34.0,
                  child: Image.asset(
                    '${kImageDir}top_logo.png',
                    fit: BoxFit.fitWidth,
                  )),
            ),
            SizedBox(
              width: 10,
            ),
          ],
        ),
        actions: [
          Builder(
            builder: (context) => IconButton(
                icon: Icon(Icons.notifications, color: kPrimaryColor),
                onPressed: () {}),
          )
        ],
      ),
      body: Obx(() {
        if (mt4AccountController.isLoading.value &&
            _depositDataCon.isLoading.value &&
            _internalDataCon.isLoading.value &&
            _withdrawalDataCon.isLoading.value) {
          return CustomLoader();
        } else {
          if (bankC.isLoading.value) {
            return CustomLoader();
          } else {
            return SingleChildScrollView(
              child: Body(),
            );
          }
        }
      }),
    );
  }

  Body() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 5.0),
      child: Column(
        children: [
          size10,
          buildHomeCarouselSlider(context),
          Container(
            child: bankC.myBankData != null
                ? bankC.myBankData.data.bankName != null ||
                        bankC.myBankData.data.accountName != null ||
                        bankC.myBankData.data.accountNumber != null
                    ? SizedBox()
                    : Column(
                        children: [size10, buildSecoundHomeImage()],
                      )
                : SizedBox(),
          ),
          SizedBox(
            height: 10,
          ),
          buildMtAccount(),
          SizedBox(
            height: 10,
          ),
          // buildHomeCasler(),
          /* SizedBox(
            height: 10,
          ),*/
          buildDeposit(),
          SizedBox(
            height: 10,
          ),
          buildInternalTransfert(),
          SizedBox(
            height: 10,
          ),
          buildWithdrawal(),
        ],
      ),
    );
  }

  buildHomeCarouselSlider(context) {
    if (list != null) {
      return Column(
        children: [
          SizedBox(
            height: 10,
          ),
          Container(
            width: double.infinity,
            height: 165,
            child: CarouselSlider(
              options: CarouselOptions(
                  viewportFraction: 1,
                  initialPage: 0,
                  enableInfiniteScroll: true,
                  reverse: false,
                  autoPlay: true,
                  autoPlayInterval: Duration(seconds: 5),
                  autoPlayAnimationDuration: Duration(milliseconds: 1000),
                  autoPlayCurve: Curves.easeInCubic,
                  enlargeCenterPage: true,
                  scrollDirection: Axis.horizontal,
                  onPageChanged: (index, reason) {
                    setState(() {
                      _current_slider = index;
                    });
                  }),
              items: list.map((i) {
                return Builder(
                  builder: (BuildContext context) {
                    return Container(
                        width: double.infinity,
                        margin: EdgeInsets.symmetric(horizontal: 10.0),
                        child: ClipRRect(
                            borderRadius: BorderRadius.all(Radius.circular(8)),
                            child: FadeInImage.assetNetwork(
                              placeholder: '${kImageDir}ple.jpg',
                              image: i,
                              fit: BoxFit.fill,
                            )));
                  },
                );
              }).toList(),
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: list.map((url) {
              int index = list.indexOf(url);
              return Container(
                width: 8.0,
                height: 8.0,
                //margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 4.0),
                margin: EdgeInsets.only(right: 5.0, left: 5.0),
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: _current_slider == index
                      ? kPrimaryColor
                      : Color.fromRGBO(112, 112, 112, .3),
                ),
              );
            }).toList(),
          ),
        ],
      );
    } else {
      return Padding(
        padding: const EdgeInsets.only(left: 4.0, right: 4.0),
        child: Shimmer.fromColors(
          baseColor: Colors.grey[100],
          highlightColor: Colors.grey[100],
          child: Container(
            height: 120,
            width: double.infinity,
            color: Colors.white,
          ),
        ),
      );
    }
  }

  buildMtAccount() {
    return Column(
      children: [
        Container(
          margin: EdgeInsets.only(left: 10, right: 10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                alignment: Alignment.centerLeft,
                //margin: EdgeInsets.only(left: 10),
                child: Text(
                  'MT4 Account',
                  style: kRegularText.copyWith(
                      color: Colors.black, fontWeight: FontWeight.w600),
                ),
              ),
              GestureDetector(
                onTap: () {
                  /*  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => ClassOne()),
                  );*/
                },
                child: Container(
                  alignment: Alignment.centerLeft,
                  margin: EdgeInsets.only(left: 10),
                  child: Text(
                    'Lihat Semua',
                    style: kRegularText.copyWith(
                      color: kPrimaryColor,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
        Container(
            child: mt4AccountController.myAccount != null
                ? mt4AccountController.myAccount.response.length > 0
                    ? Container(
                        height: 140.0,
                        child: ListView(
                          scrollDirection: Axis.horizontal,
                          shrinkWrap: true,
                          physics: ScrollPhysics(),
                          children: [
                            ListView.builder(
                              scrollDirection: Axis.horizontal,
                              itemCount: mt4AccountController
                                          .myAccount.response.length >
                                      5
                                  ? 5
                                  : mt4AccountController
                                      .myAccount.response.length,
                              shrinkWrap: true,
                              physics: NeverScrollableScrollPhysics(),
                              itemBuilder: (_, index) {
                                return GestureDetector(
                                  onTap: () {
                                    Navigator.push(context,
                                        MaterialPageRoute(builder: (context) {
                                      return ClassOne(
                                        name: "",
                                        data: mt4AccountController
                                            .myAccount.response[index],
                                      );
                                    }));
                                  },
                                  child: Column(
                                    children: [
                                      Align(
                                        alignment: Alignment.centerLeft,
                                        child: Container(
                                          alignment: Alignment.centerLeft,
                                          margin: EdgeInsets.only(left: 10),
                                          width: 210.0,
                                          child: Column(
                                            children: [
                                              SizedBox(
                                                height: 10,
                                              ),
                                              Container(
                                                //margin: EdgeInsets.only(left: 10),
                                                height: 130.0,
                                                decoration: BoxDecoration(
                                                  color: khomeFirstCardColor,
                                                  borderRadius:
                                                      new BorderRadius.all(
                                                    const Radius.circular(10.0),
                                                  ),
                                                ),
                                                child: Column(
                                                  children: [
                                                    Row(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .spaceBetween,
                                                      children: [
                                                        Container(
                                                          alignment: Alignment
                                                              .centerLeft,
                                                          margin:
                                                              EdgeInsets.only(
                                                                  left: 10,
                                                                  top: 7),
                                                          child: Text(
                                                            mt4AccountController
                                                                        .myAccount
                                                                        .response[
                                                                            index]
                                                                        .typeAccount ==
                                                                    "1"
                                                                ? 'Recreation'
                                                                : mt4AccountController
                                                                            .myAccount
                                                                            .response[
                                                                                index]
                                                                            .typeAccount ==
                                                                        "2"
                                                                    ? "Profesional"
                                                                    : mt4AccountController.myAccount.response[index].typeAccount ==
                                                                            "3"
                                                                        ? "Syariah"
                                                                        : mt4AccountController.myAccount.response[index].typeAccount ==
                                                                                "4"
                                                                            ? "Signature"
                                                                            : "",
                                                            style: kBoldStyle
                                                                .copyWith(
                                                                    color:
                                                                        kPrimaryColor),
                                                          ),
                                                        ),
                                                        GestureDetector(
                                                          onTap: () {},
                                                          child: Container(
                                                              margin: EdgeInsets
                                                                  .only(
                                                                      right:
                                                                          8.0),
                                                              alignment: Alignment
                                                                  .centerRight,
                                                              child: mt4AccountController
                                                                          .myAccount
                                                                          .response[
                                                                              index]
                                                                          .status ==
                                                                      "waiting"
                                                                  ? Icon(
                                                                      Icons
                                                                          .info,
                                                                      color:
                                                                          kSecondaryColor,
                                                                      size: 16,
                                                                    )
                                                                  : Icon(
                                                                      Icons
                                                                          .check_circle,
                                                                      color: Colors
                                                                          .green,
                                                                      size: 16,
                                                                    )),
                                                        )
                                                      ],
                                                    ),
                                                    SizedBox(
                                                      height: 15,
                                                    ),
                                                    Container(
                                                      alignment: Alignment
                                                          .bottomCenter,
                                                      child: Column(
                                                        children: [
                                                          Container(
                                                            alignment: Alignment
                                                                .centerLeft,
                                                            margin:
                                                                EdgeInsets.only(
                                                                    left: 10),
                                                            child: Text(
                                                              "Server",
                                                              style: TextStyle(
                                                                  color: Colors
                                                                      .black,
                                                                  fontSize: 11),
                                                            ),
                                                          ),
                                                          SizedBox(
                                                            height: 2,
                                                          ),
                                                          Container(
                                                            alignment: Alignment
                                                                .centerLeft,
                                                            margin:
                                                                EdgeInsets.only(
                                                                    left: 10),
                                                            child: Text(
                                                              'MaxrichGroup-Real',
                                                              style: kRegularText
                                                                  .copyWith(
                                                                      color: Colors
                                                                          .black),
                                                            ),
                                                          ),
                                                          Container(
                                                            alignment: Alignment
                                                                .centerLeft,
                                                            margin:
                                                                EdgeInsets.only(
                                                                    left: 10),
                                                            child: Text(
                                                              "Login",
                                                              style: TextStyle(
                                                                  color: Colors
                                                                      .black,
                                                                  fontSize: 11),
                                                            ),
                                                          ),
                                                          SizedBox(
                                                            height: 2,
                                                          ),
                                                          Row(
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .spaceBetween,
                                                            children: [
                                                              Container(
                                                                alignment: Alignment
                                                                    .centerLeft,
                                                                margin: EdgeInsets
                                                                    .only(
                                                                        left:
                                                                            10),
                                                                child: Text(
                                                                  '${mt4AccountController.myAccount.response[index].status}',
                                                                  style: kRegularText
                                                                      .copyWith(
                                                                          color:
                                                                              Colors.black),
                                                                ),
                                                              ),
                                                              Container(
                                                                margin: EdgeInsets
                                                                    .only(
                                                                        right:
                                                                            8.0),
                                                                alignment: Alignment
                                                                    .centerRight,
                                                                child:
                                                                    Container(
                                                                        width: AppBar().preferredSize.height -
                                                                            15,
                                                                        child: Image
                                                                            .asset(
                                                                          '${kImageDir}white_logo.png',
                                                                        )),
                                                              )
                                                            ],
                                                          ),
                                                        ],
                                                      ),
                                                    )
                                                  ],
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                );
                              },
                            ),
                            Align(
                              alignment: Alignment.centerLeft,
                              child: GestureDetector(
                                onTap: () => Navigator.pushNamed(
                                    context, Mt4AccountCreatePage.routeName),
                                child: Container(
                                  alignment: Alignment.centerLeft,
                                  margin: EdgeInsets.only(right: 10),
                                  width: 125.0,
                                  //height: 130,
                                  child: Column(
                                    children: [
                                      SizedBox(
                                        height: 10,
                                      ),
                                      Container(
                                        alignment: Alignment.center,
                                        margin: EdgeInsets.only(left: 10),
                                        height: 130.0,
                                        decoration: BoxDecoration(
                                          color: kPrimaryColor,
                                          borderRadius: new BorderRadius.all(
                                            const Radius.circular(10.0),
                                          ),
                                        ),
                                        child: Column(
                                          mainAxisAlignment: mainAxisCenter,
                                          children: [
                                            Icon(
                                              Icons.add_circle_outline_sharp,
                                              color: Colors.white,
                                              size: 46,
                                            ),
                                            size10,
                                            Container(
                                              padding:
                                                  const EdgeInsets.symmetric(
                                                      horizontal: 1.0),
                                              alignment: Alignment.center,
                                              child: Text(
                                                'Request\nMT4 Account',
                                                textAlign: TextAlign.center,
                                                style: kRegularText.copyWith(
                                                    color: Colors.white),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      )
                    : Column(
                        children: [
                          size10,
                          GestureDetector(
                            onTap: () {
                              Navigator.pushNamed(
                                  context, Mt4AccountCreatePage.routeName);
                            },
                            child: Container(
                                margin: EdgeInsets.only(left: 10, right: 10),
                                alignment: Alignment.centerLeft,
                                width: SizeConfig.screenWidth,
                                decoration: BoxDecoration(
                                  borderRadius: new BorderRadius.all(
                                    const Radius.circular(5.0),
                                  ),
                                  color: kPrimaryColor,
                                  //borderRadius: BorderRadius.all(Radius.circular(5)),
                                ),
                                child: Container(
                                  alignment: Alignment.center,
                                  padding: EdgeInsets.only(
                                      left: 10, right: 10, top: 8, bottom: 8),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Icon(
                                        Icons.add_circle_outline_sharp,
                                        color: kWhiteColor,
                                      ),
                                      SizedBox(
                                        width: 10,
                                      ),
                                      Text(
                                        'Request MT4 Account',
                                        maxLines: 1,
                                        overflow: TextOverflow.ellipsis,
                                        style: TextStyle(
                                            color: kWhiteColor,
                                            fontWeight: FontWeight.bold,
                                            fontSize: 16),
                                      ),
                                    ],
                                  ),
                                )),
                          ),
                          size10,
                        ],
                      )
                : Column(
                    children: [
                      size10,
                      GestureDetector(
                        onTap: () {
                          Navigator.pushNamed(
                              context, Mt4AccountCreatePage.routeName);
                        },
                        child: Container(
                            margin: EdgeInsets.only(left: 10, right: 10),
                            alignment: Alignment.centerLeft,
                            width: SizeConfig.screenWidth,
                            decoration: BoxDecoration(
                              borderRadius: new BorderRadius.all(
                                const Radius.circular(5.0),
                              ),
                              color: kPrimaryColor,
                              //borderRadius: BorderRadius.all(Radius.circular(5)),
                            ),
                            child: Container(
                              alignment: Alignment.center,
                              padding: EdgeInsets.only(
                                  left: 10, right: 10, top: 8, bottom: 8),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Icon(
                                    Icons.add_circle_outline_sharp,
                                    color: kWhiteColor,
                                  ),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  Text(
                                    'Request MT4 Account',
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(
                                        color: kWhiteColor,
                                        fontWeight: FontWeight.bold,
                                        fontSize: 16),
                                  ),
                                ],
                              ),
                            )),
                      ),
                      size10,
                    ],
                  )),
      ],
    );
  }

  buildDeposit() {
    return Column(
      children: [
        Container(
          margin: EdgeInsets.only(left: 10, right: 10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                alignment: Alignment.centerLeft,
                //margin: EdgeInsets.only(left: 10),
                child: Text(
                  'Deposit',
                  style: kRegularText.copyWith(
                      color: Colors.black, fontWeight: FontWeight.w600),
                ),
              ),
              GestureDetector(
                onTap: () {
                  Navigator.pushNamed(context, DepositPage.routeName);
                },
                child: Container(
                  alignment: Alignment.centerLeft,
                  margin: EdgeInsets.only(left: 10),
                  child: Text(
                    'Lihat Semua',
                    style: kRegularText.copyWith(
                      color: kPrimaryColor,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
        Container(
            child: _depositDataCon.myAccount != null
                ? _depositDataCon.myAccount.data.length > 0
                    ? Container(
                        height: 110.0,
                        child: ListView(
                          scrollDirection: Axis.horizontal,
                          shrinkWrap: true,
                          physics: ScrollPhysics(),
                          children: [
                            ListView.builder(
                              scrollDirection: Axis.horizontal,
                              itemCount:
                                  _depositDataCon.myAccount.data.length > 5
                                      ? 5
                                      : _depositDataCon.myAccount.data.length,
                              shrinkWrap: true,
                              physics: NeverScrollableScrollPhysics(),
                              itemBuilder: (_, index) {
                                return GestureDetector(
                                  onTap: () {
                                    Navigator.push(context,
                                        MaterialPageRoute(builder: (context) {
                                      return DepositDetailsPage(
                                        name: 'Deposit' +
                                            ' #${_depositDataCon.myAccount.data[index].id.toString()}',
                                        data: _depositDataCon
                                            .myAccount.data[index],
                                      );
                                    }));
                                  },
                                  child: Align(
                                    alignment: Alignment.centerLeft,
                                    child: Container(
                                      alignment: Alignment.centerLeft,
                                      width: 210.0,
                                      child: Column(
                                        children: [
                                          SizedBox(
                                            height: 10,
                                          ),
                                          Container(
                                            margin: EdgeInsets.only(left: 10),
                                            height: 100.0,
                                            decoration: BoxDecoration(
                                              color: _depositDataCon.myAccount
                                                          .data[index].status ==
                                                      'approved'
                                                  ? khomeSecCardColor
                                                  : kPrimaryColor,
                                              borderRadius:
                                                  new BorderRadius.all(
                                                const Radius.circular(10.0),
                                              ),
                                            ),
                                            child: Column(
                                              children: [
                                                Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment
                                                          .spaceBetween,
                                                  children: [
                                                    Container(
                                                      alignment:
                                                          Alignment.centerLeft,
                                                      margin: EdgeInsets.only(
                                                          left: 10, top: 7),
                                                      child: Text(
                                                        'IDR ' +
                                                                _depositDataCon
                                                                    .myAccount
                                                                    .data[index]
                                                                    .amount
                                                                    .toString() ??
                                                            "",
                                                        style:
                                                            kBoldStyle.copyWith(
                                                                color: Colors
                                                                    .white),
                                                      ),
                                                    ),
                                                    GestureDetector(
                                                      onTap: () {},
                                                      child: Container(
                                                          margin:
                                                              EdgeInsets.only(
                                                                  right: 8.0),
                                                          alignment: Alignment
                                                              .centerRight, //check_circle_outline
                                                          child: Icon(
                                                            _depositDataCon
                                                                        .myAccount
                                                                        .data[
                                                                            index]
                                                                        .status ==
                                                                    'approved'
                                                                ? Icons
                                                                    .check_circle_outline
                                                                : Icons
                                                                    .cancel_outlined,
                                                            color: Colors.white,
                                                            size: 16,
                                                          )),
                                                    )
                                                  ],
                                                ),
                                                SizedBox(
                                                  height: 15,
                                                ),
                                                Container(
                                                  alignment:
                                                      Alignment.bottomCenter,
                                                  child: Column(
                                                    children: [
                                                      Container(
                                                        alignment: Alignment
                                                            .centerLeft,
                                                        margin: EdgeInsets.only(
                                                            left: 10),
                                                        child: Text(
                                                          "Account ID",
                                                          style: TextStyle(
                                                              color: Colors
                                                                  .grey[300],
                                                              fontSize: 12),
                                                        ),
                                                      ),
                                                      SizedBox(
                                                        height: 2,
                                                      ),
                                                      Row(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .spaceBetween,
                                                        children: [
                                                          Container(
                                                            alignment: Alignment
                                                                .centerLeft,
                                                            margin:
                                                                EdgeInsets.only(
                                                                    left: 10),
                                                            child: Text(
                                                              _depositDataCon
                                                                      .myAccount
                                                                      .data[
                                                                          index]
                                                                      .metatrader ??
                                                                  "",
                                                              style: kRegularText
                                                                  .copyWith(
                                                                      color: Colors
                                                                          .white),
                                                            ),
                                                          ),
                                                          Container(
                                                            margin:
                                                                EdgeInsets.only(
                                                                    right: 8.0),
                                                            alignment: Alignment
                                                                .centerRight,
                                                            child: Container(
                                                                width: AppBar()
                                                                        .preferredSize
                                                                        .height -
                                                                    15,
                                                                child:
                                                                    Image.asset(
                                                                  '${kImageDir}white_logo.png',
                                                                )),
                                                          )
                                                        ],
                                                      ),
                                                    ],
                                                  ),
                                                )
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                );
                              },
                            ),
                            Column(
                              children: [
                                GestureDetector(
                                  onTap: () {
                                    custodyBankController.fetchCusodyBankList(
                                        prefs.getString("token"));
                                    Navigator.pushNamed(
                                        context, RequestDepositPage.routeName);
                                  },
                                  child: Align(
                                    alignment: Alignment.centerLeft,
                                    child: Container(
                                      alignment: Alignment.centerLeft,
                                      margin: EdgeInsets.only(right: 10),
                                      width: 120.0,
                                      child: Column(
                                        children: [
                                          SizedBox(
                                            height: 10,
                                          ),
                                          Container(
                                            alignment: Alignment.center,
                                            margin: EdgeInsets.only(left: 10),
                                            height: 99.0,
                                            decoration: BoxDecoration(
                                              color: kPrimaryColor,
                                              borderRadius:
                                                  new BorderRadius.all(
                                                const Radius.circular(10.0),
                                              ),
                                            ),
                                            child: Column(
                                              children: [
                                                Icon(
                                                  Icons
                                                      .add_circle_outline_sharp,
                                                  color: Colors.white,
                                                  size: 46,
                                                ),
                                                Container(
                                                  alignment: Alignment.center,
                                                  margin:
                                                      EdgeInsets.only(left: 10),
                                                  child: Text(
                                                    'Request\nDeposit',
                                                    textAlign: TextAlign.center,
                                                    style:
                                                        kRegularText.copyWith(
                                                            color:
                                                                Colors.white),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      )
                    : Column(
                        children: [
                          size10,
                          GestureDetector(
                            onTap: () {
                              custodyBankController.fetchCusodyBankList(
                                  prefs.getString("token"));
                              Navigator.pushNamed(
                                  context, RequestDepositPage.routeName);
                            },
                            child: Container(
                                margin: EdgeInsets.only(left: 10, right: 10),
                                alignment: Alignment.centerLeft,
                                width: SizeConfig.screenWidth,
                                decoration: BoxDecoration(
                                  borderRadius: new BorderRadius.all(
                                    const Radius.circular(5.0),
                                  ),
                                  color: kPrimaryColor,
                                  //borderRadius: BorderRadius.all(Radius.circular(5)),
                                ),
                                child: Container(
                                  alignment: Alignment.center,
                                  padding: EdgeInsets.only(
                                      left: 10, right: 10, top: 8, bottom: 8),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Icon(
                                        Icons.add_circle_outline_sharp,
                                        color: kWhiteColor,
                                      ),
                                      SizedBox(
                                        width: 10,
                                      ),
                                      Text(
                                        'Request Deposit',
                                        maxLines: 1,
                                        overflow: TextOverflow.ellipsis,
                                        style: TextStyle(
                                            color: kWhiteColor,
                                            fontWeight: FontWeight.bold,
                                            fontSize: 16),
                                      ),
                                    ],
                                  ),
                                )),
                          ),
                          size10,
                        ],
                      )
                : Column(
                    children: [
                      size10,
                      GestureDetector(
                        onTap: () {
                          custodyBankController
                              .fetchCusodyBankList(prefs.getString("token"));
                          Navigator.pushNamed(
                              context, RequestDepositPage.routeName);
                        },
                        child: Container(
                            margin: EdgeInsets.only(left: 10, right: 10),
                            alignment: Alignment.centerLeft,
                            width: SizeConfig.screenWidth,
                            decoration: BoxDecoration(
                              borderRadius: new BorderRadius.all(
                                const Radius.circular(5.0),
                              ),
                              color: kPrimaryColor,
                              //borderRadius: BorderRadius.all(Radius.circular(5)),
                            ),
                            child: Container(
                              alignment: Alignment.center,
                              padding: EdgeInsets.only(
                                  left: 10, right: 10, top: 8, bottom: 8),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Icon(
                                    Icons.add_circle_outline_sharp,
                                    color: kWhiteColor,
                                  ),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  Text(
                                    'Request Deposit',
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(
                                        color: kWhiteColor,
                                        fontWeight: FontWeight.bold,
                                        fontSize: 16),
                                  ),
                                ],
                              ),
                            )),
                      ),
                      size10,
                    ],
                  )),
      ],
    );
  }

  buildInternalTransfert() {
    return Column(
      children: [
        Container(
          margin: EdgeInsets.only(left: 10, right: 10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                alignment: Alignment.centerLeft,
                //margin: EdgeInsets.only(left: 10),
                child: Text(
                  'Internal Transfer',
                  style: kRegularText.copyWith(
                      color: Colors.black, fontWeight: FontWeight.w600),
                ),
              ),
              GestureDetector(
                onTap: () {
                  Navigator.pushNamed(context, InternalTransferPage.routeName);
                },
                child: Container(
                  alignment: Alignment.centerLeft,
                  margin: EdgeInsets.only(left: 10),
                  child: Text(
                    'Lihat Semua',
                    style: kRegularText.copyWith(
                      color: kPrimaryColor,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
        Container(
            child: _internalDataCon.myAccount != null
                ? _internalDataCon.myAccount.data.length > 0
                    ? Container(
                        height: 110.0,
                        child: ListView(
                          scrollDirection: Axis.horizontal,
                          shrinkWrap: true,
                          physics: ScrollPhysics(),
                          children: [
                            ListView.builder(
                              scrollDirection: Axis.horizontal,
                              itemCount:
                                  _internalDataCon.myAccount.data.length > 5
                                      ? 5
                                      : _internalDataCon.myAccount.data.length,
                              shrinkWrap: true,
                              physics: NeverScrollableScrollPhysics(),
                              itemBuilder: (_, index) {
                                return GestureDetector(
                                  onTap: () {
                                    Navigator.push(context,
                                        MaterialPageRoute(builder: (context) {
                                      return InternalTransDetailsPage(
                                        name: 'Internal Transfer' +
                                            ' #${_internalDataCon.myAccount.data[index].id.toString()}',
                                        data: _internalDataCon
                                            .myAccount.data[index],
                                      );
                                    }));
                                  },
                                  child: Align(
                                    alignment: Alignment.centerLeft,
                                    child: Container(
                                      alignment: Alignment.centerLeft,
                                      width: 210.0,
                                      child: Column(
                                        children: [
                                          SizedBox(
                                            height: 10,
                                          ),
                                          Container(
                                            margin: EdgeInsets.only(left: 10),
                                            height: 100.0,
                                            decoration: BoxDecoration(
                                              color: _internalDataCon.myAccount
                                                          .data[index].status ==
                                                      'approved'
                                                  ? khomeSecCardColor
                                                  : kPrimaryColor,
                                              borderRadius:
                                                  new BorderRadius.all(
                                                const Radius.circular(10.0),
                                              ),
                                            ),
                                            child: Column(
                                              children: [
                                                Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment
                                                          .spaceBetween,
                                                  children: [
                                                    Container(
                                                      alignment:
                                                          Alignment.centerLeft,
                                                      margin: EdgeInsets.only(
                                                          left: 10, top: 7),
                                                      child: Text(
                                                        '$kUSD ' +
                                                                _internalDataCon
                                                                    .myAccount
                                                                    .data[index]
                                                                    .amount
                                                                    .toString() ??
                                                            "",
                                                        style:
                                                            kBoldStyle.copyWith(
                                                                color: Colors
                                                                    .white),
                                                      ),
                                                    ),
                                                    GestureDetector(
                                                      onTap: () {},
                                                      child: Container(
                                                          margin:
                                                              EdgeInsets.only(
                                                                  right: 8.0),
                                                          alignment: Alignment
                                                              .centerRight, //check_circle_outline
                                                          child: Icon(
                                                            _internalDataCon
                                                                        .myAccount
                                                                        .data[
                                                                            index]
                                                                        .status ==
                                                                    'approved'
                                                                ? Icons
                                                                    .check_circle_outline
                                                                : Icons
                                                                    .cancel_outlined,
                                                            color: Colors.white,
                                                            size: 16,
                                                          )),
                                                    )
                                                  ],
                                                ),
                                                SizedBox(
                                                  height: 15,
                                                ),
                                                Container(
                                                  alignment:
                                                      Alignment.bottomCenter,
                                                  child: Column(
                                                    children: [
                                                      Container(
                                                        alignment: Alignment
                                                            .centerLeft,
                                                        margin: EdgeInsets.only(
                                                            left: 10),
                                                        child: Text(
                                                          "Account ID",
                                                          style: TextStyle(
                                                              color: Colors
                                                                  .grey[300],
                                                              fontSize: 12),
                                                        ),
                                                      ),
                                                      SizedBox(
                                                        height: 2,
                                                      ),
                                                      Row(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .spaceBetween,
                                                        children: [
                                                          Container(
                                                            alignment: Alignment
                                                                .centerLeft,
                                                            margin:
                                                                EdgeInsets.only(
                                                                    left: 10),
                                                            child: Text(
                                                              _internalDataCon
                                                                      .myAccount
                                                                      .data[
                                                                          index]
                                                                      .accountNumber ??
                                                                  "",
                                                              style: kRegularText
                                                                  .copyWith(
                                                                      color: Colors
                                                                          .white),
                                                            ),
                                                          ),
                                                          Container(
                                                            margin:
                                                                EdgeInsets.only(
                                                                    right: 8.0),
                                                            alignment: Alignment
                                                                .centerRight,
                                                            child: Container(
                                                                width: AppBar()
                                                                        .preferredSize
                                                                        .height -
                                                                    15,
                                                                child:
                                                                    Image.asset(
                                                                  '${kImageDir}white_logo.png',
                                                                )),
                                                          )
                                                        ],
                                                      ),
                                                    ],
                                                  ),
                                                )
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                );
                              },
                            ),
                            Column(
                              children: [
                                GestureDetector(
                                  onTap: () {
                                    Navigator.pushNamed(context,
                                        InternalHomeTransferPage.routeName);
                                  },
                                  child: Align(
                                    alignment: Alignment.centerLeft,
                                    child: Container(
                                      alignment: Alignment.centerLeft,
                                      margin: EdgeInsets.only(right: 10),
                                      width: 120.0,
                                      child: Column(
                                        children: [
                                          SizedBox(
                                            height: 10,
                                          ),
                                          Container(
                                            alignment: Alignment.center,
                                            margin: EdgeInsets.only(left: 10),
                                            height: 99.0,
                                            decoration: BoxDecoration(
                                              color: kPrimaryColor,
                                              borderRadius:
                                                  new BorderRadius.all(
                                                const Radius.circular(10.0),
                                              ),
                                            ),
                                            child: Column(
                                              children: [
                                                Icon(
                                                  Icons
                                                      .add_circle_outline_sharp,
                                                  color: Colors.white,
                                                  size: 46,
                                                ),
                                                Container(
                                                  alignment: Alignment.center,
                                                  margin:
                                                      EdgeInsets.only(left: 10),
                                                  child: Text(
                                                    'Internal\nTransfer',
                                                    textAlign: TextAlign.center,
                                                    style:
                                                        kRegularText.copyWith(
                                                            color:
                                                                Colors.white),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      )
                    : Column(
                        children: [
                          size10,
                          GestureDetector(
                            onTap: () {
                              Navigator.pushNamed(
                                  context, InternalHomeTransferPage.routeName);
                            },
                            child: Container(
                                margin: EdgeInsets.only(left: 10, right: 10),
                                alignment: Alignment.centerLeft,
                                width: SizeConfig.screenWidth,
                                decoration: BoxDecoration(
                                  borderRadius: new BorderRadius.all(
                                    const Radius.circular(5.0),
                                  ),
                                  color: kPrimaryColor,
                                  //borderRadius: BorderRadius.all(Radius.circular(5)),
                                ),
                                child: Container(
                                  alignment: Alignment.center,
                                  padding: EdgeInsets.only(
                                      left: 10, right: 10, top: 8, bottom: 8),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Icon(
                                        Icons.add_circle_outline_sharp,
                                        color: kWhiteColor,
                                      ),
                                      SizedBox(
                                        width: 10,
                                      ),
                                      Text(
                                        'Internal Transfer',
                                        maxLines: 1,
                                        overflow: TextOverflow.ellipsis,
                                        style: TextStyle(
                                            color: kWhiteColor,
                                            fontWeight: FontWeight.bold,
                                            fontSize: 16),
                                      ),
                                    ],
                                  ),
                                )),
                          ),
                          size10,
                        ],
                      )
                : Column(
                    children: [
                      size10,
                      GestureDetector(
                        onTap: () {
                          Navigator.pushNamed(
                              context, InternalHomeTransferPage.routeName);
                        },
                        child: Container(
                            margin: EdgeInsets.only(left: 10, right: 10),
                            alignment: Alignment.centerLeft,
                            width: SizeConfig.screenWidth,
                            decoration: BoxDecoration(
                              borderRadius: new BorderRadius.all(
                                const Radius.circular(5.0),
                              ),
                              color: kPrimaryColor,
                              //borderRadius: BorderRadius.all(Radius.circular(5)),
                            ),
                            child: Container(
                              alignment: Alignment.center,
                              padding: EdgeInsets.only(
                                  left: 10, right: 10, top: 8, bottom: 8),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Icon(
                                    Icons.add_circle_outline_sharp,
                                    color: kWhiteColor,
                                  ),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  Text(
                                    'Internal Transfer',
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(
                                        color: kWhiteColor,
                                        fontWeight: FontWeight.bold,
                                        fontSize: 16),
                                  ),
                                ],
                              ),
                            )),
                      ),
                      size10,
                    ],
                  )),
      ],
    );
  } //

  buildWithdrawal() {
    return Column(
      children: [
        Container(
          margin: EdgeInsets.only(left: 10, right: 10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                alignment: Alignment.centerLeft,
                child: Text(
                  'Withdrawal',
                  style: kRegularText.copyWith(
                      color: Colors.black, fontWeight: FontWeight.w600),
                ),
              ),
              GestureDetector(
                onTap: () {
                  Navigator.pushNamed(context, WithdrawlPage.routeName);
                },
                child: Container(
                  alignment: Alignment.centerLeft,
                  margin: EdgeInsets.only(left: 10),
                  child: Text(
                    'Lihat Semua',
                    style: kRegularText.copyWith(
                      color: kPrimaryColor,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
        Container(
            child: _withdrawalDataCon.myAccount != null
                ? _withdrawalDataCon.myAccount.data.length > 0
                    ? Container(
                        height: 110.0,
                        child: ListView(
                          scrollDirection: Axis.horizontal,
                          shrinkWrap: true,
                          physics: ScrollPhysics(),
                          children: [
                            ListView.builder(
                              scrollDirection: Axis.horizontal,
                              itemCount:
                                  _withdrawalDataCon.myAccount.data.length > 5
                                      ? 5
                                      : _withdrawalDataCon
                                          .myAccount.data.length,
                              shrinkWrap: true,
                              physics: NeverScrollableScrollPhysics(),
                              itemBuilder: (_, index) {
                                return GestureDetector(
                                  onTap: () {
                                    Navigator.push(context,
                                        MaterialPageRoute(builder: (context) {
                                      return WithdrawalDetailsPage(
                                        name: 'Withdrawal' +
                                            ' #${_withdrawalDataCon.myAccount.data[index].id.toString()}',
                                        data: _withdrawalDataCon
                                            .myAccount.data[index],
                                      );
                                    }));
                                  },
                                  child: Align(
                                    alignment: Alignment.centerLeft,
                                    child: Container(
                                      alignment: Alignment.centerLeft,
                                      width: 210.0,
                                      child: Column(
                                        children: [
                                          SizedBox(
                                            height: 10,
                                          ),
                                          Container(
                                            margin: EdgeInsets.only(left: 10),
                                            height: 100.0,
                                            decoration: BoxDecoration(
                                              color: _withdrawalDataCon
                                                          .myAccount
                                                          .data[index]
                                                          .status ==
                                                      'approved'
                                                  ? khomeSecCardColor
                                                  : kPrimaryColor,
                                              borderRadius:
                                                  new BorderRadius.all(
                                                const Radius.circular(10.0),
                                              ),
                                            ),
                                            child: Column(
                                              children: [
                                                Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment
                                                          .spaceBetween,
                                                  children: [
                                                    Container(
                                                      alignment:
                                                          Alignment.centerLeft,
                                                      margin: EdgeInsets.only(
                                                          left: 10, top: 7),
                                                      child: Text(
                                                        '$kUSD ' +
                                                                _withdrawalDataCon
                                                                    .myAccount
                                                                    .data[index]
                                                                    .amount
                                                                    .toString() ??
                                                            "",
                                                        style:
                                                            kBoldStyle.copyWith(
                                                                color: Colors
                                                                    .white),
                                                      ),
                                                    ),
                                                    GestureDetector(
                                                      onTap: () {},
                                                      child: Container(
                                                          margin:
                                                              EdgeInsets.only(
                                                                  right: 8.0),
                                                          alignment: Alignment
                                                              .centerRight, //check_circle_outline
                                                          child: Icon(
                                                            _withdrawalDataCon
                                                                        .myAccount
                                                                        .data[
                                                                            index]
                                                                        .status ==
                                                                    'approved'
                                                                ? Icons
                                                                    .check_circle_outline
                                                                : Icons
                                                                    .cancel_outlined,
                                                            color: Colors.white,
                                                            size: 16,
                                                          )),
                                                    )
                                                  ],
                                                ),
                                                SizedBox(
                                                  height: 15,
                                                ),
                                                Container(
                                                  alignment:
                                                      Alignment.bottomCenter,
                                                  child: Column(
                                                    children: [
                                                      Container(
                                                        alignment: Alignment
                                                            .centerLeft,
                                                        margin: EdgeInsets.only(
                                                            left: 10),
                                                        child: Text(
                                                          "Account ID",
                                                          style: TextStyle(
                                                              color: Colors
                                                                  .grey[300],
                                                              fontSize: 12),
                                                        ),
                                                      ),
                                                      SizedBox(
                                                        height: 2,
                                                      ),
                                                      Row(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .spaceBetween,
                                                        children: [
                                                          Container(
                                                            alignment: Alignment
                                                                .centerLeft,
                                                            margin:
                                                                EdgeInsets.only(
                                                                    left: 10),
                                                            child: Text(
                                                              _withdrawalDataCon
                                                                      .myAccount
                                                                      .data[
                                                                          index]
                                                                      .accountNumber ??
                                                                  "",
                                                              style: kRegularText
                                                                  .copyWith(
                                                                      color: Colors
                                                                          .white),
                                                            ),
                                                          ),
                                                          Container(
                                                            margin:
                                                                EdgeInsets.only(
                                                                    right: 8.0),
                                                            alignment: Alignment
                                                                .centerRight,
                                                            child: Container(
                                                                width: AppBar()
                                                                        .preferredSize
                                                                        .height -
                                                                    15,
                                                                child:
                                                                    Image.asset(
                                                                  '${kImageDir}white_logo.png',
                                                                )),
                                                          )
                                                        ],
                                                      ),
                                                    ],
                                                  ),
                                                )
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                );
                              },
                            ),
                            Column(
                              children: [
                                GestureDetector(
                                  onTap: () {
                                    Navigator.pushNamed(context,
                                        RequestWithdrawalPage.routeName);
                                  },
                                  child: Align(
                                    alignment: Alignment.centerLeft,
                                    child: Container(
                                      alignment: Alignment.centerLeft,
                                      margin: EdgeInsets.only(right: 10),
                                      width: 120.0,
                                      child: Column(
                                        children: [
                                          SizedBox(
                                            height: 10,
                                          ),
                                          Container(
                                            alignment: Alignment.center,
                                            margin: EdgeInsets.only(left: 10),
                                            height: 99.0,
                                            decoration: BoxDecoration(
                                              color: kPrimaryColor,
                                              borderRadius:
                                                  new BorderRadius.all(
                                                const Radius.circular(10.0),
                                              ),
                                            ),
                                            child: Column(
                                              children: [
                                                Icon(
                                                  Icons
                                                      .add_circle_outline_sharp,
                                                  color: Colors.white,
                                                  size: 46,
                                                ),
                                                Container(
                                                  alignment: Alignment.center,
                                                  margin:
                                                      EdgeInsets.only(left: 10),
                                                  child: Text(
                                                    'Request\nWithdrawal',
                                                    textAlign: TextAlign.center,
                                                    style:
                                                        kRegularText.copyWith(
                                                            color:
                                                                Colors.white),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      )
                    : Column(
                        children: [
                          size10,
                          GestureDetector(
                            onTap: () {
                              Navigator.pushNamed(
                                  context, RequestWithdrawalPage.routeName);
                            },
                            child: Container(
                                margin: EdgeInsets.only(left: 10, right: 10),
                                alignment: Alignment.centerLeft,
                                width: SizeConfig.screenWidth,
                                decoration: BoxDecoration(
                                  borderRadius: new BorderRadius.all(
                                    const Radius.circular(5.0),
                                  ),
                                  color: kPrimaryColor,
                                  //borderRadius: BorderRadius.all(Radius.circular(5)),
                                ),
                                child: Container(
                                  alignment: Alignment.center,
                                  padding: EdgeInsets.only(
                                      left: 10, right: 10, top: 8, bottom: 8),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Icon(
                                        Icons.add_circle_outline_sharp,
                                        color: kWhiteColor,
                                      ),
                                      SizedBox(
                                        width: 10,
                                      ),
                                      Text(
                                        'Request Withdrawal',
                                        maxLines: 1,
                                        overflow: TextOverflow.ellipsis,
                                        style: TextStyle(
                                            color: kWhiteColor,
                                            fontWeight: FontWeight.bold,
                                            fontSize: 16),
                                      ),
                                    ],
                                  ),
                                )),
                          ),
                          size20,
                        ],
                      )
                : Column(
                    children: [
                      size10,
                      GestureDetector(
                        onTap: () {
                          Navigator.pushNamed(
                              context, RequestWithdrawalPage.routeName);
                        },
                        child: Container(
                            margin: EdgeInsets.only(left: 10, right: 10),
                            alignment: Alignment.centerLeft,
                            width: SizeConfig.screenWidth,
                            decoration: BoxDecoration(
                              borderRadius: new BorderRadius.all(
                                const Radius.circular(5.0),
                              ),
                              color: kPrimaryColor,
                              //borderRadius: BorderRadius.all(Radius.circular(5)),
                            ),
                            child: Container(
                              alignment: Alignment.center,
                              padding: EdgeInsets.only(
                                  left: 10, right: 10, top: 8, bottom: 8),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Icon(
                                    Icons.add_circle_outline_sharp,
                                    color: kWhiteColor,
                                  ),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  Text(
                                    'Request Withdrawal',
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(
                                        color: kWhiteColor,
                                        fontWeight: FontWeight.bold,
                                        fontSize: 16),
                                  ),
                                ],
                              ),
                            )),
                      ),
                      size20,
                    ],
                  )),
      ],
    );
  }

  buildSecoundHomeImage() {
    return Center(
      child: Column(
        children: [
          Container(
            width: 180,
            height: 180,
            child: Hero(
              tag: 'layer',
              child: Image.asset(kImageDir + 'layer1.png'),
            ),
          ),
          Container(
            alignment: Alignment.center,
            child: Text(
              'Selamat Datang di Orbitrade!',
              style: kBoldStyle.copyWith(color: Colors.black, fontSize: 18),
            ),
          ),
          size5,
          Container(
            alignment: Alignment.center,
            child: Text(
              'Yuk mulai trading hari ini',
              style: kBoldStyle.copyWith(
                  color: Colors.grey.withOpacity(.6),
                  fontWeight: FontWeight.w600),
            ),
          ),
          size10,
          Container(
            margin: EdgeInsets.only(left: 12, right: 12),
            child: Padding(
              padding: EdgeInsets.symmetric(
                vertical: getProportionateScreenHeight(20),
              ),
              child: DefaultBtn(
                width: SizeConfig.screenWidth,
                title: 'Tambah Rekening Bank',
                onPress: () async {
                  Navigator.pushNamed(context, BankSettingPage.routeName);
                },
              ),
            ),
          ),
        ],
      ),
    );
  }

  buildHomeCasler() {
    return Column(
      children: [
        Container(
          alignment: Alignment.centerLeft,
          margin: EdgeInsets.only(left: 10),
          child: Text(
            'Cashier',
            style: kRegularText.copyWith(
                color: Colors.black, fontWeight: FontWeight.w600),
          ),
        ),
        Container(
          width: MediaQuery.of(context).size.width,
          height: 50,
          child: ListView.builder(
              shrinkWrap: true,
              scrollDirection: Axis.horizontal,
              itemCount: cashlierList.length,
              physics: ScrollPhysics(),
              itemBuilder: (context, index) {
                return Padding(
                  padding: EdgeInsets.all(5),
                  child: GestureDetector(
                    onTap: () {
                      setState(() {
                        _selectedIndex = index;
                      });
                    },
                    child: Container(
                        alignment: Alignment.center,
                        decoration: BoxDecoration(
                          borderRadius: new BorderRadius.all(
                            const Radius.circular(5.0),
                          ),
                          color: _selectedIndex == index
                              ? kSecondaryColor
                              : Colors.transparent,
                          //borderRadius: BorderRadius.all(Radius.circular(5)),
                        ),
                        child: Container(
                          padding: EdgeInsets.only(left: 10, right: 10),
                          child: Text(
                            cashlierList[index],
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(
                                color: _selectedIndex == index
                                    ? kPrimaryColor
                                    : Colors.grey,
                                fontWeight: FontWeight.bold),
                          ),
                        )),
                  ),
                );
              }),
        ),
        Container(
          alignment: Alignment.centerLeft,
          margin: EdgeInsets.only(left: 10, right: 10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                child: Text(
                  'Nominal',
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                      color: Colors.grey, fontWeight: FontWeight.bold),
                ),
              ),
              Container(
                child: Text(
                  'Account ID',
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                      color: Colors.grey, fontWeight: FontWeight.bold),
                ),
              ),
              Container(
                margin: EdgeInsets.only(right: 25),
                alignment: Alignment.center,
                child: Text(
                  'Status',
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                      color: Colors.grey, fontWeight: FontWeight.bold),
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: EdgeInsets.only(left: 10, right: 10, top: 10),
          child: _selectedIndex == 0
              ? ListView.builder(
                  shrinkWrap: true,
                  scrollDirection: Axis.vertical,
                  itemCount: 5,
                  physics: NeverScrollableScrollPhysics(),
                  itemBuilder: (context, index) {
                    return GestureDetector(
                      onTap: () {
                        /* Navigator.push(context,
                            MaterialPageRoute(builder: (context) {
                          return DepositDetailsPage(
                            name: 'Deposit' + ' #199056',
                          );
                        }));*/
                      },
                      child: Column(
                        children: [
                          Row(
                            children: [
                              Expanded(
                                flex: 3,
                                child: Container(
                                  alignment: Alignment.center,
                                  child: Text(
                                    'IDR 50.000.000',
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                    style: kRegularText.copyWith(
                                        color: Colors.black,
                                        fontWeight: FontWeight.w600),
                                  ),
                                ),
                              ),
                              Expanded(
                                flex: 3,
                                child: Container(
                                  alignment: Alignment.center,
                                  child: Text(
                                    '99987653',
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                    style: kRegularText.copyWith(
                                        color: Colors.black,
                                        fontWeight: FontWeight.w600),
                                  ),
                                ),
                              ),
                              Expanded(
                                flex: 3,
                                child: Container(
                                  alignment: Alignment.center,
                                  child: Text(
                                    index == 4 ? 'Waiting' : 'Approved',
                                    maxLines: 1,
                                    textAlign: TextAlign.left,
                                    overflow: TextOverflow.ellipsis,
                                    style: kRegularText.copyWith(
                                        color: index == 4
                                            ? kSecondaryColor
                                            : khomeSecCardColor,
                                        fontWeight: FontWeight.w600),
                                  ),
                                ),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 25,
                          )
                        ],
                      ),
                    );
                  })
              : _selectedIndex == 1
                  ? ListView.builder(
                      itemCount: depositModelData.length,
                      shrinkWrap: true,
                      physics: NeverScrollableScrollPhysics(),
                      itemBuilder: (_, index) {
                        return GestureDetector(
                          onTap: () {
                            /* Navigator.push(context,
                                MaterialPageRoute(builder: (context) {
                              return DepositDetailsPage(
                                name: 'Internal Transfer' + ' #199056',
                              );
                            }));*/
                          },
                          child: Container(
                            height: 50,
                            margin: EdgeInsets.symmetric(vertical: 5.0),
                            padding: EdgeInsets.symmetric(horizontal: 5.0),
                            decoration: containerBoxDecoration(
                                borderRadius: 5.0,
                                color: depositModelData[index].approved ==
                                        "Waiting"
                                    ? kSecondaryColor
                                    : depositModelData[index].approved ==
                                            "Rejected"
                                        ? kPrimaryColor
                                        : kSuccessColor),
                            child: Row(
                              mainAxisAlignment: mainAxisSpaceBetween,
                              children: [
                                Text(
                                  '${depositModelData[index].nominal}',
                                  style: kBoldStyle.copyWith(
                                      color: kWhiteColor,
                                      fontWeight: FontWeight.w600),
                                ),
                                Text(
                                  '${depositModelData[index].accountID}',
                                  style: kBoldStyle.copyWith(
                                      color: kWhiteColor,
                                      fontWeight: FontWeight.w600),
                                ),
                                Text(
                                  '${depositModelData[index].approved}',
                                  style: kBoldStyle.copyWith(
                                      color: kWhiteColor,
                                      fontWeight: FontWeight.w600),
                                ),
                              ],
                            ),
                          ),
                        );
                      },
                    )
                  : ListView.builder(
                      shrinkWrap: true,
                      scrollDirection: Axis.vertical,
                      itemCount: 5,
                      physics: NeverScrollableScrollPhysics(),
                      itemBuilder: (context, index) {
                        return GestureDetector(
                          onTap: () {
                            /*Navigator.push(context,
                                MaterialPageRoute(builder: (context) {
                              return DepositDetailsPage(
                                name: 'Withdrawal' + ' #199056',
                              );
                            }));*/
                          },
                          child: Column(
                            children: [
                              Row(
                                children: [
                                  Expanded(
                                    flex: 3,
                                    child: Container(
                                      alignment: Alignment.center,
                                      child: Text(
                                        'IDR 50.000.000',
                                        maxLines: 1,
                                        overflow: TextOverflow.ellipsis,
                                        style: kRegularText.copyWith(
                                            color: Colors.black,
                                            fontWeight: FontWeight.w600),
                                      ),
                                    ),
                                  ),
                                  Expanded(
                                    flex: 3,
                                    child: Container(
                                      alignment: Alignment.center,
                                      child: Text(
                                        '99987653',
                                        maxLines: 1,
                                        overflow: TextOverflow.ellipsis,
                                        style: kRegularText.copyWith(
                                            color: Colors.black,
                                            fontWeight: FontWeight.w600),
                                      ),
                                    ),
                                  ),
                                  Expanded(
                                    flex: 3,
                                    child: Container(
                                      alignment: Alignment.center,
                                      child: Text(
                                        index == 4 ? 'Waiting' : 'Approved',
                                        maxLines: 1,
                                        textAlign: TextAlign.left,
                                        overflow: TextOverflow.ellipsis,
                                        style: kRegularText.copyWith(
                                            color: index == 4
                                                ? kSecondaryColor
                                                : khomeSecCardColor,
                                            fontWeight: FontWeight.w600),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(
                                height: 25,
                              )
                            ],
                          ),
                        );
                      }),
        ),
        GestureDetector(
          onTap: () {
            Navigator.pushNamed(context, RequestDepositPage.routeName);
          },
          child: Container(
              margin: EdgeInsets.only(left: 10, right: 10),
              alignment: Alignment.centerLeft,
              width: SizeConfig.screenWidth,
              decoration: BoxDecoration(
                borderRadius: new BorderRadius.all(
                  const Radius.circular(5.0),
                ),
                color: kSecondaryColor,
                //borderRadius: BorderRadius.all(Radius.circular(5)),
              ),
              child: Container(
                alignment: Alignment.center,
                padding:
                    EdgeInsets.only(left: 10, right: 10, top: 8, bottom: 8),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(
                      Icons.add_circle_outline_sharp,
                      color: kPrimaryColor,
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Text(
                      'Request Deposit',
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                          color: kPrimaryColor,
                          fontWeight: FontWeight.bold,
                          fontSize: 16),
                    ),
                  ],
                ),
              )),
        )
      ],
    );
  }
}
