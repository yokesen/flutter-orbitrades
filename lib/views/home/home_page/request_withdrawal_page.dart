import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:orbitrade/controller/bank_setting_controller.dart';
import 'package:orbitrade/controller/withdrawal_data_controller.dart';
import 'package:orbitrade/services/network_services.dart';
import 'package:orbitrade/sheared/buildDepositAppBar.dart';
import 'package:orbitrade/sheared/custom_loader.dart';
import 'package:orbitrade/sheared/input_form_widget.dart';
import 'package:orbitrade/views/home/home_page/request_berhasil_page.dart';

import '../../../constants.dart';
import '../../../size_config.dart';

class RequestWithdrawalPage extends StatefulWidget {
  static const routeName = 'request_withdrawal_page';
  @override
  _yourPageState createState() => _yourPageState();
}

class _yourPageState extends State<RequestWithdrawalPage> {
  var bankC = Get.put(BankSettingController());
  var _withdrawalDataCon = Get.put(WithdrawalDataController());
  TextEditingController _amountController = TextEditingController();
  FocusNode amountFocusNode;
  @override
  void initState() {
    // TODO: implement initState
    amountFocusNode = FocusNode();
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    amountFocusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildDepositAppBar(
          context: context,
          title: 'Withdrawal',
          onPress: () async {
            if (_amountController.text.isEmpty) {
              Get.snackbar('Null', 'Please enter amount');
            } else {
              var p = await NetworkServices().requestWithdrawal(
                context: context,
                metatrader: '20202020',
                tipe_deposit: '10',
                bank_name: bankC.myBankData.data.bankName ?? " ",
                account_name: bankC.myBankData.data.accountName ?? " ",
                account_number: bankC.myBankData.data.accountNumber ?? " ",
                amount: _amountController.text ?? " ",
              );
              Map<String, dynamic> js = p;
              if (p['success'] == true) {
                Navigator.of(context).pop();
                //fetch new my withdrawal data
                _withdrawalDataCon.fetchWithdrawalData();
                Navigator.pushNamed(context, RequestBerhasilPage.routeName);
                Get.snackbar('Success', "Withdrawal request successfully!");
              } else {
                Navigator.of(context).pop();
                print(p['message']);
                Get.snackbar('Error', p['message']);
              }
            }
          }),
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(
            horizontal: getProportionateScreenWidth(20),
          ),
          child: Obx(() {
            if (bankC.isLoading.value) {
              return Center(child: CustomLoader());
            } else {
              return Column(
                children: [
                  size20,
                  Container(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      'Dari Account Trading',
                      style: kRegularText2.copyWith(
                        color: kBlackColor,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Container(
                    width: SizeConfig.screenWidth,
                    alignment: Alignment.center,
                    padding: EdgeInsets.all(16),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(5),
                        border: Border.all(
                          width: 1,
                          color: Colors.grey,
                        )),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          '5001236788',
                          style: kRegularText2.copyWith(
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        Icon(Icons.keyboard_arrow_down_outlined),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Container(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      'Nilai Transfer',
                      style: kRegularText2.copyWith(
                        color: kBlackColor,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        width: 65,
                        alignment: Alignment.center,
                        padding: EdgeInsets.all(16),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(5),
                            border: Border.all(
                              width: 1,
                              color: amountFocusNode.hasFocus
                                  ? kPrimaryColor
                                  : Colors.grey,
                            )),
                        child: Text(
                          kUSD,
                          style: kRegularText2.copyWith(
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Container(
                        width: SizeConfig.screenWidth - 120,
                        child: FocusScope(
                          child: Focus(
                            focusNode: amountFocusNode,
                            onFocusChange: (node) {
                              setState(() {});
                            },
                            child: InputFormWidget(
                              keyType: TextInputType.number,
                              fieldController: _amountController,
                              validation: (value) {
                                if (value.isEmpty) {
                                  return kInvalidNumberError;
                                }else if(value.length < 1){
                                  return 'Minimum withdrawal limit 1 USD';
                                }
                                return null;
                              },
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Container(
                    width: SizeConfig.screenWidth,
                    decoration: BoxDecoration(
                      color: kPrimaryColor,
                      borderRadius: new BorderRadius.all(
                        const Radius.circular(10.0),
                      ),
                    ),
                    child: Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Container(
                              alignment: Alignment.centerLeft,
                              margin: EdgeInsets.only(left: 10, top: 7),
                              child: Text(
                                'Detail Keterangan Transfer',
                                style:
                                    kRegularText.copyWith(color: Colors.white),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 15,
                        ),
                        Container(
                          alignment: Alignment.bottomCenter,
                          child: Column(
                            children: [
                              Container(
                                alignment: Alignment.centerLeft,
                                margin: EdgeInsets.only(left: 10),
                                child: Text(
                                  "Metode:",
                                  style: TextStyle(
                                      color: Colors.grey[400], fontSize: 11),
                                ),
                              ),
                              SizedBox(
                                height: 2,
                              ),
                              Container(
                                alignment: Alignment.centerLeft,
                                margin: EdgeInsets.only(left: 10),
                                child: Text(
                                  'Transfer Antar Bank',
                                  style: kRegularText.copyWith(
                                      fontSize: 16, color: Colors.white),
                                ),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              Container(
                                alignment: Alignment.centerLeft,
                                margin: EdgeInsets.only(left: 10),
                                child: Text(
                                  "Bank:",
                                  style: TextStyle(
                                      color: Colors.grey[400], fontSize: 11),
                                ),
                              ),
                              SizedBox(
                                height: 2,
                              ),
                              Container(
                                alignment: Alignment.centerLeft,
                                margin: EdgeInsets.only(left: 10),
                                child: Text(
                                  bankC.myBankData.data.bankName ?? " ",
                                  style: kRegularText.copyWith(
                                      fontSize: 16, color: Colors.white),
                                ),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              Container(
                                alignment: Alignment.centerLeft,
                                margin: EdgeInsets.only(left: 10),
                                child: Text(
                                  "Nomor Rekening:",
                                  style: TextStyle(
                                      color: Colors.grey[400], fontSize: 11),
                                ),
                              ),
                              SizedBox(
                                height: 2,
                              ),
                              Container(
                                alignment: Alignment.centerLeft,
                                margin: EdgeInsets.only(left: 10),
                                child: Text(
                                  bankC.myBankData.data.accountNumber ?? " ",
                                  style: kRegularText.copyWith(
                                      fontSize: 16, color: Colors.white),
                                ),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              Container(
                                alignment: Alignment.centerLeft,
                                margin: EdgeInsets.only(left: 10),
                                child: Text(
                                  "Nama Rekening:",
                                  style: TextStyle(
                                      color: Colors.grey[400], fontSize: 11),
                                ),
                              ),
                              SizedBox(
                                height: 2,
                              ),
                              Container(
                                alignment: Alignment.centerLeft,
                                margin: EdgeInsets.only(left: 10),
                                child: Text(
                                  bankC.myBankData.data.accountName ?? " ",
                                  style: kRegularText.copyWith(
                                      fontSize: 16, color: Colors.white),
                                ),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              Container(
                                alignment: Alignment.centerLeft,
                                margin: EdgeInsets.only(left: 10),
                                child: Text(
                                  "Batas Kadaluarsa::",
                                  style: TextStyle(
                                      color: Colors.grey[400], fontSize: 11),
                                ),
                              ),
                              SizedBox(
                                height: 2,
                              ),
                              Container(
                                alignment: Alignment.centerLeft,
                                margin: EdgeInsets.only(left: 10),
                                child: Text(
                                  bankC.myBankData.data.updatedAt ??
                                      '01-08-2021 12:00 WIB',
                                  style: kRegularText.copyWith(
                                      fontSize: 16, color: Colors.white),
                                ),
                              ),
                              SizedBox(
                                height: 5,
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 15,
                  ),
                ],
              );
            }
          }),
        ),
      ),
    );
  }
}
