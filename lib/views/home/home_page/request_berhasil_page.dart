import 'package:flutter/material.dart';
import 'package:orbitrade/sheared/default_btn.dart';

import '../../../constants.dart';
import '../../../size_config.dart';
import '../Home.dart';

class RequestBerhasilPage extends StatelessWidget {
  static const routeName = 'request_berhasil_page';
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      child: Scaffold(
        backgroundColor: Colors.white,
        body: Column(
          children: [
            Expanded(
              flex: 8,
              child: Column(
                children: [
                  SizedBox(
                    height: getProportionateScreenHeight(100),
                  ),
                  Center(
                    child: Icon(
                      Icons.check_circle,
                      size: 82.0,
                      color: khomeSecCardColor,
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Container(
                    child: Text(
                      'Request Berhasil',
                      style: kHeadLine2.copyWith(color: kPrimaryColor),
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(
                      horizontal: getProportionateScreenWidth(20),
                    ),
                    alignment: Alignment.center,
                    child: Text(
                      'Selamat! Request Deposit Kamu berhasil. Silahkan menunggu konfirmasi Admin untuk menyetujui Requestmu.',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          color: Colors.grey.withOpacity(.5),
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                ],
              ),
            ),
            Expanded(
              flex: 2,
              child: Column(
                children: [
                  Container(
                    margin: EdgeInsets.only(left: 10, right: 10, top: 20),
                    child: Padding(
                      padding: EdgeInsets.symmetric(
                        vertical: getProportionateScreenHeight(20),
                      ),
                      child: DefaultBtn(
                        width: SizeConfig.screenWidth,
                        title: 'Selesai',
                        onPress: () {
                          Navigator.of(context).pushNamedAndRemoveUntil(
                              Home.routeName, (Route<dynamic> route) => false);
                        },
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
      onWillPop: () => Navigator.of(context).pushNamedAndRemoveUntil(
          Home.routeName, (Route<dynamic> route) => false),
    );
  }
}
