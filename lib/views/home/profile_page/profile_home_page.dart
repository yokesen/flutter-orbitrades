import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:orbitrade/constants.dart';
import 'package:orbitrade/controller/mt4_account_data_controller.dart';
import 'package:orbitrade/controller/user_data_controller.dart';
import 'package:orbitrade/services/network_services.dart';
import 'package:orbitrade/sheared/custom_loader.dart';
import 'package:orbitrade/size_config.dart';
import 'package:orbitrade/views/auth/login.dart';
import 'package:orbitrade/views/home/deposit_request/deposit_request_screen.dart';
import 'package:orbitrade/views/home/profile_page/account_settings/account_setting_page.dart';
import 'package:orbitrade/views/home/profile_page/bankSetting/bank_setting_page.dart';
import 'package:orbitrade/views/home/profile_page/referral/referral_first_page.dart';

import '../../../main.dart';
import 'deposit/deposit_page.dart';
import 'internal_transfer/internal_transfer_page.dart';
import 'mt4_account/classOne.dart';
import 'mt4_account/mt4_account_create_page.dart';
import 'withdrawl/withdrawl_page.dart';

class ProfileHomePage extends StatefulWidget {
  static const routeName = 'profile_page';

  const ProfileHomePage({Key key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<ProfileHomePage> {
  final userDataController = Get.put(UserDataController());
  final mt4AccountController = Get.put(Mt4AccountDataController());

  final picker = ImagePicker();
  File firstImage;
  dynamic imagePath;
  @override
  void initState() {
    // TODO: implement initState
    mt4AccountController.fetchMyAccount();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Scaffold(
        backgroundColor: kWhiteColor,
        appBar: AppBar(
          elevation: 0.0,
          brightness: Brightness.light,
          backgroundColor: Colors.white,
          title: textRoboto("Profile", kBlackColor, fontWeight: weightBold),
        ),
        body: Container(
          height: size.height,
          width: size.width,
          //padding: EdgeInsets.symmetric(horizontal: 15.0),
          child: Obx(() {
            if (userDataController.isLoading.value) {
              return CustomLoader();
            } else {
              if (mt4AccountController.isLoading.value) {
                return CustomLoader();
              } else {
                return SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: crossAxisCenter,
                    children: [
                      size10,
                      buildProfileHeader(size),
                      SizedBox(height: 30.0),
                      size10,
                      buildMtAccount(),
                      size20,
                      Container(
                        height: 10,
                        color: Colors.grey.withOpacity(0.1),
                      ),
                      ProfilePageOptions(
                          title: "Deposit",
                          imageURL: kImageDir + 'download.svg',
                          callback: () => Navigator.pushNamed(
                              context, DepositPage.routeName)
                          // callback: () => Navigator.pushNamed(context, QuizQuestionPage.routeName)
                          ),

                      Container(
                        margin: EdgeInsets.symmetric(horizontal: 10.0),
                        height: 1,
                        color: kOrdinaryColor.withOpacity(0.1),
                      ),
                      ProfilePageOptions(
                          title: "Internal Transfer",
                          imageURL: kImageDir + 'repeat.svg',
                          callback: () => Navigator.pushNamed(
                              context, InternalTransferPage.routeName)),
                      Container(
                        margin: EdgeInsets.symmetric(horizontal: 10.0),
                        height: 1,
                        color: kOrdinaryColor.withOpacity(0.1),
                      ),
                      ProfilePageOptions(
                          title: "Withdrawl",
                          imageURL: kImageDir + 'withdraw.svg',
                          callback: () => Navigator.pushNamed(
                              context, WithdrawlPage.routeName)),
                      size10,
                      Container(
                        height: 10,
                        color: Colors.grey.withOpacity(0.1),
                      ),
                      //size10,
                      ProfilePageOptions(
                        title: "Data Pribadi",
                        imageURL: kImageDir + 'user.svg',
                        callback: () => Navigator.pushNamed(
                            context, DepositRequestScreen.routeName),
                      ),
                      Container(
                        margin: EdgeInsets.symmetric(horizontal: 10.0),
                        height: 1,
                        color: kOrdinaryColor.withOpacity(0.1),
                      ),
                      ProfilePageOptions(
                        title: "PengaTuran Bank",
                        imageURL: kImageDir + 'book-open.svg',
                        callback: () => Navigator.pushNamed(
                            context, BankSettingPage.routeName),
                      ),
                      Container(
                        margin: EdgeInsets.symmetric(horizontal: 10.0),
                        height: 1,
                        color: kOrdinaryColor.withOpacity(0.1),
                      ),
                      ProfilePageOptions(
                        title: "Documen Syrat",
                        imageURL: kImageDir + 'file-text.svg',
                      ),
                      ProfilePageOptions(
                        title: "Pengaturan Akun",
                        imageURL: kImageDir + 'file-text.svg',
                        callback: () => Navigator.pushNamed(
                            context, AccountSettingPage.routeName),
                      ),
                      ProfilePageOptions(
                        title: "Referral",
                        imageURL: kImageDir + 'referral.svg',
                        callback: () => Navigator.pushNamed(
                            context, ReferralFirstPage.routeName),
                      ),
                      size10,
                      Container(
                        height: 10,
                        color: Colors.grey.withOpacity(0.1),
                      ),
                      ProfilePageOptions(
                        title: "Log Out",
                        titleColor: kErrorColor,
                        imageURL: kImageDir + 'power.svg',
                        callback: () => _onLogout(context),
                      ),
                    ],
                  ),
                );
              }
            }
          }),
        ));
  }

  buildProfileHeader(Size size) {
    return Column(
      children: [
        Container(
          height: size.height / 7.5,
          decoration:
              BoxDecoration(shape: BoxShape.circle, color: kSecondaryColor),
          child: Center(
              child: GestureDetector(
            onTap: () {
              showModalSheet(context, cameraCallBack: () {
                userDataController.getProfileImage(ImageSource.camera, context);
                Navigator.of(context).pop();
              }, galleryCallBack: () {
                userDataController.getProfileImage(
                    ImageSource.gallery, context);
                Navigator.of(context).pop();
              });
            },
            child: Container(
                margin: EdgeInsets.only(left: 5, right: 5),
                //width: 65,
                //height: 12,
                alignment: Alignment.center,
                child: userDataController.profileImgFile != null
                    ? Container(
                        width: 130.0,
                        height: 130.0,
                        decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            image: DecorationImage(
                                image: FileImage(
                                    userDataController.profileImgFile),
                                fit: BoxFit.cover)),
                      )
                    : Container(
                        width: 130.0,
                        height: 130.0,
                        decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            image: DecorationImage(
                                image: NetworkImage(
                                    'https://st.depositphotos.com/1779253/5140/v/600/depositphotos_51405259-stock-illustration-male-avatar-profile-picture-use.jpg'),
                                fit: BoxFit.cover)),
                      )),
          )),
        ),
        size10,
        userDataController.dataResponse != null
            ? textRoboto(userDataController.dataResponse.user.name, kBlackColor,
                fontWeight: weightBold, fontSize: 22)
            : textRoboto('My Names', kBlackColor,
                fontWeight: weightBold, fontSize: 22),
        size5,
        textRoboto("Personal Account", kBlackColor.withOpacity(0.3),
            fontWeight: weight500, fontSize: 16),
      ],
    );
  }

  buildMtAccount() {
    return Column(
      children: [
        Container(
          margin: EdgeInsets.only(left: 10, right: 10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                alignment: Alignment.centerLeft,
                //margin: EdgeInsets.only(left: 10),
                child: Text(
                  'MT4 Account',
                  style: kRegularText.copyWith(
                      color: Colors.black, fontWeight: FontWeight.w600),
                ),
              ),
              GestureDetector(
                onTap: () {
                  /* Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => ClassOne()),
                  );*/
                },
                child: Container(
                  alignment: Alignment.centerLeft,
                  margin: EdgeInsets.only(left: 10),
                  child: Text(
                    'Lihat Semua',
                    style: kRegularText.copyWith(
                      color: kPrimaryColor,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
        Container(
            child: mt4AccountController.myAccount != null
                ? mt4AccountController.myAccount.response.length > 0
                    ? Container(
                        height: 140.0,
                        child: ListView(
                          scrollDirection: Axis.horizontal,
                          shrinkWrap: true,
                          physics: ScrollPhysics(),
                          children: [
                            ListView.builder(
                              scrollDirection: Axis.horizontal,
                              itemCount: mt4AccountController
                                          .myAccount.response.length >
                                      5
                                  ? 5
                                  : mt4AccountController
                                      .myAccount.response.length,
                              shrinkWrap: true,
                              physics: NeverScrollableScrollPhysics(),
                              itemBuilder: (_, index) {
                                return GestureDetector(
                                  onTap: () {
                                    Navigator.push(context,
                                        MaterialPageRoute(builder: (context) {
                                      return ClassOne(
                                        name: "",
                                        data: mt4AccountController
                                            .myAccount.response[index],
                                      );
                                    }));
                                  },
                                  child: Column(
                                    children: [
                                      Align(
                                        alignment: Alignment.centerLeft,
                                        child: Container(
                                          alignment: Alignment.centerLeft,
                                          margin: EdgeInsets.only(left: 10),
                                          width: 210.0,
                                          child: Column(
                                            children: [
                                              SizedBox(
                                                height: 10,
                                              ),
                                              Container(
                                                //margin: EdgeInsets.only(left: 10),
                                                height: 130.0,
                                                decoration: BoxDecoration(
                                                  color: khomeFirstCardColor,
                                                  borderRadius:
                                                      new BorderRadius.all(
                                                    const Radius.circular(10.0),
                                                  ),
                                                ),
                                                child: Column(
                                                  children: [
                                                    Row(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .spaceBetween,
                                                      children: [
                                                        Container(
                                                          alignment: Alignment
                                                              .centerLeft,
                                                          margin:
                                                              EdgeInsets.only(
                                                                  left: 10,
                                                                  top: 7),
                                                          child: Text(
                                                            mt4AccountController
                                                                        .myAccount
                                                                        .response[
                                                                            index]
                                                                        .typeAccount ==
                                                                    "1"
                                                                ? 'Recreation'
                                                                : mt4AccountController
                                                                            .myAccount
                                                                            .response[
                                                                                index]
                                                                            .typeAccount ==
                                                                        "2"
                                                                    ? "Profesional"
                                                                    : mt4AccountController.myAccount.response[index].typeAccount ==
                                                                            "3"
                                                                        ? "Syariah"
                                                                        : mt4AccountController.myAccount.response[index].typeAccount ==
                                                                                "4"
                                                                            ? "Signature"
                                                                            : "",
                                                            style: kBoldStyle
                                                                .copyWith(
                                                                    color:
                                                                        kPrimaryColor),
                                                          ),
                                                        ),
                                                        GestureDetector(
                                                          onTap: () {},
                                                          child: Container(
                                                              margin: EdgeInsets
                                                                  .only(
                                                                      right:
                                                                          8.0),
                                                              alignment: Alignment
                                                                  .centerRight,
                                                              child: mt4AccountController
                                                                          .myAccount
                                                                          .response[
                                                                              index]
                                                                          .status ==
                                                                      "waiting"
                                                                  ? Icon(
                                                                      Icons
                                                                          .info,
                                                                      color:
                                                                          kSecondaryColor,
                                                                      size: 16,
                                                                    )
                                                                  : Icon(
                                                                      Icons
                                                                          .check_circle,
                                                                      color: Colors
                                                                          .green,
                                                                      size: 16,
                                                                    )),
                                                        )
                                                      ],
                                                    ),
                                                    SizedBox(
                                                      height: 15,
                                                    ),
                                                    Container(
                                                      alignment: Alignment
                                                          .bottomCenter,
                                                      child: Column(
                                                        children: [
                                                          Container(
                                                            alignment: Alignment
                                                                .centerLeft,
                                                            margin:
                                                                EdgeInsets.only(
                                                                    left: 10),
                                                            child: Text(
                                                              "Server",
                                                              style: TextStyle(
                                                                  color: Colors
                                                                      .black,
                                                                  fontSize: 11),
                                                            ),
                                                          ),
                                                          SizedBox(
                                                            height: 2,
                                                          ),
                                                          Container(
                                                            alignment: Alignment
                                                                .centerLeft,
                                                            margin:
                                                                EdgeInsets.only(
                                                                    left: 10),
                                                            child: Text(
                                                              'MaxrichGroup-Real',
                                                              style: kRegularText
                                                                  .copyWith(
                                                                      color: Colors
                                                                          .black),
                                                            ),
                                                          ),
                                                          Container(
                                                            alignment: Alignment
                                                                .centerLeft,
                                                            margin:
                                                                EdgeInsets.only(
                                                                    left: 10),
                                                            child: Text(
                                                              "Login",
                                                              style: TextStyle(
                                                                  color: Colors
                                                                      .black,
                                                                  fontSize: 11),
                                                            ),
                                                          ),
                                                          SizedBox(
                                                            height: 2,
                                                          ),
                                                          Row(
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .spaceBetween,
                                                            children: [
                                                              Container(
                                                                alignment: Alignment
                                                                    .centerLeft,
                                                                margin: EdgeInsets
                                                                    .only(
                                                                        left:
                                                                            10),
                                                                child: Text(
                                                                  '${mt4AccountController.myAccount.response[index].status}',
                                                                  style: kRegularText
                                                                      .copyWith(
                                                                          color:
                                                                              Colors.black),
                                                                ),
                                                              ),
                                                              Container(
                                                                margin: EdgeInsets
                                                                    .only(
                                                                        right:
                                                                            8.0),
                                                                alignment: Alignment
                                                                    .centerRight,
                                                                child:
                                                                    Container(
                                                                        width: AppBar().preferredSize.height -
                                                                            15,
                                                                        child: Image
                                                                            .asset(
                                                                          '${kImageDir}white_logo.png',
                                                                        )),
                                                              )
                                                            ],
                                                          ),
                                                        ],
                                                      ),
                                                    )
                                                  ],
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                );
                              },
                            ),
                            Align(
                              alignment: Alignment.centerLeft,
                              child: GestureDetector(
                                onTap: () => Navigator.pushNamed(
                                    context, Mt4AccountCreatePage.routeName),
                                child: Container(
                                  alignment: Alignment.centerLeft,
                                  margin: EdgeInsets.only(right: 10),
                                  width: 125.0,
                                  //height: 130,
                                  child: Column(
                                    children: [
                                      SizedBox(
                                        height: 10,
                                      ),
                                      Container(
                                        alignment: Alignment.center,
                                        margin: EdgeInsets.only(left: 10),
                                        height: 130.0,
                                        decoration: BoxDecoration(
                                          color: kPrimaryColor,
                                          borderRadius: new BorderRadius.all(
                                            const Radius.circular(10.0),
                                          ),
                                        ),
                                        child: Column(
                                          mainAxisAlignment: mainAxisCenter,
                                          children: [
                                            Icon(
                                              Icons.add_circle_outline_sharp,
                                              color: Colors.white,
                                              size: 46,
                                            ),
                                            size10,
                                            Container(
                                              padding:
                                                  const EdgeInsets.symmetric(
                                                      horizontal: 1.0),
                                              alignment: Alignment.center,
                                              child: Text(
                                                'Request\nMT4 Account',
                                                textAlign: TextAlign.center,
                                                style: kRegularText.copyWith(
                                                    color: Colors.white),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      )
                    : Column(
                        children: [
                          size10,
                          GestureDetector(
                            onTap: () {
                              Navigator.pushNamed(
                                  context, Mt4AccountCreatePage.routeName);
                            },
                            child: Container(
                                margin: EdgeInsets.only(left: 10, right: 10),
                                alignment: Alignment.centerLeft,
                                width: SizeConfig.screenWidth,
                                decoration: BoxDecoration(
                                  borderRadius: new BorderRadius.all(
                                    const Radius.circular(5.0),
                                  ),
                                  color: kPrimaryColor,
                                  //borderRadius: BorderRadius.all(Radius.circular(5)),
                                ),
                                child: Container(
                                  alignment: Alignment.center,
                                  padding: EdgeInsets.only(
                                      left: 10, right: 10, top: 8, bottom: 8),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Icon(
                                        Icons.add_circle_outline_sharp,
                                        color: kWhiteColor,
                                      ),
                                      SizedBox(
                                        width: 10,
                                      ),
                                      Text(
                                        'Request MT4 Account',
                                        maxLines: 1,
                                        overflow: TextOverflow.ellipsis,
                                        style: TextStyle(
                                            color: kWhiteColor,
                                            fontWeight: FontWeight.bold,
                                            fontSize: 16),
                                      ),
                                    ],
                                  ),
                                )),
                          )
                        ],
                      )
                : Column(
                    children: [
                      size10,
                      GestureDetector(
                        onTap: () {
                          Navigator.pushNamed(
                              context, Mt4AccountCreatePage.routeName);
                        },
                        child: Container(
                            margin: EdgeInsets.only(left: 10, right: 10),
                            alignment: Alignment.centerLeft,
                            width: SizeConfig.screenWidth,
                            decoration: BoxDecoration(
                              borderRadius: new BorderRadius.all(
                                const Radius.circular(5.0),
                              ),
                              color: kPrimaryColor,
                              //borderRadius: BorderRadius.all(Radius.circular(5)),
                            ),
                            child: Container(
                              alignment: Alignment.center,
                              padding: EdgeInsets.only(
                                  left: 10, right: 10, top: 8, bottom: 8),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Icon(
                                    Icons.add_circle_outline_sharp,
                                    color: kWhiteColor,
                                  ),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  Text(
                                    'Request MT4 Account',
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(
                                        color: kWhiteColor,
                                        fontWeight: FontWeight.bold,
                                        fontSize: 16),
                                  ),
                                ],
                              ),
                            )),
                      )
                    ],
                  )),
      ],
    );
  }

  _onLogout(BuildContext context) {
    return showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: Text("Log Out"),
          content: Text("Are you sure want to Log Out?"),
          actions: <Widget>[
            FlatButton(
              child: Text("Cancel"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            FlatButton(
              child: Text("Yes, Log Out"),
              onPressed: () async {
                print("${prefs.getString("token")}");
                //call method
                var p = await NetworkServices().userLogout(
                  context: context,
                );
                Map<String, dynamic> js = p;
                if (p['success'] == false) {
                  Navigator.of(context).pop();
                  print(p['message']);
                  Get.snackbar('Error', p['message']);
                } else {
                  prefs.remove('token');
                  rgStatus = true;
                  Navigator.pushNamedAndRemoveUntil(
                      context, LogIn.routeName, (route) => false);
                  Get.snackbar('Success', 'Successfully logout your account!');
                }
                print('ok');
              },
            )
          ],
        );
      },
    );
  }

  Future showModalSheet(BuildContext context,
      {Function cameraCallBack, Function galleryCallBack}) {
    return Get.bottomSheet(
      Container(
        height: 180,
        padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
        decoration: BoxDecoration(
            color: kWhiteColor,
            borderRadius: BorderRadius.only(
                topRight: Radius.circular(15.0),
                topLeft: Radius.circular(15.0))),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            textRoboto("Upload Profile Picture", kBlackColor,
                fontSize: 20.0, fontWeight: FontWeight.w500),
            SizedBox(
              height: 20.0,
            ),
            GestureDetector(
              onTap: cameraCallBack,
              child: Row(
                children: [
                  Container(
                    height: 35,
                    width: 35,
                    decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: kOrdinaryColor.withOpacity(0.2)),
                    child: Center(
                      child: Icon(
                        Icons.camera,
                        color: kBlackColor,
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 10.0,
                  ),
                  textRoboto("From Camera", kBlackColor,
                      fontSize: 18.0, fontWeight: FontWeight.w500),
                ],
              ),
            ),
            SizedBox(
              height: 10.0,
            ),
            GestureDetector(
              onTap: galleryCallBack,
              child: Row(
                children: [
                  Container(
                    height: 35,
                    width: 35,
                    decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: kOrdinaryColor.withOpacity(0.2)),
                    child: Center(
                      child: Icon(
                        Icons.perm_media,
                        color: kBlackColor,
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 10.0,
                  ),
                  textRoboto("From Gallery", kBlackColor,
                      fontSize: 18.0, fontWeight: FontWeight.w500),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}

class ProfilePageOptions extends StatelessWidget {
  String title;
  String imageURL;
  Color titleColor;

  VoidCallback callback;

  ProfilePageOptions(
      {this.title, this.imageURL, this.titleColor, this.callback});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 15.0, vertical: 15.0),
      child: GestureDetector(
        onTap: callback,
        child: Row(
          //mainAxisAlignment: mainAxisStart,
          children: [
            ///Uncomment it later
            SvgPicture.asset(
              imageURL,
              height: getProportionateScreenWidth(20),
              width: getProportionateScreenWidth(40),
            ),
            width10,
            Text(
              '$title',
              style: kBoldStyle.copyWith(
                  color: titleColor ?? kBlackColor,
                  fontWeight: FontWeight.w500),
            ),
            Expanded(child: Container()),
            Icon(
              Icons.arrow_forward_ios,
              color: kBlackColor.withOpacity(0.1),
              size: 18.0,
            )
          ],
        ),
      ),
    );
  }
}
