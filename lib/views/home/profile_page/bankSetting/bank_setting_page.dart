import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:orbitrade/controller/bank_setting_controller.dart';
import 'package:orbitrade/sheared/custom_loader.dart';
import 'package:orbitrade/size_config.dart';
import 'package:orbitrade/views/home/deposit_request/widgets/info_row_eidt_widget.dart';

import '../../../../constants.dart';

class BankSettingPage extends StatefulWidget {
  static const routeName = 'bank_setting_page';

  @override
  _BankSettingPageState createState() => _BankSettingPageState();
}

class _BankSettingPageState extends State<BankSettingPage> {
  var bankC = Get.put(BankSettingController());

  var namaBankC = TextEditingController();
  var accoutNumberC = TextEditingController();
  var accoutNameC = TextEditingController();

  String _bankName = '';
  String _accountNumber = ' ';
  String _accountName = '';
  bool showHideCheck = true;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kWhiteColor,
      appBar: AppBar(
        elevation: 1.0,
        brightness: Brightness.light,
        centerTitle: true,
        backgroundColor: Colors.white,
        title:
            textRoboto("Pengaturan Bank", kBlackColor, fontWeight: weight500),
        iconTheme: IconThemeData(color: kBlackColor),
        actions: [
          Container(
            width: 100,
            padding: EdgeInsets.only(left: 10.0),
            child: Center(
              child: GestureDetector(
                  onTap: () {
                    bankC.isUbah.value == true
                        ? bankC.changeToSimpan()
                        : bankC.changeUbahToD();
                  },
                  child: Obx(
                    () => Text(
                      bankC.isUbah.value == true ? 'Ubah' : "Simpan",
                      style: kBoldStyle.copyWith(
                          color: kPrimaryColor, fontWeight: FontWeight.w600),
                    ),
                  )),
            ),
          )
        ],
      ),
      body: SingleChildScrollView(
        child: Padding(
            padding:
                const EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
            child: Obx(
              () {
                if(bankC.isLoading.value){
                  return CustomLoader();
                }else{
                  return Container(
                    width: SizeConfig.screenWidth,
                    height: SizeConfig.screenHeight,
                    child: bankC.isUbah.value
                        ? Column(
                      children: [
                        size10,
                        BankRowWidget(
                          title: "Nama Bank",
                          info: bankC.myBankData.data.bankName == null
                              ? "BCA"
                              : bankC.myBankData.data.bankName,
                        ),
                        Divider(),
                        BankRowWidget(
                          title: "Nomor Rekening",
                          info: bankC.myBankData.data.accountNumber == null
                              ? "5033003"
                              : bankC.myBankData.data.accountNumber,
                        ),
                        Divider(),
                        BankRowWidget(
                          title: "Nama Rekening",
                          info: bankC.myBankData.data.accountName == null
                              ? "Jacob Miller"
                              : bankC.myBankData.data.accountName,
                        ),
                      ],
                    )
                        : Column(
                      children: [
                        InfoEditRowWidget(
                          fieldController: namaBankC,
                          title: 'Nama Bank',
                          hint: bankC.myBankData.data.bankName == null
                              ? "BCA"
                              : bankC.myBankData.data.bankName,
                        ),
                        InfoEditRowWidget(
                          fieldController: accoutNumberC,
                          title: 'Nomor Rekening',
                          hint: bankC.myBankData.data.accountNumber == null
                              ? "1233388"
                              : bankC.myBankData.data.accountNumber,
                        ),
                        InfoEditRowWidget(
                          fieldController: accoutNameC,
                          title: 'Nomor Rekening',
                          hint: bankC.myBankData.data.accountName == null
                              ? "JacobMiller"
                              : bankC.myBankData.data.accountName,
                        ),
                      ],
                    ),
                  );
                }
              },
            )),
      ),
    );
  }

  /* void setBankInfo(TextEditingController namaBankC,
      TextEditingController accoutNumberC, TextEditingController accoutNameC2) {
    if (bankC.myBankData != null) {
      if (bankC.myBankData.data.bankName != null) {
        namaBankC.text = bankC.myBankData.data.bankName;
        _bankName = bankC.myBankData.data.bankName;
      }
      if (bankC.myBankData.data.accountNumber != null) {
        accoutNumberC.text = bankC.myBankData.data.accountNumber;
        _accountNumber = bankC.myBankData.data.accountNumber;
      }
      if (bankC.myBankData.data.accountName != null) {
        accoutNameC2.text = bankC.myBankData.data.accountName;
        _accountName = bankC.myBankData.data.accountName;
      }
      setState(() {});
    } else {}
  }*/
}

class BankRowWidget extends StatelessWidget {
  String title;
  String info;
  bool isIcon;

  BankRowWidget({this.title, this.info, this.isIcon});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10.0),
      child: Row(
        children: [
          Expanded(
            child: Text(
              title,
              style: kBoldStyle.copyWith(
                  color: kBlackColor, fontWeight: FontWeight.w600),
            ),
          ),
          Row(
            children: [
              Text(
                info,
                style: kBoldStyle.copyWith(
                    color: kBlackColor, fontWeight: FontWeight.w500),
              ),
              width5,
              Icon(
                isIcon ?? Icons.check_circle,
                color: kSuccessColor,
                size: 15.0,
              )
            ],
          )
        ],
      ),
    );
  }
}
