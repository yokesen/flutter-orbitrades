import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:orbitrade/sheared/default_btn.dart';

import '../../../../constants.dart';
import '../../../../size_config.dart';

class ReferralSecondPage extends StatefulWidget {
  static const routeName = 'referral_second_page';
  @override
  _yourPageState createState() => _yourPageState();
}

class _yourPageState extends State<ReferralSecondPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0.8,
        brightness: Brightness.light,
        backgroundColor: Colors.white,
        centerTitle: true,
        iconTheme: IconThemeData(color: kBlackColor),
        title: Text(
          'Referral',
          style: kAppBarText.copyWith(
              color: Colors.black, fontWeight: FontWeight.w600),
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            SizedBox(
              height: getProportionateScreenHeight(50),
            ),
            Center(
              child: Image.asset(kImageDir + 'referral_logo_small.png'),
            ),
            SizedBox(
              height: 30,
            ),
            Container(
              child: Text(
                'Bawa Teman, Dapat Cuan.',
                style: kHeadLine2.copyWith(color: Colors.black),
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Container(
              padding: EdgeInsets.symmetric(
                horizontal: getProportionateScreenWidth(20),
              ),
              alignment: Alignment.center,
              child: Text(
                'Dapatkan Nilai Deposit senilai \$5 setiap teman yang mendaftar dan deposit di Orbitrade menggunakan link referral kamu.',
                textAlign: TextAlign.center,
                style: TextStyle(
                    color: kOrdinaryColor, fontWeight: FontWeight.bold),
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Container(
              margin: EdgeInsets.symmetric(
                horizontal: getProportionateScreenWidth(20),
              ),
              alignment: Alignment.centerLeft,
              child: Text(
                'Link',
                style: kRegularText2.copyWith(
                  color: kPrimaryColor,
                  fontWeight: FontWeight.w600,
                ),
              ),
            ),
            size5,
            Container(
              width: SizeConfig.screenWidth,
              alignment: Alignment.center,
              margin: EdgeInsets.symmetric(
                horizontal: getProportionateScreenWidth(20),
              ),
              padding: EdgeInsets.all(16),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(5),
                  border: Border.all(
                    width: 1,
                    color: kPrimaryColor,
                  )),
              child: Text(
                'https://repo-ims-orbit.1129-1891.cyou/?ref',
                style: kRegularText2.copyWith(
                  fontWeight: FontWeight.w600,
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.symmetric(
                horizontal: getProportionateScreenWidth(20),
              ),
              width: SizeConfig.screenWidth,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    child: Padding(
                      padding: EdgeInsets.symmetric(
                        vertical: getProportionateScreenHeight(20),
                      ),
                      child: WhiteDefaultBtn(
                        width: SizeConfig.screenWidth / 2 - 30,
                        title: 'Copy Link',
                        onPress: () async {
                          Clipboard.setData(new ClipboardData(text: 'text'));
                          Get.snackbar("Success", "Referral link copied.");
                        },
                      ),
                    ),
                  ),
                  Container(
                    child: Padding(
                      padding: EdgeInsets.symmetric(
                        vertical: getProportionateScreenHeight(20),
                      ),
                      child: DefaultBtn(
                        width: SizeConfig.screenWidth / 2 - 30,
                        title: 'Share',
                        onPress: () async {},
                      ),
                    ),
                  ),
                ],
              ),
            ),
            size20,
            Container(
              margin: EdgeInsets.symmetric(
                horizontal: getProportionateScreenWidth(20),
              ),
              width: SizeConfig.screenWidth,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    child: Text(
                      'Teman yang diajak',
                      style: TextStyle(
                          color: kOrdinaryColor, fontWeight: FontWeight.bold),
                    ),
                  ),
                  Container(
                    child: Text(
                      '12',
                      style: kHeadLine2.copyWith(color: Colors.black),
                    ),
                  ),
                ],
              ),
            ),
            size5,
            Container(
              height: 100,
              margin: EdgeInsets.symmetric(
                horizontal: getProportionateScreenWidth(15),
              ),
              child: ListView.builder(
                scrollDirection: Axis.horizontal,
                itemCount: 10,
                shrinkWrap: true,
                physics: ScrollPhysics(),
                itemBuilder: (_, index) {
                  return Padding(
                    padding: EdgeInsets.symmetric(
                      horizontal: getProportionateScreenWidth(5),
                    ),
                    child: Column(
                      children: [
                        Container(
                          width: 60.0,
                          height: 60.0,
                          decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              image: DecorationImage(
                                  image: NetworkImage(
                                      'http://indonesiatatler.com/images/i/20180801183002-30226433_10155925311108127_7608152763746222080_n_resized_773x774.jpg'),
                                  fit: BoxFit.cover)),
                        ),
                        Container(
                          child: Text(
                            'Bawa',
                            style: kRegularText.copyWith(
                                fontSize: 15,
                                color: Colors.black,
                                fontWeight: FontWeight.w600),
                          ),
                        ),
                      ],
                    ),
                  );
                },
              ),
            ),
            size5,
          ],
        ),
      ),
    );
  }
}

class WhiteDefaultBtn extends StatelessWidget {
  final String title;
  final Function onPress;
  final bool isChange;
  final double radius;
  final double border;
  final Color textColor;
  final Color btnColor;
  final double width;
  WhiteDefaultBtn({
    @required this.title,
    @required this.width,
    this.textColor = kPrimaryColor,
    this.btnColor = kSecondaryColor,
    this.border = 0,
    this.onPress,
    this.isChange = false,
    this.radius = 30,
  });
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 2),
      decoration: BoxDecoration(
        color: Colors.white,
        border: Border.all(width: 2, color: kSecondaryColor),
        borderRadius: BorderRadius.circular(radius),
      ),
      width: width,
      child: FlatButton(
        child: Text(
          title,
          style: kRegularText.copyWith(
            color: textColor,
            fontWeight: FontWeight.w600,
          ),
        ),
        onPressed: onPress,
      ),
    );
  }
}
