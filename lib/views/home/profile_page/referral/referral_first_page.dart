import 'package:flutter/material.dart';
import 'package:orbitrade/views/home/profile_page/referral/referral_second_page.dart';

import '../../../../constants.dart';
import '../../../../size_config.dart';

class ReferralFirstPage extends StatefulWidget {
  static const routeName = 'referral_first_page';
  @override
  _yourPageState createState() => _yourPageState();
}

class _yourPageState extends State<ReferralFirstPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0.0,
        brightness: Brightness.light,
        centerTitle: true,
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: kBlackColor),
      ),
      body: Column(
        children: [
          Expanded(
            flex: 8,
            child: Column(
              children: [
                SizedBox(
                  height: getProportionateScreenHeight(50),
                ),
                Center(
                  child: Image.asset(kImageDir + 'reffer_logo_big.png'),
                ),
                SizedBox(
                  height: 30,
                ),
                Container(
                  child: Text(
                    'Tertarik jadi Referral?',
                    style: kHeadLine2.copyWith(color: Colors.black),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Container(
                  padding: EdgeInsets.symmetric(
                    horizontal: getProportionateScreenWidth(20),
                  ),
                  alignment: Alignment.center,
                  child: Text(
                    'Berjuta-juta manfaat bisa didapat dari mereferensikan teman. Untuk bergabung dalam program Member get Member maupun Business Partner.',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: kOrdinaryColor, fontWeight: FontWeight.bold),
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Container(
                  padding: EdgeInsets.symmetric(
                    horizontal: getProportionateScreenWidth(20),
                  ),
                  alignment: Alignment.center,
                  child: Text(
                    'Silahkan hubungi tim marketing kami.',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: kOrdinaryColor, fontWeight: FontWeight.bold),
                  ),
                ),
                SizedBox(
                  height: getProportionateScreenHeight(30),
                ),
                Container(
                  width: SizeConfig.screenWidth,
                  padding: EdgeInsets.symmetric(
                    vertical: 2,
                  ),
                  margin: EdgeInsets.symmetric(
                    horizontal: getProportionateScreenWidth(15),
                  ),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(25),
                    color: kSuccessColor,
                  ),
                  child: FlatButton(
                    onPressed: () {
                      Navigator.pushNamed(
                          context, ReferralSecondPage.routeName);
                    },
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Image.asset(kImageDir + 'w_image.png'),
                        SizedBox(
                          width: 10,
                        ),
                        Text(
                          'Hubungi Admin via Whatsapp',
                          style: kRegularText2.copyWith(
                            fontWeight: FontWeight.w600,
                            color: kWhiteColor,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
