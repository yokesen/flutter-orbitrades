import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:orbitrade/model/internal_transfer_data.dart';
import 'package:orbitrade/size_config.dart';

import '../../../../constants.dart';

class InternalTransDetailsPage extends StatefulWidget {
  static const routeName = 'internal_trans_deatils_page';
  const InternalTransDetailsPage({Key key, this.name, this.data})
      : super(key: key);
  final String name;
  final Datum data;
  @override
  _YourPageState createState() => _YourPageState();
}

class _YourPageState extends State<InternalTransDetailsPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kWhiteColor,
      appBar: AppBar(
        backgroundColor: kWhiteColor,
        elevation: 0.5,
        centerTitle: true,
        title: Text(
          widget.name,
          style: kBoldStyle.copyWith(
              color: kBlackColor, fontWeight: FontWeight.w600),
        ),
        iconTheme: IconThemeData(color: kBlackColor),
      ),
      body: Container(
        height: SizeConfig.screenHeight,
        width: SizeConfig.screenWidth,
        padding: EdgeInsets.symmetric(horizontal: 15.0),
        child: SingleChildScrollView(
          child: Column(
            children: [
              size20,
              buildDepositBody(),
              size20,
            ],
          ),
        ),
      ),
    );
  }

  buildDepositBody() {
    return Container(
      width: SizeConfig.screenWidth,
      padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 13.0),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(5.0),
          border: Border.all(
            color: kOrdinaryColor.withOpacity(0.2),
          )),
      child: Column(
        crossAxisAlignment: crossAxisStart,
        children: [
          Text(
            'Created Date',
            style: kBoldStyle.copyWith(
                color: kPrimaryColor, fontWeight: FontWeight.w600),
          ),
          size5,
          Text(
            '${DateFormat('dd - MMMM - yy H:m:s').format(widget.data.createdAt)} WIB',
            style: kBoldStyle.copyWith(
                color: kBlackColor, fontWeight: FontWeight.w600),
          ),
          size20,
          Text(
            'Respond Date',
            style: kBoldStyle.copyWith(
                color: kPrimaryColor, fontWeight: FontWeight.w600),
          ),
          size5,
          Text(
            '${DateFormat('dd - MMMM - yy H:m:s').format(widget.data.updatedAt)} WIB',
            style: kBoldStyle.copyWith(
                color: kBlackColor, fontWeight: FontWeight.w600),
          ),
          size20,
          Row(
            //mainAxisAlignment: mainAxisSpaceBetween,
            children: [
              Container(
                width: SizeConfig.screenWidth / 2.2,
                child: Column(
                  crossAxisAlignment: crossAxisStart,
                  children: [
                    Text(
                      'From',
                      style: kBoldStyle.copyWith(
                          color: kPrimaryColor, fontWeight: FontWeight.w600),
                    ),
                    size5,
                    Text(
                      widget.data.accountNumber,
                      style: kBoldStyle.copyWith(
                          color: kBlackColor, fontWeight: FontWeight.w600),
                    ),
                  ],
                ),
              ),
              Container(
                child: Column(
                  crossAxisAlignment: crossAxisStart,
                  children: [
                    Text(
                      'To',
                      style: kBoldStyle.copyWith(
                          color: kPrimaryColor, fontWeight: FontWeight.w600),
                    ),
                    size5,
                    Text(
                      widget.data.accountName,
                      style: kBoldStyle.copyWith(
                          color: kBlackColor, fontWeight: FontWeight.w600),
                    ),
                  ],
                ),
              )
            ],
          ),
          size20,
          Row(
            //mainAxisAlignment: mainAxisSpaceBetween,
            children: [
              Container(
                width: SizeConfig.screenWidth / 2.2,
                child: Column(
                  crossAxisAlignment: crossAxisStart,
                  children: [
                    Text(
                      'Description',
                      style: kBoldStyle.copyWith(
                          color: kPrimaryColor, fontWeight: FontWeight.w600),
                    ),
                    size5,
                    Text(
                      'Internal Transfer',
                      style: kBoldStyle.copyWith(
                          color: kBlackColor, fontWeight: FontWeight.w600),
                    ),
                  ],
                ),
              ),
              Container(
                child: Column(
                  crossAxisAlignment: crossAxisStart,
                  children: [
                    Text(
                      'Transfer Amount',
                      style: kBoldStyle.copyWith(
                          color: kPrimaryColor, fontWeight: FontWeight.w600),
                    ),
                    size5,
                    Text(
                      '$kUSD ${widget.data.amount}',
                      style: kBoldStyle.copyWith(
                          color: kBlackColor, fontWeight: FontWeight.w600),
                    ),
                  ],
                ),
              )
            ],
          ),
          size20,
          Row(
            //mainAxisAlignment: mainAxisSpaceBetween,
            children: [
              Container(
                child: Column(
                  crossAxisAlignment: crossAxisStart,
                  children: [
                    Text(
                      'Approved Amount',
                      style: kBoldStyle.copyWith(
                          color: kPrimaryColor, fontWeight: FontWeight.w600),
                    ),
                    size5,
                    Text(
                      '$kUSD ' + widget.data.approvedAmount.toString(),
                      style: kBoldStyle.copyWith(
                          color: kBlackColor, fontWeight: FontWeight.w600),
                    ),
                  ],
                ),
              )
            ],
          )
        ],
      ),
    );
  }
}
