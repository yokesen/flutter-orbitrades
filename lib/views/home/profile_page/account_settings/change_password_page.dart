import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:orbitrade/controller/account_settingd_controller.dart';

import '../../../../constants.dart';

class ChangePasswordPage extends StatefulWidget {
  static const routeName = 'change_pass_page';

  @override
  _ChangePasswordPageState createState() => _ChangePasswordPageState();
}

class _ChangePasswordPageState extends State<ChangePasswordPage> {
  var accountC = Get.put(AccountSettingController());

  var oldPassController = TextEditingController();
  var newPassController = TextEditingController();
  var confirmPassController = TextEditingController();
  final _formKey = GlobalKey<FormState>();


  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Scaffold(
      backgroundColor: kWhiteColor,
      appBar: AppBar(
        elevation: 1.0,
        brightness: Brightness.light,
        centerTitle: true,
        backgroundColor: Colors.white,
        title: textRoboto("Pengaturan Akun", kBlackColor, fontWeight: weight500),
        iconTheme: IconThemeData(color: kBlackColor),
        actions: [
          Container(
            width: 100,
            padding: EdgeInsets.only(left: 10.0),
            child: Center(
              child: GestureDetector(
                onTap: (){

                  if(newPassController.text!=confirmPassController.text){
                    Get.snackbar("Alert", "Password doesn not match");
                    return;
                  }else{
                    accountC.changePassword(context: context,password: confirmPassController.text);

                  }

                },
                child: Text(
                  'Selesai',
                  style: kBoldStyle.copyWith(color: kPrimaryColor, fontWeight: FontWeight.w600),
                ),
              ),
            ),
          )
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Form(
          key: _formKey,
          child: Column(
            children: [
              Row(
                children: [
                  Expanded(
                    flex: 1,
                    child: Text(
                      'Password Lama',
                      style: kBoldStyle.copyWith(color: kBlackColor, fontWeight: FontWeight.w600),
                    ),
                  ),
                  Expanded(
                      child: Obx(
                            () => TextFormField(
                          controller: oldPassController,
                          cursorColor: Colors.black,
                          obscureText: !accountC.oldShowPass.value,
                          textAlign: TextAlign.right,
                          keyboardType: TextInputType.text,

                          decoration: buildInputDecoration(
                              context: context,
                              callback: () {
                                accountC.oldShowPass.value = !accountC.oldShowPass.value;
                                FocusScope.of(context).unfocus();
                              },
                              value: accountC.oldShowPass.value),
                        ),
                      )),
                ],
              ),
              Row(
                children: [
                  Expanded(
                    flex: 1,
                    child: Text(
                      'Password Baru',
                      style: kBoldStyle.copyWith(color: kBlackColor, fontWeight: FontWeight.w600),
                    ),
                  ),
                  Expanded(
                      child: Obx(
                            () => TextFormField(
                          controller: newPassController,
                          cursorColor: Colors.black,
                          obscureText: !accountC.newShowPass.value,
                          textAlign: TextAlign.right,
                          keyboardType: TextInputType.text,
                          decoration: buildInputDecoration(
                              context: context,
                              callback: () {
                                accountC.newShowPass.value = !accountC.newShowPass.value;
                                FocusScope.of(context).unfocus();
                              },
                              value: accountC.newShowPass.value),
                        ),
                      )),
                ],
              ),
              Row(
                children: [
                  Expanded(
                    flex: 1,
                    child: Text(
                      'Konfirmasi Password',
                      style: kBoldStyle.copyWith(color: kBlackColor, fontWeight: FontWeight.w600),
                    ),
                  ),
                  Expanded(
                      child: Obx(
                            () => TextFormField(
                          controller: confirmPassController,
                          cursorColor: Colors.black,
                          obscureText: !accountC.confirmShowPass.value,
                          textAlign: TextAlign.right,
                          keyboardType: TextInputType.text,

                          decoration: buildInputDecoration(
                              context: context,
                              callback: () {
                                accountC.confirmShowPass.value = !accountC.confirmShowPass.value;
                                FocusScope.of(context).unfocus();
                              },
                              value: accountC.confirmShowPass.value),
                        ),
                      )),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  InputDecoration buildInputDecoration({BuildContext context, VoidCallback callback, bool value}) {
    return new InputDecoration(
        border: InputBorder.none,
        focusedBorder: InputBorder.none,
        enabledBorder: InputBorder.none,
        errorBorder: InputBorder.none,
        disabledBorder: InputBorder.none,
        suffixIcon: GestureDetector(
            onTap: callback,
            child: Icon(
              value ? Icons.visibility : Icons.visibility_off,
              color: kOrdinaryColor,size: 18.0,
            )),
        contentPadding: EdgeInsets.only(top: 16),
        hintText: "*********");
  }
}
