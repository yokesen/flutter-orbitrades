import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:orbitrade/size_config.dart';

import '../../../../constants.dart';

class PinSettingPage extends StatefulWidget {
  static const routeName = 'pin_setting_page';

  @override
  _PinSettingPageState createState() => _PinSettingPageState();
}

class _PinSettingPageState extends State<PinSettingPage> {
  String text = "";

  var controller = TextEditingController();

  var currentPin = ["", "", "", "", "", ""];
  TextEditingController pinOneController = TextEditingController();
  var pinTwoController = TextEditingController();
  var pinThreeController = TextEditingController();
  var pinFourController = TextEditingController();
  var pinFiveController = TextEditingController();
  var pinSixController = TextEditingController();

  var outLineInputBorder = OutlineInputBorder(
      borderRadius: BorderRadius.circular(10.0),
      borderSide: BorderSide(color: Colors.red));

  int pinIndex = 0;

  String _otp = "";
  var _otpSymbols = [
    "\u{25CB}", //U+1F534
    "\u{25CB}",
    "\u{25CB}",
    "\u{25CB}",
    "\u{25CB}",
    "\u{25CB}"
  ];

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  void _handleKeypadClick(String val) {
    setState(() {
      if (_otp.length < 6) {
        _otp = _otp + val;
        for (int i = 0; i < _otp.length; i++) _otpSymbols[i] = "\u{25CF}";
      }
    });
    print(val);
  }

  void _handleKeypadDel() {
    setState(() {
      if (_otp.length > 0) {
        _otp = _otp.substring(0, _otp.length - 1);
        for (int i = _otp.length; i < 6; i++) _otpSymbols[i] = "\u{25CB}";
      }
    });
  }

  void _handleSubmit() {
    if (_otp.length == 6)
      _scaffoldKey.currentState.showSnackBar(SnackBar(
        content: Text('Entered OTP is $_otp'),
      ));
    else
      _scaffoldKey.currentState.showSnackBar(SnackBar(
        content: Text('OTP has to be of 4 digits'),
        backgroundColor: Colors.red,
      ));
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Scaffold(
        backgroundColor: kWhiteColor,
        appBar: AppBar(
          elevation: 0.8,
          brightness: Brightness.light,
          centerTitle: true,
          backgroundColor: Colors.white,
          iconTheme: IconThemeData(color: kBlackColor),
        ),
        body: Column(
          crossAxisAlignment: crossAxisCenter,
          children: [
            SizedBox(
              height: 20,
            ),
            Expanded(
                child: Container(
              alignment: Alignment(0, 0.5),
              padding: EdgeInsets.symmetric(horizontal: 16.0),
              child: SingleChildScrollView(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    SvgPicture.asset(
                      kImageDir + 'pin_set.svg',
                      height: getProportionateScreenWidth(140),
                      width: getProportionateScreenWidth(200),
                    ),
                    SizedBox(
                      height: 50,
                    ),
                    Text(
                      'Masukkan PIN Lama Kamu',
                      style: kBoldStyle.copyWith(
                          color: kBlackColor,
                          fontSize: 22.0,
                          fontWeight: FontWeight.w600),
                    ),
                    SizedBox(height: 30.0),
                    buildPinRow(),
                  ],
                ),
              ),
            )),
            SizedBox(
              height: 50,
            ),
            buildNumberPad()
            //size20,
            /*SvgPicture.asset(
            kImageDir + 'pin_set.svg',
            height: getProportionateScreenWidth(140),
            width: getProportionateScreenWidth(200),
          ),*/
            /*SizedBox(height: 30.0,),
          Text(
            'Masukkan PIN Lama Kamu',
            style: kBoldStyle.copyWith(
                color: kBlackColor,fontSize: 22.0,
                fontWeight: FontWeight.w600),
          ),*/
          ],
        ));
  }

  buildNumberPad() {
    return Expanded(
        child: Container(
      alignment: Alignment.bottomCenter,
      color: Color(0xFFD1D5DB),
      child: Padding(
        padding: EdgeInsets.only(bottom: 20),
        child: Column(
          children: [
            size8,
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                KeyBoardNumber(
                    n: 1,
                    callback: () {
                      //  pinIndexSetUp("1");
                      _handleKeypadClick('1');
                    }),
                KeyBoardNumber(
                    n: 2,
                    callback: () {
                      _handleKeypadClick("2");
                    }),
                KeyBoardNumber(
                    n: 3,
                    callback: () {
                      _handleKeypadClick("3");
                    })
              ],
            ),
            size8,
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                KeyBoardNumber(
                    n: 4,
                    callback: () {
                      _handleKeypadClick("4");
                    }),
                KeyBoardNumber(
                    n: 5,
                    callback: () {
                      _handleKeypadClick("5");
                    }),
                KeyBoardNumber(
                    n: 6,
                    callback: () {
                      _handleKeypadClick("6");
                    })
              ],
            ),
            size8,
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                KeyBoardNumber(
                    n: 7,
                    callback: () {
                      _handleKeypadClick('7');
                    }),
                KeyBoardNumber(
                    n: 8,
                    callback: () {
                      _handleKeypadClick('8');
                    }),
                KeyBoardNumber(
                    n: 9,
                    callback: () {
                      //pinIndexSetUp("9");
                      _handleKeypadClick('9');
                    })
              ],
            ),
            size8,
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Container(
                  width: 60,
                  child: MaterialButton(
                    onPressed: null,
                    child: SizedBox(),
                  ),
                ),
                KeyBoardNumber(
                    n: 0,
                    callback: () {
                      //pinIndexSetUp("0");
                      _handleKeypadClick('0');
                    }),
                Container(
                  width: 60,
                  child: MaterialButton(
                    height: 60.0,
                    onPressed: () {
                      //clearPin();
                      _handleKeypadDel();
                    },
                    child: SvgPicture.asset(
                      kImageDir + 'backspace.svg',
                      height: 40.0,
                      width: 40.0,
                    ),
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    ));
  }

  clearPin() {
    if (pinIndex == 0)
      pinIndex = 0;
    else if (pinIndex == 6) {
      setPin(pinIndex, "");
      currentPin[pinIndex - 1] = "";
      pinIndex--;
    } else {
      setPin(pinIndex, "");
      currentPin[pinIndex - 1] = "";
      pinIndex--;
    }
  }

  pinIndexSetUp(String text) {
    if (pinIndex == 0)
      pinIndex = 1;
    else if (pinIndex < 6) pinIndex++;

    setPin(pinIndex, text);
    currentPin[pinIndex - 1] = text;
    String strPin = "";
    currentPin.forEach((element) {
      strPin += element;
    });
    if (pinIndex == 6) print(strPin);
  }

  setPin(int n, String text) {
    switch (n) {
      case 1:
        pinOneController.text = text;
        break;
      case 2:
        pinTwoController.text = text;
        break;
      case 3:
        pinThreeController.text = text;
        break;
      case 4:
        pinFourController.text = text;
        break;
      case 5:
        pinFiveController.text = text;
        break;
      case 6:
        pinSixController.text = text;
        break;
    }
  }

  buildExitButton() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        Padding(
          padding: EdgeInsets.all(8.0),
          child: MaterialButton(
            onPressed: () {},
            height: 50,
            minWidth: 50,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(50.0)),
            child: Icon(
              Icons.clear,
              color: Colors.white,
            ),
          ),
        )
      ],
    );
  }

  buildSecurityText() {
    return Text(
      "Security Pin",
      style: TextStyle(
          color: Colors.white70, fontSize: 21.0, fontWeight: FontWeight.bold),
    );
  }

  buildPinRow() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          _otpSymbols[0],
          style: kPinStyle,
        ),
        width10,
        Text(
          _otpSymbols[1],
          style: kPinStyle,
        ),
        width10,
        Text(
          _otpSymbols[2],
          style: kPinStyle,
        ),
        width10,
        Text(
          _otpSymbols[3],
          style: kPinStyle,
        ),
        width10,
        Text(
          _otpSymbols[4],
          style: kPinStyle,
        ),
        width10,
        Text(
          _otpSymbols[5],
          style: kPinStyle,
        ),
      ],
    );
  }

  final kPinStyle = TextStyle(
      color: kPrimaryColor, fontSize: 35, fontWeight: FontWeight.w200);
}

class KeyBoardNumber extends StatelessWidget {
  final int n;
  final VoidCallback callback;

  KeyBoardNumber({this.n, this.callback});

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 2.0,
      child: Container(
        width: SizeConfig.screenWidth / 3 - 10,
        height: 50,
        decoration: BoxDecoration(
            shape: BoxShape.rectangle,
            color: kWhiteColor,
            borderRadius: BorderRadius.all(Radius.circular(5.0)),
            boxShadow: [
              BoxShadow(
                color: kOrdinaryColor.withOpacity(0.1),
                spreadRadius: 5,
                blurRadius: 7,
                offset: Offset(0, 3), // changes position of shadow
              ),
            ]),
        alignment: Alignment.center,
        child: MaterialButton(
          minWidth: 50,
          onPressed: callback,
          height: 50.0,
          elevation: 10.0,
          child: Text(
            "$n",
            textAlign: TextAlign.center,
            style: TextStyle(
                fontSize: 24.0 * MediaQuery.of(context).textScaleFactor,
                color: kBlackColor,
                fontWeight: FontWeight.bold),
          ),
        ),
      ),
    );
  }
}

class PinNumber extends StatelessWidget {
  final TextEditingController textEditingController;
  final OutlineInputBorder outlineInputBorder;

  PinNumber({this.textEditingController, this.outlineInputBorder});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 40,
      width: 40,
      child: TextField(
        controller: textEditingController,
        enabled: false,
        obscureText: true,
        textAlign: TextAlign.center,
        decoration: InputDecoration(
            contentPadding: EdgeInsets.only(bottom: 3),
            filled: true,
            fillColor: Colors.white,
            border: outlineInputBorder),
        style: TextStyle(
          fontWeight: FontWeight.bold,
          fontSize: 30.0,
          color: kPrimaryColor,
        ),
      ),
    );
  }
}
