import 'package:flutter/material.dart';
import 'package:orbitrade/views/home/profile_page/account_settings/change_password_page.dart';
import 'package:orbitrade/views/home/profile_page/account_settings/pin_setting_page.dart';

import '../../../../constants.dart';
import '../profile_home_page.dart';

class AccountSettingPage extends StatefulWidget {

  static const routeName = 'account_setting_page';


  @override
  _AccountSettingPageState createState() => _AccountSettingPageState();
}

class _AccountSettingPageState extends State<AccountSettingPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kWhiteColor,
      appBar: AppBar(
        elevation: 1.0,
        brightness: Brightness.light,
        centerTitle: true,
        backgroundColor: Colors.white,
        title: textRoboto("Pengaturan Akun", kBlackColor, fontWeight: weight500),
        iconTheme: IconThemeData(color: kBlackColor),

      ),
      body: Column(
        children: [
          AccountOptions(
            title: "Ubah Password",
            callback: ()=>Navigator.pushNamed(
                context, ChangePasswordPage.routeName),
          ),
          Container(
            margin: EdgeInsets.symmetric(horizontal: 10.0),
            height: 1,
            color: kOrdinaryColor.withOpacity(0.1),
          ),
         /* AccountOptions(
            title: "Ubah Pin",
            callback: ()=>Navigator.pushNamed(
                context, PinSettingPage.routeName),
          ),
          Container(
            margin: EdgeInsets.symmetric(horizontal: 10.0),
            height: 1,
            color: kOrdinaryColor.withOpacity(0.1),
          ),
          AccountOptions(
            title: "Syarat dan Ketentuan",
            callback: ()=>Navigator.pushNamed(
                context, AccountSettingPage.routeName),
          ),
          Container(
            margin: EdgeInsets.symmetric(horizontal: 10.0),
            height: 1,
            color: kOrdinaryColor.withOpacity(0.1),
          ),
          AccountOptions(
            title: "Kebijakan Privasi",
            callback: ()=>Navigator.pushNamed(
                context, AccountSettingPage.routeName),
          ),*/
        ],
      ),
    );
  }



}


class AccountOptions extends StatelessWidget {
  String title;
  Color titleColor;

  VoidCallback callback;

  AccountOptions(
      {this.title, this.titleColor, this.callback});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 15.0, vertical: 15.0),
      child: GestureDetector(
        onTap: callback,
        child: Row(
          //mainAxisAlignment: mainAxisStart,
          children: [
            ///Uncomment it later
            //width10,
            Text(
              '$title',
              style: kBoldStyle.copyWith(
                  color: titleColor ?? kBlackColor,
                  fontWeight: FontWeight.w500),
            ),
            Expanded(child: Container()),
            Icon(
              Icons.arrow_forward_ios,
              color: kBlackColor.withOpacity(0.1),
              size: 18.0,
            )
          ],
        ),
      ),
    );
  }
}

