import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:orbitrade/model/my_deposit_data.dart';
import '../../../../constants.dart';
import '../../../../size_config.dart';

class DepositDetailsPage extends StatefulWidget {
  static const routeName = 'deposit_details_page';
  const DepositDetailsPage({Key key, this.name, this.data}) : super(key: key);
  final String name;
  final Datum data;
  @override
  _DepositDetailsPageState createState() => _DepositDetailsPageState();
}

class _DepositDetailsPageState extends State<DepositDetailsPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kWhiteColor,
      appBar: AppBar(
        backgroundColor: kWhiteColor,
        elevation: 0.5,
        centerTitle: true,
        title: Text(
          widget.name,
          style: kBoldStyle.copyWith(
              color: kBlackColor, fontWeight: FontWeight.w600),
        ),
        iconTheme: IconThemeData(color: kBlackColor),
      ),
      body: Container(
        height: SizeConfig.screenHeight,
        width: SizeConfig.screenWidth,
        padding: EdgeInsets.symmetric(horizontal: 15.0),
        child: SingleChildScrollView(
          child: Column(
            children: [
              size20,
              buildDepositBody(),
              size20,
              buildBankDetails(),
              size20,
            ],
          ),
        ),
      ),
    );
  }

  buildBankDetails() {
    return Container(
      width: SizeConfig.screenWidth,
      padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 13.0),
      decoration:
      containerBoxDecoration(borderRadius: 5.0, color: kPrimaryColor),
      child: Column(
        crossAxisAlignment: crossAxisStart,
        children: [
          Text(
            'Payment Detail',
            style: kBoldStyle.copyWith(
                color: kWhiteColor,
                fontWeight: FontWeight.w600,
                fontSize: 20.0),
          ),
          size20,
          Text(
            'Bank:',
            style: kBoldStyle.copyWith(
                color: kWhiteColor.withOpacity(0.6),
                fontWeight: FontWeight.w600,
                fontSize: 16.0),
          ),
          size5,
          Text(
            widget.data.fromBank ?? "",
            style: kBoldStyle.copyWith(
                color: kWhiteColor,
                fontWeight: FontWeight.w600,
                fontSize: 18.0),
          ),
          size20,
          Text(
            'Nomor Rekening:',
            style: kBoldStyle.copyWith(
                color: kWhiteColor.withOpacity(0.6),
                fontWeight: FontWeight.w600,
                fontSize: 16.0),
          ),
          size5,
          Text(
            widget.data.fromRekening ?? "",
            style: kBoldStyle.copyWith(
                color: kWhiteColor,
                fontWeight: FontWeight.w600,
                fontSize: 18.0),
          ),
          size20,
          Text(
            'Nomor Rekening:',
            style: kBoldStyle.copyWith(
                color: kWhiteColor.withOpacity(0.6),
                fontWeight: FontWeight.w600,
                fontSize: 16.0),
          ),
          size5,
          Text(
            widget.data.fromName ?? "",
            style: kBoldStyle.copyWith(
                color: kWhiteColor,
                fontWeight: FontWeight.w600,
                fontSize: 18.0),
          ),
          SizedBox(
            height: 30,
          ),
          Text(
            'Payment Overview',
            style: kBoldStyle.copyWith(
                color: kWhiteColor,
                fontWeight: FontWeight.w600,
                fontSize: 20.0),
          ),
          size20,
          Text(
            'Bank:',
            style: kBoldStyle.copyWith(
                color: kWhiteColor.withOpacity(0.6),
                fontWeight: FontWeight.w600,
                fontSize: 16.0),
          ),
          size5,
          Text(
            widget.data.toBank ?? "",
            style: kBoldStyle.copyWith(
                color: kWhiteColor,
                fontWeight: FontWeight.w600,
                fontSize: 18.0),
          ),
          size20,
          Text(
            'Nomor Rekening:',
            style: kBoldStyle.copyWith(
                color: kWhiteColor.withOpacity(0.6),
                fontWeight: FontWeight.w600,
                fontSize: 16.0),
          ),
          size5,
          Text(
            widget.data.toRekening ?? "",
            style: kBoldStyle.copyWith(
                color: kWhiteColor,
                fontWeight: FontWeight.w600,
                fontSize: 18.0),
          ),
          size20,
          Text(
            'Nama Rekening:',
            style: kBoldStyle.copyWith(
                color: kWhiteColor.withOpacity(0.6),
                fontWeight: FontWeight.w600,
                fontSize: 16.0),
          ),
          size5,
          Text(
            widget.data.toName ?? "",
            style: kBoldStyle.copyWith(
                color: kWhiteColor,
                fontWeight: FontWeight.w600,
                fontSize: 18.0),
          ),
        ],
      ),
    );
  }

  buildDepositBody() {
    return Container(
      width: SizeConfig.screenWidth,
      padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 13.0),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(5.0),
          border: Border.all(
            color: kOrdinaryColor.withOpacity(0.2),
          )),
      child: Column(
        crossAxisAlignment: crossAxisStart,
        children: [
          Text(
            'Created Date',
            style: kBoldStyle.copyWith(
                color: kPrimaryColor, fontWeight: FontWeight.w600),
          ),
          size5,
          Text(
            '${DateFormat('dd - MMMM - yy H:m:s').format(widget.data.createdAt  ?? DateTime.now())} WIB',
            style: kBoldStyle.copyWith(
                color: kBlackColor, fontWeight: FontWeight.w600),
          ),
          size20,
          Text(
            'Respond Date',
            style: kBoldStyle.copyWith(
                color: kPrimaryColor, fontWeight: FontWeight.w600),
          ),
          size5,
          Text(
            '${DateFormat('dd - MMMM - yy H:m:s').format(widget.data.updatedAt ?? DateTime.now())} WIB',
            style: kBoldStyle.copyWith(
                color: kBlackColor, fontWeight: FontWeight.w600),
          ),
          size20,
          Row(
            //mainAxisAlignment: mainAxisSpaceBetween,
            children: [
              Container(
                width: SizeConfig.screenWidth / 2.2,
                child: Column(
                  crossAxisAlignment: crossAxisStart,
                  children: [
                    Text(
                      'Deposit from',
                      style: kBoldStyle.copyWith(
                          color: kPrimaryColor, fontWeight: FontWeight.w600),
                    ),
                    size5,
                    Text(
                      widget.data.fromRekening?? " ",
                      style: kBoldStyle.copyWith(
                          color: kBlackColor, fontWeight: FontWeight.w600),
                    ),
                  ],
                ),
              ),
              Container(
                child: Column(
                  crossAxisAlignment: crossAxisStart,
                  children: [
                    Text(
                      'Deposit By',
                      style: kBoldStyle.copyWith(
                          color: kPrimaryColor, fontWeight: FontWeight.w600),
                    ),
                    size5,
                    Text(
                      widget.data.fromName ?? "",
                      style: kBoldStyle.copyWith(
                          color: kBlackColor, fontWeight: FontWeight.w600),
                    ),
                  ],
                ),
              )
            ],
          ),
          size20,
          Row(
            //mainAxisAlignment: mainAxisSpaceBetween,
            children: [
              Container(
                width: SizeConfig.screenWidth / 2.2,
                child: Column(
                  crossAxisAlignment: crossAxisStart,
                  children: [
                    Text(
                      'Description',
                      style: kBoldStyle.copyWith(
                          color: kPrimaryColor, fontWeight: FontWeight.w600),
                    ),
                    size5,
                    Text(
                      'Deposit Account',
                      style: kBoldStyle.copyWith(
                          color: kBlackColor, fontWeight: FontWeight.w600),
                    ),
                  ],
                ),
              ),
              Container(
                child: Column(
                  crossAxisAlignment: crossAxisStart,
                  children: [
                    Text(
                      'Transfer Amount',
                      style: kBoldStyle.copyWith(
                          color: kPrimaryColor, fontWeight: FontWeight.w600),
                    ),
                    size5,
                    Text(
                      '$kIDR ${widget.data.amount ?? ""}',
                      style: kBoldStyle.copyWith(
                          color: kBlackColor, fontWeight: FontWeight.w600),
                    ),
                  ],
                ),
              )
            ],
          ),
          size20,
          Row(
            //mainAxisAlignment: mainAxisSpaceBetween,
            children: [
              Container(
                width: SizeConfig.screenWidth / 2.2,
                child: Column(
                  crossAxisAlignment: crossAxisStart,
                  children: [
                    Text(
                      'Rate',
                      style: kBoldStyle.copyWith(
                          color: kPrimaryColor, fontWeight: FontWeight.w600),
                    ),
                    size5,
                    Text(
                      '0.4%',
                      style: kBoldStyle.copyWith(
                          color: kBlackColor, fontWeight: FontWeight.w600),
                    ),
                  ],
                ),
              ),
              Container(
                child: Column(
                  crossAxisAlignment: crossAxisStart,
                  children: [
                    Text(
                      'Approved Amount',
                      style: kBoldStyle.copyWith(
                          color: kPrimaryColor, fontWeight: FontWeight.w600),
                    ),
                    size5,
                    Text(
                      widget.data.status,
                      style: kBoldStyle.copyWith(
                          color: widget.data.status == "pending"
                              ? kSecondaryColor
                              : widget.data.status == "Rejected"
                              ? kPrimaryColor
                              : kSuccessColor,
                          fontWeight: FontWeight.w600),
                    ),
                  ],
                ),
              )
            ],
          )
        ],
      ),
    );
  }
}