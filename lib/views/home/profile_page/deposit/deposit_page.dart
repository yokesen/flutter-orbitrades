import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:orbitrade/controller/custody_bank_list_controller.dart';
import 'package:orbitrade/controller/my_deposit_data_controller.dart';
import 'package:orbitrade/model/depositModelClass.dart';
import 'package:orbitrade/sheared/custom_loader.dart';
import 'package:orbitrade/views/home/home_page/request_deposit_page.dart';
import 'package:orbitrade/views/home/home_page/request_withdrawal_page.dart';
import 'package:orbitrade/views/home/profile_page/deposit/deposit_deatils_page.dart';
import 'package:orbitrade/views/home/profile_page/mt4_account/classOne.dart';

import '../../../../constants.dart';
import '../../../../main.dart';
import '../../../../size_config.dart';

class DepositPage extends StatefulWidget {
  static const routeName = 'deposit_page';

  @override
  _DepositProfileState createState() => _DepositProfileState();
}

class _DepositProfileState extends State<DepositPage> {
  var _depositDataCon = Get.put(MyDepositDataController());
  var custodyBankController = Get.put(CusodyBankListController());
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kWhiteColor,
      appBar: AppBar(
        backgroundColor: kWhiteColor,
        elevation: 0.5,
        centerTitle: true,
        title: Text(
          'Deposit',
          style: kBoldStyle.copyWith(
              color: kBlackColor, fontWeight: FontWeight.w600),
        ),
        iconTheme: IconThemeData(color: kBlackColor),
        actions: [
          IconButton(
            onPressed: () {
              custodyBankController
                  .fetchCusodyBankList(prefs.getString("token"));
              Navigator.pushNamed(context, RequestDepositPage.routeName);
            },
            icon: Icon(
              Icons.add,
              color: kPrimaryColor,
            ),
          )
        ],
      ),
      body: Container(
        height: SizeConfig.screenHeight,
        width: SizeConfig.screenWidth,
        padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
        child: Obx(() {
          if (_depositDataCon.isLoading.value) {
            return Center(
              child: CustomLoader(),
            );
          } else {
            return Column(
              children: [
                Padding(
                  padding: const EdgeInsets.only(left: 5.0, right: 5.0),
                  child: Row(
                    mainAxisAlignment: mainAxisSpaceBetween,
                    children: [
                      Text(
                        'Nominal',
                        style: kBoldStyle.copyWith(
                            color: kBlackColor.withOpacity(0.3),
                            fontWeight: FontWeight.w400),
                      ),
                      Text(
                        'Account ID',
                        style: kBoldStyle.copyWith(
                            color: kBlackColor.withOpacity(0.3),
                            fontWeight: FontWeight.w400),
                      ),
                      Text(
                        'Status',
                        style: kBoldStyle.copyWith(
                            color: kBlackColor.withOpacity(0.3),
                            fontWeight: FontWeight.w400),
                      ),
                    ],
                  ),
                ),
                Expanded(
                  child: ListView.builder(
                    itemCount: _depositDataCon.myAccount.data.length,
                    shrinkWrap: true,
                    physics: NeverScrollableScrollPhysics(),
                    itemBuilder: (_, index) {
                      return GestureDetector(
                        onTap: () {
                          Navigator.push(context,
                              MaterialPageRoute(builder: (context) {
                                return DepositDetailsPage(
                                  name: 'Deposit' +
                                      ' #${_depositDataCon.myAccount.data[index].id.toString()}',
                                  data: _depositDataCon.myAccount.data[index],
                                );
                              }));
                        },
                        child: Container(
                          height: 50,
                          margin: EdgeInsets.symmetric(vertical: 5.0),
                          padding: EdgeInsets.symmetric(horizontal: 5.0),
                          decoration: containerBoxDecoration(
                              borderRadius: 5.0,
                              color: _depositDataCon
                                  .myAccount.data[index].status ==
                                  "pending"
                                  ? kSecondaryColor
                                  : depositModelData[index].approved ==
                                  "Rejected"
                                  ? kPrimaryColor
                                  : kSuccessColor),
                          child: Row(
                            mainAxisAlignment: mainAxisSpaceBetween,
                            children: [
                              Text(
                                '$kIDR ${_depositDataCon.myAccount.data[index].amount}',
                                style: kBoldStyle.copyWith(
                                    color: kWhiteColor,
                                    fontWeight: FontWeight.w600),
                              ),
                              Text(
                                '${_depositDataCon.myAccount.data[index].metatrader}',
                                style: kBoldStyle.copyWith(
                                    color: kWhiteColor,
                                    fontWeight: FontWeight.w600),
                              ),
                              Text(
                                '${_depositDataCon.myAccount.data[index].status}',
                                style: kBoldStyle.copyWith(
                                    color: kWhiteColor,
                                    fontWeight: FontWeight.w600),
                              ),
                            ],
                          ),
                        ),
                      );
                    },
                  ),
                )
              ],
            );
          }
        }),
      ),
    );
  }
}
