import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:orbitrade/constants.dart';

class SummaryListDetailsPage extends StatefulWidget {
  static const routeName="summaryList_details_page";

  @override
  _SummaryListDetailsPageState createState() => _SummaryListDetailsPageState();
}

class _SummaryListDetailsPageState extends State<SummaryListDetailsPage> {

  bool isClicked=false;

  @override
  Widget build(BuildContext context) {

    Size size=MediaQuery.of(context).size;

    return Scaffold(
      backgroundColor: kWhiteColor,
      appBar: AppBar(
        backgroundColor: kWhiteColor,
        elevation: 0.5,
        centerTitle: true,
        title: Text(
          '-\$396.91, 29 Juli 2021',
          style: kBoldStyle.copyWith(
              color: kBlackColor, fontWeight: FontWeight.w600),
        ),
        iconTheme: IconThemeData(color: kBlackColor),
      ),
      body: Container(
        height: size.height,width: size.width,
        margin: EdgeInsets.symmetric(horizontal: 15.0,vertical: 10.0),
        child: SingleChildScrollView(
          child: Column(
            children: [
              GestureDetector(
                onTap: (){
                  setState(() {
                    isClicked=!isClicked;
                  });
                },
                child: Container(
                  padding: EdgeInsets.all(15.0),
                  decoration: containerBoxDecoration(
                    color: kWhiteColor,
                    borderRadius: 5.0,
                      boxShadow: [
                        BoxShadow(
                          color: kOrdinaryColor.withOpacity(0.1),
                          spreadRadius: 5,
                          blurRadius: 7,
                          offset:
                          Offset(0, 3), // changes position of shadow
                        ),
                      ]
                  ),
                  child: Column(
                    crossAxisAlignment: crossAxisStart,
                    children: [
                      Row(
                        mainAxisAlignment: mainAxisSpaceBetween,
                        children: [
                          Column(
                            crossAxisAlignment: crossAxisStart,
                            children: [
                              Row(
                                children: [
                                  textRoboto("EURUSD, ", kBlackColor,fontWeight: weightBold,fontSize: 18.0),
                                  textRoboto("sell ${"0.10"}", kErrorColor,fontWeight: weightBold,fontSize: 18.0),
                                ],
                              ),
                              size5,
                              Row(
                                children: [
                                  textRoboto("1.17680 -> ", kOrdinaryColor,fontWeight: weight500,fontSize: 18.0),
                                  textRoboto("1.18815", kOrdinaryColor,fontWeight: weight500,fontSize: 18.0),
                                ],
                              )
                            ],
                          ),
                          textRoboto("-56.75", kErrorColor,fontWeight: weightBold,fontSize: 30.0),
                        ],
                      ),
                      isClicked==true?Container(
                        child: Column(
                          crossAxisAlignment: crossAxisStart,
                          children: [
                            size20,
                            Row(

                              children: [
                                Expanded(
                                  flex: 2,
                                  child: Column(
                                    crossAxisAlignment: crossAxisStart,
                                    children: [
                                      Text(
                                        'S/L',
                                        style: kBoldStyle.copyWith(
                                            color: kOrdinaryColor.withOpacity(0.7), fontWeight: FontWeight.w500),
                                      ),
                                      size5,
                                      Text(
                                        '1.17680',
                                        style: kBoldStyle.copyWith(
                                            color: kBlackColor, fontWeight: FontWeight.w600,fontSize: 18),
                                      ),
                                    ],
                                  ),
                                ),
                                Expanded(
                                  flex: 2,
                                  child: Column(
                                    crossAxisAlignment: crossAxisStart,
                                    children: [
                                      Text(
                                        'Swap',
                                        style: kBoldStyle.copyWith(
                                            color: kOrdinaryColor.withOpacity(0.7), fontWeight: FontWeight.w500),
                                      ),
                                      size5,
                                      Text(
                                        '0.55',
                                        style: kBoldStyle.copyWith(
                                            color: kSuccessColor, fontWeight: FontWeight.w600,fontSize: 18),
                                      ),
                                    ],
                                  ),
                                )
                              ],
                            ),
                            size20,
                            Row(

                              children: [
                                Expanded(
                                  flex: 2,
                                  child: Column(
                                    crossAxisAlignment: crossAxisStart,
                                    children: [
                                      Text(
                                        'T/P',
                                        style: kBoldStyle.copyWith(
                                            color: kOrdinaryColor.withOpacity(0.7), fontWeight: FontWeight.w500),
                                      ),
                                      size5,
                                      Text(
                                        '1.18815',
                                        style: kBoldStyle.copyWith(
                                            color: kBlackColor, fontWeight: FontWeight.w600,fontSize: 18),
                                      ),
                                    ],
                                  ),
                                ),
                                Expanded(
                                  flex: 2,
                                  child: Column(
                                    crossAxisAlignment: crossAxisStart,
                                    children: [
                                      Text(
                                        'Taxes',
                                        style: kBoldStyle.copyWith(
                                            color: kOrdinaryColor.withOpacity(0.7), fontWeight: FontWeight.w500),
                                      ),
                                      size5,
                                      Text(
                                        '0.00',
                                        style: kBoldStyle.copyWith(
                                            color: kBlackColor, fontWeight: FontWeight.w600,fontSize: 18),
                                      ),
                                    ],
                                  ),
                                )
                              ],
                            ),
                            size20,
                            Row(

                              children: [
                                Expanded(
                                  flex: 2,
                                  child: Column(
                                    crossAxisAlignment: crossAxisStart,
                                    children: [
                                      Text(
                                        'ID',
                                        style: kBoldStyle.copyWith(
                                            color: kOrdinaryColor.withOpacity(0.7), fontWeight: FontWeight.w500),
                                      ),
                                      size5,
                                      Text(
                                        '9161857',
                                        style: kBoldStyle.copyWith(
                                            color: kBlackColor, fontWeight: FontWeight.w600,fontSize: 18),
                                      ),
                                    ],
                                  ),
                                ),
                                Expanded(
                                  flex: 2,
                                  child: Column(
                                    crossAxisAlignment: crossAxisStart,
                                    children: [
                                      Text(
                                        'Charge',
                                        style: kBoldStyle.copyWith(
                                            color: kOrdinaryColor.withOpacity(0.7), fontWeight: FontWeight.w500),
                                      ),
                                      size5,
                                      Text(
                                        '-0.16',
                                        style: kBoldStyle.copyWith(
                                            color: kBlackColor, fontWeight: FontWeight.w600,fontSize: 18),
                                      ),
                                    ],
                                  ),
                                )
                              ],
                            ),
                            size20,
                            Text(
                              '2 Aug 2021 15:24:09',
                              style: kBoldStyle.copyWith(
                                  color: kOrdinaryColor.withOpacity(0.7), fontWeight: FontWeight.w500),
                            ),
                          ],
                        ),
                      ):Container()
                    ],
                  ),
                ),
              ),
              size20,
              Container(
                padding: EdgeInsets.all(15.0),
                decoration: containerBoxDecoration(
                    color: kWhiteColor,
                    borderRadius: 5.0,
                    boxShadow: [
                      BoxShadow(
                        color: kOrdinaryColor.withOpacity(0.1),
                        spreadRadius: 5,
                        blurRadius: 7,
                        offset:
                        Offset(0, 3), // changes position of shadow
                      ),
                    ]
                ),
                child: Column(
                  crossAxisAlignment: crossAxisStart,
                  children: [
                    Row(
                      mainAxisAlignment: mainAxisSpaceBetween,
                      children: [
                        Column(
                          crossAxisAlignment: crossAxisStart,
                          children: [
                            Row(
                              children: [
                                textRoboto("EURUSD, ", kBlackColor,fontWeight: weightBold,fontSize: 18.0),
                                textRoboto("sell ${"0.10"}", kErrorColor,fontWeight: weightBold,fontSize: 18.0),
                              ],
                            ),
                            size5,
                            Row(
                              children: [
                                textRoboto("1.17680 -> ", kOrdinaryColor,fontWeight: weight500,fontSize: 18.0),
                                textRoboto("1.18815", kOrdinaryColor,fontWeight: weight500,fontSize: 18.0),
                              ],
                            )
                          ],
                        ),
                        textRoboto("56.75", kSuccessColor,fontWeight: weightBold,fontSize: 30.0),
                      ],
                    ),

                  ],
                ),
              ),
              size20,
              Container(
                padding: EdgeInsets.all(15.0),
                decoration: containerBoxDecoration(
                    color: kWhiteColor,
                    borderRadius: 5.0,
                    boxShadow: [
                      BoxShadow(
                        color: kOrdinaryColor.withOpacity(0.1),
                        spreadRadius: 5,
                        blurRadius: 7,
                        offset:
                        Offset(0, 3), // changes position of shadow
                      ),
                    ]
                ),
                child: Column(
                  crossAxisAlignment: crossAxisStart,
                  children: [
                    Row(
                      mainAxisAlignment: mainAxisSpaceBetween,
                      children: [
                        Column(
                          crossAxisAlignment: crossAxisStart,
                          children: [
                            Row(
                              children: [
                                textRoboto("XAUUSD, ", kBlackColor,fontWeight: weightBold,fontSize: 18.0),
                                textRoboto("buy ${"0.20"}", kSuccessColor,fontWeight: weightBold,fontSize: 18.0),
                              ],
                            ),
                            size5,
                            Row(
                              children: [
                                textRoboto("1.17680 -> ", kOrdinaryColor,fontWeight: weight500,fontSize: 18.0),
                                textRoboto("1.18815", kOrdinaryColor,fontWeight: weight500,fontSize: 18.0),
                              ],
                            )
                          ],
                        ),
                        textRoboto("-156.75", kErrorColor,fontWeight: weightBold,fontSize: 30.0),
                      ],
                    ),

                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
