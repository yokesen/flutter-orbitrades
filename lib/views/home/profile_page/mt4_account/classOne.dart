import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:orbitrade/constants.dart';
import 'package:orbitrade/model/myAccountModel.dart';
import 'package:orbitrade/views/home/profile_page/mt4_account/daily_summary_page.dart';

class ClassOne extends StatefulWidget {
  static const routeName = "class_one";
  const ClassOne({Key key, this.name, this.data}) : super(key: key);
  final String name;
  final ResponseData data;

  @override
  _ClassOneState createState() => _ClassOneState();
}

class _ClassOneState extends State<ClassOne> {
  bool isClicked = false;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: kWhiteColor,
      appBar: AppBar(
        backgroundColor: kWhiteColor,
        elevation: 0.5,
        centerTitle: true,
        title: Text(
          widget.data.mt4Id ?? "",
          style: kBoldStyle.copyWith(
              color: kBlackColor, fontWeight: FontWeight.w600),
        ),
        iconTheme: IconThemeData(color: kBlackColor),
      ),
      body: Container(
        height: size.height,
        width: size.width,
        padding: EdgeInsets.symmetric(horizontal: 15.0, vertical: 15.0),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: crossAxisStart,
            children: [
              Text(
                widget.data.typeAccount ?? " ",
                style: kBoldStyle.copyWith(
                    color: kPrimaryColor,
                    fontWeight: FontWeight.w600,
                    fontSize: 20.0),
              ),
              size20,
              Text(
                'Equity',
                style: kBoldStyle.copyWith(
                    color: kOrdinaryColor.withOpacity(0.7),
                    fontWeight: FontWeight.w500),
              ),
              size5,
              Text(
                '\$8973.94',
                style: kBoldStyle.copyWith(
                    color: kBlackColor,
                    fontWeight: FontWeight.w600,
                    fontSize: 30),
              ),
              size20,
              Row(
                children: [
                  Expanded(
                    flex: 2,
                    child: Column(
                      crossAxisAlignment: crossAxisStart,
                      children: [
                        Text(
                          'Balance',
                          style: kBoldStyle.copyWith(
                              color: kOrdinaryColor.withOpacity(0.7),
                              fontWeight: FontWeight.w500),
                        ),
                        size5,
                        Text(
                          '\$8973.94',
                          style: kBoldStyle.copyWith(
                              color: kBlackColor,
                              fontWeight: FontWeight.w600,
                              fontSize: 18),
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                    flex: 2,
                    child: Column(
                      crossAxisAlignment: crossAxisStart,
                      children: [
                        Text(
                          'Average profit',
                          style: kBoldStyle.copyWith(
                              color: kOrdinaryColor.withOpacity(0.7),
                              fontWeight: FontWeight.w500),
                        ),
                        size5,
                        Text(
                          '\$s1942.23',
                          style: kBoldStyle.copyWith(
                              color: kSuccessColor,
                              fontWeight: FontWeight.w600,
                              fontSize: 18),
                        ),
                      ],
                    ),
                  )
                ],
              ),
              size20,
              Row(
                children: [
                  Expanded(
                    flex: 2,
                    child: Column(
                      crossAxisAlignment: crossAxisStart,
                      children: [
                        Text(
                          'No. of trades',
                          style: kBoldStyle.copyWith(
                              color: kOrdinaryColor.withOpacity(0.7),
                              fontWeight: FontWeight.w500),
                        ),
                        size5,
                        Text(
                          '7',
                          style: kBoldStyle.copyWith(
                              color: kBlackColor,
                              fontWeight: FontWeight.w600,
                              fontSize: 18),
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                    flex: 2,
                    child: Column(
                      crossAxisAlignment: crossAxisStart,
                      children: [
                        Text(
                          'Average loss',
                          style: kBoldStyle.copyWith(
                              color: kOrdinaryColor.withOpacity(0.7),
                              fontWeight: FontWeight.w500),
                        ),
                        size5,
                        Text(
                          '-\$973',
                          style: kBoldStyle.copyWith(
                              color: kErrorColor,
                              fontWeight: FontWeight.w600,
                              fontSize: 18),
                        ),
                      ],
                    ),
                  )
                ],
              ),
              isClicked == false ? size20 : size5,
              GestureDetector(
                onTap: () {
                  setState(() {
                    isClicked = !isClicked;
                  });
                },
                child: Text(
                  isClicked == false ? '. . . . .  Lihat Semua' : "",
                  style: kBoldStyle.copyWith(
                      color: kPrimaryColor, fontWeight: FontWeight.w600),
                ),
              ),
              Visibility(
                visible: isClicked,
                child: Column(
                  crossAxisAlignment: crossAxisStart,
                  children: [
                    Row(
                      children: [
                        Expanded(
                          flex: 2,
                          child: Column(
                            crossAxisAlignment: crossAxisStart,
                            children: [
                              Text(
                                'Lots',
                                style: kBoldStyle.copyWith(
                                    color: kOrdinaryColor.withOpacity(0.7),
                                    fontWeight: FontWeight.w500),
                              ),
                              size5,
                              Text(
                                '0.60',
                                style: kBoldStyle.copyWith(
                                    color: kBlackColor,
                                    fontWeight: FontWeight.w600,
                                    fontSize: 18),
                              ),
                            ],
                          ),
                        ),
                        Expanded(
                          flex: 2,
                          child: Column(
                            crossAxisAlignment: crossAxisStart,
                            children: [
                              Text(
                                'Average RRR',
                                style: kBoldStyle.copyWith(
                                    color: kOrdinaryColor.withOpacity(0.7),
                                    fontWeight: FontWeight.w500),
                              ),
                              size5,
                              Text(
                                '\$1942.23',
                                style: kBoldStyle.copyWith(
                                    color: kBlackColor,
                                    fontWeight: FontWeight.w600,
                                    fontSize: 18),
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                    size20,
                    Row(
                      children: [
                        Expanded(
                          flex: 2,
                          child: Column(
                            crossAxisAlignment: crossAxisStart,
                            children: [
                              Text(
                                'Winrate',
                                style: kBoldStyle.copyWith(
                                    color: kOrdinaryColor.withOpacity(0.7),
                                    fontWeight: FontWeight.w500),
                              ),
                              size5,
                              Text(
                                '0.60',
                                style: kBoldStyle.copyWith(
                                    color: kBlackColor,
                                    fontWeight: FontWeight.w600,
                                    fontSize: 18),
                              ),
                            ],
                          ),
                        ),
                        Expanded(
                          flex: 2,
                          child: Column(
                            crossAxisAlignment: crossAxisStart,
                            children: [
                              Text(
                                'Expectancy',
                                style: kBoldStyle.copyWith(
                                    color: kOrdinaryColor.withOpacity(0.7),
                                    fontWeight: FontWeight.w500),
                              ),
                              size5,
                              Text(
                                '-\$734.23',
                                style: kBoldStyle.copyWith(
                                    color: kBlackColor,
                                    fontWeight: FontWeight.w600,
                                    fontSize: 18),
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                    size20,
                    Text(
                      'Update Terakhir: ${DateFormat('dd - MMMM - yy H:m:s').format(widget.data.updatedAt ?? DateTime.now())}',
                      style: kBoldStyle.copyWith(
                          color: kOrdinaryColor.withOpacity(0.7),
                          fontWeight: FontWeight.w500),
                    ),
                    size20,
                    GestureDetector(
                      onTap: () => Navigator.pushNamed(
                          context, DailySummaryPage.routeName),
                      child: Container(
                        height: 50,
                        decoration: containerBoxDecoration(
                            borderRadius: 10.0, color: kPrimaryColor),
                        child: Center(
                          child: Text(
                            'Lihat Rangkuman Harian',
                            style: kBoldStyle.copyWith(
                                color: kWhiteColor,
                                fontWeight: FontWeight.w500),
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
