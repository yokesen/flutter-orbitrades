import 'package:flutter/material.dart';
import 'package:orbitrade/constants.dart';
import 'package:orbitrade/views/home/profile_page/mt4_account/summaryList_detail_page.dart';

class DailySummaryPage extends StatefulWidget {

  static const routeName="daily_summary_page";


  @override
  _DailySummaryPageState createState() => _DailySummaryPageState();
}

class _DailySummaryPageState extends State<DailySummaryPage> {


  @override
  Widget build(BuildContext context) {

    Size size=MediaQuery.of(context).size;

    return Scaffold(
      backgroundColor: kWhiteColor,
      appBar: AppBar(
        backgroundColor: kWhiteColor,
        elevation: 0.5,
        centerTitle: true,
        title: Text(
          'Rangkuman Harian',
          style: kBoldStyle.copyWith(
              color: kBlackColor, fontWeight: FontWeight.w600),
        ),
        iconTheme: IconThemeData(color: kBlackColor),
      ),
      body: Container(
        height: size.height,width: size.width,
        child: ListView.builder(
          itemCount: rangukamList.length,
          itemBuilder: (_,index){
            return GestureDetector(
              onTap: (){
                Navigator.pushNamed(context, SummaryListDetailsPage.routeName);
              },
              child: Container(
                decoration: containerBoxDecoration(
                  borderRadius: 5.0,
                  color: kWhiteColor,
                  boxShadow: [
                    BoxShadow(
                      color: kOrdinaryColor.withOpacity(0.1),
                      spreadRadius: 5,
                      blurRadius: 7,
                      offset:
                      Offset(0, 3), // changes position of shadow
                    ),
                  ]
                ),
                padding: EdgeInsets.all(15.0),
                margin: EdgeInsets.symmetric(vertical: 10.0,horizontal: 15.0),
                child: Column(
                  children: [

                    Row(
                      mainAxisAlignment: mainAxisSpaceBetween,
                      children: [
                        Text(
                          '${rangukamList[index].ammount}',
                          style: kBoldStyle.copyWith(
                              color: rangukamList[index].color, fontWeight: FontWeight.w600),
                        ),
                        Text(
                          'Trades/Lots',
                          style: kBoldStyle.copyWith(
                              color: kOrdinaryColor, fontWeight: FontWeight.w600),
                        ),
                      ],
                    ),
                    size5,
                    Row(
                      mainAxisAlignment: mainAxisSpaceBetween,
                      children: [
                        Text(
                          '${rangukamList[index].date}',
                          style: kBoldStyle.copyWith(
                              color: kOrdinaryColor, fontWeight: FontWeight.w600),
                        ),
                        Text(
                          '${rangukamList[index].tradeAmmount}',
                          style: kBoldStyle.copyWith(
                              color: kBlackColor, fontWeight: FontWeight.w600),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}

class Rangkuman {
  String date;
  String ammount;
  String tradeAmmount;
  Color color;

  Rangkuman({this.date,this.ammount,this.tradeAmmount,this.color});

}

var rangukamList=[
  Rangkuman(date: "29 Juli 2021",ammount: "-\$396.91",tradeAmmount: "3/0.3",color: kErrorColor),
  Rangkuman(date: "27 Juli 2021",ammount: "-\$332.91",tradeAmmount: "2/0.3",color: kErrorColor),
  Rangkuman(date: "26 Juli 2021",ammount: "-\512.91",tradeAmmount: "2/0.1",color: kSuccessColor),
  Rangkuman(date: "25 Juli 2021",ammount: "-\141.91",tradeAmmount: "3/0.3",color: kErrorColor),
];
