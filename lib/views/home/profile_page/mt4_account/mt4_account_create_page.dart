import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:orbitrade/controller/mt4_account_data_controller.dart';
import 'package:orbitrade/controller/mt4_account_type_controller.dart';
import 'package:orbitrade/services/network_services.dart';
import 'package:orbitrade/sheared/buildDepositAppBar.dart';
import 'package:orbitrade/sheared/custom_loader.dart';

import '../../../../constants.dart';
import '../../../../size_config.dart';
import '../../Home.dart';

enum Q1Options { one, two, three, four }

class Mt4AccountCreatePage extends StatefulWidget {
  static const routeName = 'mt4_account_create_page';
  @override
  _yourPageState createState() => _yourPageState();
}

class _yourPageState extends State<Mt4AccountCreatePage> {

  Q1Options _site = Q1Options.one;

  final _mt4AccountTypeController = Get.put(Mt4AccountTypeController());
  final _accountMt4Data = Get.put(Mt4AccountDataController());
  String accountTypeID;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    if(_mt4AccountTypeController.dataResponse != null){
      setState(() {
        accountTypeID = _mt4AccountTypeController.dataResponse.response[3].id.toString();
        print('account type first id: '+accountTypeID);
      });


    }
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildDepositAppBar(
          context: context,
          title: 'Request MT4 Account',
          onPress: () async {
            var p = await NetworkServices().createMt4Account(
              context: context,
              id: accountTypeID,
            );
            Map<String, dynamic> js = p;
            if (p['success'] == true) {
              Navigator.of(context).pop();
              Get.snackbar('Success', "Account Created Successfully");
              _accountMt4Data.fetchMyAccount();
              Navigator.of(context).pushNamedAndRemoveUntil(
                  Home.routeName, (Route<dynamic> route) => false);
            } else {
              Navigator.of(context).pop();
              print(p['message']);
              Get.snackbar('Error', p['message']);
            }
          }), body: Obx((){
            if(_mt4AccountTypeController.isLoading.value){
              return CustomLoader();
            }else{
              return buildBoady();
            }}),
        );
  }

  buildBoady() {
    return SingleChildScrollView(
      child: Padding(
        padding: EdgeInsets.symmetric(
          horizontal: getProportionateScreenWidth(20),
        ),
        child: _mt4AccountTypeController.dataResponse != null ?
        Column(
          children: [
            size20,
            Container(
              decoration: BoxDecoration(
                color: kPrimaryColor,
                borderRadius: BorderRadius.circular(10.0),
                boxShadow: [
                  BoxShadow(
                    color: kOrdinaryColor.withOpacity(0.1),
                    spreadRadius: 5,
                    blurRadius: 7,
                    offset:
                    Offset(0, 3), // changes position of shadow
                  ),
                ],
              ),
              child: Column(
                children: [
                  Padding(
                    padding: EdgeInsets.only(right: 10.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          children: [
                            Radio(
                              value: Q1Options.one,
                              activeColor: kSecondaryColor,
                              groupValue: _site,
                              onChanged: (Q1Options value) {
                                setState(() {
                                  _site = value;
                                  accountTypeID = _mt4AccountTypeController.dataResponse.response[3].id.toString();

                                });
                              },
                            ),
                            Text(
                              _mt4AccountTypeController.dataResponse.response[3].namaAccount,
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 18,
                                color: kSecondaryColor,
                              ),
                            )
                          ],
                        ),
                        Text(
                          '\$'+_mt4AccountTypeController.dataResponse.response[3].minimumDepo.toString(),
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 18,
                              color: kSecondaryColor),
                        ),

                      ],
                    ),
                  ),
                  Container(
                    child: _site == Q1Options.one ?
                    Column(
                      children: [
                        size10,
                        Padding(
                            padding: EdgeInsets.symmetric(
                              horizontal: getProportionateScreenWidth(10),
                            ),
                            child:  Row(
                              children: [
                                Container(
                                  alignment: Alignment.centerLeft,
                                  width: SizeConfig.screenWidth / 2 - 24,
                                  child: offerTopList(title: 'Community'),
                                ),
                                Container(
                                  alignment: Alignment.centerLeft,
                                  width: SizeConfig.screenWidth / 2 - 36,
                                  child: offerTopList(title: 'Live Trading'),
                                ),
                              ],)
                        ),
                        size8,
                        Padding(
                            padding: EdgeInsets.symmetric(
                              horizontal: getProportionateScreenWidth(10),
                            ),
                            child:  Row(
                              children: [
                                Container(
                                  alignment: Alignment.centerLeft,
                                  width: SizeConfig.screenWidth / 2 - 24,
                                  child: offerTopList(title: 'Free Signal'),
                                ),
                                Container(
                                  alignment: Alignment.centerLeft,
                                  width: SizeConfig.screenWidth / 2 - 36,
                                  child: offerTopList(title: 'Video Education'),
                                ),
                              ],)
                        ),
                        size8,
                        Padding(
                            padding: EdgeInsets.symmetric(
                              horizontal: getProportionateScreenWidth(10),
                            ),
                            child:  Row(
                              children: [
                                Container(
                                  alignment: Alignment.centerLeft,
                                  width: SizeConfig.screenWidth / 2 - 24,
                                  child: offerTopList(title: 'Trading School'),
                                ),
                                Container(
                                  alignment: Alignment.centerLeft,
                                  width: SizeConfig.screenWidth / 2 - 36,
                                  child: offerTopList(title: 'Trading Plan'),
                                ),
                              ],)
                        ),
                        size8,
                        Padding(
                          padding: EdgeInsets.symmetric(
                            horizontal: getProportionateScreenWidth(10),
                          ),
                          child: Row(
                            children: [
                              Container(
                                alignment: Alignment.centerLeft,
                                width: SizeConfig.screenWidth / 2 - 24,
                                child: offerTopList(title: 'Trading Competition'),
                              ),
                              Container(
                                alignment: Alignment.centerLeft,
                                width: SizeConfig.screenWidth / 2 - 36,
                                child: offerTopList(title: 'Private Consultation\nWith Master'),
                              ),
                            ],
                          ),
                        ),
                        size8,
                        Padding(
                            padding: EdgeInsets.symmetric(
                              horizontal: getProportionateScreenWidth(10),
                            ),
                            child:  Row(
                              children: [
                                Container(
                                  alignment: Alignment.centerLeft,
                                  width: SizeConfig.screenWidth / 2 - 24,
                                  child: offerTopList(title: 'Peta Harga'),
                                ),
                              ],)
                        ),
                        size15,
                      ],
                    ):
                    SizedBox(),
                  )
                ],
              ),
            ),
            size10,
            Container(
              decoration: BoxDecoration(
                color: kWhiteColor,
                borderRadius: BorderRadius.circular(10.0),
                boxShadow: [
                  BoxShadow(
                    color: kOrdinaryColor.withOpacity(0.1),
                    spreadRadius: 5,
                    blurRadius: 7,
                    offset:
                    Offset(0, 3), // changes position of shadow
                  ),
                ],
              ),
              child: Column(
                children: [
                  Padding(
                    padding: EdgeInsets.only(right: 10.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          children: [
                            Radio(
                              value: Q1Options.two,
                              activeColor: kPrimaryColor,
                              groupValue: _site,
                              onChanged: (Q1Options value) {
                                setState(() {
                                  _site = value;
                                  accountTypeID = _mt4AccountTypeController.dataResponse.response[2].id.toString();
                                });
                              },
                            ),
                            Text(
                              _mt4AccountTypeController.dataResponse.response[2].namaAccount,
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 18,
                                color: kPrimaryColor,
                              ),
                            )
                          ],
                        ),
                        Text(
                          '\$'+_mt4AccountTypeController.dataResponse.response[2].minimumDepo.toString(),
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 18,
                              color: Colors.grey.withOpacity(.5)),
                        ),

                      ],
                    ),
                  ),
                  Container(
                    child: _site == Q1Options.two ?
                    Column(
                      children: [
                        size10,
                        Padding(
                            padding: EdgeInsets.symmetric(
                              horizontal: getProportionateScreenWidth(10),
                            ),
                            child:  Row(
                              children: [
                                Container(
                                  alignment: Alignment.centerLeft,
                                  width: SizeConfig.screenWidth / 2 - 24,
                                  child: offerList(title: 'Community'),
                                ),
                                Container(
                                  alignment: Alignment.centerLeft,
                                  width: SizeConfig.screenWidth / 2 - 36,
                                  child: offerList(title: 'Live Trading'),
                                ),
                              ],)
                        ),
                        size8,
                        Padding(
                            padding: EdgeInsets.symmetric(
                              horizontal: getProportionateScreenWidth(10),
                            ),
                            child:  Row(
                              children: [
                                Container(
                                  alignment: Alignment.centerLeft,
                                  width: SizeConfig.screenWidth / 2 - 24,
                                  child: offerList(title: 'Free Signal'),
                                ),
                                Container(
                                  alignment: Alignment.centerLeft,
                                  width: SizeConfig.screenWidth / 2 - 36,
                                  child: offerList(title: 'Video Education'),
                                ),
                              ],)
                        ),
                        size8,
                        Padding(
                            padding: EdgeInsets.symmetric(
                              horizontal: getProportionateScreenWidth(10),
                            ),
                            child:  Row(
                              children: [
                                Container(
                                  alignment: Alignment.centerLeft,
                                  width: SizeConfig.screenWidth / 2 - 24,
                                  child: offerList(title: 'Trading School'),
                                ),
                                Container(
                                  alignment: Alignment.centerLeft,
                                  width: SizeConfig.screenWidth / 2 - 36,
                                  child: offerList(title: 'Trading Plan'),
                                ),
                              ],)
                        ),
                        size8,
                        Padding(
                          padding: EdgeInsets.symmetric(
                            horizontal: getProportionateScreenWidth(10),
                          ),
                          child: Row(
                            children: [
                              Container(
                                alignment: Alignment.centerLeft,
                                width: SizeConfig.screenWidth / 2 - 24,
                                child: offerList(title: 'Trading Competition'),
                              ),
                              Container(
                                alignment: Alignment.centerLeft,
                                width: SizeConfig.screenWidth / 2 - 36,
                                child: offerList(status: false,title: 'Private Consultation\nWith Master'),
                              ),
                            ],
                          ),
                        ),
                        size8,
                        Padding(
                            padding: EdgeInsets.symmetric(
                              horizontal: getProportionateScreenWidth(10),
                            ),
                            child:  Row(
                              children: [
                                Container(
                                  alignment: Alignment.centerLeft,
                                  width: SizeConfig.screenWidth / 2 - 24,
                                  child: offerList(title: 'Peta Harga'),
                                ),
                              ],)
                        ),
                        size15,
                      ],
                    ):
                    SizedBox(),
                  )
                ],
              ),
            ),
            size10,
            Container(
              decoration: BoxDecoration(
                color: kWhiteColor,
                borderRadius: BorderRadius.circular(10.0),
                boxShadow: [
                  BoxShadow(
                    color: kOrdinaryColor.withOpacity(0.1),
                    spreadRadius: 5,
                    blurRadius: 7,
                    offset:
                    Offset(0, 3), // changes position of shadow
                  ),
                ],
              ),
              child: Column(
                children: [
                  Padding(
                    padding: EdgeInsets.only(right: 10.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          children: [
                            Radio(
                              value: Q1Options.three,
                              activeColor: kPrimaryColor,
                              groupValue: _site,
                              onChanged: (Q1Options value) {
                                setState(() {
                                  _site = value;
                                  accountTypeID = _mt4AccountTypeController.dataResponse.response[1].id.toString();
                                });
                              },
                            ),
                            Text(
                              _mt4AccountTypeController.dataResponse.response[1].namaAccount,
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 18,
                                color: kPrimaryColor,
                              ),
                            )
                          ],
                        ),
                        Text(
                          '\$'+_mt4AccountTypeController.dataResponse.response[1].minimumDepo.toString(),
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 18,
                              color: Colors.grey.withOpacity(.5)),
                        ),

                      ],
                    ),
                  ),
                  Container(
                    child: _site == Q1Options.three ?
                    Column(
                      children: [
                        size10,
                        Padding(
                            padding: EdgeInsets.symmetric(
                              horizontal: getProportionateScreenWidth(10),
                            ),
                            child:  Row(
                              children: [
                                Container(
                                  alignment: Alignment.centerLeft,
                                  width: SizeConfig.screenWidth / 2 - 24,
                                  child: offerList(title: 'Community'),
                                ),
                                Container(
                                  alignment: Alignment.centerLeft,
                                  width: SizeConfig.screenWidth / 2 - 36,
                                  child: offerList(title: 'Live Trading'),
                                ),
                              ],)
                        ),
                        size8,
                        Padding(
                            padding: EdgeInsets.symmetric(
                              horizontal: getProportionateScreenWidth(10),
                            ),
                            child:  Row(
                              children: [
                                Container(
                                  alignment: Alignment.centerLeft,
                                  width: SizeConfig.screenWidth / 2 - 24,
                                  child: offerList(title: 'Free Signal'),
                                ),
                                Container(
                                  alignment: Alignment.centerLeft,
                                  width: SizeConfig.screenWidth / 2 - 36,
                                  child: offerList(status: false,title: 'Video Education'),
                                ),
                              ],)
                        ),
                        size8,
                        Padding(
                            padding: EdgeInsets.symmetric(
                              horizontal: getProportionateScreenWidth(10),
                            ),
                            child:  Row(
                              children: [
                                Container(
                                  alignment: Alignment.centerLeft,
                                  width: SizeConfig.screenWidth / 2 - 24,
                                  child: offerList(title: 'Trading School'),
                                ),
                                Container(
                                  alignment: Alignment.centerLeft,
                                  width: SizeConfig.screenWidth / 2 - 36,
                                  child: offerList(status: false,title: 'Trading Plan'),
                                ),
                              ],)
                        ),
                        size8,
                        Padding(
                          padding: EdgeInsets.symmetric(
                            horizontal: getProportionateScreenWidth(10),
                          ),
                          child: Row(
                            children: [
                              Container(
                                alignment: Alignment.centerLeft,
                                width: SizeConfig.screenWidth / 2 - 24,
                                child: offerList(title: 'Trading Competition'),
                              ),
                              Container(
                                alignment: Alignment.centerLeft,
                                width: SizeConfig.screenWidth / 2 - 36,
                                child: offerList(status: false,title: 'Private Consultation\nWith Master'),
                              ),
                            ],
                          ),
                        ),
                        size8,
                        Padding(
                            padding: EdgeInsets.symmetric(
                              horizontal: getProportionateScreenWidth(10),
                            ),
                            child:  Row(
                              children: [
                                Container(
                                  alignment: Alignment.centerLeft,
                                  width: SizeConfig.screenWidth / 2 - 24,
                                  child: offerList(title: 'Peta Harga'),
                                ),
                              ],)
                        ),
                        size15,
                      ],
                    ):
                    SizedBox(),
                  )
                ],
              ),
            ),
            size10,
            Container(
              decoration: BoxDecoration(
                color: kWhiteColor,
                borderRadius: BorderRadius.circular(10.0),
                boxShadow: [
                  BoxShadow(
                    color: kOrdinaryColor.withOpacity(0.1),
                    spreadRadius: 5,
                    blurRadius: 7,
                    offset:
                    Offset(0, 3), // changes position of shadow
                  ),
                ],
              ),
              child: Column(
                children: [
                  Padding(
                    padding: EdgeInsets.only(right: 10.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          children: [
                            Radio(
                              value: Q1Options.four,
                              activeColor: kPrimaryColor,
                              groupValue: _site,
                              onChanged: (Q1Options value) {
                                setState(() {
                                  _site = value;
                                  accountTypeID = _mt4AccountTypeController.dataResponse.response[0].id.toString();
                                });
                              },
                            ),
                            Text(
                              _mt4AccountTypeController.dataResponse.response[0].namaAccount,
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 18,
                                color: kPrimaryColor,
                              ),
                            )
                          ],
                        ),
                        Text(
                          '\$'+_mt4AccountTypeController.dataResponse.response[0].minimumDepo.toString(),
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 18,
                              color: Colors.grey.withOpacity(.5)),
                        ),

                      ],
                    ),
                  ),
                  Container(
                    child: _site == Q1Options.four ?
                    Column(
                      children: [
                        size10,
                        Padding(
                            padding: EdgeInsets.symmetric(
                              horizontal: getProportionateScreenWidth(10),
                            ),
                            child:  Row(
                              children: [
                                Container(
                                  alignment: Alignment.centerLeft,
                                  width: SizeConfig.screenWidth / 2 - 24,
                                  child: offerList(title: 'Community'),
                                ),
                                Container(
                                  alignment: Alignment.centerLeft,
                                  width: SizeConfig.screenWidth / 2 - 36,
                                  child: offerList(title: 'Live Trading'),
                                ),
                              ],)
                        ),
                        size8,
                        Padding(
                            padding: EdgeInsets.symmetric(
                              horizontal: getProportionateScreenWidth(10),
                            ),
                            child:  Row(
                              children: [
                                Container(
                                  alignment: Alignment.centerLeft,
                                  width: SizeConfig.screenWidth / 2 - 24,
                                  child: offerList(title: 'Free Signal'),
                                ),
                                Container(
                                  alignment: Alignment.centerLeft,
                                  width: SizeConfig.screenWidth / 2 - 36,
                                  child: offerList(status: false,title: 'Video Education'),
                                ),
                              ],)
                        ),
                        size8,
                        Padding(
                            padding: EdgeInsets.symmetric(
                              horizontal: getProportionateScreenWidth(10),
                            ),
                            child:  Row(
                              children: [
                                Container(
                                  alignment: Alignment.centerLeft,
                                  width: SizeConfig.screenWidth / 2 - 24,
                                  child: offerList(title: 'Trading School'),
                                ),
                                Container(
                                  alignment: Alignment.centerLeft,
                                  width: SizeConfig.screenWidth / 2 - 36,
                                  child: offerList(status: false,title: 'Trading Plan'),
                                ),
                              ],)
                        ),
                        size8,
                        Padding(
                          padding: EdgeInsets.symmetric(
                            horizontal: getProportionateScreenWidth(10),
                          ),
                          child: Row(
                            children: [
                              Container(
                                alignment: Alignment.centerLeft,
                                width: SizeConfig.screenWidth / 2 - 24,
                                child: offerList(title: 'Trading Competition'),
                              ),
                              Container(
                                alignment: Alignment.centerLeft,
                                width: SizeConfig.screenWidth / 2 - 36,
                                child: offerList(status: false,title: 'Private Consultation\nWith Master'),
                              ),
                            ],
                          ),
                        ),
                        size8,
                        Padding(
                            padding: EdgeInsets.symmetric(
                              horizontal: getProportionateScreenWidth(10),
                            ),
                            child:  Row(
                              children: [
                                Container(
                                  alignment: Alignment.centerLeft,
                                  width: SizeConfig.screenWidth / 2 - 24,
                                  child: offerList(status: false,title: 'Peta Harga'),
                                ),
                              ],)
                        ),
                        size15,
                      ],
                    ):
                    SizedBox(),
                  )
                ],
              ),
            ),
            size20,
            Container(
              alignment: Alignment.center,
              child: Text(
                'Dengan Request Account MT4 di Orbitrade, Kamu menyetujui Kebijakan Privasi dan Syarat dan Ketentuan',
                textAlign: TextAlign.start,
                style: TextStyle(
                    color: Colors.grey.withOpacity(.5),
                    fontWeight: FontWeight.bold
                ),
              ),
            ),
          ],
        ):SizedBox()
      ),
    );
  }

  offerList({status = true, String title,}){
    return Row(
      //mainAxisAlignment: mainAxisStart,
      children: [
        ///Uncomment it later
        Icon(status ? Icons.check : Icons.close, color: status ? kBlackColor : Colors.grey.withOpacity(.5), size: 15, ),
        width5,
        Text(
          '$title',
          style: kBoldStyle.copyWith(
              color: status ? kBlackColor : Colors.grey.withOpacity(.5),
              fontSize: 12.3,
              fontWeight: FontWeight.bold),
        ),
      ],
    );
  }

  offerTopList({String title,}){
    return Row(
      //mainAxisAlignment: mainAxisStart,
      children: [
        ///Uncomment it later
        Icon(Icons.check, color: kWhiteColor, size: 15,),
        width5,
        Text(
          '$title',
          style: kBoldStyle.copyWith(
              color: kWhiteColor,
              fontSize: 12.3,
              fontWeight: FontWeight.bold),
        ),
      ],
    );
  }
}
