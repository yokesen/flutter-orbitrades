import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:orbitrade/constants.dart';
import 'package:orbitrade/controller/withdrawal_data_controller.dart';
import 'package:orbitrade/model/depositModelClass.dart';
import 'package:orbitrade/sheared/custom_loader.dart';
import 'package:orbitrade/size_config.dart';
import 'package:orbitrade/views/home/home_page/request_withdrawal_page.dart';
import 'package:orbitrade/views/home/profile_page/withdrawl/withdawal_deatils_page.dart';

class WithdrawlPage extends StatefulWidget {
  static const routeName = 'withdrawl_page';

  @override
  _WithdrawlPageState createState() => _WithdrawlPageState();
}

class _WithdrawlPageState extends State<WithdrawlPage> {
  var _withdrawalDataCon = Get.put(WithdrawalDataController());
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kWhiteColor,
      appBar: AppBar(
        backgroundColor: kWhiteColor,
        elevation: 0.5,
        centerTitle: true,
        title: Text(
          'Withdrawl',
          style: kBoldStyle.copyWith(
              color: kBlackColor, fontWeight: FontWeight.w600),
        ),
        iconTheme: IconThemeData(color: kBlackColor),
        actions: [
          IconButton(
            onPressed: () =>  Navigator.pushNamed(context, RequestWithdrawalPage.routeName),
            icon: Icon(
              Icons.add,
              color: kPrimaryColor,
            ),
          )
        ],
      ),
      body: Container(
        height: SizeConfig.screenHeight,
        width: SizeConfig.screenWidth,
        padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
        child: Obx(() {
          if (_withdrawalDataCon.isLoading.value) {
            return Center(
              child: CustomLoader(),
            );
          } else {
            return Column(
              children: [
                Padding(
                  padding: const EdgeInsets.only(left: 5.0, right: 5.0),
                  child: Row(
                    mainAxisAlignment: mainAxisSpaceBetween,
                    children: [
                      Text(
                        'Nominal',
                        style: kBoldStyle.copyWith(
                            color: kBlackColor.withOpacity(0.3),
                            fontWeight: FontWeight.w400),
                      ),
                      Text(
                        'Account ID',
                        style: kBoldStyle.copyWith(
                            color: kBlackColor.withOpacity(0.3),
                            fontWeight: FontWeight.w400),
                      ),
                      Text(
                        'Status',
                        style: kBoldStyle.copyWith(
                            color: kBlackColor.withOpacity(0.3),
                            fontWeight: FontWeight.w400),
                      ),
                    ],
                  ),
                ),
                Expanded(
                  child: ListView.builder(
                    itemCount: _withdrawalDataCon.myAccount.data.length,
                    shrinkWrap: true,
                    physics: NeverScrollableScrollPhysics(),
                    itemBuilder: (_, index) {
                      return GestureDetector(
                        onTap: () {
                          Navigator.push(context,
                              MaterialPageRoute(builder: (context) {
                            return WithdrawalDetailsPage(
                              name: 'Withdrawal' +
                                  ' #${_withdrawalDataCon.myAccount.data[index].metatrader}',
                              data: _withdrawalDataCon.myAccount.data[index],
                            );
                          }));
                        },
                        child: Container(
                          height: 50,
                          margin: EdgeInsets.symmetric(vertical: 5.0),
                          padding: EdgeInsets.symmetric(horizontal: 5.0),
                          decoration: containerBoxDecoration(
                              borderRadius: 5.0,
                              color: _withdrawalDataCon
                                          .myAccount.data[index].status ==
                                      "pending"
                                  ? kSecondaryColor
                                  : depositModelData[index].approved ==
                                          "Rejected"
                                      ? kPrimaryColor
                                      : kSuccessColor),
                          child: Row(
                            mainAxisAlignment: mainAxisSpaceBetween,
                            children: [
                              Text(
                                '$kUSD ${_withdrawalDataCon.myAccount.data[index].metatrader}',
                                style: kBoldStyle.copyWith(
                                    color: kWhiteColor,
                                    fontWeight: FontWeight.w600),
                              ),
                              Text(
                                '${_withdrawalDataCon.myAccount.data[index].accountNumber}',
                                style: kBoldStyle.copyWith(
                                    color: kWhiteColor,
                                    fontWeight: FontWeight.w600),
                              ),
                              Text(
                                '${_withdrawalDataCon.myAccount.data[index].status}',
                                style: kBoldStyle.copyWith(
                                    color: kWhiteColor,
                                    fontWeight: FontWeight.w600),
                              ),
                            ],
                          ),
                        ),
                      );
                    },
                  ),
                )
              ],
            );
          }
        }),
      ),
    );
  }
}
