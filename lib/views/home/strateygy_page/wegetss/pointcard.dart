import 'package:flutter/material.dart';
import 'package:orbitrade/constants.dart';
import 'package:orbitrade/size_config.dart';

class TrainingPoint extends StatelessWidget {
  TrainingPoint({Key key, this.point})
      : super(key: key);
  final int point;
  @override
  Widget build(BuildContext context) {
    return Card(
      color: kPrimaryColor,
      child: Container(
          height: 65,
          width: SizeConfig.screenWidth,
          child: Stack(
            children: [
              Positioned(
                  left: 10,
                  top: 12,
                  child: Text(
                    "My Trading Academy",
                    style: TextStyle(
                      fontSize: 18,
                      color: Colors.white.withOpacity(.9),
                      fontWeight: FontWeight.w600,
                      // fontFamily: "Poppins",
                    ),
                  )),
              Positioned(
                left: 10,
                top: 35,
                child: Text(
                  "Training Completion Point",
                  style: TextStyle(
                      fontSize: 10, color: Colors.white.withOpacity(.4)),
                ),
              ),
              Positioned(
                right: 10,
                top: 10,
                child: Text(
                  point.toString(),
                  style: TextStyle(
                      fontSize: 30,
                      color: Colors.white.withOpacity(.9),
                      fontWeight: FontWeight.w700),
                ),
              ),
            ],
          )),
    );
  }
}
