import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:orbitrade/sheared/custom_loader.dart';

import '../../../../constants.dart';

class SelectSignalCard extends StatelessWidget {
  const SelectSignalCard({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(
          color: khomeFirstCardColor,
          borderRadius: new BorderRadius.all(
            const Radius.circular(10.0),
          ),
        ),
        child: Center(
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Container(
                    margin: EdgeInsets.only(left: 5, right: 5),
                    width: 120,
                    height: 120,
                    alignment: Alignment.center,
                    child: CachedNetworkImage(
                      imageUrl:
                          '${'https://i.postimg.cc/02DPH2gp/imageedit-1-8923540065.png'}',
                      fit: BoxFit.cover,
                      placeholder: (context, url) => CustomLoader(),
                      errorWidget: (context, url, error) => Icon(
                        Icons.error,
                        color: Colors.red,
                      ),
                    )),
                Container(
                  margin:
                      EdgeInsets.only(top: 15, left: 5, right: 5, bottom: 10),
                  child: Text(
                      'Money Swinger fokus kepada keuntungan yang pasti dengan mengandalkan analisa Boolinger Band',
                      textAlign: TextAlign.center,
                      style: TextStyle(color: Colors.black, fontSize: 14)),
                ),
              ]),
        ));
  }
}
