import 'package:flutter/material.dart';
import 'package:orbitrade/views/home/strateygy_page/components/academy_screen.dart';
import 'package:orbitrade/views/home/strateygy_page/components/roadmap_screen.dart';
import 'package:orbitrade/views/home/strateygy_page/components/signal_screen.dart';

import '../../../constants.dart';

class StrategyHomePage extends StatefulWidget {
  static const routeName = 'strategy_page_home';
  const StrategyHomePage({Key key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<StrategyHomePage> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        backgroundColor: kWhiteColor,
        appBar: AppBar(
          elevation: 1,
          brightness: Brightness.light,
          backgroundColor: Colors.white,
          title: Text(
            'Trading Strategy',
            style: kAppBarText.copyWith(
              color: kBlackColor,
              fontWeight: FontWeight.w600,
            ),
          ),
          bottom: TabBar(
            indicatorColor: kBlackColor,
            unselectedLabelColor: kBlackColor.withOpacity(.4),
            labelColor: kBlackColor,
            tabs: [
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 10),
                child: Text(
                  'Academy',
                  style: kDescriptionText.copyWith(
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 10),
                child: Text(
                  'Signal',
                  style: kDescriptionText.copyWith(
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 10),
                child: Text(
                  'Roadmap',
                  style: kSmallText.copyWith(
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ),
            ],
          ),
        ),
        body: TabBarView(
          children: [
            AcademyScreen(),
            SignalScreen(),
            RoadmapScreen(),
          ],
        ),
      ),
    );
  }
}
