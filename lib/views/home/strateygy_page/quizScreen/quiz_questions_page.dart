import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:orbitrade/constants.dart';
import 'package:orbitrade/controller/quiazQuestionsController.dart';
import 'package:orbitrade/controller/video_list_data_controller.dart';
import 'package:orbitrade/model/quiz_questions_model.dart';
import 'package:orbitrade/services/network_services.dart';
import 'package:orbitrade/sheared/custom_loader.dart';
import 'package:orbitrade/views/home/strateygy_page/quiz_complete_page.dart';

enum Q1Options { one, two, three, four }
enum Q2Options { one, two, three, four }
enum Q3Options { one, two, three, four }

class QuizQuestionPage extends StatefulWidget {
  static const routeName = 'quiz_questions_page';

  String id;

  QuizQuestionPage({this.id});

  @override
  _QuizQuestionPageState createState() => _QuizQuestionPageState();
}

class _QuizQuestionPageState extends State<QuizQuestionPage> {
  // PageController controller = PageController(viewportFraction: 1, keepPage: true);

  final PageController _controller = PageController(initialPage: 0);
  var videoController = Get.put(VideoListDataController());

  int pageChanged = 0;
  int questionIndex = 1;

  Q1Options _site = Q1Options.three;
  Q2Options _site2 = Q2Options.two;
  Q3Options _site3 = Q3Options.two;

  var quizC = Get.put(QuizQuestionController());

  List<Data> dtaa=List();

  var indexList=[];

  int _selectedIndex=0;

  var a="A";

  _onSelected(int index, String ans) {
    setState(() {
      _selectedIndex = index;
      selectedQuiz.add(ans);
      print(selectedQuiz.toString());
    });

  }

  var map;
  List<String> selectedQuiz = List();

  @override
  void initState() {
    print(widget.id);
    quizC.getQuizQuestion(widget.id);

    //map=Map.fromIterable(quizC.list,value: (e)=>e.reactive);


    // TODO: implement initState
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    print('quiz: '+selectedQuiz.toString());
    return Scaffold(
        backgroundColor: kWhiteColor,
        appBar: AppBar(
          backgroundColor: kWhiteColor,
          elevation: 0.5,
          centerTitle: true,
          title: Text(
            'Post Video Quiz',
            style: kBoldStyle.copyWith(color: kBlackColor, fontWeight: FontWeight.w600),
          ),
          iconTheme: IconThemeData(color: kBlackColor),
          actions: [
            Container(
              margin: EdgeInsets.only(right: 10.0),
              child: Center(
                child: GestureDetector(
                  onTap: () async{
                    if (pageChanged < quizC.quizData.value.data.length) {
                      ++questionIndex;
                      _controller.animateToPage(++pageChanged, duration: Duration(milliseconds: 1), curve: Curves.fastLinearToSlowEaseIn);
                    } else {
                      var p =
                          await NetworkServices().submitQuiz(
                        context: context,
                        vid: widget.id,
                        qJawaban: selectedQuiz,

                      );
                      Map<String, dynamic> js = p;
                      if (p['success'] == false) {
                        Navigator.of(context).pop();
                        print(p['message']);
                        Get.snackbar('Error', p['message']);
                      } else {
                        Navigator.of(context).pop();
                        videoController.getScore();
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => QuizCompletePage(
                                  point: p['poin'].toString(),
                                )),);

                        Get.snackbar('Error', p['Data']);
                        //success
                      }
                      print("finish");

                    }
                  },
                  child: Text(
                    'Next',
                    style: kBoldStyle.copyWith(color: kPrimaryColor, fontWeight: FontWeight.w600),
                  ),
                ),
              ),
            ),
          ],
        ),
        body: Obx(
          () => quizC.quizData.value.data == null
              ? CustomLoader()
              : quizC.quizData.value.data.length == 0
                  ? textRoboto("NO Data Found", kBlackColor)
                  : PageView.builder(
                      itemCount: quizC.quizData.value.data.length,
                      controller: _controller,
                      scrollDirection: Axis.horizontal,
                      onPageChanged: (index) {
                        setState(() {
                          pageChanged = index;
                          questionIndex = index + 1;
                          print(pageChanged);
                        });
                      },
                      itemBuilder: (_, index) {
                        return Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 15.0, vertical: 15.0),
                          child: Column(
                            children: [
                              Row(
                                mainAxisAlignment: mainAxisCenter,
                                children: [
                                  Text(
                                    'Pertanyaan $questionIndex/${quizC.quizData.value.data.length}',
                                    style: kBoldStyle.copyWith(color: kBlackColor.withOpacity(0.3), fontWeight: FontWeight.w600, fontSize: 20),
                                  ),
                                ],
                              ),
                              size10,
                              Stack(
                                children: [
                                  Container(
                                    height: 5,
                                    color: kOrdinaryColor.withOpacity(0.5),
                                  ),
                                  AnimatedContainer(
                                    height: 5,
                                      duration: Duration(microseconds: 100),
                                    width:questionIndex==1?size.width / 4:
                                    questionIndex==2?size.width/2 : size.width,
                                    color: kPrimaryColor,
                                    curve: Curves.bounceIn,),

                                  /*Container(
                                    height: 5,
                                    width:questionIndex==1?size.width / 4:
                                    questionIndex==2?size.width/2 : size.width,
                                    color: kPrimaryColor,
                                  )*/
                                ],
                              ),
                              size20,

                              Text(
                                '${quizC.quizData.value.data[index].qQuestion}',
                                style: kBoldStyle.copyWith(color: kBlackColor, fontWeight: FontWeight.w600, fontSize: 20),
                              ),
                              size20,
                              Obx(()=>Container(
                                decoration: BoxDecoration(
                                  color: kWhiteColor,
                                  borderRadius: BorderRadius.circular(8.0),
                                  boxShadow: [
                                    BoxShadow(
                                      color: kOrdinaryColor.withOpacity(0.1),
                                      spreadRadius: 5,
                                      blurRadius: 7,
                                      offset: Offset(0, 3), // changes position of shadow
                                    ),
                                  ],
                                ),
                                child: ListTile(
                                  onTap: (){
                                    print(index);
                                    _onSelected(index, 'a');
                                    if(quizC.quizData.value.data[index].qJawaban=="a"){
                                      print("TRUE");
                                      //quizC.indexValue.value=0;
                                      quizC.ansA.value="true";
                                      quizC.ansB.value="false";
                                      quizC.ansC.value="false";
                                      quizC.ansD.value="false";

                                    }else if(quizC.quizData.value.data[index].qJawaban=="b"){
                                      print("TRUE");
                                      quizC.ansA.value="false";
                                      quizC.ansB.value="true";
                                      quizC.ansC.value="false";
                                      quizC.ansD.value="false";
                                    }else if(quizC.quizData.value.data[index].qJawaban=="c"){
                                      print("TRUE");
                                      quizC.ansA.value="false";
                                      quizC.ansB.value="false";
                                      quizC.ansC.value="true";
                                      quizC.ansD.value="false";

                                    }else if(quizC.quizData.value.data[index].qJawaban=="d"){
                                      print("TRUE");
                                      quizC.ansA.value="false";
                                      quizC.ansB.value="false";
                                      quizC.ansC.value="false";
                                      quizC.ansD.value="true";
                                    }else{
                                      //quizC.ansA.value="false";
                                      print("false");
                                    }
                                    print("$index value");
                                  },
                                  title: Row(
                                    children: [
                                      Container(
                                        height: 15,width: 15,
                                        decoration: BoxDecoration(
                                            shape: BoxShape.circle,
                                            color:_selectedIndex!=null&& _selectedIndex==index && quizC.ansA.value=="true"?kPrimaryColor:kWhiteColor,
                                            border: Border.all(color:_selectedIndex!=null&& _selectedIndex==index && quizC.ansA.value=="true"?kPrimaryColor:kBlackColor)
                                        ),
                                      ),
                                      width10,
                                      Expanded(
                                        child: Text(
                                          '${quizC.quizData.value.data[index].qAnswerA}',
                                          style: TextStyle(color:_selectedIndex!=null&& _selectedIndex==index && quizC.ansA.value=="true"? kPrimaryColor : kBlackColor),
                                        ),
                                      ),
                                    ],
                                  ),

                                ),
                              )),
                              size20,
                              Obx(()=>Container(
                                decoration: BoxDecoration(
                                  color: kWhiteColor,
                                  borderRadius: BorderRadius.circular(8.0),
                                  boxShadow: [
                                    BoxShadow(
                                      color: kOrdinaryColor.withOpacity(0.1),
                                      spreadRadius: 5,
                                      blurRadius: 7,
                                      offset: Offset(0, 3), // changes position of shadow
                                    ),
                                  ],
                                ),
                                child: ListTile(
                                  onTap: (){
                                    _onSelected(index, 'b');
                                    if(quizC.quizData.value.data[index].qJawaban=="b"){
                                      print("TRUE");
                                      //quizC.indexValue.value=0;
                                      quizC.ansA.value="false";
                                      quizC.ansB.value="true";
                                      quizC.ansC.value="false";
                                      quizC.ansD.value="false";
                                    }else if(quizC.quizData.value.data[index].qJawaban=="a"){
                                      print("TRUE");
                                      quizC.ansA.value="true";
                                      quizC.ansB.value="false";
                                      quizC.ansC.value="false";
                                      quizC.ansD.value="false";
                                    }else if(quizC.quizData.value.data[index].qJawaban=="c"){
                                      print("TRUE");
                                      quizC.ansA.value="false";
                                      quizC.ansB.value="false";
                                      quizC.ansC.value="true";
                                      quizC.ansD.value="false";
                                    }else if(quizC.quizData.value.data[index].qJawaban=="d"){
                                      print("TRUE");
                                      quizC.ansA.value="false";
                                      quizC.ansB.value="false";
                                      quizC.ansC.value="false";
                                      quizC.ansD.value="true";
                                    }else{
                                      //quizC.ansA.value="false";
                                      print("false");
                                    }
                                    /*if(quizC.quizData.value.data[index].qJawaban==quizC.ansB.value){
                                      print("TRUE");
                                    }else{
                                      print("false");
                                    }*/
                                  },

                                  title: Row(
                                    children: [
                                      Container(
                                        height: 15,width: 15,
                                        decoration: BoxDecoration(
                                          shape: BoxShape.circle,
                                            color:_selectedIndex!=null&& _selectedIndex==index && quizC.ansB.value=="true"?kPrimaryColor:kWhiteColor,
                                            border: Border.all(color:_selectedIndex!=null&& _selectedIndex==index && quizC.ansB.value=="true"?kPrimaryColor:kBlackColor)
                                        ),
                                      ),
                                      width10,
                                      Expanded(
                                        child: Text(
                                          '${quizC.quizData.value.data[index].qAnswerB}',
                                          style: TextStyle(color:_selectedIndex!=null&&
                                              _selectedIndex==index && quizC.ansB.value=="true"? kPrimaryColor : kBlackColor,),
                                        ),
                                      ),
                                    ],
                                  ),

                                ),
                              ),),
                              size20,
                              Obx(()=>Container(
                                decoration: BoxDecoration(
                                  color: kWhiteColor,
                                  borderRadius: BorderRadius.circular(8.0),
                                  boxShadow: [
                                    BoxShadow(
                                      color: kOrdinaryColor.withOpacity(0.1),
                                      spreadRadius: 5,
                                      blurRadius: 7,
                                      offset: Offset(0, 3), // changes position of shadow
                                    ),
                                  ],
                                ),
                                child: ListTile(
                                  onTap: (){
                                    _onSelected(index, 'c');
                                    if(quizC.quizData.value.data[index].qJawaban=="c"){
                                      //print("TRUE");
                                      quizC.indexValue.value=0;
                                      quizC.ansA.value="false";
                                      quizC.ansB.value="false";
                                      quizC.ansC.value="true";
                                      quizC.ansD.value="false";
                                    }else if(quizC.quizData.value.data[index].qJawaban=="a"){
                                      //print("TRUE");
                                      quizC.ansA.value="true";
                                      quizC.ansB.value="false";
                                      quizC.ansC.value="false";
                                      quizC.ansD.value="false";
                                    }else if(quizC.quizData.value.data[index].qJawaban=="b"){
                                      print("TRUE");
                                      quizC.ansA.value="false";
                                      quizC.ansB.value="true";
                                      quizC.ansC.value="false";
                                      quizC.ansD.value="false";
                                    }else if(quizC.quizData.value.data[index].qJawaban=="d"){
                                      print("TRUE");
                                      quizC.ansA.value="false";
                                      quizC.ansB.value="false";
                                      quizC.ansC.value="false";
                                      quizC.ansD.value="true";
                                    }else{
                                      //quizC.ansA.value="false";
                                      print("false");
                                    }
                                    /*if(quizC.quizData.value.data[index].qJawaban==quizC.ansC.value){
                                      print("TRUE");
                                    }else{
                                      print("false");
                                    }*/
                                  },
                                  title: Row(
                                    children: [
                                      Container(
                                        height: 15,width: 15,
                                        decoration: BoxDecoration(
                                            shape: BoxShape.circle,
                                            color:_selectedIndex!=null&& _selectedIndex==index && quizC.ansC.value=="true"?kPrimaryColor:kWhiteColor,
                                            border: Border.all(color:_selectedIndex!=null&& _selectedIndex==index && quizC.ansC.value=="true"?kPrimaryColor:kBlackColor)
                                        ),
                                      ),
                                      width20,
                                      Expanded(
                                        child: Text(
                                          '${quizC.quizData.value.data[index].qAnswerC}',
                                          style: TextStyle(color: _selectedIndex!=null&& _selectedIndex==index && quizC.ansC.value=="true" ? kPrimaryColor : kBlackColor),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              )),
                              size10,
                              Obx(()=>Container(
                                decoration: BoxDecoration(
                                  color: kWhiteColor,
                                  borderRadius: BorderRadius.circular(8.0),
                                  boxShadow: [
                                    BoxShadow(
                                      color: kOrdinaryColor.withOpacity(0.1),
                                      spreadRadius: 5,
                                      blurRadius: 7,
                                      offset: Offset(0, 3), // changes position of shadow
                                    ),
                                  ],
                                ),
                                child: ListTile(
                                  onTap: (){
                                    _onSelected(index, 'd');
                                    if(quizC.quizData.value.data[index].qJawaban=="d"){
                                      print("TRUE");
                                      quizC.indexValue.value=0;
                                      quizC.ansA.value="false";
                                      quizC.ansB.value="false";
                                      quizC.ansC.value="false";
                                      quizC.ansD.value="true";
                                    }else if(quizC.quizData.value.data[index].qJawaban=="b"){
                                      print("TRUE");
                                      quizC.ansA.value="false";
                                      quizC.ansB.value="true";
                                      quizC.ansC.value="false";
                                      quizC.ansD.value="false";
                                    }else if(quizC.quizData.value.data[index].qJawaban=="c"){
                                      print("TRUE");
                                      quizC.ansA.value="false";
                                      quizC.ansB.value="false";
                                      quizC.ansC.value="true";
                                      quizC.ansD.value="false";
                                    }else if(quizC.quizData.value.data[index].qJawaban=="a"){
                                      print("TRUE");
                                      quizC.ansA.value="true";
                                      quizC.ansB.value="false";
                                      quizC.ansC.value="false";
                                      quizC.ansD.value="false";
                                    }else{
                                      //quizC.ansA.value="false";
                                      print("false");
                                    }
                                  },
                                  title: Row(
                                    children: [
                                      Container(
                                        height: 15,width: 15,
                                        decoration: BoxDecoration(
                                            shape: BoxShape.circle,color:_selectedIndex!=null&& _selectedIndex==index && quizC.ansD.value=="true"?kPrimaryColor:kWhiteColor,
                                            border: Border.all(color:_selectedIndex!=null&& _selectedIndex==index && quizC.ansD.value=="true"?kPrimaryColor:kBlackColor)
                                        ),
                                      ),
                                      width20,
                                      Expanded(
                                        child: Text(
                                          '${quizC.quizData.value.data[index].qAnswerD}',
                                          style: TextStyle(color: _selectedIndex!=null&& _selectedIndex==index && quizC.ansD.value=="true" ? kPrimaryColor : kBlackColor),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              )),
                            ],
                          ),
                        );
                      },
                    ),
        )
        /*body: PageView(
          pageSnapping: true,
          controller: _controller,
          onPageChanged: (index) {
            setState(() {
              pageChanged = index;
              print(pageChanged);
            });
          },
          children: [
            Container(
              padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
              child: Column(
                crossAxisAlignment: crossAxisStart,
                children: [
                  size10,
                  Row(
                    mainAxisAlignment: mainAxisCenter,
                    children: [
                      Text(
                        'Pertanyaan 1/3',
                        style: kBoldStyle.copyWith(
                            color: kBlackColor.withOpacity(0.3),
                            fontWeight: FontWeight.w600,
                            fontSize: 20),
                      ),
                    ],
                  ),
                  size20,
                  Stack(
                    children: [
                      Container(
                        height: 5,
                        color: kOrdinaryColor.withOpacity(0.5),
                      ),
                      Container(
                        height: 5,
                        width: size.width / 4,
                        color: kPrimaryColor,
                      )
                    ],
                  ),
                  size20,
                  Text(
                    'Apa yang dimaksud dengan Short Selling?',
                    style: kBoldStyle.copyWith(
                        color: kBlackColor,
                        fontWeight: FontWeight.w600,
                        fontSize: 20),
                  ),
                  size20,
                  Container(
                    decoration: _site == Q1Options.one
                        ? BoxDecoration(
                            color: kPrimaryColor,
                            borderRadius: BorderRadius.circular(10.0),
                            boxShadow: [
                              BoxShadow(
                                color: kOrdinaryColor.withOpacity(0.1),
                                spreadRadius: 5,
                                blurRadius: 7,
                                offset:
                                    Offset(0, 3), // changes position of shadow
                              ),
                            ],
                          )
                        : BoxDecoration(
                            color: kWhiteColor,
                            borderRadius: BorderRadius.circular(10.0),
                            boxShadow: [
                              BoxShadow(
                                color: kOrdinaryColor.withOpacity(0.1),
                                spreadRadius: 5,
                                blurRadius: 7,
                                offset:
                                    Offset(0, 3), // changes position of shadow
                              ),
                            ],
                          ),
                    child: ListTile(
                      title: Text(
                        'Transaksi Penjualan Instrumen tanpa memiliki instrumen tersebut',
                        style: TextStyle(
                            color: _site == Q1Options.one
                                ? kWhiteColor
                                : kBlackColor),
                      ),
                      leading: Radio(
                        value: Q1Options.one,
                        activeColor: kWhiteColor,
                        groupValue: _site,
                        onChanged: (Q1Options value) {
                          setState(() {
                            _site = value;
                          });
                        },
                      ),
                    ),
                  ),
                  size10,
                  Container(
                    decoration: _site == Q1Options.two
                        ? BoxDecoration(
                            color: kPrimaryColor,
                            borderRadius: BorderRadius.circular(10.0),
                            boxShadow: [
                              BoxShadow(
                                color: kOrdinaryColor.withOpacity(0.1),
                                spreadRadius: 5,
                                blurRadius: 7,
                                offset:
                                    Offset(0, 3), // changes position of shadow
                              ),
                            ],
                          )
                        : BoxDecoration(
                            color: kWhiteColor,
                            borderRadius: BorderRadius.circular(10.0),
                            boxShadow: [
                              BoxShadow(
                                color: kOrdinaryColor.withOpacity(0.1),
                                spreadRadius: 5,
                                blurRadius: 7,
                                offset:
                                    Offset(0, 3), // changes position of shadow
                              ),
                            ],
                          ),
                    child: ListTile(
                      title: Text(
                        'Dewan rapat kebijakan bank sentral Amerika Serikat.',
                        style: TextStyle(
                            color: _site == Q1Options.two
                                ? kWhiteColor
                                : kBlackColor),
                      ),
                      leading: Radio(
                        value: Q1Options.two,
                        activeColor: kWhiteColor,
                        groupValue: _site,
                        onChanged: (Q1Options value) {
                          setState(() {
                            _site = value;
                          });
                        },
                      ),
                    ),
                  ),
                  size10,
                  Container(
                    decoration: _site == Q1Options.three
                        ? BoxDecoration(
                            color: kPrimaryColor,
                            borderRadius: BorderRadius.circular(10.0),
                            boxShadow: [
                              BoxShadow(
                                color: kOrdinaryColor.withOpacity(0.1),
                                spreadRadius: 5,
                                blurRadius: 7,
                                offset:
                                    Offset(0, 3), // changes position of shadow
                              ),
                            ],
                          )
                        : BoxDecoration(
                            color: kWhiteColor,
                            borderRadius: BorderRadius.circular(10.0),
                            boxShadow: [
                              BoxShadow(
                                color: kOrdinaryColor.withOpacity(0.1),
                                spreadRadius: 5,
                                blurRadius: 7,
                                offset:
                                    Offset(0, 3), // changes position of shadow
                              ),
                            ],
                          ),
                    child: ListTile(
                      title: Text(
                        'Model Eksekusi dengan Feed harga bursa asli dari pasar acuan',
                        style: TextStyle(
                            color: _site == Q1Options.three
                                ? kWhiteColor
                                : kBlackColor),
                      ),
                      leading: Radio(
                        value: Q1Options.three,
                        activeColor: kWhiteColor,
                        groupValue: _site,
                        onChanged: (Q1Options value) {
                          setState(() {
                            _site = value;
                          });
                        },
                      ),
                    ),
                  ),
                  size10,
                  Container(
                    decoration: _site == Q1Options.four
                        ? BoxDecoration(
                            color: kPrimaryColor,
                            borderRadius: BorderRadius.circular(10.0),
                            boxShadow: [
                              BoxShadow(
                                color: kOrdinaryColor.withOpacity(0.1),
                                spreadRadius: 5,
                                blurRadius: 7,
                                offset:
                                    Offset(0, 3), // changes position of shadow
                              ),
                            ],
                          )
                        : BoxDecoration(
                            color: kWhiteColor,
                            borderRadius: BorderRadius.circular(10.0),
                            boxShadow: [
                              BoxShadow(
                                color: kOrdinaryColor.withOpacity(0.1),
                                spreadRadius: 5,
                                blurRadius: 7,
                                offset:
                                    Offset(0, 3), // changes position of shadow
                              ),
                            ],
                          ),
                    child: ListTile(
                      title: Text(
                        'Proses pengisian dana ke akun trading',
                        style: TextStyle(
                            color: _site == Q1Options.four
                                ? kWhiteColor
                                : kBlackColor),
                      ),
                      leading: Radio(
                        value: Q1Options.four,
                        activeColor: kWhiteColor,
                        groupValue: _site,
                        onChanged: (Q1Options value) {
                          setState(() {
                            _site = value;
                          });
                        },
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
              child: Column(
                crossAxisAlignment: crossAxisStart,
                children: [
                  size10,
                  Row(
                    mainAxisAlignment: mainAxisCenter,
                    children: [
                      Text(
                        'Pertanyaan 2/3',
                        style: kBoldStyle.copyWith(
                            color: kBlackColor.withOpacity(0.3),
                            fontWeight: FontWeight.w600,
                            fontSize: 20),
                      ),
                    ],
                  ),
                  size20,
                  Stack(
                    children: [
                      Container(
                        height: 5,
                        color: kOrdinaryColor.withOpacity(0.5),
                      ),
                      Container(
                        height: 5,
                        width: size.width / 2,
                        color: kPrimaryColor,
                      )
                    ],
                  ),
                  size20,
                  Text(
                    'Apa yang dimaksud Dividen?',
                    style: kBoldStyle.copyWith(
                        color: kBlackColor,
                        fontWeight: FontWeight.w600,
                        fontSize: 20),
                  ),
                  size20,
                  Container(
                    decoration: _site == Q1Options.one
                        ? BoxDecoration(
                            color: kPrimaryColor,
                            borderRadius: BorderRadius.circular(10.0),
                            boxShadow: [
                              BoxShadow(
                                color: kOrdinaryColor.withOpacity(0.1),
                                spreadRadius: 5,
                                blurRadius: 7,
                                offset:
                                    Offset(0, 3), // changes position of shadow
                              ),
                            ],
                          )
                        : BoxDecoration(
                            color: kWhiteColor,
                            borderRadius: BorderRadius.circular(10.0),
                            boxShadow: [
                              BoxShadow(
                                color: kOrdinaryColor.withOpacity(0.1),
                                spreadRadius: 5,
                                blurRadius: 7,
                                offset:
                                    Offset(0, 3), // changes position of shadow
                              ),
                            ],
                          ),
                    child: ListTile(
                      title: Text(
                        'Transaksi Penjualan Instrumen tanpa memiliki instrumen tersebut',
                        style: TextStyle(
                            color: _site == Q1Options.one
                                ? kWhiteColor
                                : kBlackColor),
                      ),
                      leading: Radio(
                        value: Q1Options.one,
                        activeColor: kWhiteColor,
                        groupValue: _site,
                        onChanged: (Q1Options value) {
                          setState(() {
                            _site = value;
                          });
                        },
                      ),
                    ),
                  ),
                  size10,
                  Container(
                    decoration: _site == Q1Options.two
                        ? BoxDecoration(
                            color: kPrimaryColor,
                            borderRadius: BorderRadius.circular(10.0),
                            boxShadow: [
                              BoxShadow(
                                color: kOrdinaryColor.withOpacity(0.1),
                                spreadRadius: 5,
                                blurRadius: 7,
                                offset:
                                    Offset(0, 3), // changes position of shadow
                              ),
                            ],
                          )
                        : BoxDecoration(
                            color: kWhiteColor,
                            borderRadius: BorderRadius.circular(10.0),
                            boxShadow: [
                              BoxShadow(
                                color: kOrdinaryColor.withOpacity(0.1),
                                spreadRadius: 5,
                                blurRadius: 7,
                                offset:
                                    Offset(0, 3), // changes position of shadow
                              ),
                            ],
                          ),
                    child: ListTile(
                      title: Text(
                        'Dewan rapat kebijakan bank sentral Amerika Serikat.',
                        style: TextStyle(
                            color: _site == Q1Options.two
                                ? kWhiteColor
                                : kBlackColor),
                      ),
                      leading: Radio(
                        value: Q1Options.two,
                        activeColor: kWhiteColor,
                        groupValue: _site,
                        onChanged: (Q1Options value) {
                          setState(() {
                            _site = value;
                          });
                        },
                      ),
                    ),
                  ),
                  size10,
                  Container(
                    decoration: _site == Q1Options.three
                        ? BoxDecoration(
                            color: kPrimaryColor,
                            borderRadius: BorderRadius.circular(10.0),
                            boxShadow: [
                              BoxShadow(
                                color: kOrdinaryColor.withOpacity(0.1),
                                spreadRadius: 5,
                                blurRadius: 7,
                                offset:
                                    Offset(0, 3), // changes position of shadow
                              ),
                            ],
                          )
                        : BoxDecoration(
                            color: kWhiteColor,
                            borderRadius: BorderRadius.circular(10.0),
                            boxShadow: [
                              BoxShadow(
                                color: kOrdinaryColor.withOpacity(0.1),
                                spreadRadius: 5,
                                blurRadius: 7,
                                offset:
                                    Offset(0, 3), // changes position of shadow
                              ),
                            ],
                          ),
                    child: ListTile(
                      title: Text(
                        'Model Eksekusi dengan Feed harga bursa asli dari pasar acuan',
                        style: TextStyle(
                            color: _site == Q1Options.three
                                ? kWhiteColor
                                : kBlackColor),
                      ),
                      leading: Radio(
                        value: Q1Options.three,
                        activeColor: kWhiteColor,
                        groupValue: _site,
                        onChanged: (Q1Options value) {
                          setState(() {
                            _site = value;
                          });
                        },
                      ),
                    ),
                  ),
                  size10,
                  Container(
                    decoration: _site == Q1Options.four
                        ? BoxDecoration(
                            color: kPrimaryColor,
                            borderRadius: BorderRadius.circular(10.0),
                            boxShadow: [
                              BoxShadow(
                                color: kOrdinaryColor.withOpacity(0.1),
                                spreadRadius: 5,
                                blurRadius: 7,
                                offset:
                                    Offset(0, 3), // changes position of shadow
                              ),
                            ],
                          )
                        : BoxDecoration(
                            color: kWhiteColor,
                            borderRadius: BorderRadius.circular(10.0),
                            boxShadow: [
                              BoxShadow(
                                color: kOrdinaryColor.withOpacity(0.1),
                                spreadRadius: 5,
                                blurRadius: 7,
                                offset:
                                    Offset(0, 3), // changes position of shadow
                              ),
                            ],
                          ),
                    child: ListTile(
                      title: Text(
                        'Proses pengisian dana ke akun trading',
                        style: TextStyle(
                            color: _site == Q1Options.four
                                ? kWhiteColor
                                : kBlackColor),
                      ),
                      leading: Radio(
                        value: Q1Options.four,
                        activeColor: kWhiteColor,
                        groupValue: _site,
                        onChanged: (Q1Options value) {
                          setState(() {
                            _site = value;
                          });
                        },
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
              child: Column(
                crossAxisAlignment: crossAxisStart,
                children: [
                  size10,
                  Row(
                    mainAxisAlignment: mainAxisCenter,
                    children: [
                      Text(
                        'Pertanyaan 3/3',
                        style: kBoldStyle.copyWith(
                            color: kBlackColor.withOpacity(0.3),
                            fontWeight: FontWeight.w600,
                            fontSize: 20),
                      ),
                    ],
                  ),
                  size20,
                  Stack(
                    children: [
                      Container(
                        height: 5,
                        color: kOrdinaryColor.withOpacity(0.5),
                      ),
                      Container(
                        height: 5,
                        width: size.width / 1,
                        color: kPrimaryColor,
                      )
                    ],
                  ),
                  size20,
                  Text(
                    'Konsep CFD adalah?',
                    style: kBoldStyle.copyWith(
                        color: kBlackColor,
                        fontWeight: FontWeight.w600,
                        fontSize: 20),
                  ),
                  size20,
                  Container(
                    decoration: _site == Q1Options.one
                        ? BoxDecoration(
                            color: kPrimaryColor,
                            borderRadius: BorderRadius.circular(10.0),
                            boxShadow: [
                              BoxShadow(
                                color: kOrdinaryColor.withOpacity(0.1),
                                spreadRadius: 5,
                                blurRadius: 7,
                                offset:
                                    Offset(0, 3), // changes position of shadow
                              ),
                            ],
                          )
                        : BoxDecoration(
                            color: kWhiteColor,
                            borderRadius: BorderRadius.circular(10.0),
                            boxShadow: [
                              BoxShadow(
                                color: kOrdinaryColor.withOpacity(0.1),
                                spreadRadius: 5,
                                blurRadius: 7,
                                offset:
                                    Offset(0, 3), // changes position of shadow
                              ),
                            ],
                          ),
                    child: ListTile(
                      title: Text(
                        'Transaksi Penjualan Instrumen tanpa memiliki instrumen tersebut',
                        style: TextStyle(
                            color: _site == Q1Options.one
                                ? kWhiteColor
                                : kBlackColor),
                      ),
                      leading: Radio(
                        value: Q1Options.one,
                        activeColor: kWhiteColor,
                        groupValue: _site,
                        onChanged: (Q1Options value) {
                          setState(() {
                            _site = value;
                          });
                        },
                      ),
                    ),
                  ),
                  size10,
                  Container(
                    decoration: _site == Q1Options.two
                        ? BoxDecoration(
                            color: kPrimaryColor,
                            borderRadius: BorderRadius.circular(10.0),
                            boxShadow: [
                              BoxShadow(
                                color: kOrdinaryColor.withOpacity(0.1),
                                spreadRadius: 5,
                                blurRadius: 7,
                                offset:
                                    Offset(0, 3), // changes position of shadow
                              ),
                            ],
                          )
                        : BoxDecoration(
                            color: kWhiteColor,
                            borderRadius: BorderRadius.circular(10.0),
                            boxShadow: [
                              BoxShadow(
                                color: kOrdinaryColor.withOpacity(0.1),
                                spreadRadius: 5,
                                blurRadius: 7,
                                offset:
                                    Offset(0, 3), // changes position of shadow
                              ),
                            ],
                          ),
                    child: ListTile(
                      title: Text(
                        'Dewan rapat kebijakan bank sentral Amerika Serikat.',
                        style: TextStyle(
                            color: _site == Q1Options.two
                                ? kWhiteColor
                                : kBlackColor),
                      ),
                      leading: Radio(
                        value: Q1Options.two,
                        activeColor: kWhiteColor,
                        groupValue: _site,
                        onChanged: (Q1Options value) {
                          setState(() {
                            _site = value;
                          });
                        },
                      ),
                    ),
                  ),
                  size10,
                  Container(
                    decoration: _site == Q1Options.three
                        ? BoxDecoration(
                            color: kPrimaryColor,
                            borderRadius: BorderRadius.circular(10.0),
                            boxShadow: [
                              BoxShadow(
                                color: kOrdinaryColor.withOpacity(0.1),
                                spreadRadius: 5,
                                blurRadius: 7,
                                offset:
                                    Offset(0, 3), // changes position of shadow
                              ),
                            ],
                          )
                        : BoxDecoration(
                            color: kWhiteColor,
                            borderRadius: BorderRadius.circular(10.0),
                            boxShadow: [
                              BoxShadow(
                                color: kOrdinaryColor.withOpacity(0.1),
                                spreadRadius: 5,
                                blurRadius: 7,
                                offset:
                                    Offset(0, 3), // changes position of shadow
                              ),
                            ],
                          ),
                    child: ListTile(
                      title: Text(
                        'Model Eksekusi dengan Feed harga bursa asli dari pasar acuan',
                        style: TextStyle(
                            color: _site == Q1Options.three
                                ? kWhiteColor
                                : kBlackColor),
                      ),
                      leading: Radio(
                        value: Q1Options.three,
                        activeColor: kWhiteColor,
                        groupValue: _site,
                        onChanged: (Q1Options value) {
                          setState(() {
                            _site = value;
                          });
                        },
                      ),
                    ),
                  ),
                  size10,
                  Container(
                    decoration: _site == Q1Options.four
                        ? BoxDecoration(
                            color: kPrimaryColor,
                            borderRadius: BorderRadius.circular(10.0),
                            boxShadow: [
                              BoxShadow(
                                color: kOrdinaryColor.withOpacity(0.1),
                                spreadRadius: 5,
                                blurRadius: 7,
                                offset:
                                    Offset(0, 3), // changes position of shadow
                              ),
                            ],
                          )
                        : BoxDecoration(
                            color: kWhiteColor,
                            borderRadius: BorderRadius.circular(10.0),
                            boxShadow: [
                              BoxShadow(
                                color: kOrdinaryColor.withOpacity(0.1),
                                spreadRadius: 5,
                                blurRadius: 7,
                                offset:
                                    Offset(0, 3), // changes position of shadow
                              ),
                            ],
                          ),
                    child: ListTile(
                      title: Text(
                        'Proses pengisian dana ke akun trading',
                        style: TextStyle(
                            color: _site == Q1Options.four
                                ? kWhiteColor
                                : kBlackColor),
                      ),
                      leading: Radio(
                        value: Q1Options.four,
                        activeColor: kWhiteColor,
                        groupValue: _site,
                        onChanged: (Q1Options value) {
                          setState(() {
                            _site = value;
                          });
                        },
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        )*/
        );
  }
}
