import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:orbitrade/controller/quiazQuestionsController.dart';
import 'package:orbitrade/sheared/buildDepositAppBar.dart';
import 'package:orbitrade/sheared/default_btn.dart';

import '../../../constants.dart';
import '../../../size_config.dart';
import '../Home.dart';

class QuizCompletePage extends StatefulWidget {


  static const routeName = 'complete_quiz_page';
  String point;

  QuizCompletePage({this.point});
  @override
  _QuizCompletePageState createState() => _QuizCompletePageState();
}

class _QuizCompletePageState extends State<QuizCompletePage> {

  @override
  void initState() {
    // TODO: implement initState
    //quizC.getQuizQuestion(id);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: buildDepositAppBar(
        context: context,
        title: 'Hasil Post Video Quiz',
        rightTitle: 'Finish',
        onPress: (){
          Navigator.of(context).pushNamedAndRemoveUntil(
              Home.routeName, (Route<dynamic> route) => false);
        }
      ),
      body: Column(
        children: [
          Expanded(
            flex: 8,
            child: Column(
              children: [
                SizedBox(
                  height: getProportionateScreenHeight(100),
                ),
                Center(
                  child: Container(
                    width: 180,
                    height: 180,
                    child: Hero(
                      tag: 'winning',
                      child: Image.asset(kImageDir + 'winning.png'),
                    ),
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Container(
                  child: Text(
                    'Selamat!',
                    style: kHeadLine2.copyWith(color: Colors.black),
                  ),
                ),
                SizedBox(
                  height: 15,
                ),
                Container(
                  margin: EdgeInsets.only(left: 15, right: 15),
                  child: Text(
                    'Kamu telah menyelesaikan Video Materi “Apa itu OTC Market?”',
                    textAlign: TextAlign.center,
                    style:
                        kBoldStyle.copyWith(fontSize: 14, color: Colors.black),
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Container(
                  alignment: Alignment.center,
                  child: Text(
                    'Poin yang kamu dapatkan',
                    textAlign: TextAlign.center,
                    style: kSmallText.copyWith(
                      color: Colors.black,
                    ),
                  ),
                ),
                SizedBox(
                  height: 15,
                ),
                Container(
                  child: Text(
                    widget.point,
                    style: kHeadLine.copyWith(color: kSecondaryColor),
                  ),
                ),
              ],
            ),
          ),
          Expanded(
            flex: 2,
            child: Column(
              children: [
                Container(
                  margin: EdgeInsets.only(left: 10, right: 10, top: 20),
                  child: Padding(
                    padding: EdgeInsets.symmetric(
                      vertical: getProportionateScreenHeight(20),
                    ),
                    child: DefaultBtn(
                      width: SizeConfig.screenWidth,
                      title: 'Selesai',
                      onPress: () async {
                        Navigator.of(context).pushNamedAndRemoveUntil(
                            Home.routeName, (Route<dynamic> route) => false);
                      },
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
