import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:orbitrade/controller/categoryContoller.dart';
import 'package:orbitrade/model/signal_item.dart';
import 'package:orbitrade/model/singnal_list_model.dart';
import 'package:orbitrade/sheared/custom_loader.dart';
import 'package:flutter_dropdown/flutter_dropdown.dart';
import '../../../app_config.dart';
import '../../../constants.dart';
import '../../../size_config.dart';

class SignalDetailsPage extends StatefulWidget {
  static const routeName = 'signal_details_page';

  String id;

  SignalDetailsPage({this.id});

  @override
  _yourPageState createState() => _yourPageState();
}

class _yourPageState extends State<SignalDetailsPage> {
  String selectedSignal_id;

  String firstItem;

  var controllerC=Get.put(CategoryController());



  @override
  void initState() {
    // TODO: implement initState

    controllerC.getSignalRelease(widget.id);

    super.initState();
  }

  Row buildDropDownRow(Datas data) {
    return Row(
      children: <Widget>[
        Text(data.signalName)
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildAppBar(context: context, title: 'Signal'),
      body: SingleChildScrollView(
        child: buildBodyTwo(),
      ),
    );
  }

  buildBody() {
    return Padding(
      padding: EdgeInsets.symmetric(
        horizontal: getProportionateScreenWidth(15),
      ),
      child: Column(
        children: [
          SizedBox(
            height: 10,
          ),
          Obx(()=>Container(
            alignment: Alignment.centerLeft,
            padding: EdgeInsets.all(16),
            decoration: BoxDecoration(
              color: Colors.white,
              border: Border.all(width: 2, color: Color(0xFFF2F2F2)),
              borderRadius: new BorderRadius.all(
                const Radius.circular(5.0),
              ),
            ),
            margin: EdgeInsets.only(
              top: 2,
            ),
            width: MediaQuery.of(context).size.width,
            child: DropdownButtonHideUnderline(
              child: DropdownButton<String>(
                icon: Icon(
                  Icons.keyboard_arrow_down_outlined,
                  color: kBlackColor,
                  size: 20.09,
                ),
                hint: Text("${controllerC.categoryListType.value}"),
                value: controllerC.categoryListType.value,
                isDense: true,
                onChanged: (String newValue) async{
                  controllerC.categoryListType.value = newValue;
                  //controllerC.getSignalRelease(controllerC.signalReleaseID.value);
                  print("${controllerC.categoryListType.value}");
                },
                items: controllerC.singnalListData.value.data.map((Datas map) {
                  return DropdownMenuItem<String>(
                      value: map.signalName.toString(),
                      child: Container(
                        margin: EdgeInsets.only(left: 10),
                        child: SizedBox(
                          width: SizeConfig.screenWidth - 110, // for example
                          child: Text(map.signalName.toString(), textAlign: TextAlign.left),
                        ),
                      ));
                }).toList(),
              ),
            ),
          ),),
          size20,
          Obx(()=> controllerC.signalReleaseData.value.data==null?
          CustomLoader():controllerC.signalReleaseData.value.data.length==0?textRoboto("No data found", kBlackColor):Container(
            alignment: Alignment.centerLeft,
            margin: EdgeInsets.only(left: 10, right: 10, top: 10),
            child: ListView.builder(
                shrinkWrap: true,
                scrollDirection: Axis.vertical,
                itemCount: controllerC.signalReleaseData.value.data.length,
                physics: NeverScrollableScrollPhysics(),
                itemBuilder: (context, index) {
                  var obj=controllerC.signalReleaseData.value.data;
                  return GestureDetector(
                    onTap: () {},
                    child: Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Container(
                                  alignment: Alignment.centerLeft,
                                  child: Text(
                                    '${obj[index].type}',
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                    style: kRegularText.copyWith(
                                        color: Colors.black,
                                        fontWeight: FontWeight.w600),
                                  ),
                                ),
                                Container(
                                  child: Text(
                                    '${obj[index].pair}',
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(
                                        fontSize:
                                        getProportionateScreenWidth(16.0),
                                        color: Colors.grey),
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Column(
                              children: [
                                Container(
                                  child: Text(
                                    'Price',
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(
                                        fontSize:
                                        getProportionateScreenWidth(15.0),
                                        color: Colors.grey),
                                  ),
                                ),
                                Container(
                                  child: Text(
                                    '${obj[index].price}',
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                    style: kRegularText.copyWith(
                                        fontSize:
                                        getProportionateScreenWidth(15.0),
                                        color: Colors.black,
                                        fontWeight: FontWeight.w600),
                                  ),
                                ),
                              ],
                            ),
                            Column(
                              children: [
                                Container(
                                  child: Text(
                                    'TP',
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(
                                        fontSize:
                                        getProportionateScreenWidth(15.0),
                                        color: Colors.grey),
                                  ),
                                ),
                                Container(
                                  child: Text(
                                    '1766',
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                    style: kRegularText.copyWith(
                                        fontSize:
                                        getProportionateScreenWidth(15.0),
                                        color: Colors.black,
                                        fontWeight: FontWeight.w600),
                                  ),
                                ),
                              ],
                            ),
                            Column(
                              children: [
                                Container(
                                  child: Text(
                                    'SL',
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(
                                        fontSize:
                                        getProportionateScreenWidth(15.0),
                                        color: Colors.grey),
                                  ),
                                ),
                                Container(
                                  child: Text(
                                    '1796',
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                    style: kRegularText.copyWith(
                                        fontSize:
                                        getProportionateScreenWidth(15.0),
                                        color: Colors.black,
                                        fontWeight: FontWeight.w600),
                                  ),
                                ),
                              ],
                            ),
                            Column(
                              children: [
                                Container(
                                  child: Text(
                                    'Status',
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(
                                        fontSize:
                                        getProportionateScreenWidth(15.0),
                                        color: Colors.grey),
                                  ),
                                ),
                                Container(
                                  child: Text(
                                    '${obj[index].status}',
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                    style: kRegularText.copyWith(
                                        fontSize:
                                        getProportionateScreenWidth(15.0),
                                        color: Colors.black,
                                        fontWeight: FontWeight.w600),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Divider(),
                      ],
                    ),
                  );
                }),
          ),)
        ],
      ),
    );
  }

  buildBodyTwo(){
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: Stack(
        children: [
          Container(
            alignment: Alignment.centerLeft,
            padding: EdgeInsets.all(16),
            decoration: BoxDecoration(
              color: Colors.white,
              border: Border.all(width: 2, color: Color(0xFFF2F2F2)),
              borderRadius: new BorderRadius.all(
                const Radius.circular(5.0),
              ),
            ),
            margin: EdgeInsets.only(
              top: 2,
            ),
            child: Obx(()=>GestureDetector(
              onTap: (){
                controllerC.showDropDown.value=!controllerC.showDropDown.value;
                print("${controllerC.showDropDown.value}");
              },
              child: Row(
                mainAxisAlignment: mainAxisSpaceBetween,
                children: [
                  textRoboto("${controllerC.categoryListType.value}",kBlackColor,fontSize: 18.0,fontWeight: weight500),
                controllerC.showDropDown.value? Icon(Icons.arrow_drop_up):Icon(Icons.arrow_drop_down),
                ],
              ),
            )),
          ),
          Obx(()=> controllerC.signalReleaseData.value.data==null?
          CustomLoader():controllerC.signalReleaseData.value.data.length==0?
          Align(alignment: Alignment.center,child: Container(margin: EdgeInsets.only(top: 100.0),child: textRoboto("No data found", kBlackColor))):Container(
            alignment: Alignment.centerLeft,
            margin: EdgeInsets.only(left: 10, right: 10, top: 10),
            child: Container(
              margin: EdgeInsets.only(top: 80.0),
              child: ListView.builder(
                  shrinkWrap: true,
                  scrollDirection: Axis.vertical,
                  itemCount: controllerC.signalReleaseData.value.data.length,
                  physics: NeverScrollableScrollPhysics(),
                  itemBuilder: (context, index) {
                    var obj=controllerC.signalReleaseData.value.data;
                    return GestureDetector(
                      onTap: () {},
                      child: Column(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Container(
                                    alignment: Alignment.centerLeft,
                                    child: Text(
                                      '${obj[index].type}',
                                      maxLines: 1,
                                      overflow: TextOverflow.ellipsis,
                                      style: kRegularText.copyWith(
                                          color: Colors.black,
                                          fontWeight: FontWeight.w600),
                                    ),
                                  ),
                                  Container(
                                    child: Text(
                                      '${obj[index].pair}',
                                      maxLines: 1,
                                      overflow: TextOverflow.ellipsis,
                                      style: TextStyle(
                                          fontSize:
                                          getProportionateScreenWidth(16.0),
                                          color: Colors.grey),
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              Column(
                                children: [
                                  Container(
                                    child: Text(
                                      'Price',
                                      maxLines: 1,
                                      overflow: TextOverflow.ellipsis,
                                      style: TextStyle(
                                          fontSize:
                                          getProportionateScreenWidth(15.0),
                                          color: Colors.grey),
                                    ),
                                  ),
                                  Container(
                                    child: Text(
                                      '${obj[index].price}',
                                      maxLines: 1,
                                      overflow: TextOverflow.ellipsis,
                                      style: kRegularText.copyWith(
                                          fontSize:
                                          getProportionateScreenWidth(15.0),
                                          color: Colors.black,
                                          fontWeight: FontWeight.w600),
                                    ),
                                  ),
                                ],
                              ),
                              Column(
                                children: [
                                  Container(
                                    child: Text(
                                      'TP',
                                      maxLines: 1,
                                      overflow: TextOverflow.ellipsis,
                                      style: TextStyle(
                                          fontSize:
                                          getProportionateScreenWidth(15.0),
                                          color: Colors.grey),
                                    ),
                                  ),
                                  Container(
                                    child: Text(
                                      '1766',
                                      maxLines: 1,
                                      overflow: TextOverflow.ellipsis,
                                      style: kRegularText.copyWith(
                                          fontSize:
                                          getProportionateScreenWidth(15.0),
                                          color: Colors.black,
                                          fontWeight: FontWeight.w600),
                                    ),
                                  ),
                                ],
                              ),
                              Column(
                                children: [
                                  Container(
                                    child: Text(
                                      'SL',
                                      maxLines: 1,
                                      overflow: TextOverflow.ellipsis,
                                      style: TextStyle(
                                          fontSize:
                                          getProportionateScreenWidth(15.0),
                                          color: Colors.grey),
                                    ),
                                  ),
                                  Container(
                                    child: Text(
                                      '1796',
                                      maxLines: 1,
                                      overflow: TextOverflow.ellipsis,
                                      style: kRegularText.copyWith(
                                          fontSize:
                                          getProportionateScreenWidth(15.0),
                                          color: Colors.black,
                                          fontWeight: FontWeight.w600),
                                    ),
                                  ),
                                ],
                              ),
                              Column(
                                children: [
                                  Container(
                                    child: Text(
                                      'Status',
                                      maxLines: 1,
                                      overflow: TextOverflow.ellipsis,
                                      style: TextStyle(
                                          fontSize:
                                          getProportionateScreenWidth(15.0),
                                          color: Colors.grey),
                                    ),
                                  ),
                                  Container(
                                    child: Text(
                                      '${obj[index].status}',
                                      maxLines: 1,
                                      overflow: TextOverflow.ellipsis,
                                      style: kRegularText.copyWith(
                                          fontSize:
                                          getProportionateScreenWidth(15.0),
                                          color: Colors.black,
                                          fontWeight: FontWeight.w600),
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Divider(),
                        ],
                      ),
                    );
                  }),
            ),
          ),),
          Obx(()=>Visibility(
            visible: controllerC.showDropDown.value,
            child: Container(
              margin: EdgeInsets.only(top: 60.0),
              height: 300,

              decoration: BoxDecoration(
                color: kWhiteColor,
                border: Border.all(width: 2, color: Color(0xFFF2F2F2)),
                borderRadius: new BorderRadius.all(
                  const Radius.circular(5.0),
                ),
              ),
              child: Scrollbar(
                isAlwaysShown: true,
                child: ListView.builder(
                  physics: BouncingScrollPhysics(),
                  itemCount: controllerC.singnalListData.value.data.length,
                  itemBuilder: (_,index){
                    return GestureDetector(
                      onTap: (){
                        controllerC.showDropDown.value=!controllerC.showDropDown.value;
                        controllerC.categoryListType.value=controllerC.singnalListData.value.data[index].signalName;
                        controllerC.getSignalRelease(controllerC.singnalListData.value.data[index].id);
                      },
                      child: Container(
                        color: kWhiteColor,
                        height: 50,
                        padding: EdgeInsets.symmetric(horizontal: 1.0),
                        child: Row(
                          children: [
                            Container(
                                margin: EdgeInsets.only(left: 5, right: 5),
                                width: 60,
                                height: 60,
                                alignment: Alignment.center,
                                child: CachedNetworkImage(
                                  imageUrl:
                                  'https://cb.1129-1891.cyou/${controllerC.singnalListData.value.data[index].signalLogo}',
                                  fit: BoxFit.cover,
                                  placeholder: (context, url) => CustomLoader(),
                                  errorWidget: (context, url, error) => Icon(
                                    Icons.error,
                                    color: kBlackColor,
                                  ),
                                )),
                            width10,
                            textRoboto("${controllerC.singnalListData.value.data[index].signalName}", kBlackColor,fontSize: 18.0,fontWeight: weight500)
                          ],
                        ),

                      ),
                    );
                  },
                ),
              ),
            ),
          )),


        ],
      ),
    );
  }
}
