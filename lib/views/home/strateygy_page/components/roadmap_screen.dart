import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:orbitrade/app_config.dart';
import 'package:orbitrade/constants.dart';
import 'package:orbitrade/controller/roadmap_list_data_controller.dart';
import 'package:orbitrade/controller/video_list_data_controller.dart';
import 'package:orbitrade/sheared/custom_loader.dart';
import 'package:photo_view/photo_view.dart';

import '../../../../FadeAnimation.dart';
import '../../../../size_config.dart';

class RoadmapScreen extends StatelessWidget {
  var roadMapController = Get.put(RoadMapDataController());

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(
        horizontal: 10,
        vertical: 20,
      ),
      child: Obx(
        () => roadMapController.dataResponse.value.images == null
            ? CustomLoader()
            : roadMapController.dataResponse.value.images.length == 0
                ? textRoboto("No Data Found", kBlackColor)
                : GridView.builder(
                    shrinkWrap: true,
                    physics: ScrollPhysics(),
                    itemCount: roadMapController.dataResponse.value.images.length,
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2,
                      crossAxisSpacing: 20,
                      mainAxisSpacing: 0,
                    ),
                    itemBuilder: (BuildContext context, int index) {
                      return GridTile(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            GestureDetector(
                              onTap: () {
                                showDialog(
                                  builder: (context) {
                                    return FadeAnimation(
                                      0.2,
                                      Align(
                                        alignment: Alignment.center,
                                        child: Stack(
                                          children: [
                                            Container(
                                              height:
                                                  getProportionateScreenWidth(
                                                      200),
                                              width:
                                                  SizeConfig.screenWidth / 1.2,
                                              margin: EdgeInsets.only(top: 28),
                                              child: ClipRRect(
                                                //borderRadius: BorderRadius.circular(5),
                                                child: CachedNetworkImage(
                                                  imageUrl:
                                                  '${BASE_PATH + roadMapController.dataResponse.value.images[index].roadmad}',
                                                  fit: BoxFit.fill,
                                                  placeholder: (context, url) => CustomLoader(),
                                                  errorWidget: (context, url, error) => Icon(
                                                    Icons.error,
                                                    color: Colors.red,
                                                  ),
                                                )
                                              ),
                                            ),
                                            Positioned(
                                              top: 0,
                                              right: 0,
                                              child: GestureDetector(
                                                onTap: () {
                                                  Navigator.pop(context);
                                                },
                                                child: Icon(
                                                  Icons.close,
                                                  color: Colors.white,
                                                )
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    );
                                  },
                                  context: context,
                                );
                              },
                              child: ClipRRect(
                                borderRadius: BorderRadius.circular(5),
                                child:  CachedNetworkImage(
                                  imageUrl:
                                  '${BASE_PATH + roadMapController.dataResponse.value.images[index].roadmad}',
                                  fit: BoxFit.cover,
                                  placeholder: (context, url) => CustomLoader(),
                                  errorWidget: (context, url, error) => Icon(
                                    Icons.error,
                                    color: Colors.red,
                                  ),
                                )
                              ),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Text(
                              'Peta Harga - GBP/NZD',
                              maxLines: 2,
                              style: kRegularText2.copyWith(
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Text(
                              'MRG Premiere Roadmap',
                              maxLines: 1,
                              style: kSmallText.copyWith(
                                fontWeight: FontWeight.w600,
                                color: kBlackColor.withOpacity(.4),
                              ),
                            ),
                          ],
                        ),
                      );
                    },
                  ),
      ),
    );
  }
}
