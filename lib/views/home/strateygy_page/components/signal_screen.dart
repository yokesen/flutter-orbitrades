import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:get/get.dart';
import 'package:orbitrade/app_config.dart';
import 'package:orbitrade/controller/categoryContoller.dart';
import 'package:orbitrade/sheared/custom_loader.dart';
import 'package:orbitrade/views/home/strateygy_page/signal_details_page.dart';
import 'package:orbitrade/views/home/strateygy_page/wegetss/SelectSignalCard.dart';

import '../../../../constants.dart';
import '../../../../size_config.dart';

class SignalScreen extends StatefulWidget {
  @override
  _SignalScreenState createState() => _SignalScreenState();
}

class _SignalScreenState extends State<SignalScreen> {
  double size = 500;

  var categoryC = Get.put(CategoryController());

  @override
  void initState() {
    // TODO: implement initState
    categoryC.getSignalList();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Padding(
        padding: EdgeInsets.symmetric(
          horizontal: getProportionateScreenWidth(15),
        ),
        child: Column(
          children: [
            Container(
              alignment: Alignment.centerLeft,
              margin: EdgeInsets.only(top: 10, bottom: 10),
              child: Text(
                'Pilih Robot Signal',
                style: kBoldStyle.copyWith(color: Colors.black, fontSize: 18),
              ),
            ),
            Obx(() => categoryC.singnalListData.value.data == null
                ? CustomLoader()
                : categoryC.singnalListData.value.data.length == 0
                    ? textRoboto("No Data Found", kBlackColor)
                    : StaggeredGridView.countBuilder(
                        shrinkWrap: true,
                        physics: NeverScrollableScrollPhysics(),
                        itemCount: categoryC.singnalListData.value.data.length,
                        crossAxisCount: 2,
                        staggeredTileBuilder: (int index) => new StaggeredTile.fit(1),
                        mainAxisSpacing: 10.0,
                        crossAxisSpacing: 10.0,
                        itemBuilder: (context, ix) {
                          return GestureDetector(
                              onTap: () {

                                print("${categoryC.singnalListData.value.data[ix].id}");
                                categoryC.categoryListType.value=categoryC.singnalListData.value.data[ix].signalName;
                                //Navigator.pushNamed(context, SignalDetailsPage.routeName);

                                Navigator.push(
                                  context,
                                  MaterialPageRoute(builder: (context) => SignalDetailsPage(
                                      id: categoryC.singnalListData.value.data[ix].id.toString(),
                                  )),
                                );

                              },
                              child: Container(
                                  decoration: BoxDecoration(
                                    color: khomeFirstCardColor,
                                    borderRadius: new BorderRadius.all(
                                      const Radius.circular(10.0),
                                    ),
                                  ),
                                  child: Center(
                                    child: Column(
                                        crossAxisAlignment: CrossAxisAlignment.center,
                                        children: <Widget>[
                                          Container(
                                              margin: EdgeInsets.only(left: 5, right: 5),
                                              width: 120,
                                              height: 120,
                                              alignment: Alignment.center,
                                              child: CachedNetworkImage(
                                                imageUrl:
                                                'https://cb.1129-1891.cyou/${categoryC.singnalListData.value.data[ix].signalLogo}',
                                                fit: BoxFit.cover,
                                                placeholder: (context, url) => CustomLoader(),
                                                errorWidget: (context, url, error) => Icon(
                                                  Icons.error,
                                                  color: Colors.red,
                                                ),
                                              )),
                                          Container(
                                            margin:
                                            EdgeInsets.only(top: 15, left: 5, right: 5, bottom: 10),
                                            child: Text(
                                                '${categoryC.singnalListData.value.data[ix].signalName}',
                                                textAlign: TextAlign.center,
                                                style: TextStyle(color: Colors.black, fontSize: 14)),
                                          ),
                                        ]),
                                  )));
                        })),
          ],
        ),
      ),
    );
  }
}
