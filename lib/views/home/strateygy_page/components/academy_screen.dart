import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:orbitrade/app_config.dart';
import 'package:orbitrade/constants.dart';
import 'package:orbitrade/controller/video_list_data_controller.dart';
import 'package:orbitrade/sheared/custom_loader.dart';
import 'package:orbitrade/views/home/strateygy_page/acedmy_details/detail.dart';
import 'package:orbitrade/views/home/strateygy_page/wegetss/pointcard.dart';

class AcademyScreen extends StatelessWidget {
  var videoController = Get.put(VideoListDataController());

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Container(
      height: size.height,
      width: size.width,
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Obx(() => videoController.score.value != null
                ? videoController.videoListData.value.data == null
                    ? CustomLoader()
                    : videoController.videoListData.value.data.length == 0
                        ? textRoboto("No Data Found", kBlackColor)
                        : Column(
                            children: [
                              TrainingPoint(
                                point: videoController.score.value.score ?? 0,
                              ),
                              Container(
                                margin: EdgeInsets.symmetric(
                                    horizontal: 10.0, vertical: 10.0),
                                child: GridView.builder(
                                  gridDelegate:
                                      SliverGridDelegateWithMaxCrossAxisExtent(
                                          maxCrossAxisExtent: 300,
                                          childAspectRatio: 5 / 6,
                                          crossAxisSpacing: 12,
                                          mainAxisSpacing: 12),
                                  itemCount: videoController
                                      .videoListData.value.data.length,
                                  shrinkWrap: true,
                                  physics: NeverScrollableScrollPhysics(),
                                  itemBuilder: (_, index) {
                                    var videoObj = videoController
                                        .videoListData.value.data;
                                    return GestureDetector(
                                      onTap: () {
                                        Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) => Detail(
                                                    uID:
                                                        "${videoObj[index].uavid}",
                                                    url:
                                                        "${videoObj[index].videoUrl}",
                                                    videoTitle:
                                                        "${videoObj[index].videoTitle}",
                                                    videoDesc:
                                                        "${videoObj[index].videoDesc}",
                                                    videoPoint:
                                                        "${videoObj[index].videoPoin}",
                                                  )),
                                        );

                                        /*Navigator.of(context).pushNamed(Detail.routeName,
                                        arguments: Detail(
                                          url: "${videoObj[index].videoUrl}",
                                        ));*/
                                      },
                                      child: Container(
                                        width: size.width,
                                        decoration: BoxDecoration(
                                          border:
                                              Border.all(color: kOrdinaryColor),
                                          borderRadius:
                                              BorderRadius.circular(10.0),
                                        ),
                                        child: Column(
                                          crossAxisAlignment: crossAxisStart,
                                          children: [
                                            ClipRRect(
                                              //borderRadius: BorderRadius.circular(10.0),
                                              child: CachedNetworkImage(
                                                imageUrl:
                                                    "https://cb.1129-1891.cyou/${videoObj[index].photo}",
                                                height: 120,
                                                width: size.width,
                                                fit: BoxFit.cover,
                                                placeholder: (context, url) =>
                                                    CustomLoader(),
                                                errorWidget:
                                                    (context, url, error) =>
                                                        Icon(
                                                  Icons.error,
                                                  color: kPrimaryColor,
                                                  size: 35,
                                                ),
                                              ),
                                            ),
                                            Padding(
                                              padding:
                                                  EdgeInsets.only(left: 10),
                                              child: Text(
                                                "${videoObj[index].videoTitle}",
                                                overflow: TextOverflow.ellipsis,
                                                maxLines: 2,
                                                style: TextStyle(
                                                    fontSize: 14,
                                                    fontWeight:
                                                        FontWeight.w800),
                                              ),
                                            ),
                                            size10,
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                  left: 10.0),
                                              child: Text(
                                                "${videoObj[index].videoPoin} poin",
                                                style: TextStyle(
                                                    fontSize: 13,
                                                    fontWeight:
                                                        FontWeight.w300),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    );
                                  },
                                ),
                              ),
                            ],
                          )
                : CustomLoader()),
            // Itemsegment(segType: "Basic", secondText: "Lihat Semua"),
          ],
        ),
      ),
    );
  }
}
