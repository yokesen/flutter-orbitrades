import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:get/get.dart';
import 'package:orbitrade/constants.dart';
import 'package:orbitrade/controller/video_list_data_controller.dart';
import 'package:orbitrade/sheared/buildDepositAppBar.dart';
import 'package:orbitrade/views/home/strateygy_page/quizScreen/quiz_questions_page.dart';
import 'package:video_player/video_player.dart';

class Detail extends StatefulWidget {
  static const routeName = '/product-detail';

  String uID;
  String url;
  String videoTitle;
  String videoDesc;
  String videoPoint;

  Detail(
      {this.uID, this.url, this.videoTitle, this.videoDesc, this.videoPoint});

  @override
  _DetailState createState() => _DetailState();
}

class _DetailState extends State<Detail> {
  VideoPlayerController _controller;
  VoidCallback listener;

  var videoC = Get.put(VideoListDataController());
  InAppWebViewController webView;

  @override
  void initState() {
    print("UID ${widget.uID}");

    // TODO: implement initState
    videoC.getSingleVideoData(widget.uID);
    // if (Platform.isAndroid) WebView.platform = SurfaceAndroidWebView();

    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Scaffold(
      appBar: buildDepositAppBar(
          context: context,
          title: ' ',
          rightTitle: 'Next  ',
          onPress: () async {
            //Navigator.pushNamed(context, QuizQuestionPage.routeName);
            Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => QuizQuestionPage(
                        id: widget.uID,
                      )),
            );
          }),
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: SingleChildScrollView(
          child: Column(
            children: [
              /* Container(
                height: size.height/3.5,
                child: WebView(
                  initialUrl: widget.url,
                  javascriptMode: JavascriptMode.unrestricted,

                ),
              ),*/
              Container(
                  width: size.width,
                  height: size.height / 3.5,
                  child: Column(children: <Widget>[
                    Expanded(
                        child: InAppWebView(
                      initialUrl: widget.url,
                      initialHeaders: {},
                      onWebViewCreated: (InAppWebViewController controller) {
                        webView = controller;
                      },
                      initialOptions: InAppWebViewGroupOptions(
                        android: AndroidInAppWebViewOptions(
                          disableDefaultErrorPage: true,
                        ),
                        crossPlatform: InAppWebViewOptions(
                            mediaPlaybackRequiresUserGesture: false,
                            horizontalScrollBarEnabled: false,
                            // userAgent: userAgent,
                            verticalScrollBarEnabled: false),
                      ),
                      androidOnPermissionRequest:
                          (InAppWebViewController controller, String origin,
                              List<String> resources) async {
                        return PermissionRequestResponse(
                            resources: resources,
                            action: PermissionRequestResponseAction.GRANT);
                      },
                      onEnterFullscreen: (controller) async {
                        await SystemChrome.setPreferredOrientations([
                          DeviceOrientation.landscapeRight,
                          DeviceOrientation.landscapeLeft,
                        ]);
                      },
                      onExitFullscreen: (controller) async {
                        await SystemChrome.setPreferredOrientations([
                          DeviceOrientation.portraitDown,
                          DeviceOrientation.portraitUp,
                          DeviceOrientation.landscapeRight,
                          DeviceOrientation.landscapeLeft,
                        ]);
                      },
                      onLoadStart:
                          (InAppWebViewController controller, String url) {},
                      onLoadStop:
                          (InAppWebViewController controller, String url) {},
                    ))
                  ])),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
                child: Column(
                  crossAxisAlignment: crossAxisStart,
                  children: [
                    size10,
                    Text(
                      '${widget.videoTitle}',
                      style: kRegularText.copyWith(
                          color: Colors.black,
                          fontWeight: FontWeight.w600,
                          fontSize: 25.0),
                    ),
                    size10,
                    Text(
                      '${widget.videoPoint}',
                      style: kRegularText.copyWith(
                          color: kOrdinaryColor, fontWeight: FontWeight.w600),
                    ),
                    size20,
                    Text(
                      '${widget.videoDesc}',
                      style: kRegularText.copyWith(
                          color: Colors.black, fontWeight: FontWeight.w500),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
