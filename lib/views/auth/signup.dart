import 'dart:convert';
import 'dart:io';

import 'package:dotted_border/dotted_border.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:orbitrade/controller/custody_bank_list_controller.dart';
import 'package:orbitrade/model/custody-bank-list.dart';
import 'package:orbitrade/services/network_services.dart';
import 'package:orbitrade/sheared/custom_loader.dart';
import 'package:orbitrade/sheared/default_btn.dart';
import 'package:orbitrade/sheared/input_form_widget.dart';
import 'package:orbitrade/views/auth/login.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../constants.dart';
import '../../main.dart';
import '../../size_config.dart';

class SignUp extends StatefulWidget {
  static const routeName = 'signup_screen';

  const SignUp({Key key}) : super(key: key);

  @override
  _SignUpState createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  final _formKey = GlobalKey<FormState>();
  final _formKey2 = GlobalKey<FormState>();
  final _custodyBankController = Get.put(CusodyBankListController());
  TextEditingController _emailController = TextEditingController();
  TextEditingController _passController = TextEditingController();
  TextEditingController _confirmPassController = TextEditingController();
  TextEditingController _nameController = TextEditingController();
  TextEditingController _numberController = TextEditingController();
  TextEditingController _bankNameController = TextEditingController();
  TextEditingController _rank1Controller = TextEditingController();
  TextEditingController _rank2Controller = TextEditingController();
  int currentPage = 1;
  String userToken;
  String _selected_bank_name;
  bool isAccepted = false;
  FocusNode emailFocusNode;
  FocusNode passFocusNode;
  FocusNode confirmPassFocusNode;
  FocusNode nameFocusNode;
  FocusNode numberFocusNode;
  FocusNode bankFocusNode;
  FocusNode rank1FocusNode;
  FocusNode rank2FocusNode;

  final picker = ImagePicker();

  //final result = FilePicker.platform.pickFiles();

  File firstImage;
  String firstImageBase64;
  File secondImage;
  String secondImageBase64;
  File thirdImage;
  String thirdImageBase64;
  File firstDocFile;
  String firstFileName;
  File secondDocFile;
  File thirdDocsFile;
  String secondFileName;
  String thirdFileName;

  @override
  void initState() {
    // TODO: implement initState
    emailFocusNode = FocusNode();
    passFocusNode = FocusNode();
    confirmPassFocusNode = FocusNode();
    nameFocusNode = FocusNode();
    numberFocusNode = FocusNode();
    rank2FocusNode = FocusNode();
    rank1FocusNode = FocusNode();
    bankFocusNode = FocusNode();
    super.initState();
  }

  @override
  void dispose() {
    _emailController.dispose();
    _passController.dispose();
    _nameController.dispose();
    _numberController.dispose();
    emailFocusNode.dispose();
    passFocusNode.dispose();
    confirmPassFocusNode.dispose();
    nameFocusNode.dispose();
    numberFocusNode.dispose();
    rank1FocusNode.dispose();
    rank2FocusNode.dispose();
    bankFocusNode.dispose();
    _bankNameController.dispose();
    _rank1Controller.dispose();
    _rank2Controller.dispose();
    // TODO: implement dispose
    super.dispose();
  }

  void _getFirstImage() async {
    final pickedFile = await picker.getImage(source: ImageSource.camera);
    if (pickedFile != null) {
      setState(() {
        firstImage = File(pickedFile.path);
        firstImageBase64 = base64Encode(firstImage.readAsBytesSync());
        print(firstImage.toString());
      });
      _getSnackBar(true);
    } else {
      _getSnackBar(false);
    }
  }

  void getFirstFile() async {
    final result = await FilePicker.getFile();

    if (result != null) {
      final path = result.path;
      setState(() {
        firstDocFile = File(path);
        var completePath = path;
        firstFileName = (completePath.split('/').last);

        print("File Name $firstFileName");
        print("Path ${firstDocFile.toString()}");
      });
    } else {
      // User canceled the picker
    }
  }

  void getSecondFile() async {
    final result = await FilePicker.getFile();

    if (result != null) {
      final path = result.path;
      setState(() {
        secondDocFile = File(path);

        var completePath = path;
        secondFileName = (completePath.split('/').last);

        print("File Name $secondFileName");
        print("Path ${secondDocFile.toString()}");
      });
    } else {
      // User canceled the picker
    }
  }

  void _getSecondImage() async {
    final pickedFile = await picker.getImage(source: ImageSource.camera);
    if (pickedFile != null) {
      setState(() {
        secondImage = File(pickedFile.path);
        secondImageBase64 = base64Encode(secondImage.readAsBytesSync());
        print(secondImage.toString());
      });
      _getSnackBar(true);
    } else {
      _getSnackBar(false);
    }
  }

  void getThirdImage() async {
    final result = await FilePicker.getFile();

    if (result != null) {
      final path = result.path;
      setState(() {
        thirdDocsFile = File(path);

        var completePath = path;
        thirdFileName = (completePath.split('/').last);

        print("File Name $thirdFileName");
        print("Path ${thirdDocsFile.toString()}");
      });
    } else {
      // User canceled the picker
    }
  }

  void _getThirdImage() async {
    final pickedFile = await picker.getImage(source: ImageSource.camera);
    if (pickedFile != null) {
      setState(() {
        thirdImage = File(pickedFile.path);
        thirdImageBase64 = base64Encode(thirdImage.readAsBytesSync());
        print(thirdImage.toString());
      });
      _getSnackBar(true);
    } else {
      _getSnackBar(false);
    }
  }

  void _getSnackBar(bool isSelected) {
    Get.snackbar(
        isSelected == true ? 'Image Selected' : 'No Image Selected', '',
        colorText: kBlackColor);
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      backgroundColor: kWhiteColor,
      appBar: AppBar(
        title: currentPage == 3
            ? SizedBox()
            : Row(
                children: [
                  SizedBox(
                    width: currentPage == 2
                        ? SizeConfig.screenWidth / 10
                        : SizeConfig.screenWidth / 7,
                  ),
                  GestureDetector(
                    onTap: () {
                      if (currentPage != 1) {
                        setState(() {
                          currentPage = 1;
                        });
                      }
                    },
                    child: Container(
                      width: 40,
                      height: 2,
                      decoration: BoxDecoration(
                        color: kPrimaryColor,
                        borderRadius: BorderRadius.circular(1),
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 5,
                  ),
                  Container(
                    width: 40,
                    height: 2,
                    decoration: BoxDecoration(
                      color: currentPage > 1 ? kPrimaryColor : kOrdinaryColor2,
                      borderRadius: BorderRadius.circular(1),
                    ),
                  ),
                  SizedBox(
                    width: 5,
                  ),
                  Container(
                    width: 40,
                    height: 2,
                    decoration: BoxDecoration(
                      color: kOrdinaryColor2,
                      borderRadius: BorderRadius.circular(1),
                    ),
                  ),
                ],
              ),
        actions: [
          currentPage == 2
              ? FlatButton(
                  onPressed: () async {
                    if (firstDocFile != null) {
                      //call method
                      var p = await NetworkServices()
                          .uploadKTPFile(context, firstDocFile);
                      Map<String, dynamic> js = p;
                      if (p['success'] == true) {
                        Navigator.of(context).pop();
                        setState(() {
                          currentPage = 3;
                        });
                        _custodyBankController.fetchCusodyBankList(userToken);
                        Get.snackbar('Success', 'upload KTP successfully!');
                      } else {
                        print(p['message']);
                        Get.snackbar('Error', p['message']);
                      }
                      print('ok');
                    } else {
                      Get.snackbar('Error', 'Select KTP Dokumen');
                    }
                  },
                  child: Text(
                    'Lanjut',
                    style: kDescriptionText.copyWith(
                      color: kPrimaryColor,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                )
              : SizedBox(),
        ],
        centerTitle: true,
        iconTheme: IconThemeData(color: kBlackColor),
        elevation: 0,
        backgroundColor: kWhiteColor,
      ),
      body: SafeArea(
          child: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(
            horizontal: getProportionateScreenWidth(20),
          ),
          child: currentPage == 1
              ? Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: EdgeInsets.only(
                        top: getProportionateScreenHeight(23),
                      ),
                      child: Text(
                        'Registrasi Akun',
                        style: kHeadLine2,
                      ),
                    ),
                    Row(
                      children: [
                        Text(
                          'Sudah punya akun? ',
                          style: kRegularText.copyWith(
                              color: kOrdinaryColor,
                              fontWeight: FontWeight.w600),
                        ),
                        GestureDetector(
                          onTap: () {
                            if(rgStatus){
                              Navigator.of(context).pop();
                            }else{
                              Navigator.pushNamed(context, LogIn.routeName);
                            }
                          },
                          child: Container(
                            padding: EdgeInsets.only(
                              bottom: 1, // Space between underline and text
                            ),
                            decoration: BoxDecoration(
                                border: Border(
                                    bottom: BorderSide(
                              color: kPrimaryColor,
                              width: 1.0, // Underline thickness
                            ))),
                            child: Text(
                              "Masuk",
                              style: kRegularText.copyWith(
                                color: kPrimaryColor,
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                    SizedBox(
                      height: getProportionateScreenHeight(30),
                    ),
                    Form(
                      key: _formKey,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            child: Text(
                              'Nama Lengkap',
                              style: kRegularText2.copyWith(
                                color: nameFocusNode.hasFocus
                                    ? kPrimaryColor
                                    : kBlackColor,
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                          ),
                          FocusScope(
                            child: Focus(
                              focusNode: nameFocusNode,
                              onFocusChange: (node) {
                                setState(() {});
                              },
                              child: InputFormWidget(
                                hintText: 'Nama Lengkap',
                                keyType: TextInputType.name,
                                fieldController: _nameController,
                                validation: (value) {
                                  if (value.isEmpty) {
                                    return kInvalidNumberError;
                                  }
                                  return null;
                                },
                              ),
                            ),
                          ),
                          Container(
                            child: Text(
                              'Email',
                              style: kRegularText2.copyWith(
                                color: emailFocusNode.hasFocus
                                    ? kPrimaryColor
                                    : kBlackColor,
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                          ),
                          FocusScope(
                            child: Focus(
                              focusNode: emailFocusNode,
                              onFocusChange: (node) {
                                setState(() {});
                              },
                              child: InputFormWidget(
                                hintText: 'Email',
                                keyType: TextInputType.emailAddress,
                                fieldController: _emailController,
                                validation: (value) {
                                  if (value.isEmpty) {
                                    return kInvalidNumberError;
                                  } else if (value.length < 11) {
                                    return kInvalidNumberError;
                                  }
                                  return null;
                                },
                              ),
                            ),
                          ),
                          Container(
                            child: Text(
                              'Whatsapp',
                              style: kRegularText2.copyWith(
                                color: numberFocusNode.hasFocus
                                    ? kPrimaryColor
                                    : kBlackColor,
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                width: 65,
                                alignment: Alignment.center,
                                padding: EdgeInsets.all(16),
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(5),
                                    border: Border.all(
                                      width: 1,
                                      color: numberFocusNode.hasFocus
                                          ? kPrimaryColor
                                          : Colors.grey,
                                    )),
                                child: Text(
                                  '+62',
                                  style: kRegularText2.copyWith(
                                    fontWeight: FontWeight.w600,
                                  ),
                                ),
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              Container(
                                width: SizeConfig.screenWidth - 120,
                                child: FocusScope(
                                  child: Focus(
                                    focusNode: numberFocusNode,
                                    onFocusChange: (node) {
                                      setState(() {});
                                    },
                                    child: InputFormWidget(
                                      keyType: TextInputType.number,
                                      fieldController: _numberController,
                                      validation: (value) {
                                        if (value.isEmpty) {
                                          return kInvalidNumberError;
                                        }
                                        return null;
                                      },
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Container(
                            child: Text(
                              'Password',
                              style: kRegularText2.copyWith(
                                color: passFocusNode.hasFocus
                                    ? kPrimaryColor
                                    : kBlackColor,
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                          ),
                          FocusScope(
                            child: Focus(
                              focusNode: passFocusNode,
                              onFocusChange: (node) {
                                setState(() {});
                              },
                              child: InputFormWidget(
                                hintText: 'Password',
                                fieldController: _passController,
                                icon: Icons.remove_red_eye,
                                keyType: TextInputType.visiblePassword,
                                isProtected: true,
                                validation: (value) {
                                  if (value.isEmpty) {
                                    return kPassNullError;
                                  } else if (value.length < 4) {
                                    return kShortPassError;
                                  }
                                  return null;
                                },
                              ),
                            ),
                          ),
                          Container(
                            child: Text(
                              'Confirm Password',
                              style: kRegularText2.copyWith(
                                color: confirmPassFocusNode.hasFocus
                                    ? kPrimaryColor
                                    : kBlackColor,
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                          ),
                          FocusScope(
                            child: Focus(
                              focusNode: confirmPassFocusNode,
                              onFocusChange: (node) {
                                setState(() {});
                              },
                              child: InputFormWidget(
                                hintText: 'Confirm Password',
                                fieldController: _confirmPassController,
                                icon: Icons.remove_red_eye,
                                keyType: TextInputType.visiblePassword,
                                isProtected: true,
                                validation: (value) {
                                  if (value.isEmpty) {
                                    return kPassNullError;
                                  } else if (value.length < 4) {
                                    return kShortPassError;
                                  }
                                  return null;
                                },
                              ),
                            ),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              SizedBox(
                                width: 20,
                                height: 30,
                                child: Checkbox(
                                  activeColor: kPrimaryColor,
                                  value: isAccepted,
                                  onChanged: (bool value) {
                                    setState(() {
                                      isAccepted = value;
                                    });
                                  },
                                ),
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              Flexible(
                                child: RichText(
                                  textAlign: TextAlign.start,
                                  text: TextSpan(children: [
                                    TextSpan(
                                      text: 'I agree to ',
                                      style: kRegularText2.copyWith(
                                        color: kBlackColor,
                                        fontWeight: FontWeight.w600,
                                      ),
                                    ),
                                    TextSpan(
                                      text:
                                          'the User Agreement, Terms & Condition',
                                      style: kRegularText2.copyWith(
                                        color: kPrimaryColor,
                                        fontWeight: FontWeight.w600,
                                      ),
                                    ),
                                    TextSpan(
                                      text: ' and ',
                                      style: kRegularText2.copyWith(
                                        color: kBlackColor,
                                        fontWeight: FontWeight.w600,
                                      ),
                                    ),
                                    TextSpan(
                                      text: 'Privacy Policy.',
                                      style: kRegularText2.copyWith(
                                        color: kPrimaryColor,
                                        fontWeight: FontWeight.w600,
                                      ),
                                    ),
                                  ]),
                                ),
                              ),
                            ],
                          ),
                          Padding(
                            padding: EdgeInsets.symmetric(
                              vertical: getProportionateScreenHeight(30),
                            ),
                            child: DefaultBtn(
                              width: SizeConfig.screenWidth,
                              title: 'Daftar',
                              onPress: () async {
                                if (_formKey.currentState.validate()) {
                                  var p = await NetworkServices()
                                      .userRegistration(
                                          context: context,
                                          username: _nameController.text,
                                          email: _emailController.text,
                                          phone: "62" + _numberController.text,
                                          password: _confirmPassController.text
                                          //password: _passController.text,
                                          //number: _numberController.text,
                                          );
                                  Map<String, dynamic> js = p;
                                  if (js.containsKey('error')) {
                                    Navigator.of(context).pop();
                                    print(p['error']);
                                    Get.snackbar('Error', p['error']);
                                  } else {
                                    var p =
                                        await NetworkServices().userLoginText(
                                      context: context,
                                      password: _passController.text,
                                      username: _emailController.text,
                                    );
                                    Map<String, dynamic> js = p;
                                    if (p['success'] == false) {
                                      Navigator.of(context).pop();
                                      print(p['message']);
                                      Get.snackbar('Error', p['message']);
                                    } else {
                                      Navigator.of(context).pop();
                                      //success
                                      store(p, context);
                                      setState(() {
                                        currentPage = 2;
                                        userToken = p['token'];
                                      });
                                      Get.snackbar('Success',
                                          'Successfully registered your account!');
                                    }
                                  }
                                  print('ok');
                                }
                              },
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                )
              : (currentPage == 2
                  ? Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        size20,
                        Padding(
                          padding: EdgeInsets.symmetric(
                            vertical: getProportionateScreenHeight(23),
                          ),
                          child: Text(
                            'Verifikasi Data',
                            style: kHeadLine2,
                          ),
                        ),

                        Text(
                          'Sebelum mulai, kami membutuhkan verifikasi untuk komunikasi dengan Kamu. Silahkan klik tombol di bawah ini.',
                          style: kDescriptionText.copyWith(
                            fontWeight: FontWeight.w600,
                          ),
                          textAlign: TextAlign.justify,
                        ),
                        SizedBox(
                          height: getProportionateScreenHeight(42),
                        ),
                        Container(
                          width: SizeConfig.screenWidth,
                          padding: EdgeInsets.symmetric(
                            vertical: 2,
                          ),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(25),
                            color: kSuccessColor,
                          ),
                          child: FlatButton(
                            onPressed: () {
                              openWhatsApp();
                            },
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Image.asset(kImageDir + 'w_image.png'),
                                SizedBox(
                                  width: 10,
                                ),
                                Text(
                                  'Verifikasi Disini',
                                  style: kRegularText2.copyWith(
                                    fontWeight: FontWeight.w600,
                                    color: kWhiteColor,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        SizedBox(
                          height: getProportionateScreenHeight(50),
                        ),
                        size20,
                        Text(
                          'Silahkan upload foto KTP beserta selfie diri Kamu dengan foto KTP',
                          style: kDescriptionText.copyWith(
                            fontWeight: FontWeight.w600,
                          ),
                          textAlign: TextAlign.justify,
                        ),
                        SizedBox(
                          height: getProportionateScreenHeight(25),
                        ),

                        Text(
                          'Upload Foto KTP',
                          style: kRegularText2.copyWith(
                            fontWeight: FontWeight.w600,
                          ),
                          textAlign: TextAlign.justify,
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        GestureDetector(
                          onTap: () {
                            getFirstFile();
                            //_getFirstImage();
                          },
                          child: DottedBorder(
                            color: kOrdinaryColor2,
                            radius: Radius.circular(5),
                            borderType: BorderType.RRect,
                            child: Container(
                              alignment: Alignment.center,
                              child: Padding(
                                padding:
                                    const EdgeInsets.symmetric(vertical: 20),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    SvgPicture.asset(
                                      kImageDir + 'upload.svg',
                                      height: getProportionateScreenWidth(40),
                                      width: getProportionateScreenWidth(40),
                                    ),
                                    SizedBox(
                                      height: 10,
                                    ),
                                    Text(
                                      '${firstFileName ?? "Klik disini untuk upload"}',
                                      style: kDescriptionText,
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: getProportionateScreenHeight(30),
                        ),
                    /*    Text(
                          'Upload Selfie Foto KTP',
                          style: kRegularText2.copyWith(
                            fontWeight: FontWeight.w600,
                          ),
                          textAlign: TextAlign.justify,
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        GestureDetector(
                          onTap: () {
                            //_getSecondImage();
                            getSecondFile();
                          },
                          child: DottedBorder(
                            color: kOrdinaryColor2,
                            radius: Radius.circular(5),
                            borderType: BorderType.RRect,
                            child: Container(
                              alignment: Alignment.center,
                              child: Padding(
                                padding:
                                    const EdgeInsets.symmetric(vertical: 20),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    SvgPicture.asset(
                                      kImageDir + 'upload.svg',
                                      height: getProportionateScreenWidth(40),
                                      width: getProportionateScreenWidth(40),
                                    ),
                                    SizedBox(
                                      height: 10,
                                    ),
                                    Text(
                                      '${secondFileName ?? "Klik disini untuk upload"}',
                                      style: kDescriptionText,
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),*/
                      ],
                    )
                  : Obx(() {
                      if (_custodyBankController.isLoading.value) {
                        return CustomLoader();
                      } else {
                        return Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding: EdgeInsets.symmetric(
                                vertical: getProportionateScreenHeight(10),
                              ),
                              child: Text(
                                'Masuk Akun',
                                style: kHeadLine2,
                              ),
                            ),
                            SizedBox(
                              height: getProportionateScreenHeight(30),
                            ),
                            Form(
                              key: _formKey2,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(
                                    child: Text(
                                      'Nama Bank',
                                      style: kRegularText2.copyWith(
                                        color: bankFocusNode.hasFocus
                                            ? kPrimaryColor
                                            : kBlackColor,
                                        fontWeight: FontWeight.w600,
                                      ),
                                    ),
                                  ),
                                  FocusScope(
                                    child: Focus(
                                      focusNode: bankFocusNode,
                                      onFocusChange: (node) {
                                        setState(() {});
                                      },
                                      child: Container(
                                        alignment: Alignment.centerLeft,
                                        padding: EdgeInsets.all(16),
                                        decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(5),
                                            border: Border.all(
                                              width: 1,
                                              color: Colors.grey,
                                            )),
                                        margin: EdgeInsets.only(
                                          top: 2,
                                        ),
                                        width:
                                            MediaQuery.of(context).size.width,
                                        child: DropdownButtonHideUnderline(
                                          child: DropdownButton<String>(
                                            icon: Icon(
                                              Icons
                                                  .keyboard_arrow_down_outlined,
                                              color: Colors.redAccent,
                                              size: 20.09,
                                            ),
                                            hint: Text("Pilih Bank"),
                                            value: _selected_bank_name,
                                            isDense: true,
                                            onChanged: (String newValue) {
                                              setState(() {
                                                _selected_bank_name = newValue;
                                              });
                                              print(_selected_bank_name);
                                            },
                                            items: _custodyBankController
                                                .dataResponse.data
                                                .map((Datum map) {
                                              return DropdownMenuItem<String>(
                                                  value:
                                                      map.namaBank.toString(),
                                                  child: Container(
                                                    margin: EdgeInsets.only(
                                                        left: 10),
                                                    child: SizedBox(
                                                      width: SizeConfig
                                                              .screenWidth -
                                                          110, // for example
                                                      child: Text(
                                                          map.namaBank
                                                              .toString(),
                                                          textAlign:
                                                              TextAlign.left),
                                                    ),
                                                  ));
                                            }).toList(),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Container(
                                    child: Text(
                                      'Nomor Rekening',
                                      style: kRegularText2.copyWith(
                                        color: rank1FocusNode.hasFocus == true
                                            ? kPrimaryColor
                                            : kBlackColor,
                                        fontWeight: FontWeight.w600,
                                      ),
                                    ),
                                  ),
                                  FocusScope(
                                    child: Focus(
                                      focusNode: rank1FocusNode,
                                      onFocusChange: (node) {
                                        setState(() {});
                                      },
                                      child: InputFormWidget(
                                        hintText: 'Nomor Rekening',
                                        fieldController: _rank1Controller,
                                        keyType: TextInputType.text,
                                        isProtected: true,
                                        validation: (value) {
                                          if (value.isEmpty) {
                                            return 'Required cant\'t be empty';
                                          }
                                          return null;
                                        },
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Container(
                                    child: Text(
                                      'Nama di Rekening',
                                      style: kRegularText2.copyWith(
                                        color: rank2FocusNode.hasFocus == true
                                            ? kPrimaryColor
                                            : kBlackColor,
                                        fontWeight: FontWeight.w600,
                                      ),
                                    ),
                                  ),
                                  FocusScope(
                                    child: Focus(
                                      focusNode: rank2FocusNode,
                                      onFocusChange: (node) {
                                        setState(() {});
                                      },
                                      child: InputFormWidget(
                                        hintText: 'Nama di Rekening',
                                        fieldController: _rank2Controller,
                                        keyType: TextInputType.text,
                                        isProtected: true,
                                        validation: (value) {
                                          if (value.isEmpty) {
                                            return 'Required cant\'t be empty';
                                          }
                                          return null;
                                        },
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Text(
                                    'Upload Buku Tabungan',
                                    style: kRegularText2.copyWith(
                                      fontWeight: FontWeight.w600,
                                    ),
                                    textAlign: TextAlign.justify,
                                  ),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  GestureDetector(
                                    onTap: () {
                                      getThirdImage();
                                    },
                                    child: DottedBorder(
                                      color: kOrdinaryColor2,
                                      radius: Radius.circular(5),
                                      borderType: BorderType.RRect,
                                      child: Container(
                                        alignment: Alignment.center,
                                        child: Padding(
                                          padding: const EdgeInsets.symmetric(
                                              vertical: 20),
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            children: [
                                              SvgPicture.asset(
                                                kImageDir + 'upload.svg',
                                                height:
                                                    getProportionateScreenWidth(
                                                        40),
                                                width:
                                                    getProportionateScreenWidth(
                                                        40),
                                              ),
                                              SizedBox(
                                                height: 10,
                                              ),
                                              Text(
                                                '${thirdFileName ?? "Klik disini untuk upload"}',
                                                style: kDescriptionText,
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.symmetric(
                                      vertical:
                                          getProportionateScreenHeight(30),
                                    ),
                                    child: DefaultBtn(
                                        width: SizeConfig.screenWidth,
                                        title: 'Selesai',
                                        onPress: () async {
                                          if (_selected_bank_name == null) {
                                            Get.snackbar(
                                                'Error', 'Pilih Bank Name');
                                          } else {
                                            if (_formKey2.currentState
                                                .validate()) {
                                              if (thirdDocsFile != null) {
                                                await NetworkServices()
                                                    .updateBank(
                                                        _selected_bank_name,
                                                        _rank1Controller.text,
                                                        _rank2Controller.text,
                                                        context,
                                                        firstDocFile);
                                              } else {
                                                Get.snackbar('Error',
                                                    'Pilih BukuTabungan Dokumen');
                                              }

                                              /*Map<String, dynamic> js = p;
                                        if (p['success'] == true) {
                                          await NetworkServices().uploadBukuTabunganFile(context, firstDocFile);
                                          Navigator.of(context).pop();
                                          Navigator.of(context).pushNamedAndRemoveUntil(
                                              Home.routeName, (Route<dynamic> route) => false);
                                          Get.snackbar('Success', 'upload BukuTabungan successfully!');
                                        } else {
                                          print(p['message']);
                                          Get.snackbar('Error', p['message']);
                                        }
                                        print('ok');
                                      }else{
                                        Get.snackbar('Error', 'Select BukuTabungan Dokumen');
                                      }*/
                                            }
                                          }
                                        }),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        );
                      }
                    })),
        ),
      )),
    );
  }
  openWhatsApp() async{
    var whatsapp ="6281287608190";
    var whatsappURl_android = "whatsapp://send?phone="+whatsapp+"&text=verify 57cfa6c545e362ddecbf9bf13e77965e47a331cd1624779231";
    var whatappURL_ios ="https://wa.me/$whatsapp?text=${Uri.parse("verify 57cfa6c545e362ddecbf9bf13e77965e47a331cd1624779231")}";
    if(Platform.isIOS){
      // for iOS phone only
      if( await canLaunch(whatappURL_ios)){
        await launch(whatappURL_ios, forceSafariVC: false);
      }else{
        ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(content: new Text("whatsapp no installed")));

      }

    }else{
      // android , web
      if( await canLaunch(whatsappURl_android)){
        await launch(whatsappURl_android);
      }else{
        ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(content: new Text("whatsapp no installed")));

      }


    }

  }

  void store(var mat, BuildContext context) async {
    prefs = await SharedPreferences.getInstance();
    prefs.setString('token', mat['token'].toString()); // token save
    prefs.setString('user_uid', mat['token'].toString()); // uid save
    prefs.setBool('status', mat['status']);
  }
}
