import 'dart:async';
import 'dart:convert' show json;

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_sign_in/google_sign_in.dart';
import "package:http/http.dart" as http;
import 'package:orbitrade/services/network_services.dart';
import 'package:orbitrade/sheared/default_btn.dart';
import 'package:orbitrade/sheared/input_form_widget.dart';
import 'package:orbitrade/sheared/input_password_form_widget.dart';
import 'package:orbitrade/views/auth/signup.dart';
import 'package:orbitrade/views/home/Home.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../constants.dart';
import '../../main.dart';
import '../../size_config.dart';

class LogIn extends StatefulWidget {
  static const routeName = 'logIn_screen';

  const LogIn({Key key}) : super(key: key);

  @override
  _LogInState createState() => _LogInState();
}

class _LogInState extends State<LogIn> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController _emailController = TextEditingController();
  TextEditingController _passController = TextEditingController();
  bool isAccepted = false;
  FocusNode emailFocusNode;
  FocusNode passFocusNode;

  @override
  void initState() {
    // TODO: implement initState
    emailFocusNode = FocusNode();
    passFocusNode = FocusNode();

    //print("saved email ${prefs.getString('email')}");

    getData(_emailController, _passController, isAccepted);
    super.initState();

  }
  GoogleSignIn _googleSignIn = GoogleSignIn(
   scopes: [
     'email'
   ]
  );
  Future<void> _handleSignIn() async {
    try {
      await _googleSignIn.signIn().then((value) {
        print('displayName: '+ value.displayName);
        print('displayEmail: '+ value.email);
        print('id: '+ value.id);
        print('PhotoUrl: '+ value.photoUrl);

        value.authentication.then((value2) async{
          print('id token: '+ value2.idToken);
          var p = await NetworkServices()
              .userGoogleRegistration(
              context: context,
              displayName: value.displayName,
              email: value.email,
              id: value.id,
              photoUrl: value.photoUrl,
              idToken: value2.idToken,
          );
          Map<String, dynamic> js = p;
          if (p['success'] == false) {
            Navigator.of(context).pop();
            print(p['message']);
            Get.snackbar('Error', p['message']);
          } else {
            Navigator.of(context).pop();
            //success
            store(p, context);
            Get.snackbar('Success',
                'Successfully Login!');
          }
          print('ok');
        });
      });
    } catch (error) {
      print(error);
    }
  }

  @override
  void dispose() {
    _emailController.dispose();
    _passController.dispose();
    emailFocusNode.dispose();
    passFocusNode.dispose();
    // TODO: implement dispose
    super.dispose();
  }

  bool _obscureText = true;
  bool status = false;
  // Toggles the password show status
  void _toggle() {
    setState(() {
      _obscureText = !_obscureText;
    });
  }

  void isAccepted_toggle() {
    print('fff');
    //Get.snackbar('Aleart', 'Password ');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: kWhiteColor,
      appBar: AppBar(
        iconTheme: IconThemeData(color: kBlackColor),
        elevation: 0,
        backgroundColor: kWhiteColor,
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(
            horizontal: getProportionateScreenWidth(20),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: EdgeInsets.only(
                  top: getProportionateScreenHeight(10),
                ),
                child: Text(
                  'Masuk Akun',
                  style: kHeadLine2,
                ),
              ),
              Row(
                children: [
                  Text(
                    'Belum punya akun? ',
                    style: kRegularText.copyWith(
                        color: kOrdinaryColor, fontWeight: FontWeight.w600),
                  ),
                  GestureDetector(
                    onTap: () {
                      rgStatus = true;
                      Navigator.pushNamed(context, SignUp.routeName);
                    },
                    child: Container(
                      padding: EdgeInsets.only(
                        bottom: 1, // Space between underline and text
                      ),
                      decoration: BoxDecoration(
                          border: Border(
                              bottom: BorderSide(
                        color: kPrimaryColor,
                        width: 1.0, // Underline thickness
                      ))),
                      child: Text(
                        "Daftar",
                        style: kRegularText.copyWith(
                          color: kPrimaryColor,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),
                  )
                ],
              ),
              SizedBox(
                height: getProportionateScreenHeight(30),
              ),
              Form(
                key: _formKey,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      child: Text(
                        'Email',
                        style: kRegularText2.copyWith(
                          color: emailFocusNode.hasFocus
                              ? kPrimaryColor
                              : kBlackColor,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),
                    FocusScope(
                      child: Focus(
                        focusNode: emailFocusNode,
                        onFocusChange: (node) {
                          setState(() {});
                        },
                        child: InputFormWidget(
                          hintText: /*prefs.containsKey('checkAccepted') ? prefs.getString('email'):*/ 'Email',
                          fieldController: _emailController,
                          keyType: TextInputType.emailAddress,
                          validation: (value) {
                            if (value.isEmpty) {
                              return kEmailNullError;
                            }
                            return null;
                          },
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Container(
                      child: Text(
                        'Password',
                        style: kRegularText2.copyWith(
                          color: passFocusNode.hasFocus == true
                              ? kPrimaryColor
                              : kBlackColor,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),
                    FocusScope(
                      child: Focus(
                        focusNode: passFocusNode,
                        onFocusChange: (node) {
                          setState(() {});
                        },
                        child: InputPasswordFormWidget(
                          hintText: 'Password',
                          icon: _obscureText
                              ? Icons.visibility_off
                              : Icons.visibility,
                          onIconPress: prefs.containsKey('checkAccepted')
                              ? isAccepted_toggle
                              : _toggle,
                          fieldController: _passController,
                          keyType: TextInputType.text,
                          isProtected: _obscureText,
                          validation: (value) {
                            if (value.isEmpty) {
                              return kPassNullError;
                            } else if (value.length < 4) {
                              return kShortPassError;
                            }
                            return null;
                          },
                        ),
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        SizedBox(
                          width: 20,
                          height: 30,
                          child: Checkbox(
                            activeColor: kPrimaryColor,
                            value: isAccepted,
                            onChanged: (bool value) {
                              setState(() {
                                isAccepted = value;
                              });
                            },
                          ),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Text(
                          'Ingat Saya',
                          style: kRegularText2,
                        ),
                      ],
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(
                        vertical: getProportionateScreenHeight(30),
                      ),
                      child: DefaultBtn(
                        width: SizeConfig.screenWidth,
                        title: 'Masuk',
                        onPress: () async {
                          /*  Navigator.of(context).pushNamedAndRemoveUntil(
                              Home.routeName, (Route<dynamic> route) => false);*/
                          if (_formKey.currentState.validate()) {
                            //call method
                            var p = await NetworkServices().userLogin(
                              context: context,
                              password: _passController.text,
                              username: _emailController.text,
                            );
                            Map<String, dynamic> js = p;
                            if (p['success'] == false) {
                              Navigator.of(context).pop();
                              print(p['message']);
                              Get.snackbar('Error', p['message']);
                            } else {
                              //success
                              store(p, context);
                            }
                            print('ok');
                          }
                        },
                      ),
                    ),
                    Container(
                      alignment: Alignment.center,
                      padding: EdgeInsets.only(bottom: 30),
                      child: Text(
                        'Atau',
                        style: kRegularText.copyWith(
                          color: kOrdinaryColor,
                        ),
                      ),
                    ),
                    Container(
                      width: SizeConfig.screenWidth,
                      padding: EdgeInsets.symmetric(
                        vertical: 2,
                      ),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(25),
                        color: Color(0xFFFEF3F1),
                      ),
                      child: FlatButton(
                        onPressed: () {
                          _handleSignIn();
                        },
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Image.asset(kImageDir + 'g_image.png'),
                            // SvgPicture.asset(
                            //   kImageDir + 'g_image.svg',
                            //   height: getProportionateScreenWidth(18),
                            // ),
                            SizedBox(
                              width: 10,
                            ),
                            Text(
                              'Sign in with Google',
                              style: kRegularText2.copyWith(
                                fontWeight: FontWeight.w600,
                                color: Color(0xFFDC1E34),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void store(var mat, BuildContext context) async {
    prefs = await SharedPreferences.getInstance();
    prefs.setString('token', mat['token'].toString()); // token save
    prefs.setString('user_uid', mat['token'].toString()); // uid save
    prefs.setBool('status', mat['status']);
    if (isAccepted) {
      print(_emailController.text);
      print(_passController.text);

      prefs.setBool('checkAccepted', true);
      prefs.setString('email', _emailController.text);
      prefs.setString('password', _passController.text);
    }
    Navigator.of(context).pushNamedAndRemoveUntil(
        Home.routeName, (Route<dynamic> route) => false);
  }

  void getData(TextEditingController email, TextEditingController pass,
      bool isActive) async {
    prefs = await SharedPreferences.getInstance();

    setState(() {
      email.text = prefs.getString('email');
      pass.text = prefs.getString('password');
      isActive = prefs.getBool('checkAccepted');
      print("saved email ${prefs.getString('email')}");
      print("saved pass ${prefs.getString('password')}");
      print("saved bool ${prefs.getBool('checkAccepted')}");
    });

    // print("saved pass ${_passController.text}");
  }
}
