import 'package:flutter/material.dart';
import 'package:orbitrade/sheared/default_btn.dart';
import 'package:orbitrade/views/auth/login.dart';
import 'package:orbitrade/views/auth/signup.dart';

import '../../constants.dart';
import '../../main.dart';
import '../../size_config.dart';

class AuthHome extends StatelessWidget {
  static const routeName = 'auth_home';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kPrimaryColor,
      body: SafeArea(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          SizedBox(),
          Center(
            child: Container(
              width: SizeConfig.screenWidth,
              child: Hero(
                tag: 'logo',
                child: Image.asset(kImageDir + 'splash.png'),
              ),
            ),
          ),
          Column(
            children: [
              DefaultBtn(
                width: SizeConfig.screenWidth / 1.2,
                title: 'Mulai',
                onPress: () {
                  rgStatus = false;
                  Navigator.pushNamed(context, LogIn.routeName);
                },
              ),
              SizedBox(
                height: 15,
              ),
              DefaultBtn(
                width: SizeConfig.screenWidth / 1.2,
                btnColor: kPrimaryColor,
                textColor: kSecondaryColor,
                border: 2,
                title: 'Masuk',
                onPress: () {
                  rgStatus = false;
                  Navigator.pushNamed(context, SignUp.routeName);
                },
              ),
            ],
          ),
        ],
      )),
    );
  }
}
