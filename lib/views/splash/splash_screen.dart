import 'dart:async';

import 'package:flutter/material.dart';
import 'package:orbitrade/constants.dart';
import 'package:orbitrade/views/auth/auth_home.dart';
import 'package:orbitrade/views/home/Home.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../main.dart';
import '../../size_config.dart';

class SplashScreen extends StatefulWidget {
  static const routeName = 'splash_screen';

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen>
    with SingleTickerProviderStateMixin {
  String version = '1.0.1';
  var addresses;
  var first;
  var lanLong;
  AnimationController _animationController;
  Animation _animation;
  @override
  void initState() {
    // TODO: implement initState
    SharedPreferences.getInstance().then((pr) {
      prefs = pr;
    });
    _animationController = AnimationController(
      vsync: this,
      duration: Duration(seconds: 2),
    );
    _animationController.forward();
    _animation = CurvedAnimation(
      parent: _animationController,
      curve: Curves.fastOutSlowIn,
    );
    Timer(Duration(seconds: 3), () {
      print('success');
      navigation();
    });
    super.initState();
  }

  void navigation() {
    rgStatus = false;
    if (prefs.containsKey('token')) {
      Navigator.pushNamedAndRemoveUntil(
          context, Home.routeName, (route) => false);
    } else {
      Navigator.pushNamedAndRemoveUntil(
          context, AuthHome.routeName, (route) => false);

      //test
      /* Navigator.pushNamedAndRemoveUntil(
          context, Home.routeName, (route) => false);*/
    }
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      backgroundColor: kPrimaryColor,
      body: SafeArea(
        child: Center(
          child: Container(
            width: SizeConfig.screenWidth,
            child: FadeTransition(
              opacity: _animation,
              child: Hero(
                tag: 'logo',
                child: Image.asset(kImageDir + 'splash.png'),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
