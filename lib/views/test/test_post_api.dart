import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '../../constants.dart';
import '../../size_config.dart';

class TestPostApi extends StatelessWidget {
  static const routeName = 'test_stapst_widget';
  final _formKey = GlobalKey<FormState>();
  TextEditingController _userNameController = new TextEditingController();
  TextEditingController _emailController = new TextEditingController();
  TextEditingController _phoneController = new TextEditingController();
  TextEditingController _passwordController = new TextEditingController();
  TextEditingController _nameController = new TextEditingController();
  TextEditingController _sureNameController = new TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kWhiteColor,
      appBar: AppBar(
        backgroundColor: kWhiteColor,
        brightness: Brightness.light,
        elevation: 0.5,
        centerTitle: true,
        title: Text(
          'Post Data ',
          style: kBoldStyle.copyWith(
              color: kBlackColor, fontWeight: FontWeight.w600),
        ),
        iconTheme: IconThemeData(color: kBlackColor),
        actions: [
          Container(
            margin: EdgeInsets.only(right: 10.0),
            child: Center(
              child: GestureDetector(
                onTap: () {
                  if (_formKey.currentState.validate()) {}
                },
                child: Text(
                  'Submit  ',
                  style: kBoldStyle.copyWith(
                      color: kPrimaryColor, fontWeight: FontWeight.w600),
                ),
              ),
            ),
          ),
        ],
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.symmetric(
          horizontal: getProportionateScreenWidth(20),
        ),
        child: Form(
          key: _formKey,
          child: Column(
            children: [
              size15,
              TextFormField(
                controller: _userNameController,
                decoration: InputDecoration(
                  labelText: 'Username*',
                  labelStyle: TextStyle(color: Colors.grey[400]),
                ),
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Please enter your username';
                  }
                  return null;
                },
              ),
              size15,
              TextFormField(
                controller: _nameController,
                decoration: InputDecoration(
                  labelText: 'Name*',
                  labelStyle: TextStyle(color: Colors.grey[400]),
                ),
                validator: (value) {
                  if (value.isEmpty) {
                    return kNameNullError;
                  }
                  return null;
                },
              ),
              size15,
              TextFormField(
                controller: _emailController,
                decoration: InputDecoration(
                  labelText: 'Email*',
                  labelStyle: TextStyle(color: Colors.grey[400]),
                ),
                validator: (value) {
                  if (value.isEmpty) {
                    return kEmailNullError;
                  }
                  return null;
                },
              ),
              size15,
              TextFormField(
                controller: _phoneController,
                decoration: InputDecoration(
                  labelText: 'Phone number*',
                  labelStyle: TextStyle(color: Colors.grey[400]),
                ),
                validator: (value) {
                  if (value.isEmpty) {
                    return kPhoneNumberNullError;
                  }
                  return null;
                },
              ),
              size15,
              TextFormField(
                controller: _sureNameController,
                decoration: InputDecoration(
                  labelText: 'Address*',
                  labelStyle: TextStyle(color: Colors.grey[400]),
                ),
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Please enter your Address';
                  }
                  return null;
                },
              ),
              size15,
              Container(
                alignment: Alignment.centerLeft,
                child: Text(
                  'Upload File',
                  style: TextStyle(color: Colors.grey[400]),
                  textAlign: TextAlign.justify,
                ),
              ),
              size5,
              GestureDetector(
                onTap: () {},
                child: DottedBorder(
                  color: kOrdinaryColor2,
                  radius: Radius.circular(5),
                  borderType: BorderType.RRect,
                  child: Container(
                    alignment: Alignment.center,
                    child: Padding(
                      padding: const EdgeInsets.symmetric(vertical: 20),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          SvgPicture.asset(
                            kImageDir + 'upload.svg',
                            height: getProportionateScreenWidth(40),
                            width: getProportionateScreenWidth(40),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Text(
                            'Click here to upload',
                            style: kDescriptionText,
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
              size15,
            ],
          ),
        ),
      ),
    );
  }
}
